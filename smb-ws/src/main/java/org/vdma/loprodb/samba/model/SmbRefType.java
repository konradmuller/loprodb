package org.vdma.loprodb.samba.model;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum SmbRefType {
    CONTACT_AREA,
    CONTACT_POSITION,
    U_SALUTATION,

    @JsonEnumDefaultValue
    UNKNOWN
}
