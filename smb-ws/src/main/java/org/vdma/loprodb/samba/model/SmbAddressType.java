package org.vdma.loprodb.samba.model;

public enum SmbAddressType {
    COMPANY,
    SUBSIDIARY,
    BRANCH,
    CONTACT,
    PERSON_WITH_ADDRESS,
    PERSON_NO_ADDRESS,
    EVENT_PERSON,
    USER;

    public String getTranslateKey() {
        return "SmbAddressType." + name();
    }
}
