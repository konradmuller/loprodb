package org.vdma.loprodb.samba.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.vdma.loprodb.samba.model.SmbContactData;

@Service
@Slf4j
@RequiredArgsConstructor
public class SmbContactService {

    private final RestTemplate restTemplate;

    @Value("${rest.api.url.smb-contact-service.contact-info}")
    private String contactInfoUrl;

    @Value("${rest.api.url.smb-contact-service.reactivate}")
    private String reactivateUrl;

    public SmbContactData getContactById(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(contactInfoUrl)
                .queryParam("id", id);

        ResponseEntity<SmbContactData> response = restTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                null,
                SmbContactData.class);
        return response.getBody();
    }

    public byte[] getContactPhoto(String url) {
        try {
            BufferedImage bufferedImage = ImageIO.read(new URL(url));
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", stream);
            return stream.toByteArray();
        } catch (final IOException | IllegalArgumentException ignored) {
            return null;
        }
    }

    public void reactivate(Long id) {
        restTemplate.exchange(reactivateUrl, HttpMethod.PATCH, null, Void.class, id);
    }
}
