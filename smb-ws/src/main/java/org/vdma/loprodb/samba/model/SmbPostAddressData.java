package org.vdma.loprodb.samba.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmbPostAddressData {
    private String street;
    private String location;
    private String district;
    private String zip;
    private String city;
    private String country;
    private String countryCode;
}
