package org.vdma.loprodb.samba.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.util.UriComponentsBuilder;

@Getter
@Setter
public class SmbContactData {
    private Long id;

    private SmbAddressStatus status;

    private Long aimId;

    private String email;

    private SmbEmailType emailType;

    private String publicEmail;

    private SmbReference salutation;

    private String title;

    private String firstName;

    private String lastName;

    private String department;

    private SmbCompanyData company;

    private List<SmbAreaPosition> areaPositions = new ArrayList<>();

    private String phone;

    private String publicPhone;

    private String publicMobile;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm")
    private LocalDate birthday;

    private String function;

    private String picture100;

    private String picture500;

    private String pictureOriginal;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm")
    private Date photoChangedOn;

    private String photoCheckSum;

    private String url;

    private Long loprodbSyncVersion;

    private Long loprodbUserVersion;

    private boolean vdmaMember;

    private String vdmaMemberId;

    private boolean vdmaEmployee;

    private long documentsCount;

    @JsonProperty("title")
    private void unpackTitle(Map<String, Object> titleMap) {
        this.title = (String) titleMap.get("businessCard");
    }

    public String getPicture100() {
        if (picture100 == null) {
            return null;
        }
        return UriComponentsBuilder.fromUriString(picture100)
                .queryParam("ver", System.currentTimeMillis()).build().toString();
    }

    public String getPicture500() {
        if (picture500 == null) {
            return null;
        }
        return UriComponentsBuilder.fromUriString(picture500)
                .queryParam("ver", System.currentTimeMillis()).build().toString();
    }

    public String getPictureOriginal() {
        if (pictureOriginal == null) {
            return null;
        }
        return UriComponentsBuilder.fromUriString(pictureOriginal)
                .queryParam("ver", System.currentTimeMillis()).build().toString();
    }

    @JsonIgnore
    public boolean isNotActive() {
        return status != SmbAddressStatus.ACTIVE;
    }

    @JsonIgnore
    public boolean isEmailPersonal() {
        return emailType == SmbEmailType.PERSONAL_EMAIL;
    }

    @JsonIgnore
    public String getFullName() {
        return String.format("%s, %s", lastName, firstName);
    }
}
