package org.vdma.loprodb.samba.model;

public enum SmbEmailType {
    PERSONAL_EMAIL,
    PUBLIC_EMAIL,
    SECRETARIAT_EMAIL,
    COMPANY_EMAIL,
    PRIVATE_EMAIL
}
