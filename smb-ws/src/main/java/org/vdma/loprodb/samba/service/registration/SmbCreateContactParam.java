package org.vdma.loprodb.samba.service.registration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class SmbCreateContactParam {
    private SmbCreateContactRegData registrationData;
    private Long forCompanyId;
}
