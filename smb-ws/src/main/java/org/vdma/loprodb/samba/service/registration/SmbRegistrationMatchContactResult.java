package org.vdma.loprodb.samba.service.registration;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbContactData;

@Getter
@Setter
public class SmbRegistrationMatchContactResult {
    private List<SmbContactData> contacts = new ArrayList<>();
    private List<SmbCompanyData> companies = new ArrayList<>();
    private List<SmbScoredRelatedCompany> relatedCompanies = new ArrayList<>();

    @Getter
    @Setter
    public static final class SmbScoredRelatedCompany {
        private SmbCompanyData company;
        private int score;
    }
}
