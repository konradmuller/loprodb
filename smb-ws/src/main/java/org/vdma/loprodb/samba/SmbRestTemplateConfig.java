package org.vdma.loprodb.samba;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
@RequiredArgsConstructor
public class SmbRestTemplateConfig {

    private static final int TIMEOUT = 30_000;

    private final ObjectMapper objectMapper;

    @Value("${middleware.token}")
    private String middlewareToken;

    private static final String MIDDLEWARE_TOKEN_HEADER_NAME = "X-API-KEY";

    @Primary
    @Bean
    public RestTemplate smbRestTemplate() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(TIMEOUT);
        requestFactory.setReadTimeout(TIMEOUT);

        final RestTemplate restTemplate = new RestTemplateBuilder()
                .defaultHeader(MIDDLEWARE_TOKEN_HEADER_NAME, middlewareToken)
                .requestFactory(() -> requestFactory)
                .messageConverters(mappingJackson2HttpMessageConverter())
                .build();
        return restTemplate;
    }

    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);
        return converter;
    }
}
