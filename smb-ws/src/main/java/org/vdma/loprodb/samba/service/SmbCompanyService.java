package org.vdma.loprodb.samba.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vdma.loprodb.samba.model.SmbCompanyData;

@Service
@Slf4j
@RequiredArgsConstructor
public class SmbCompanyService {

    private final RestTemplate restTemplate;

    @Value("${rest.api.url.smb-company-service.by-id}")
    private String companyByIdUrl;

    public SmbCompanyData getCompanyById(Long id) {
        return restTemplate.getForObject(companyByIdUrl, SmbCompanyData.class, id);
    }
}
