package org.vdma.loprodb.samba.model;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum SmbAddressStatus {
    ACTIVE,
    BE_CHECKED,
    DELETED,
    DECEASED,
    PARENTAL_LEAVE,
    PRIVACY_DELETION,
    RETIRED,
    CHANGE_WITHIN_COMPANY,
    LEFT_COMPANY,
    BANKRUPTCY,
    IN_LIQUIDATION,
    DOES_NOT_EXIST,

    @JsonEnumDefaultValue
    UNKNOWN;
}
