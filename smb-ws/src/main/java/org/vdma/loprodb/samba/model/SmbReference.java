package org.vdma.loprodb.samba.model;

import java.util.Locale;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmbReference {
    private Long id;
    private String valueDe;
    private String valueEn;
    private SmbRefType refType;

    public String getNameByLocale(Locale locale) {
        if (locale.getLanguage().equalsIgnoreCase(Locale.GERMAN.getLanguage())) {
            return getValueDe();
        } else {
            return getValueEn();
        }
    }
}
