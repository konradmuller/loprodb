package org.vdma.loprodb.samba.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmbAreaPosition {
    private SmbReference area;
    private SmbReference position;
}
