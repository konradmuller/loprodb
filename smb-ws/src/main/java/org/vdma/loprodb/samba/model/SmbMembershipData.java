package org.vdma.loprodb.samba.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmbMembershipData {
    private String memberId;
    private String membershipColor;
    private SmbMembershipStatus membershipStatus;
    private String ouName;
    private String ouShortName;
}
