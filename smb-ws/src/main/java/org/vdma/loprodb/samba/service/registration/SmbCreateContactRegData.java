package org.vdma.loprodb.samba.service.registration;

import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.Gender;

@Getter
@Setter
class SmbCreateContactRegData {
    private Gender gender;
    private String title;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String mobile;
    private String function;
    private Long positionId;
    private Long businessAreaId;
}
