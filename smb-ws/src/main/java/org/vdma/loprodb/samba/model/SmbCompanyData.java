package org.vdma.loprodb.samba.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmbCompanyData {
    private Long id;
    private SmbAddressStatus status;
    private SmbAddressType type;
    private String name;
    private String name1;
    private String name2;
    private String name3;
    private SmbPostAddressData address;
    private String url;
    private boolean vdmaMember;
    private String vdmaMemberId;
    private List<SmbMembershipData> memberships;
    private String homepage;

    @JsonIgnore
    public boolean isActive() {
        return status == SmbAddressStatus.ACTIVE;
    }

    @JsonIgnore
    public boolean isActiveVdmaMember() {
        return isVdmaMember() && isActive();
    }
}
