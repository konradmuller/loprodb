package org.vdma.loprodb.samba.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vdma.loprodb.samba.model.SmbReference;

@Service
@Slf4j
@RequiredArgsConstructor
public class SmbReferenceService {

    private final RestTemplate restTemplate;

    @Value("${rest.api.url.smb-reference-service.list-by-type}")
    private String listByTypeUrl;

    @Value("${rest.api.url.smb-reference-service.by-id}")
    private String byIdUrl;

    public SmbReference getReferenceById(Long id) {
        return restTemplate.getForObject(byIdUrl, SmbReference.class, id);
    }

    public List<SmbReference> getReferencesByType(String type) {
        SmbReference[] refsArray = restTemplate.getForObject(listByTypeUrl, SmbReference[].class, type);
        if (refsArray == null || refsArray.length == 0) {
            return Collections.emptyList();
        }
        return Arrays.asList(refsArray);
    }

}
