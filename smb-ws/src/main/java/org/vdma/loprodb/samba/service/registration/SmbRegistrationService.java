package org.vdma.loprodb.samba.service.registration;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.samba.model.SmbContactData;

@Service
@RequiredArgsConstructor
public class SmbRegistrationService {

    private final RestTemplate restTemplate;

    @Value("${rest.api.url.smb-registration-service.similar-emails}")
    private String similarEmailsUrl;

    @Value("${rest.api.url.smb-registration-service.match-contact}")
    private String matchContactUrl;

    @Value("${rest.api.url.smb-registration-service.create-contact}")
    private String createContactUrl;

    @Value("${rest.api.url.smb-registration-service.company-contacts}")
    private String companyContactsUrl;

    public Set<String> getSimilarContactEmails(String email) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(similarEmailsUrl)
                .queryParam("email", email);

        String[] emails = restTemplate.getForObject(builder.build().toString(), String[].class);
        if (emails == null || emails.length == 0) {
            return Collections.emptySet();
        }
        return new HashSet<>(Arrays.asList(emails));
    }

    public SmbRegistrationMatchContactResult matchContact(RegistrationData reg) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(matchContactUrl)
                .queryParam("email", reg.getEmail())
                .queryParam("companyName", reg.getCompany().getName())
                .queryParam("companyStreet", reg.getCompany().getStreet())
                .queryParam("companyHouseNumber", reg.getCompany().getHouseNumber())
                .queryParam("companyCity", reg.getCompany().getCity());

        return restTemplate.getForObject(builder.build().toString(), SmbRegistrationMatchContactResult.class);
    }

    public SmbContactData createContact(RegistrationData registrationData, Long companyId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createContactUrl);

        SmbCreateContactParam param = new SmbCreateContactParam();
        param.setForCompanyId(companyId);

        SmbCreateContactRegData regData = new SmbCreateContactRegData();
        regData.setEmail(registrationData.getEmail());
        regData.setFirstName(registrationData.getFirstName());
        regData.setLastName(registrationData.getLastName());
        regData.setFunction(registrationData.getFunction());
        regData.setGender(registrationData.getGender());
        regData.setPhone(registrationData.getPhone());
        regData.setMobile(registrationData.getMobile());
        regData.setTitle(registrationData.getTitle());
        regData.setPositionId(registrationData.getPosition().getSambaId());
        regData.setBusinessAreaId(registrationData.getBusinessArea().getSambaId());

        param.setRegistrationData(regData);

        return restTemplate.postForObject(builder.build().toString(), param, SmbContactData.class);
    }

    public List<SmbContactData> getCompanyContacts(Long companyId, String firstName, String lastName) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(companyContactsUrl)
                .queryParam("companyId", companyId)
                .queryParam("firstName", firstName)
                .queryParam("lastName", lastName);

        SmbContactData[] result = restTemplate.getForObject(builder.build().toString(), SmbContactData[].class);
        if (result == null || result.length == 0) {
            return Collections.emptyList();
        }
        return Arrays.asList(result);
    }

}
