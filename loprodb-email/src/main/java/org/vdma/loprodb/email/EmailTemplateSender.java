package org.vdma.loprodb.email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.FileSystemUtils;
import org.vdma.loprodb.dto.EmailTemplateData;
import org.vdma.loprodb.entity.EmailTemplateType;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.service.IEmailTemplateService;

@Component
@RequiredArgsConstructor
@Slf4j
public class EmailTemplateSender {

    public static final String DEFAULT_FROM = "noreply@vdma.org";

    private final JavaMailSender emailSender;
    private final IEmailTemplateService emailTemplateService;
    private final Configuration freeMarkerConf;

    public void send(EmailTemplateType templateType, Language lang, String to, String from, Map<String, Object> params)
            throws Exception {
        EmailTemplateData emailTemplate = emailTemplateService.getEmailTemplate(templateType);

        String subj = emailTemplate.getSubjectByLang(lang);
        String body = emailTemplate.getBodyByLang(lang);

        if (StringUtils.isBlank(subj)) {
            log.warn("Failed to send {} email, subject is blank", templateType.name());
            return;
        }

        if (StringUtils.isBlank(body)) {
            log.warn("Failed to send {} email, body is blank", templateType.name());
            return;
        }

        Locale locale = lang == Language.DE ? Locale.GERMANY : Locale.ENGLISH;

        Template subjTemplate = new Template("subj", new StringReader(subj), freeMarkerConf);
        subjTemplate.setLocale(locale);

        Template bodyTemplate = new Template("body", new StringReader(body), freeMarkerConf);
        bodyTemplate.setLocale(locale);

        String processedSubj = FreeMarkerTemplateUtils.processTemplateIntoString(subjTemplate, params);
        String processedBody = FreeMarkerTemplateUtils.processTemplateIntoString(bodyTemplate, params);

        InlineBase64Result inlineBase64Result = InlineBase64.inline(processedBody);

        List<String> cidFiles = new ArrayList<>();

        try {
            emailSender.send(mimeMessage -> {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
                helper.setTo(to);
                helper.setSubject(processedSubj);
                helper.setText(inlineBase64Result.getOutput(), true);
                helper.setFrom(from);

                for (InlineBase64Result.CIDFile f : inlineBase64Result.getCidFiles()) {
                    helper.addInline(f.getCid(), new FileSystemResource(f.getFile()), f.getContentType());
                    cidFiles.add(f.getFile());
                }
            });
        } finally {
            cidFiles.forEach(f -> FileSystemUtils.deleteRecursively(new File(f)));
        }

    }
}
