package org.vdma.loprodb.email;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.util.concurrent.ListenableFuture;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserAdData;
import static org.vdma.loprodb.email.EmailTemplateSender.DEFAULT_FROM;
import org.vdma.loprodb.entity.EmailTemplateType;
import static org.vdma.loprodb.entity.EmailTemplateType.OPT_IN;
import static org.vdma.loprodb.entity.EmailTemplateType.REGISTRATION_CONFIRMED;
import static org.vdma.loprodb.entity.EmailTemplateType.REGISTRATION_MANUAL_CHECK;
import static org.vdma.loprodb.entity.EmailTemplateType.REGISTRATION_REJECTED;
import org.vdma.loprodb.event.registration.RegistrationCreatedEvent;
import org.vdma.loprodb.event.registration.RegistrationManualCheckEvent;
import org.vdma.loprodb.event.registration.RegistrationRejectedEvent;

@Component
@RequiredArgsConstructor
@Slf4j
public class RegistrationEmailSender {

    private final EmailTemplateSender emailSender;

    @Async
    @TransactionalEventListener
    public void created(RegistrationCreatedEvent event) {
        RegistrationData reg = event.getRegistration();
        log.info("Registration {} has been CREATED, sending email to {}", reg.getToken(), reg.getEmail());

        send(OPT_IN, reg, Collections.emptyMap());
    }

    @Async
    public ListenableFuture<Boolean> sendLoginEmail(RegistrationData reg, UserAdData userAd) {
        log.info("AD user for registration {} has been created, sending email to {}", reg.getToken(), reg.getEmail());

        Map<String, Object> params = new HashMap<>();
        params.put("userAd", userAd);

        return new AsyncResult<>(send(REGISTRATION_CONFIRMED, reg, params));
    }

    @Async
    @TransactionalEventListener
    public void rejected(RegistrationRejectedEvent event) {
        RegistrationData reg = event.getRegistration();
        log.info("Registration {} has been REJECTED, sending email to {}", reg.getToken(), reg.getEmail());

        send(REGISTRATION_REJECTED, reg, Collections.emptyMap());
    }
    
    @Async
    @TransactionalEventListener
    public void sentToManualCheck(RegistrationManualCheckEvent event) {
        RegistrationData reg = event.getRegistration();
        log.info("Registration {} has been set to MANUAL_REVIEW_PENDING, sending email to {}", 
                reg.getToken(), reg.getEmail());

        send(REGISTRATION_MANUAL_CHECK, reg, Collections.emptyMap());
    }

    private boolean send(EmailTemplateType type, RegistrationData reg, Map<String, Object> params) {
        Map<String, Object> templateModel = new HashMap<>(params);
        templateModel.put("registration", reg);

        try {
            emailSender.send(type, reg.getLanguage(), reg.getEmail(), DEFAULT_FROM, templateModel);
            log.info("{} email has been successfully sent to {}", type.name(), reg.getEmail());
        } catch (Exception e) {
            log.warn("Failed to send " + type.name() + " email to " + reg.getEmail(), e);
            return false;
        }
        return true;
    }
}
