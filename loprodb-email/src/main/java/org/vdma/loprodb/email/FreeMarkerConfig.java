package org.vdma.loprodb.email;

import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;
import java.nio.charset.StandardCharsets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@org.springframework.context.annotation.Configuration
public class FreeMarkerConfig {

    @Primary
    @Bean
    public Configuration freeMarkerConf() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);

        BeansWrapperBuilder builder = new BeansWrapperBuilder(Configuration.VERSION_2_3_30);
        builder.setSimpleMapWrapper(true);
        builder.setTreatDefaultMethodsAsBeanMembers(true);

        configuration.setObjectWrapper(builder.build());
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configuration.setURLEscapingCharset(StandardCharsets.UTF_8.name());

        return configuration;
    }

}
