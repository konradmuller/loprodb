package org.vdma.loprodb.email;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.util.Base64Utils;
import org.springframework.util.StreamUtils;

public final class InlineBase64 {

    private static final Pattern PATTERN = Pattern.compile("(data:([a-z+./]+);base64,([^\"]+))");
    private static final String CID = "cid:";
    private static final String INLINE = "inline-";

    private InlineBase64() {
    }

    public static InlineBase64Result inline(String input) throws Exception {
        InlineBase64Result result = new InlineBase64Result();
        Matcher matcher = PATTERN.matcher(input);
        StringBuilder finalOut = new StringBuilder();
        int lastGroupEnd = 0;
        while (matcher.find()) {
            String cid = UUID.randomUUID().toString();

            finalOut.append(input, lastGroupEnd, matcher.start(1));
            finalOut.append(CID).append(INLINE).append(cid);
            result.getCidFiles().add(processFragment(matcher.group(3), matcher.group(2), cid));
            lastGroupEnd = matcher.end(1);
        }
        if (lastGroupEnd < input.length()) {
            finalOut.append(input.substring(lastGroupEnd));
        }
        result.setOutput(finalOut.toString());
        return result;
    }

    private static InlineBase64Result.CIDFile processFragment(String base64, String contentType, String cid)
            throws Exception {
        byte[] bytes = Base64Utils.decodeFromString(base64);
        File tmp = File.createTempFile(INLINE, "-file");
        FileOutputStream outputStream = new FileOutputStream(tmp);
        StreamUtils.copy(new ByteArrayInputStream(bytes), outputStream);
        outputStream.close();

        InlineBase64Result.CIDFile cidFile = new InlineBase64Result.CIDFile();
        cidFile.setFile(tmp.getAbsolutePath());
        cidFile.setCid(INLINE + cid);
        cidFile.setContentType(contentType);
        return cidFile;
    }
}
