package org.vdma.loprodb.email;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InlineBase64Result {
    private String output;
    private List<CIDFile> cidFiles = new ArrayList<>();

    @Getter
    @Setter
    public static class CIDFile {
        private String file;
        private String cid;
        private String contentType;
    }

}
