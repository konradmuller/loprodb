package org.vdma.loprodb.security;

public interface CurrentUser {
    UiUserDetails getUser();
}
