package org.vdma.loprodb.security.saml;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.exception.SAMLException;
import io.swagger.models.HttpMethod;
import java.io.IOException;
import java.util.Collections;
import java.util.UUID;
import javax.security.auth.login.LoginException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.security.Role;
import org.vdma.loprodb.security.UiUserDetails;
import org.vdma.loprodb.service.IUserService;

@Slf4j
@Component
public class SamlAcsFilter extends OncePerRequestFilter {

    private static final String EMAIL_ATTR_NAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
    public static final String SAML_ERROR = "SAML_ERROR";

    private final RequestMatcher uriMatcher =
            new AntPathRequestMatcher("/saml-acs", HttpMethod.POST.name());

    @Value("${application.loprodb.code:loprodb}")
    private String loprodbAppCode;

    @Autowired
    private IUserService userService;

    @Autowired
    private SamlProperties samlProperties;

    @SneakyThrows
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        try {
            Auth auth = new Auth(LoprodbSamlSettings.getSamlSettings(), request, response);
            auth.processResponse();
            if (auth.isAuthenticated()) {
                String email = auth.getAttribute(EMAIL_ATTR_NAME).stream()
                        .map(String::toLowerCase)
                        .findFirst().orElse(null);
                if (isBlank(email)) {
                    throw new SAMLException("Attribute 'emailaddress' should not be empty!");
                }
                log.info("SAML login successful, email: " + email);

                UserData user = getUser(email);

                if (user != null) {
                    UiUserDetails userDetails = new UiUserDetails(user.getExternalId(), email,
                            UUID.randomUUID().toString(),
                            true,
                            true,
                            true,
                            true,
                            true,
                            Collections.singleton(new SimpleGrantedAuthority(Role.USER)));
                    SecurityContextHolder.getContext().setAuthentication(new SamlAuthentication(userDetails, Collections.singleton(new SimpleGrantedAuthority(Role.USER))));
                    String relayState = request.getParameter("RelayState");
                    String originalUri = request.getRequestURI();
                    String targetUri = isEmpty(relayState) ? originalUri.substring(0, originalUri.lastIndexOf('/')) : relayState;
                    String serverUrl = samlProperties.getSpAcsUrl().substring(0, samlProperties.getSpAcsUrl().lastIndexOf("/"));
                    response.sendRedirect(serverUrl + targetUri);
                } else {
                    setSamlloginError(request, response, "Login successful for email '" + email +
                            "', but no corresponding Loprodb user has been found!", null);
                }
            } else {
                setSamlloginError(request, response, auth.getLastErrorReason(), null);
            }
        } catch (Exception e) {
            setSamlloginError(request, response, e.getMessage(), e);
        }
        chain.doFilter(request, response);
    }

    private UserData getUser(final String email) {
        UserData user;
        try {
            user = userService.getUserByIdType(UserIdType.EMAIL, email);
        } catch (Exception e) {
            log.error("Failed to load user by email " + email, e);
            throw new UsernameNotFoundException("No user found with email: " + email);
        }
        log.debug("User with email {} found", email);
        if (user.getApps().stream()
                .noneMatch(app -> loprodbAppCode.equalsIgnoreCase(app.getAppCode()))) {
            log.error("User with email " + email +
                    " does not have access to loprodb application");
            return null;
        }
        return user;
    }

    private void setSamlloginError(HttpServletRequest request, HttpServletResponse response, String error, Exception e) throws IOException, LoginException {
        log.error(error, e);
        final HttpSession session = request.getSession();
        session.setAttribute(SAML_ERROR, error);
    }

    @Override
    public void destroy() {
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        RequestMatcher matcher = new NegatedRequestMatcher(uriMatcher);
        return matcher.matches(request);
    }
}
