package org.vdma.loprodb.security.cas;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "sso.cas")
public class CasConfiguration {
    private boolean enabled = false;
    private String url = "https://login.vdmaservices.com/cas";
    private String urlLogout = "https://login.vdmaservices.com/cas/logout";
    private String service = "http://lrp-db.vdma.local/login/cas";
}
