package org.vdma.loprodb.security;

import java.util.Collections;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Primary
@RequiredArgsConstructor
public class BackdoorUserDetailsService implements UserDetailsService {

    private final PasswordEncoder passwordEncoder;

    @Value("${login.admin-password:admin}")
    private String adminPassword;

    @Override
    // @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if (!email.equals("admin")) {
            log.warn("No user found with email: {}", email);
            throw new UsernameNotFoundException("No user found with email: " + email);
        }
        log.debug("user: {} found", email);
        return new UiUserDetails("uuid", email,
                passwordEncoder.encode(adminPassword),
                false,
                true,
                true,
                true,
                true,
                Collections.singleton(new SimpleGrantedAuthority(Role.ADMIN)));
    }
}
