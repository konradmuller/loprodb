package org.vdma.loprodb.security.saml;

import com.onelogin.saml2.model.Contact;
import com.onelogin.saml2.model.Organization;
import com.onelogin.saml2.settings.Saml2Settings;
import com.onelogin.saml2.util.Util;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import static java.util.Collections.singletonList;

public class LoprodbSamlSettings extends Saml2Settings {

    public static final String SAML_BINDING_HTTP_POST = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
    public static final String SAML_BINDING_HTTP_REDIRECT = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect";
    public static final String SAML_NAMEID_FORMAT_UNSPECIFIED = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
    public static final String ALGO_XMLDSIG_MORE_RSA_SHA_256 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
    public static final String ALGO_XMLENC_SHA_256 = "http://www.w3.org/2001/04/xmlenc#sha256";
    public static final String ALGO_SHA_256 = "sha256";
    public static final Organization SAML_VDMA_ORGANIZATION =
            new Organization("LoProDB SP", "LoProDB SAML Service Provider", "https://vdma.org", "DE");
    public static final Contact SAML_VDMA_CONTACT = new Contact(
            "technical", "VDMA", "ServiceTeam", "ServiceTeam",
            singletonList("serviceteam@vdma.org"),
            singletonList("(49 69) 6603 - 1157"));

    public static Saml2Settings getSamlSettings() throws MalformedURLException, GeneralSecurityException {

        org.vdma.loprodb.security.saml.LoprodbSamlSettings settings = new org.vdma.loprodb.security.saml.LoprodbSamlSettings();
        settings.setStrict(false);
        settings.setDebug(true);
        settings.setSpEntityId(SpringUtil.getProperty("onelogin.saml2.sp.entityid"));
        settings.setSpAssertionConsumerServiceUrl(new URL(SpringUtil.getProperty("onelogin.saml2.sp.assertion_consumer_service.url")));
        settings.setSpAssertionConsumerServiceBinding(SAML_BINDING_HTTP_POST);
        settings.setSpSingleLogoutServiceUrl(new URL(SpringUtil.getProperty("onelogin.saml2.sp.single_logout_service.url")));
        settings.setSpSingleLogoutServiceBinding(SAML_BINDING_HTTP_REDIRECT);
        settings.setSpNameIDFormat(SAML_NAMEID_FORMAT_UNSPECIFIED);
        settings.setSpX509cert(Util.loadCert(SpringUtil.getProperty("onelogin.saml2.sp.x509cert")));
        settings.setSpPrivateKey(Util.loadPrivateKey(SpringUtil.getProperty("onelogin.saml2.sp.privatekey")));
        settings.setOrganization(SAML_VDMA_ORGANIZATION);
        settings.setContacts(singletonList(SAML_VDMA_CONTACT));
        settings.setIdpEntityId(SpringUtil.getProperty("onelogin.saml2.idp.entityid"));
        settings.setIdpSingleSignOnServiceUrl(new URL(SpringUtil.getProperty("onelogin.saml2.idp.single_sign_on_service.url")));
        settings.setIdpSingleSignOnServiceBinding(SAML_BINDING_HTTP_REDIRECT);
        settings.setIdpSingleLogoutServiceUrl(new URL(SpringUtil.getProperty("onelogin.saml2.idp.single_logout_service.url")));
        settings.setIdpSingleLogoutServiceBinding(SAML_BINDING_HTTP_REDIRECT);
        settings.setIdpx509cert(Util.loadCert(SpringUtil.getProperty("onelogin.saml2.idp.x509cert")));
        settings.setIdpCertFingerprintAlgorithm(ALGO_SHA_256);
        settings.setNameIdEncrypted(false);
        settings.setAuthnRequestsSigned(true);
        settings.setLogoutRequestSigned(true);
        settings.setLogoutResponseSigned(true);
        settings.setWantMessagesSigned(true);
        settings.setWantAssertionsSigned(true);
        settings.setWantAssertionsEncrypted(false);
        settings.setWantNameIdEncrypted(false);
        settings.setAllowRepeatAttributeName(false);
        settings.setWantXMLValidation(true);
        settings.setSignatureAlgorithm(ALGO_XMLDSIG_MORE_RSA_SHA_256);
        settings.setDigestAlgorithm(ALGO_XMLENC_SHA_256);
        settings.setRejectDeprecatedAlg(true);
        settings.setTrimNameIds(false);
        settings.setTrimAttributeValues(false);
        return settings;
    }
}
