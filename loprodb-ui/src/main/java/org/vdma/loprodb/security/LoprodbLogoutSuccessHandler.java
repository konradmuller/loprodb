package org.vdma.loprodb.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.security.cas.CasConfiguration;

@Component
public class LoprodbLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private CasConfiguration casConfiguration;

    @Value("/${login.url:login}")
    private String logoutUrl;

    private SimpleUrlLogoutSuccessHandler urlLogoutSuccessHandler = new SimpleUrlLogoutSuccessHandler();

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        String targetUrl = logoutUrl;
        if (authentication != null) {
            UiUserDetails uiUserDetails = (UiUserDetails) authentication.getPrincipal();
            targetUrl = uiUserDetails.isFromCas() ? casConfiguration.getUrlLogout() : targetUrl;
        }
        urlLogoutSuccessHandler.setDefaultTargetUrl(targetUrl);
        urlLogoutSuccessHandler.onLogoutSuccess(request, response, authentication);
    }

}
