package org.vdma.loprodb.security;

import java.util.Collections;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jasig.cas.client.validation.Cas30ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import static org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.vdma.loprodb.security.cas.CasConfiguration;
import org.vdma.loprodb.security.cas.CasUserDetailsService;
import org.vdma.loprodb.security.saml.SamlAcsFilter;
import org.vdma.loprodb.security.saml.SamlAuthenticationEntryPoint;
import org.vdma.loprodb.security.saml.SamlLogoutHandler;
import org.vdma.loprodb.security.saml.SamlProperties;

@Slf4j
@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String CAS_PROVIDER_KEY = "CAS_PROVIDER_LOPRODB";

    private final BackdoorUserDetailsService backdoorUserDetailsService;
    private final CasUserDetailsService casUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final LoprodbLogoutSuccessHandler loprodbLogoutSuccessHandler;
    private final CasConfiguration casConfiguration;

    @Value("/${login.url:login}")
    private String loginUrl;

    @Value("/${login.url:login}")
    private String loginProcessingUrl;

    @Value("/${login.url:login}?error")
    private String loginFailureUrl;

    @Autowired
    private SamlProperties samlProperties;

    @Autowired
    private SamlAcsFilter samlAcsFilter;

    @Autowired
    private SamlAuthenticationEntryPoint samlAuthenticationEntryPoint;

    @Autowired
    private SamlLogoutHandler samlLogoutHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .requestCache().requestCache(new CustomRequestCache())
                .and()
                .authorizeRequests()
                .antMatchers(loginUrl).permitAll()
                .requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll()
                .anyRequest().authenticated();

        if (casConfiguration.isEnabled()) {
            http.exceptionHandling().authenticationEntryPoint(casAuthenticationEntryPoint());
        }

        http.formLogin().loginPage(loginUrl).permitAll()
                .loginProcessingUrl(loginProcessingUrl)
                .failureUrl(loginFailureUrl)
                // Configure logout
                .and()
                .logout()
                .logoutSuccessHandler(loprodbLogoutSuccessHandler)
                .deleteCookies(SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY);
        if (casConfiguration.isEnabled()) {
            http.addFilterAt(casFilter(), CasAuthenticationFilter.class);
            // .addFilterBefore(singleSignOutFilter(), CasAuthenticationFilter.class)
        }
        if (samlProperties.isSamlSsoEnabled()) {
            http.exceptionHandling().authenticationEntryPoint(samlAuthenticationEntryPoint);
            http.addFilterAfter(samlAcsFilter, BasicAuthenticationFilter.class);
            http.logout().addLogoutHandler(samlLogoutHandler);
        }
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(backdoorUserDetailsService).passwordEncoder(passwordEncoder);
        if (casConfiguration.isEnabled()) {
            auth.authenticationProvider(casAuthenticationProvider());
        }
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                // Vaadin Flow static resources
                "/VAADIN/**",

                // the standard favicon URI
                "/favicon.ico",

                // the robots exclusion standard
                "/robots.txt",

                // web application manifest
                "/manifest.webmanifest",
                "/sw.js",
                "/offline-page.html",

                // icons and images
                "/icons/**",
                "/images/**",

                // (development mode) static resources
                "/frontend/**",

                // (development mode) webjars
                "/webjars/**",

                // (development mode) H2 debugging console
                "/h2-console/**",

                // (production mode) static resources
                "/frontend-es5/**", "/frontend-es6/**",

                // Backdoor login page
                "/login-backdoor");
    }

    private AuthenticationEntryPoint casAuthenticationEntryPoint() {
        CasAuthenticationEntryPoint entryPoint = new CasAuthenticationEntryPoint();
        entryPoint.setLoginUrl(casConfiguration.getUrl());
        entryPoint.setServiceProperties(serviceProperties());
        return entryPoint;
    }

    // IMPORTANT!!! CasAuthenticationFilter processesUrl MUST be the same as end of service path (in service properties)
    // (/login/cas by default)
    private CasAuthenticationFilter casFilter() {
        CasAuthenticationFilter filter = new CasAuthenticationFilter();
        filter.setAuthenticationManager(new ProviderManager(Collections.singletonList(casAuthenticationProvider())));
        filter.setServiceProperties(serviceProperties());
        return filter;
    }

    private ServiceProperties serviceProperties() {
        ServiceProperties serviceProperties = new ServiceProperties();
        serviceProperties.setService(casConfiguration.getService());
        serviceProperties.setSendRenew(false);
        return serviceProperties;
    }

    private TicketValidator ticketValidator() {
        return new Cas30ServiceTicketValidator(casConfiguration.getUrl());
    }

    private CasAuthenticationProvider casAuthenticationProvider() {
        CasAuthenticationProvider provider = new CasAuthenticationProvider();
        provider.setServiceProperties(serviceProperties());
        provider.setTicketValidator(ticketValidator());
        provider.setUserDetailsService(casUserDetailsService);
        provider.setKey(CAS_PROVIDER_KEY);
        return provider;
    }

    // @Bean
    // public SingleSignOutFilter singleSignOutFilter() {
    // SingleSignOutFilter singleSignOutFilter = new SingleSignOutFilter();
    // // singleSignOutFilter.setCasServerUrlPrefix("https://localhost:8443");
    // singleSignOutFilter.setLogoutCallbackPath("/exit/cas");
    // singleSignOutFilter.setIgnoreInitConfiguration(true);
    // return singleSignOutFilter;
    // }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CurrentUser currentUser() {
        return SecurityUtils::getCurrentUser;
    }
}
