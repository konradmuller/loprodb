package org.vdma.loprodb.security;

import java.util.Collection;
import java.util.Objects;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@Getter
public class UiUserDetails extends User {

    private String externalId;
    private boolean fromCas;

    public UiUserDetails(String externalId, String username, String password,
            boolean fromCas,
            boolean enabled,
            boolean accountNonExpired,
            boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.externalId = externalId;
        this.fromCas = fromCas;
    }

    public boolean isAdmin() {
        return getAuthorities().stream().anyMatch(authority -> Objects.equals(authority.getAuthority(), Role.ADMIN));
    }
}
