package org.vdma.loprodb.security.saml;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.settings.Saml2Settings;
import io.swagger.models.HttpMethod;
import java.io.IOException;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@Component
public class SamlMetadataFilter extends OncePerRequestFilter {

    private final RequestMatcher uriMatcher =
            new AntPathRequestMatcher("/saml-metadata", HttpMethod.GET.name());

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        String metadata;
        try {
            Auth auth = new Auth(LoprodbSamlSettings.getSamlSettings(), null, null);
            Saml2Settings settings = auth.getSettings();
            settings.setSPValidationOnly(true);
            List<String> errors = settings.checkSettings();
            if (errors.isEmpty()) {
                metadata = settings.getSPMetadata();
            } else {
                metadata = String.join("\n", errors);
            }
        } catch (Exception e) {
            metadata = e.getMessage();
        }
        try {
            response.setContentType(String.valueOf(ContentType.APPLICATION_XML));
            response.setHeader("Content-Disposition", "inline;filename=metadata.xml");
            response.getWriter().println(metadata);
        } catch (IOException ignored) {
            // ignore
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        RequestMatcher matcher = new NegatedRequestMatcher(uriMatcher);
        return matcher.matches(request);
    }
}
