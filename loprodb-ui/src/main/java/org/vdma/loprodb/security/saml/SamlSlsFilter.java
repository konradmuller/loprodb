package org.vdma.loprodb.security.saml;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.logout.LogoutRequestParams;
import io.swagger.models.HttpMethod;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@Component
@WebFilter
public class SamlSlsFilter extends OncePerRequestFilter {

    private final RequestMatcher uriMatcher = 
                        new AntPathRequestMatcher("/saml-sls", HttpMethod.GET.name());
    
    @Autowired
    private SamlProperties samlProperties;
    
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        try {
            Auth auth = new Auth(LoprodbSamlSettings.getSamlSettings(), request, null);
            HttpSession session = request.getSession();

            String nameId = null;
            if (session.getAttribute("nameId") != null) {
                nameId = session.getAttribute("nameId").toString();
            }
            String nameIdFormat = null;
            if (session.getAttribute("nameIdFormat") != null) {
                nameIdFormat = session.getAttribute("nameIdFormat").toString();
            }
            String nameIdNameQualifier = null;
            if (session.getAttribute("nameidNameQualifier") != null) {
                nameIdNameQualifier = session.getAttribute("nameidNameQualifier").toString();
            }
            String nameIdSPNameQualifier = null;
            if (session.getAttribute("nameidSPNameQualifier") != null) {
                nameIdSPNameQualifier = session.getAttribute("nameidSPNameQualifier").toString();
            }
            String sessionIndex = null;
            if (session.getAttribute("sessionIndex") != null) {
                sessionIndex = session.getAttribute("sessionIndex").toString();
            }
            String originalUri = request.getRequestURI();
            String relayState = originalUri.substring(0, originalUri.lastIndexOf('/'));
            LogoutRequestParams logoutRequestParams = new LogoutRequestParams(
                    sessionIndex, nameId, nameIdFormat, nameIdNameQualifier, nameIdSPNameQualifier);
            String samlLogoutUrl = auth.logout(relayState, logoutRequestParams, true);
            SecurityContextHolder.getContext().setAuthentication(null);
            response.sendRedirect(samlLogoutUrl);
        } catch (Exception e) {
            log.error("Error in SAML SLS Filter", e);
        }
        chain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        RequestMatcher matcher = new NegatedRequestMatcher(uriMatcher);
        return matcher.matches(request);
    }
}
