package org.vdma.loprodb.security.saml;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SamlLogoutHandler implements LogoutHandler {
    
    @Autowired
    private SamlProperties samlProperties;
    
    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        try {
            httpServletResponse.sendRedirect(samlProperties.getSpSlsUrl());
        } catch (Exception ex) {
            log.error("Error while logging out!", ex);
        }
    }
}
