package org.vdma.loprodb.security.saml;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.authn.AuthnRequestParams;
import com.vaadin.flow.server.VaadinService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import static org.vdma.loprodb.security.saml.SamlAcsFilter.SAML_ERROR;

@Slf4j
@Component
public class SamlAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

        final HttpSession session = httpServletRequest.getSession();
        final String authError = (String) session.getAttribute(SAML_ERROR);
        if (isBlank(authError)) {
            try {
                log.error("Redirecting to SAML login...");
                Auth auth = new Auth(
                        LoprodbSamlSettings.getSamlSettings(),
                        (HttpServletRequest) VaadinService.getCurrentRequest(),
                        (HttpServletResponse) VaadinService.getCurrentResponse());
                String samlRedirectUrl = auth.login(
                        httpServletRequest.getRequestURI(),
                        new AuthnRequestParams(false, false, true),
                        true);
                httpServletResponse.sendRedirect(samlRedirectUrl);
            } catch (final Exception ex) {
                log.error("Error constructing SAML authentication entry point", ex);
            }
        } else {
            log.error("Redirecting to standard login...");
            httpServletResponse.sendRedirect("/login-backdoor?error");
        }
    }
}
