package org.vdma.loprodb.security.saml;

import java.util.Collection;
import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.vdma.loprodb.security.UiUserDetails;

public class SamlAuthentication extends AbstractAuthenticationToken {
    
    @Getter
    private final UiUserDetails userDetails;

    public SamlAuthentication(UiUserDetails userDetails, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.userDetails = userDetails;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return userDetails;
    }
}
