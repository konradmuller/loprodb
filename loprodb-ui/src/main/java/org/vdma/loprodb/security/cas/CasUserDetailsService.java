package org.vdma.loprodb.security.cas;

import java.util.Collections;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.security.Role;
import org.vdma.loprodb.security.UiUserDetails;
import org.vdma.loprodb.service.IUserService;

@Service
@Slf4j
@RequiredArgsConstructor
public class CasUserDetailsService implements UserDetailsService {

    private final IUserService userService;

    @Value("${application.loprodb.code:loprodb}")
    private String loprodbAppCode;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserData user;
        try {
            user = userService.getUserByIdType(UserIdType.EMAIL, email);
        } catch (Exception e) {
            log.error("Failed to load user by email " + email, e);
            throw new UsernameNotFoundException("No user found with email: " + email);
        }
        log.debug("User with email {} found", email);

        if (user.getApps().stream()
                .noneMatch(app -> loprodbAppCode.equalsIgnoreCase(app.getAppCode()))) {
            throw new UsernameNotFoundException("User with email " + email +
                    " does not have access to loprodb application");
        }

        return new UiUserDetails(user.getExternalId(), email,
                UUID.randomUUID().toString(),
                true,
                true,
                true,
                true,
                true,
                Collections.singleton(new SimpleGrantedAuthority(Role.USER)));
    }

}
