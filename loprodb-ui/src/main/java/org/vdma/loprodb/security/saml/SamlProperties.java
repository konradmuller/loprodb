package org.vdma.loprodb.security.saml;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class SamlProperties {
    
    @Value("${saml.sso.enabled}")
    private boolean samlSsoEnabled;

    @Value("${onelogin.saml2.sp.entityid}")
    private String spEntityId;

    @Value("${onelogin.saml2.sp.assertion_consumer_service.url}")
    private String spAcsUrl;

    @Value("${onelogin.saml2.sp.single_logout_service.url}")
    private String spSlsUrl;

    @Value("${onelogin.saml2.sp.x509cert}")
    private String spCert;

    @Value("${onelogin.saml2.sp.privatekey}")
    private String spPrivateKey;

    @Value("${onelogin.saml2.idp.entityid}")
    private String idpEntityId;

    @Value("${onelogin.saml2.idp.single_sign_on_service.url}")
    private String idpSsoUrl;

    @Value("${onelogin.saml2.idp.single_logout_service.url}")
    private String idkSlsUrl;

    @Value("${onelogin.saml2.idp.x509cert}")
    private String idpCert;

}
