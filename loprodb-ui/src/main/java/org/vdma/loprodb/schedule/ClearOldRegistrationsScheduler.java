package org.vdma.loprodb.schedule;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.service.IRegistrationService;

@Component
@Slf4j
@RequiredArgsConstructor
public class ClearOldRegistrationsScheduler {

    public final IRegistrationService registrationService;

    @Scheduled(cron = "${cron.clean-old-registrations:0 0 2 * * *}")
    public void cleanUp() {
        log.info("Cleaning up old registrations...");

        int oldCount = registrationService.deleteOldRegistrations();

        if (oldCount > 0) {
            log.info("Old registrations have been successfully deleted, count = {}", oldCount);
        } else {
            log.info("No old registrations found.");
        }
    }

}
