package org.vdma.loprodb.schedule;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.vdma.loprodb.dto.UserAdData;
import org.vdma.loprodb.email.RegistrationEmailSender;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserAdService;

@Slf4j
@Service
@RequiredArgsConstructor
public class ActiveDirectoryPollService {

    private static final long SLEEP_MINUTES = 5;
    private static final long DELAY_MILLIS = SLEEP_MINUTES * 60 * 1000L;

    private final IRegistrationService registrationService;
    private final IUserAdService userAdService;
    private final RegistrationEmailSender registrationEmailSender;

    @Scheduled(fixedDelay = DELAY_MILLIS)
    void poll() {
        log.info("Checking for new AD users with MS ids..");
        List<UserAdData> recentUsersWithUnsentLoginEmails = userAdService.findRecentUsersWithUnsentLoginEmails();
        log.info(recentUsersWithUnsentLoginEmails.size() + " users fetched");
        recentUsersWithUnsentLoginEmails
                .forEach(userAdData ->
                        registrationService.getConfirmedRegistrationByUser(userAdData.getUserId())
                                .ifPresent(registrationData -> registrationEmailSender.sendLoginEmail(
                                        registrationData, userAdData)
                                        .addCallback(
                                                emailSent -> userAdService.setLoginEmailSent(userAdData, emailSent),
                                                error -> log.warn("Sending login email for user #" +
                                                        userAdData.getUserId() + " failed", error))
                                ));

        log.info("Processed " + recentUsersWithUnsentLoginEmails.size() + " users, going to sleep for " +
                SLEEP_MINUTES + " minutes");
    }
}

