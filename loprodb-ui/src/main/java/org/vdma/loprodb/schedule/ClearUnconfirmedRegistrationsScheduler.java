package org.vdma.loprodb.schedule;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.service.IRegistrationService;

@Component
@Slf4j
@RequiredArgsConstructor
public class ClearUnconfirmedRegistrationsScheduler {

    public final IRegistrationService registrationService;

    @Scheduled(cron = "0 0/5 * * * *")
    public void cleanUp() {
        log.info("Cleaning up unconfirmed registrations older than 48 hours...");

        int oldCount = registrationService.deleteUnconfirmedRegistrations();

        if (oldCount > 0) {
            log.info("Unconfirmed registrations have been successfully deleted, count = {}", oldCount);
        } else {
            log.info("No unconfirmed registrations older than 48 hours found.");
        }
    }

}
