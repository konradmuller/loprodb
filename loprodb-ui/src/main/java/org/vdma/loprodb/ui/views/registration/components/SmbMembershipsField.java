package org.vdma.loprodb.ui.views.registration.components;

import com.vaadin.flow.component.customfield.CustomField;
import java.util.List;
import org.vdma.loprodb.samba.model.SmbMembershipData;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Right;

public class SmbMembershipsField extends CustomField<List<SmbMembershipData>> {

    @Override
    protected List<SmbMembershipData> generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(List<SmbMembershipData> memberships) {
        getChildren().forEach(this::remove);
        if (memberships != null && !memberships.isEmpty()) {
            FlexBoxLayout flbMembership = new FlexBoxLayout();
            flbMembership.setSpacing(Right.S);
            memberships.forEach(membership -> flbMembership.add(new SmbMembershipBadge(membership)));
            add(flbMembership);
        }
    }
}
