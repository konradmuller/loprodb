package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PageConfigurator;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.springframework.beans.factory.annotation.Value;
import static org.vdma.loprodb.security.saml.SamlAcsFilter.SAML_ERROR;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.UIUtils;

// Route is set in ConfigureUIServiceInitListener
@PageTitle("Login")
@CssImport(value = "./styles/login-form.css", themeFor = "vaadin-login-form-wrapper")
@CssImport(value = "./styles/login.css")
public class LoginView extends FlexBoxLayout implements AfterNavigationObserver, PublicView, PageConfigurator {

    public static final String CLASS_NAME = "login";
    private final LoginForm loginForm;

    @Value("/${login.url:login}")
    private String loginUrl;

    public LoginView() {
        LoginI18n i18n = LoginI18n.createDefault();
        i18n.setAdditionalInformation(null);
        i18n.setForm(new LoginI18n.Form());
        i18n.getForm().setSubmit("Log in");
        i18n.getForm().setUsername("Email");
        i18n.getForm().setPassword("Password");
        i18n.getForm().setTitle("Login");
        i18n.getForm().setForgotPassword("Forgot password?");

        String samlErrorMessage = (String) UI.getCurrent().getSession().getSession().getAttribute(SAML_ERROR);
        if (isNotBlank(samlErrorMessage)) {
            i18n.getErrorMessage().setTitle("SAML login error");
            i18n.getErrorMessage().setMessage(samlErrorMessage);
            UI.getCurrent().getSession().getSession().removeAttribute(SAML_ERROR);
        }

        loginForm = new LoginForm();
        loginForm.setI18n(i18n);
        loginForm.setForgotPasswordButtonVisible(false);
        UIUtils.setTheme("login-form", loginForm);

        Image logo = new Image();
        logo.setSrc(UIUtils.IMG_PATH + "logo-with-title.svg");

        FlexBoxLayout title = new FlexBoxLayout(logo);
        title.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        title.setMargin(Bottom.XL);

        FlexBoxLayout content = new FlexBoxLayout(loginForm);
        content.setFlexDirection(FlexDirection.COLUMN);
        content.setPadding(Vertical.XL);
        content.addClassName(CLASS_NAME);

        add(title, content);
        setFlexDirection(FlexDirection.COLUMN);
        setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        setAlignItems(FlexComponent.Alignment.CENTER);
        setSizeFull();
        setBackgroundColor("var(--lumo-primary-color)");
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        loginForm.setAction(loginUrl);
        loginForm.setError(event.getLocation().getQueryParameters().getParameters().containsKey("error"));
    }

    @Override
    public void configurePage(InitialPageSettings settings) {
        UIUtils.setFavIcon(settings);
    }
}
