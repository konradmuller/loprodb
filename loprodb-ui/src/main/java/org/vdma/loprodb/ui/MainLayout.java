package org.vdma.loprodb.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PageConfigurator;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.security.CurrentUser;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.navigation.bar.AppBar;
import org.vdma.loprodb.ui.components.navigation.bar.TabBar;
import org.vdma.loprodb.ui.components.navigation.drawer.NaviDrawer;
import org.vdma.loprodb.ui.components.navigation.drawer.NaviItem;
import org.vdma.loprodb.ui.components.navigation.drawer.NaviMenu;
import org.vdma.loprodb.ui.components.navigation.tab.BaseNaviTab;
import org.vdma.loprodb.ui.components.navigation.tab.HasChanges;
import org.vdma.loprodb.ui.components.navigation.tab.NaviTab;
import org.vdma.loprodb.ui.components.navigation.tab.ParamNaviTab;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.Overflow;
import org.vdma.loprodb.ui.views.EmailBlackList;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.Home;
import org.vdma.loprodb.ui.views.ParamRoute;
import org.vdma.loprodb.ui.views.References;
import org.vdma.loprodb.ui.views.application.Applications;
import org.vdma.loprodb.ui.views.emailtemplate.EmailTemplates;
import org.vdma.loprodb.ui.views.registration.Registrations;
import org.vdma.loprodb.ui.views.user.Users;

@CssImport(value = "./styles/components/charts.css", themeFor = "vaadin-chart", include = "vaadin-chart-default-theme")
@CssImport(value = "./styles/components/button.css", themeFor = "vaadin-button")
@CssImport(value = "./styles/components/grid.css", themeFor = "vaadin-grid")
@CssImport(value = "./styles/components/form-item.css", themeFor = "vaadin-form-item")
@CssImport(value = "./styles/no-padding-dialog-overlay.css", themeFor = "vaadin-dialog-overlay")
@CssImport("./styles/lumo/border-radius.css")
@CssImport("./styles/lumo/icon-size.css")
@CssImport("./styles/lumo/margin.css")
@CssImport("./styles/lumo/padding.css")
@CssImport("./styles/lumo/shadow.css")
@CssImport("./styles/lumo/spacing.css")
@CssImport("./styles/lumo/typography.css")
@CssImport("./styles/misc/box-shadow-borders.css")
@CssImport(value = "./styles/styles.css", include = "lumo-badge")
@CssImport(value = "./styles/text-field.css", themeFor = "vaadin-text-field")
@JsModule("@vaadin/vaadin-lumo-styles/badge")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@Push
public class MainLayout extends FlexBoxLayout implements RouterLayout, PageConfigurator,
        AfterNavigationObserver, HasChanges {

    private static final Logger log = LoggerFactory.getLogger(MainLayout.class);
    private static final String CLASS_NAME = "root";

    private Div appHeaderOuter;

    private FlexBoxLayout row;
    private NaviDrawer naviDrawer;
    private FlexBoxLayout column;

    private Div appHeaderInner;
    private FlexBoxLayout viewContainer;
    private Div appFooterInner;

    private Div appFooterOuter;

    private TabBar tabBar;
    private boolean navigationTabs = true;
    private AppBar appBar;
    private HasElement layoutContent;

    private Set<EqualCmp> loadedTabs = new HashSet<>();
    private Map<BaseNaviTab, EqualCmp> tabToContentMap = new HashMap<>();

    @Autowired
    public MainLayout(CurrentUser currentUser, IUserService userService) {
        VaadinSession.getCurrent()
                .setErrorHandler(errorEvent -> {
                    log.error("Uncaught UI exception", errorEvent.getThrowable());
                    Notification.show("We are sorry, but an internal error occurred");
                });
        // locale from browser
        VaadinSession.getCurrent().setLocale(VaadinService.getCurrentRequest().getLocale());

        addClassName(CLASS_NAME);
        setFlexDirection(FlexDirection.ROW_REVERSE);
        setSizeFull();

        // Initialise the UI building blocks
        initStructure();

        // Populate the navigation drawer
        initNaviItems();

        // Configure the headers and footers (optional)
        initHeadersAndFooters(currentUser, userService);
    }

    /**
     * Initialise the required components and containers.
     */
    private void initStructure() {
        naviDrawer = new NaviDrawer();

        viewContainer = new FlexBoxLayout();
        viewContainer.addClassName(CLASS_NAME + "__view-container");
        viewContainer.setOverflow(Overflow.HIDDEN);

        column = new FlexBoxLayout(viewContainer);
        column.addClassName(CLASS_NAME + "__column");
        column.setFlexDirection(FlexDirection.COLUMN);
        column.setFlexGrow(1, viewContainer);
        column.setOverflow(Overflow.HIDDEN);

        row = new FlexBoxLayout(naviDrawer, column);
        row.addClassName(CLASS_NAME + "__row");
        row.setFlexGrow(1, column);
        row.setOverflow(Overflow.HIDDEN);
        add(row);
        setFlexGrow(1, row);
    }

    /**
     * Initialise the navigation items.
     */
    private void initNaviItems() {
        NaviMenu menu = naviDrawer.getMenu();
        menu.addNaviItem(VaadinIcon.HOME, getTranslation("menu.home"), Home.class);
        menu.addNaviItem(VaadinIcon.USERS, getTranslation("menu.users"), Users.class);
        menu.addNaviItem(VaadinIcon.CLIPBOARD_USER, getTranslation("menu.registrations"), Registrations.class);
        menu.addNaviItem(VaadinIcon.SHIELD, getTranslation("menu.applications"), Applications.class);
        menu.addNaviItem(VaadinIcon.EXTERNAL_LINK, getTranslation("menu.references"), References.class);
        menu.addNaviItem(VaadinIcon.BAN, getTranslation("menu.email-black-list"), EmailBlackList.class);
        menu.addNaviItem(VaadinIcon.AT, getTranslation("menu.email-templates"), EmailTemplates.class);
        // if (SecurityUtils.isAccessGranted(Customers.class)) {
        // menu.addNaviItem(VaadinIcon.USERS, "Users", Users.class);
        // }
    }

    /**
     * Configure the app's inner and outer headers and footers.
     */
    private void initHeadersAndFooters(CurrentUser currentUser, IUserService userService) {
        // setAppHeaderOuter(new Label("app header outer"));
        // setAppFooterInner();
        // setAppFooterOuter();

        // Default inner header setup:
        // - When using tabbed navigation the view title, user avatar and main menu button will appear in the TabBar.
        // - When tabbed navigation is turned off they appear in the AppBar.
        // todo: replace with a real user
        UserData user = new UserData();
        user.setFirstName("Thomas");
        user.setLastName("Müller");
        // userService.getUser(currentUser.getUser().getExternalId());

        appBar = new AppBar("", user, () -> false);

        // Tabbed navigation
        if (navigationTabs) {
            tabBar = new TabBar(user, this);
            tabBar.setAddTabVisible(false);

            // Shift-click to add a new tab
            for (NaviItem item : naviDrawer.getMenu().getNaviItems()) {
                item.addClickListener(e -> {
                    if (e.getButton() == 0) {
                        navigate(item.getText(), item.getNavigationTarget());
                    }
                });
            }
            appBar.getAvatar().setVisible(false);
            setAppHeaderInner(tabBar, appBar);

            // Default navigation
        } else {
            setAppHeaderInner(appBar);
        }
    }

    private void setAppHeaderOuter(Component... components) {
        if (appHeaderOuter == null) {
            appHeaderOuter = new Div();
            appHeaderOuter.addClassName("app-header-outer");
            getElement().insertChild(0, appHeaderOuter.getElement());
        }
        appHeaderOuter.removeAll();
        appHeaderOuter.add(components);
    }

    private void setAppHeaderInner(Component... components) {
        if (appHeaderInner == null) {
            appHeaderInner = new Div();
            appHeaderInner.addClassName("app-header-inner");
            column.getElement().insertChild(0, appHeaderInner.getElement());
        }
        appHeaderInner.removeAll();
        appHeaderInner.add(components);
    }

    private void setAppFooterInner(Component... components) {
        if (appFooterInner == null) {
            appFooterInner = new Div();
            appFooterInner.addClassName("app-footer-inner");
            column.getElement().insertChild(column.getElement().getChildCount(),
                    appFooterInner.getElement());
        }
        appFooterInner.removeAll();
        appFooterInner.add(components);
    }

    private void setAppFooterOuter(Component... components) {
        if (appFooterOuter == null) {
            appFooterOuter = new Div();
            appFooterOuter.addClassName("app-footer-outer");
            getElement().insertChild(getElement().getChildCount(),
                    appFooterOuter.getElement());
        }
        appFooterOuter.removeAll();
        appFooterOuter.add(components);
    }

    @Override
    public void configurePage(InitialPageSettings settings) {
        settings.addMetaTag("apple-mobile-web-app-capable", "yes");
        settings.addMetaTag("apple-mobile-web-app-status-bar-style", "black");

        settings.addFavIcon("icon", "frontend/images/favicons/favicon.ico",
                "256x256");
        UIUtils.setFavIcon(settings);
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        if (content instanceof EqualCmp) {
            Optional<EqualCmp> cmp = loadedTabs.stream().filter(c -> c.eq(content)).findFirst();
            if (cmp.isPresent()) {
                this.layoutContent = cmp.get();
            } else {
                this.loadedTabs.add((EqualCmp) content);
                this.layoutContent = content;
            }
        } else {
            // default
            this.layoutContent = content;
        }
        this.viewContainer.removeAll();
        this.viewContainer.getElement().appendChild(layoutContent.getElement());
    }

    public HasElement getLayoutContent() {
        return layoutContent;
    }

    public NaviDrawer getNaviDrawer() {
        return naviDrawer;
    }

    public static MainLayout get() {
        return (MainLayout) UI.getCurrent().getChildren()
                .filter(component -> component.getClass() == MainLayout.class)
                .findFirst().orElse(null);
    }

    public AppBar getAppBar() {
        return appBar;
    }

    public TabBar getTabBar() {
        return tabBar;
    }

    public <T, C extends Component & HasUrlParameter<T>> void navigate(String text, Class<? extends C> navigationTarget,
            T parameter) {
        tabBar.setSelectedTab(
                tabBar.getTabs().filter(tab -> {
                    if (tab instanceof ParamNaviTab) {
                        ParamNaviTab paramNaviTab = (ParamNaviTab) tab;
                        return paramNaviTab.getNavigationTarget() == navigationTarget &&
                                Objects.equals(paramNaviTab.getParam(), parameter);
                    } else {
                        return false;
                    }
                }).findFirst().orElseGet(() -> tabBar.addTab(text, navigationTarget, parameter, true)));
    }

    public void navigate(String text, Class<? extends Component> navigationTarget) {
        tabBar.setSelectedTab(
                tabBar.getTabs().filter(tab -> {
                    if (tab instanceof NaviTab) {
                        NaviTab naviTab = (NaviTab) tab;
                        return naviTab.getNavigationTarget() == navigationTarget;
                    } else {
                        return false;
                    }
                }).findFirst().orElseGet(() -> tabBar.addTab(text, navigationTarget, true)));
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        if (navigationTabs) {
            afterNavigationWithTabs(event);
        } else {
            afterNavigationWithoutTabs(event);
        }
    }

    public void onCloseTab(BaseNaviTab tab) {
        if (tabToContentMap.containsKey(tab)) {
            loadedTabs.remove(tabToContentMap.get(tab));
            tabToContentMap.remove(tab);
        }
    }

    private void afterNavigationWithTabs(AfterNavigationEvent event) {
        // if it's direct navigation by url let's try to add first tab
        Tab currentTab = null;
        if (tabBar.getTabCount() == 0) {
            NaviItem active = getActiveItem(event);
            if (active != null) {
                currentTab = tabBar.addTab(active.getText(), active.getNavigationTarget(), true);
            } else if (layoutContent instanceof ParamRoute) {
                ParamRoute paramRoute = (ParamRoute) layoutContent;
                String url = RouteConfiguration.forApplicationScope().getUrl(paramRoute.getRouteType(),
                        paramRoute.getRouteParam());
                if (event.getLocation().getPath().equals(url)) {
                    currentTab = MainLayout.get().getTabBar().addTab(paramRoute.getTabLabel(),
                            paramRoute.getRouteType(), paramRoute.getRouteParam(), true);
                }
            }
        } else {
            currentTab = tabBar.getSelectedTab();
        }
        if (currentTab instanceof BaseNaviTab && layoutContent instanceof EqualCmp) {
            tabToContentMap.put((BaseNaviTab) currentTab, (EqualCmp) layoutContent);
        }
        appBar.getMenuIcon().setVisible(false);
    }

    private NaviItem getActiveItem(AfterNavigationEvent e) {
        for (NaviItem item : naviDrawer.getMenu().getNaviItems()) {
            if (item.isSameLocation(e)) {
                return item;
            }
        }
        return null;
    }

    private void afterNavigationWithoutTabs(AfterNavigationEvent e) {
        NaviItem active = getActiveItem(e);
        if (active != null) {
            getAppBar().setTitle(active.getText());
        }
    }

    @Override
    public boolean hasChanges() {
        return (layoutContent instanceof HasChanges) ? ((HasChanges) layoutContent).hasChanges() : false;
    }
}
