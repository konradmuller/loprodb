package org.vdma.loprodb.ui.components.detailsdrawer;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

@Getter
public class DetailsDrawerFooter extends FlexBoxLayout {

    private Button save;
    private Button cancel;
    private Button remove;

    public DetailsDrawerFooter() {

        setBackgroundColor(LumoStyles.Color.Contrast._5);
        setPadding(Horizontal.RESPONSIVE_L, Vertical.S);
        setSpacing(Right.S);
        setWidthFull();
        setBoxSizing(BoxSizing.BORDER_BOX);

        save = UIUtils.createPrimaryButton("Save");
        cancel = UIUtils.createTertiaryButton("Cancel");

        Div space = new Div();
        space.getStyle().set("flex", "auto");

        add(save, cancel, space, addMoreActions());

    }

    public Registration addSaveListener(ComponentEventListener<ClickEvent<Button>> listener) {
        return save.addClickListener(listener);

    }

    public Registration addCancelListener(ComponentEventListener<ClickEvent<Button>> listener) {
        return cancel.addClickListener(listener);
    }

    public Registration addRemoveListener(Runnable listener) {
        return remove.addClickListener(event -> listener.run());
    }

    public void setRemoveVisible(boolean visible) {
        remove.setVisible(visible);
    }

    protected Component addMoreActions() {
        remove = UIUtils.createErrorTertiaryButton("Delete...");
        setRemoveVisible(false);
        return remove;
    }

    public void setSaveVisible(boolean visible) {
        save.setVisible(visible);
    }

    public void setCancelVisible(boolean visible) {
        cancel.setVisible(visible);
    }
}
