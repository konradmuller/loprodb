package org.vdma.loprodb.ui.components.navigation.tab;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import java.util.Objects;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.vdma.loprodb.ui.MainLayout;

@Getter
@EqualsAndHashCode(of = "navigationTarget", callSuper = false)
public class NaviTab extends BaseNaviTab {

    private Class<? extends Component> navigationTarget;

    public NaviTab(String label, Class<? extends Component> navigationTarget) {
        super(label);
        setNavigationTarget(navigationTarget);
    }

    public void setNavigationTarget(Class<? extends Component> navigationTarget) {
        this.navigationTarget = Objects.requireNonNull(navigationTarget);
    }

    @Override
    public void navigate() {
        UI.getCurrent().navigate(navigationTarget);
    }

    public static void close(Class navigationTarget) {
        MainLayout.get().getTabBar().closeTab(tab -> {
            if (tab instanceof NaviTab) {
                NaviTab naviTab = (NaviTab) tab;
                return naviTab.getNavigationTarget() == navigationTarget;
            } else {
                return false;
            }
        });
    }
}
