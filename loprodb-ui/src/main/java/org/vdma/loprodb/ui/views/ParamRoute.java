package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.HasUrlParameter;

public interface ParamRoute<T, C extends Component & HasUrlParameter<T>> {

    Class<C> getRouteType();

    T getRouteParam();

    String getTabLabel();
}
