package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.HasElement;

public interface EqualCmp extends HasElement {
    default boolean eq(HasElement other) {
        return getClass().equals(other.getClass());
    }
}
