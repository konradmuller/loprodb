package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyPressEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.UserStatus;
import static org.vdma.loprodb.event.user.UpdateSource.LOPRODB;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.param.UserListParam;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.ListItem;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawer;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerFooter;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerHeader;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.ReferenceComboBox;
import org.vdma.loprodb.ui.views.SplitViewFrame;
import org.vdma.loprodb.ui.views.user.components.UserEmailField;
import org.vdma.loprodb.ui.views.user.components.UserStatusBadge;

@PageTitle("Users")
@Route(value = "users", layout = MainLayout.class)
public class Users extends SplitViewFrame implements EqualCmp {

    private Grid<UserData> grid;
    private ConfigurableFilterDataProvider<UserData, Void, UserListParam> dataProvider;
    private UserListParam filter = new UserListParam();

    private DetailsDrawer detailsDrawer;
    private DetailsDrawerHeader detailsDrawerHeader;
    private DetailsDrawerFooter detailsDrawerFooter;

    private Binder<UserData> binder;
    private UserData selectedUser;

    private final IUserService userService;
    private final IReferenceService referenceService;

    @Autowired
    public Users(IUserService userService, IReferenceService referenceService) {
        this.userService = userService;
        this.referenceService = referenceService;
        buildContent();
    }

    private void buildContent() {
        Button btnNewUser = UIUtils.createNewObjButton(getTranslation("user.list.new"), this::onNewUser);

        FlexBoxLayout content = new FlexBoxLayout(buildFilter(), btnNewUser, buildGrid());
        content.setAlignItems(FlexComponent.Alignment.START);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setHeightFull();
        content.setPadding(Horizontal.RESPONSIVE_X, Vertical.RESPONSIVE_M);

        setViewContent(content);
        setViewDetails(createDetailsDrawer());
    }

    private void onNewUser() {
        showDetails(new UserData());
        grid.deselectAll();
    }

    private Component buildFilter() {
        Button btnSearch = UIUtils.createPrimaryButton(getTranslation("user.list.filter.search"));
        ComponentEventListener<KeyPressEvent> enterListener = e -> btnSearch.click();

        TextField tfEmail = new TextField();
        tfEmail.addKeyPressListener(Key.ENTER, enterListener);
        tfEmail.setWidthFull();

        IntegerField intfSambaId = new IntegerField();
        intfSambaId.addKeyPressListener(Key.ENTER, enterListener);
        intfSambaId.setWidthFull();

        TextField tfFirstName = new TextField();
        tfFirstName.addKeyPressListener(Key.ENTER, enterListener);
        tfFirstName.setWidthFull();

        TextField tfLastName = new TextField();
        tfLastName.addKeyPressListener(Key.ENTER, enterListener);
        tfLastName.setWidthFull();

        TextField tfCompany = new TextField();
        tfCompany.addKeyPressListener(Key.ENTER, enterListener);
        tfCompany.setWidthFull();

        TextField tfCity = new TextField();
        tfCity.addKeyPressListener(Key.ENTER, enterListener);
        tfCity.setWidthFull();

        FlexBoxLayout fblStatus = new FlexBoxLayout();
        fblStatus.setBoxSizing(BoxSizing.BORDER_BOX);
        fblStatus.setSpacing(Right.M);

        for (UserStatus s : UserStatus.values()) {
            Checkbox chbStatus = new Checkbox(getTranslation(UserStatus.class.getSimpleName() + "." + s.name()));
            chbStatus.addValueChangeListener(event -> {
                if (event.getValue()) {
                    filter.getStatuses().add(s);
                } else {
                    filter.getStatuses().remove(s);
                }
            });
            fblStatus.add(chbStatus);
        }

        btnSearch.addClickListener(event -> {
            filter.setEmail(tfEmail.getValue());
            filter.setSambaId(intfSambaId.getValue() != null ? intfSambaId.getValue().longValue() : null);
            filter.setFirstName(tfFirstName.getValue());
            filter.setLastName(tfLastName.getValue());
            filter.setCompanyName(tfCompany.getValue());
            filter.setCompanyCity(tfCity.getValue());
            dataProvider.setFilter(filter);
        });
        FlexBoxLayout fblSearchBtnWrapper = new FlexBoxLayout(btnSearch);
        fblSearchBtnWrapper.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        fblSearchBtnWrapper.setAlignItems(FlexComponent.Alignment.END);

        FormLayout form = new FormLayout();
        form.setWidthFull();
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("1024px", 3, FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addFormItem(tfEmail, getTranslation("user.list.filter.email"));
        form.addFormItem(intfSambaId, getTranslation("user.list.filter.samba-id"));
        form.addFormItem(fblStatus, getTranslation("user.list.filter.status"));
        form.addFormItem(tfFirstName, getTranslation("user.list.filter.first-name"));
        form.addFormItem(tfLastName, getTranslation("user.list.filter.last-name"));
        form.addFormItem(new Div(), "");
        form.addFormItem(tfCompany, getTranslation("user.list.filter.company"));
        form.addFormItem(tfCity, getTranslation("user.list.filter.city"));
        form.addFormItem(fblSearchBtnWrapper, "");
        form.getChildren().forEach(c -> UIUtils.setMargin(c, Vertical.ZERO));

        FlexBoxLayout result = new FlexBoxLayout(form);
        result.setBoxSizing(BoxSizing.BORDER_BOX);
        result.setPadding(Horizontal.M, Vertical.S);
        result.setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        result.setMargin(Bottom.RESPONSIVE_M);
        UIUtils.setBorder(result);

        return result;
    }

    private Grid buildGrid() {
        dataProvider = SpringDataProviderBuilder.users(userService).build().withConfigurableFilter();
        dataProvider.setFilter(filter);

        grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addItemDoubleClickListener(event -> openUserDetailsTab(event.getItem()));
        grid.addSelectionListener(event -> event.getFirstSelectedItem().ifPresent(this::showDetails));
        UIUtils.setMargin(grid, Top.RESPONSIVE_M);
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();
        grid.addColumn(new ComponentRenderer<>(this::createItemStatus))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader(getTranslation("user.list.status"));
        grid.addColumn(new ComponentRenderer<>(this::createItemName))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setSortProperty("lastName", "firstName")
                .setHeader(getTranslation("user.list.name"));
        grid.addColumn(new ComponentRenderer<>(this::createItemCompany))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setSortProperty("company.name")
                .setHeader(getTranslation("user.list.company"));
        grid.addColumn(new LocalDateTimeRenderer<>(UserData::getLastSeenOn,
                DateTimeFormatter.ofPattern(UIUtils.DATE_TIME_FORMAT_PATTERN)))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setSortProperty("lastSeenOn")
                .setHeader(getTranslation("user.list.last-seen"));
        grid.addColumn(new ComponentRenderer<>(this::createItemEdit))
                .setAutoWidth(true)
                .setFlexGrow(0);
        return grid;
    }

    private Component createItemEdit(UserData userData) {
        Button edit = UIUtils.createOutlinedButton(getTranslation("commons.edit"));
        edit.addClickListener(event -> openUserDetailsTab(userData));
        return edit;
    }

    private void openUserDetailsTab(UserData userData) {
        if (selectedUser != null && Objects.equals(userData.getExternalId(), selectedUser.getExternalId())) {
            detailsDrawer.hide();
            grid.getSelectionModel().deselectFromClient(userData);
        }
        MainLayout.get().navigate(UserDetails.getTabLabel(userData), UserDetails.class,
                UserDetails.getNaviParam(userData));
    }

    private Component createItemCompany(UserData userData) {
        ListItem item = new ListItem(userData.getCompany().getName(), userData.getCompany().getCity());
        item.setPadding(Vertical.XS);
        return item;
    }

    private Component createItemName(UserData userData) {
        ListItem item = new ListItem(userData.getFullName(), userData.getEmail());
        item.setPadding(Vertical.XS);
        return item;
    }

    private Component createItemStatus(UserData userData) {
        Badge badge = UserStatusBadge.createForStatus(userData.getStatus());
        return badge != null ? badge : new Span();
    }

    private DetailsDrawer createDetailsDrawer() {
        detailsDrawer = new DetailsDrawer(DetailsDrawer.Position.RIGHT);

        // Header
        detailsDrawerHeader = new DetailsDrawerHeader("");
        detailsDrawerHeader.addCloseListener(buttonClickEvent -> {
            detailsDrawer.hide();
            grid.deselectAll();
        });
        detailsDrawer.setHeader(detailsDrawerHeader);

        // Footer
        detailsDrawerFooter = new DetailsDrawerFooter();
        detailsDrawerFooter.addSaveListener(e -> {
            if (binder.writeBeanIfValid(selectedUser)) {
                if (selectedUser.getId() == null) {
                    selectedUser.setStatus(UserStatus.ACTIVE);
                    selectedUser = userService.create(selectedUser);
                    dataProvider.refreshAll();
                } else {
                    selectedUser = userService.update(selectedUser.getExternalId(), selectedUser, LOPRODB);
                    dataProvider.refreshItem(selectedUser);
                    grid.deselect(selectedUser);
                }
                grid.select(selectedUser);
                UIUtils.showNotification(getTranslation("commons.changes-saved-msg"));
            } else {
                UIUtils.showNotification(getTranslation("commons.validation.error-msg"));
            }
        });
        detailsDrawerFooter.addRemoveListener(() -> ConfirmDialog
                .createQuestion()
                .withCaption(getTranslation("commons.confirm-deletion"))
                .withMessage(getTranslation("user.confirm-deletion-msg"))
                .withOkButton(() -> {
                    userService.delete(selectedUser.getExternalId());
                    UIUtils.showNotification(getTranslation("user.deletion-successful-msg"));
                    dataProvider.refreshAll();
                    detailsDrawer.hide();
                    grid.deselectAll();
                    UserDetails.close(selectedUser);
                    selectedUser = null;
                }, ButtonOption.focus(), ButtonOption.caption(getTranslation("commons.yes")))
                .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                .open());
        detailsDrawerFooter.addCancelListener(e -> showDetails(selectedUser));
        detailsDrawer.setFooter(detailsDrawerFooter);

        return detailsDrawer;
    }

    private void showDetails(UserData user) {
        selectedUser = user;
        if (user.getId() == null) {
            detailsDrawerHeader.setTitle(getTranslation("user.list.new"));
        } else {
            detailsDrawerHeader.setTitle(user.getFullName());
        }
        detailsDrawerFooter.setRemoveVisible(user.getId() != null);
        FormLayout details = createDetails(user);
        detailsDrawer.setContent(details);
        detailsDrawer.show();
    }

    private FormLayout createDetails(UserData user) {
        binder = new Binder<>();

        ComboBox<Gender> cbSalutation = new ComboBox<>(null, Gender.values());
        cbSalutation.setWidthFull();
        cbSalutation.setItemLabelGenerator(
                gender -> getTranslation(Gender.class.getSimpleName() + "." + gender.name()));
        binder.forField(cbSalutation).asRequired().bind(UserData::getGender, UserData::setGender);

        TextField tfFirstName = new TextField();
        tfFirstName.setWidthFull();
        binder.forField(tfFirstName).asRequired().bind(UserData::getFirstName, UserData::setFirstName);

        TextField tfLastName = new TextField();
        tfLastName.setWidthFull();
        binder.forField(tfLastName).asRequired().bind(UserData::getLastName, UserData::setLastName);

        UserEmailField efEmail = new UserEmailField(val -> !userService.emailExists(val, user.getExternalId()),
                selectedUser.getExternalId() != null);
        efEmail.setWidthFull();
        efEmail.bind(binder);

        TextField tfFunction = new TextField();
        tfFunction.setWidthFull();
        binder.forField(tfFunction).asRequired().bind(UserData::getFunction, UserData::setFunction);

        ComboBox<ReferenceData> cbPosition = new ReferenceComboBox(referenceService, RefType.USER_POSITION);
        cbPosition.setWidthFull();
        binder.forField(cbPosition).asRequired().bind(UserData::getPosition, UserData::setPosition);

        ComboBox<ReferenceData> cbBusinessArea = new ReferenceComboBox(referenceService, RefType.USER_BUSINESS_AREA);
        cbBusinessArea.setWidthFull();
        binder.forField(cbBusinessArea).asRequired().bind(UserData::getBusinessArea, UserData::setBusinessArea);

        ComboBox<Language> cbLang = new ComboBox<>(null, Language.values());
        cbLang.setWidthFull();
        cbLang.setItemLabelGenerator(lang -> getTranslation(Language.class.getSimpleName() + "." + lang.name()));
        binder.forField(cbLang).asRequired().bind(UserData::getLanguage, UserData::setLanguage);

        FormLayout form = new FormLayout();
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addClassNames(LumoStyles.Padding.Bottom.L, LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        UIUtils.asRequired(form.addFormItem(cbSalutation, getTranslation("user.details.basic.salutation")));
        UIUtils.asRequired(form.addFormItem(tfFirstName, getTranslation("user.details.basic.first-name")));
        UIUtils.asRequired(form.addFormItem(tfLastName, getTranslation("user.details.basic.last-name")));
        UIUtils.asRequired(form.addFormItem(efEmail, getTranslation("user.details.basic.email")));
        UIUtils.asRequired(form.addFormItem(tfFunction, getTranslation("user.details.basic.function")));
        UIUtils.asRequired(form.addFormItem(cbPosition, getTranslation("user.details.basic.position")));
        UIUtils.asRequired(form.addFormItem(cbBusinessArea, getTranslation("user.details.basic.business-area")));
        UIUtils.asRequired(form.addFormItem(cbLang, getTranslation("commons.lang")));

        binder.readBean(user);

        return form;
    }
}
