package org.vdma.loprodb.ui.views.emailtemplate;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.function.ValueProvider;
import lombok.Getter;
import org.vdma.loprodb.dto.EmailTemplateData;
import org.vdma.loprodb.service.IEmailTemplateService;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.layout.size.Uniform;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

class EmailTemplateDetails extends FlexBoxLayout {

    @Getter
    private EmailTemplateData emailTemplate;

    private final Binder<EmailTemplateData> binder = new Binder<>();
    private final IEmailTemplateService emailTemplateService;

    EmailTemplateDetails(EmailTemplateData emailTemplate, IEmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X, Vertical.S);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);

        buildContent();

        setEmailTemplate(emailTemplate);
    }

    private void setEmailTemplate(EmailTemplateData emailTemplate) {
        this.emailTemplate = emailTemplate;
        binder.readBean(emailTemplate);
    }

    private void buildContent() {
        FlexBoxLayout headerDe = new FlexBoxLayout(UIUtils.createH5Label(getTranslation("commons.lang.german")));
        headerDe.setBoxSizing(BoxSizing.BORDER_BOX);
        headerDe.setAlignItems(FlexComponent.Alignment.CENTER);
        headerDe.setJustifyContentMode(JustifyContentMode.CENTER);

        FlexBoxLayout headerEn = new FlexBoxLayout(UIUtils.createH5Label(getTranslation("commons.lang.english")));
        headerEn.setBoxSizing(BoxSizing.BORDER_BOX);
        headerEn.setAlignItems(FlexComponent.Alignment.CENTER);
        headerEn.setJustifyContentMode(JustifyContentMode.CENTER);

        FlexBoxLayout header = new FlexBoxLayout(headerDe, headerEn);
        header.setBoxSizing(BoxSizing.BORDER_BOX);
        header.setFlexBasis("0", headerDe, headerEn);
        header.setFlexGrow(1, headerDe, headerEn);
        header.setPadding(Uniform.S);
        header.getElement().getStyle().set("color", LumoStyles.Color.BASE_COLOR);
        header.setBackgroundColor(LumoStyles.Color.Primary._100);

        Component contentDe = buildContentForLang(EmailTemplateData::getSubjectDe, EmailTemplateData::setSubjectDe,
                EmailTemplateData::getBodyDe, EmailTemplateData::setBodyDe);
        Component contentEn = buildContentForLang(EmailTemplateData::getSubjectEn, EmailTemplateData::setSubjectEn,
                EmailTemplateData::getBodyEn, EmailTemplateData::setBodyEn);

        FlexBoxLayout content = new FlexBoxLayout(contentDe, contentEn);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setSpacing(Right.M);
        content.setFlexBasis("0", contentDe, contentEn);
        content.setFlexGrow(1, contentDe, contentEn);

        add(header, content, buildActions());
    }

    private Component buildContentForLang(ValueProvider<EmailTemplateData, String> subjGetter,
            Setter<EmailTemplateData, String> subjSetter,
            ValueProvider<EmailTemplateData, String> bodyGetter,
            Setter<EmailTemplateData, String> bodySetter) {

        TextArea taSubject = new TextArea(getTranslation("email-template.subject"));
        taSubject.setWidthFull();
        taSubject.setHeight("100px");
        binder.forField(taSubject).asRequired().bind(subjGetter, subjSetter);

        TextArea taBody = new TextArea(getTranslation("email-template.body"));
        taBody.setWidthFull();
        taBody.setHeight("550px");
        binder.forField(taBody).asRequired().bind(bodyGetter, bodySetter);

        FormLayout fl = new FormLayout();
        fl.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP));
        fl.add(taSubject);
        fl.add(taBody);

        return fl;
    }

    private FlexBoxLayout buildActions() {
        Button btnSave = UIUtils.createPrimaryButton(getTranslation("commons.save"));
        UIUtils.setMargin(btnSave, Left.S);
        btnSave.addClickListener(event -> {
            if (binder.writeBeanIfValid(emailTemplate)) {
                setEmailTemplate(emailTemplateService.update(emailTemplate.getId(), emailTemplate));
                UIUtils.showNotification(getTranslation("commons.changes-saved-msg"));
            } else {
                UIUtils.showNotification(getTranslation("commons.validation.error-msg"));
            }
        });

        Button btnCancel = UIUtils.createOutlinedButton(getTranslation("commons.cancel"));
        btnCancel.addClickListener(event -> binder.readBean(emailTemplate));

        FlexBoxLayout actions = new FlexBoxLayout(btnCancel, btnSave);
        actions.setAlignItems(FlexComponent.Alignment.CENTER);
        actions.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        actions.setBoxSizing(BoxSizing.BORDER_BOX);
        return actions;
    }

}
