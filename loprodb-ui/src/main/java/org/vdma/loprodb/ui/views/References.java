package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.samba.messaging.reference.SmbReferenceSynchronizer;
import org.vdma.loprodb.samba.model.SmbRefType;
import org.vdma.loprodb.samba.service.SmbReferenceService;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

@PageTitle("Reference management")
@Route(value = "references", layout = MainLayout.class)
@Slf4j
public class References extends SplitViewFrame {

    private final SmbReferenceService smbReferenceService;
    private final SmbReferenceSynchronizer smbReferenceSynchronizer;

    private UI ui;
    private Button btnImportPositionsAndAreas;

    @Autowired
    public References(SmbReferenceService smbReferenceService, SmbReferenceSynchronizer smbReferenceSynchronizer) {
        setId("references");
        setViewContent(buildContent());
        this.smbReferenceService = smbReferenceService;
        this.smbReferenceSynchronizer = smbReferenceSynchronizer;
    }

    private Component buildContent() {
        btnImportPositionsAndAreas = UIUtils.createPrimaryButton("Import positions & areas from Samba");
        btnImportPositionsAndAreas.addClickListener(e -> {
            btnImportPositionsAndAreas.setEnabled(false);
            new Thread(new ReferenceImporter()).start();
        });

        FlexBoxLayout content = new FlexBoxLayout(btnImportPositionsAndAreas);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setHeightFull();
        content.setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        content.setAlignSelf(FlexComponent.Alignment.START, btnImportPositionsAndAreas);
        return content;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        ui = attachEvent.getUI();
    }

    private class ReferenceImporter implements Runnable {

        @Override
        public void run() {
            try {
                smbReferenceService.getReferencesByType(SmbRefType.CONTACT_AREA.name())
                        .forEach(smbReferenceSynchronizer::synchronize);
                smbReferenceService.getReferencesByType(SmbRefType.CONTACT_POSITION.name())
                        .forEach(smbReferenceSynchronizer::synchronize);
                ui.access(() -> {
                    UIUtils.showNotification("Positions & areas have been successfully imported");
                    btnImportPositionsAndAreas.setEnabled(true);
                });
            } catch (Exception e) {
                ui.access(() -> {
                    UIUtils.showNotification("Failed to import positions & areas: " + e.getMessage());
                    btnImportPositionsAndAreas.setEnabled(true);
                });
                log.error("Failed to import positions & areas", e);
            }
        }
    }
}
