package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.timepicker.TimePicker;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import org.vdma.loprodb.ui.layout.size.Right;

public class OffsetDateTimePicker extends CustomField<OffsetDateTime> {

    private final DatePicker date = new DatePicker();
    private final TimePicker time = new TimePicker();

    public OffsetDateTimePicker(String label) {
        this();
        setLabel(label);
    }
    public OffsetDateTimePicker() {
        date.setClearButtonVisible(true);
        date.setSizeUndefined();

        time.setClearButtonVisible(true);
        time.setStep(Duration.ofSeconds(1));

        FlexBoxLayout content = new FlexBoxLayout(date, time);
        content.setSpacing(Right.XS);
        content.setFlexGrow(1, date, time);
        add(content);
    }

    @Override
    protected OffsetDateTime generateModelValue() {
        if (date.getValue() == null || time.getValue() == null) {
            return null;
        }
        final ZoneId zone = ZoneId.of("Europe/Berlin");
        LocalDateTime localDateTime = LocalDateTime.now();
        ZoneOffset offset = zone.getRules().getOffset(localDateTime);
        return localDateTime.atOffset(offset);
    }

    @Override
    protected void setPresentationValue(OffsetDateTime newPresentationValue) {
        if (newPresentationValue == null) {
            date.setValue(null);
            time.setValue(null);
        } else {
            date.setValue(newPresentationValue.toLocalDate());
            time.setValue(newPresentationValue.toLocalTime());
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        date.setReadOnly(readOnly);
        time.setReadOnly(readOnly);
    }

    @Override
    public boolean isReadOnly() {
        return date.isReadOnly();
    }
}
