package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.data.binder.Binder;
import java.util.Collections;
import java.util.Comparator;
import static java.util.Comparator.reverseOrder;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ButtonType;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.web.client.HttpStatusCodeException;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbCompanyService;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationMatchContactResult;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationService;
import org.vdma.loprodb.security.SecurityUtils;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.registration.components.BindSmbCompanyDlg;
import org.vdma.loprodb.ui.views.user.SmbContactUiFetcher;
import org.vdma.loprodb.ui.views.user.UserDetails;
import org.vdma.loprodb.ui.views.user.components.BindSmbContactDlg;

@Slf4j
class RegistrationBasicInfo extends FlexBoxLayout {

    private final Binder<RegistrationData> binder = new Binder<>();
    private final Binder<UserData> userBinder = new Binder<>();
    private final Binder<SmbContactData> sambaBinder = new Binder<>();
    private final Binder<SmbCompanyData> sambaCompanyBinder = new Binder<>();

    private final SmbContactUiFetcher contactUiFetcher;
    private final SmbCompanyUiFetcher companyUiFetcher;

    private final SmbRegistrationService smbRegistrationService;
    private final IRegistrationService registrationService;

    private RegistrationData registration;
    private UI ui;

    private RegistrationMainInfoForm mainInfoForm;
    private RegistrationCompanyForm companyForm;
    private FlexBoxLayout companyContactsComponent;

    RegistrationBasicInfo(SmbContactService smbContactService, SmbCompanyService smbCompanyService,
            SmbRegistrationService smbRegistrationService, IRegistrationService registrationService, 
                          RegistrationData registration, UI ui) {
        this.contactUiFetcher = new SmbContactUiFetcher(ui, smbContactService);
        this.companyUiFetcher = new SmbCompanyUiFetcher(ui, smbCompanyService);
        this.smbRegistrationService = smbRegistrationService;
        this.registrationService = registrationService;
        this.ui = ui;
        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);

        buildMainInfoForm();
        buildCompanyForm();

        setRegistration(registration);
    }

    private void matchSmbContact(Runnable onResult) {
        new Thread(() -> {
            try {
                SmbRegistrationMatchContactResult result = smbRegistrationService.matchContact(registration);
                ui.access(() -> {
                    onMatchSmbContactResult(result);
                    if (onResult != null) {
                        onResult.run();
                    }
                });
            } catch (HttpStatusCodeException ex) {
                log.error("failed to match samba contact for registration " + registration.getEmail(), ex);
                ui.access(() -> {
                    UIUtils.showNotification(
                            getTranslation("registration.details.match-contact.failed-msg", ex.getMessage()));
                    if (onResult != null) {
                        onResult.run();
                    }
                });
            }
        }).start();
    }

    private void onMatchSmbContactResult(SmbRegistrationMatchContactResult result) {
        removeAll();

        mainInfoForm.setCreateContactBtnVisibility(result.getContacts().isEmpty());
        companyForm.setLinkCompanyBtnVisibility(result.getContacts().isEmpty());

        if (result.getContacts().isEmpty() && result.getCompanies().isEmpty() &&
                result.getRelatedCompanies().isEmpty()) {
            add(mainInfoForm, companyForm);
            return;
        }

        Button btnRefresh = UIUtils.createOutlinedButton(
                getTranslation("registration.details.match-contact.refresh-list"));
        btnRefresh.addClickListener(event -> {
            btnRefresh.setEnabled(false);
            matchSmbContact(() -> btnRefresh.setEnabled(true));
        });
        btnRefresh.addClassName(LumoStyles.Margin.Left.AUTO);

        FlexBoxLayout header = new FlexBoxLayout();
        header.setAlignItems(Alignment.CENTER);
        if (!result.getContacts().isEmpty()) {
            header.add(UIUtils.createH5Label(getTranslation("registration.details.match-contact.contacts-found")));
        }
        header.add(btnRefresh);

        FlexBoxLayout matchSmbContactComponent = new FlexBoxLayout(header);
        matchSmbContactComponent.setPadding(Top.S);
        matchSmbContactComponent.setFlexDirection(FlexDirection.COLUMN);
        if (!result.getContacts().isEmpty()) {
            RegistrationSmbContactsGrid contactsGrid =
                    new RegistrationSmbContactsGrid(result.getContacts(), this::setContactBean);
            matchSmbContactComponent.add(contactsGrid);
            add(mainInfoForm, companyForm);
            companyContactsComponent = null;
            // if company is selected from the table or manually AND contact is NOT selected we should clear
            // company fields
            if (sambaBinder.getBean() == null) {
                sambaCompanyBinder.setBean(null);
            }
        } else {
            
            List<SmbCompanyData> companies;
            if (result.getCompanies().stream().anyMatch(SmbCompanyData::isActiveVdmaMember)) {
                companies = result.getCompanies();
            } else if (result.getRelatedCompanies().stream().anyMatch(r -> r.getCompany().isActiveVdmaMember())) {
                companies = getSortedRelatedCompanies(result);
            } else if (!result.getCompanies().isEmpty()) {
                companies = result.getCompanies();
            } else {
                companies = getSortedRelatedCompanies(result);
            }
            
            RegistrationSmbCompaniesGrid companiesGrid =
                    new RegistrationSmbCompaniesGrid(companies, this::setCompanyBean);
            companiesGrid.setHeightByRows(false);
            companiesGrid.setMaxHeight("300 px");
            matchSmbContactComponent.add(companiesGrid);
            add(companyForm);
            if (companyContactsComponent != null) {
                add(companyContactsComponent);
            }
            add(mainInfoForm);
        }

        addComponentAsFirst(matchSmbContactComponent);
    }
    
    private List<SmbCompanyData> getSortedRelatedCompanies(SmbRegistrationMatchContactResult result) {
        return result.getRelatedCompanies().stream()
                            .sorted(Comparator.comparing(
                                    SmbRegistrationMatchContactResult.SmbScoredRelatedCompany::getScore,
                                    reverseOrder()))
                            .map(SmbRegistrationMatchContactResult.SmbScoredRelatedCompany::getCompany)
                            .collect(Collectors.toList());
    }

    void setRegistration(RegistrationData registration) {
        this.registration = registration;
        readBeans(registration);

        removeAll();
        add(mainInfoForm, companyForm);

        if (registration.getStatus() == RegistrationStatus.MANUAL_REVIEW_PENDING) {
            matchSmbContact(null);
        }
        
        if (registration.getEditor() == null) {
            registrationService.updateEditor(registration.getToken(), SecurityUtils.getCurrentUser().getExternalId());
        }
    }

    Long getLinkedSambaContactId() {
        return mainInfoForm.getSambaContactId();
    }

    boolean isLinkedSambaContactVdmaMember() {
        return sambaBinder.getBean() != null && sambaBinder.getBean().isVdmaMember();
    }

    private void readBeans(RegistrationData registration) {
        binder.readBean(registration);
        if (registration.getUser() != null) {
            userBinder.readBean(registration.getUser());
            contactUiFetcher.fetch(registration.getUser().getSambaId(), this::setContactBean);
        }
    }

    private void setContactBean(SmbContactData contact) {
        mainInfoForm.setSambaContactId(contact != null ? contact.getId() : null);
        sambaBinder.setBean(contact);
        sambaCompanyBinder.setBean(contact != null ? contact.getCompany() : null);
    }

    private void setCompanyBean(SmbCompanyData company) {
        sambaCompanyBinder.setBean(company);
        sambaBinder.setBean(null);
        mainInfoForm.setSambaContactId(null);
        fetchCompanyContacts(this::buildCompanyContactsComponent);
    }

    private void buildCompanyContactsComponent(List<SmbContactData> contacts) {
        if (companyContactsComponent != null) {
            remove(companyContactsComponent);
        }
        if (contacts.isEmpty()) {
            return;
        }

        Button btnRefresh = UIUtils.createOutlinedButton(
                getTranslation("registration.details.match-contact.refresh-list"));
        btnRefresh.addClickListener(event -> {
            btnRefresh.setEnabled(false);
            fetchCompanyContacts(companyContacts -> {
                btnRefresh.setEnabled(true);
                buildCompanyContactsComponent(companyContacts);
            });
        });
        btnRefresh.addClassName(LumoStyles.Margin.Left.AUTO);

        FlexBoxLayout header = new FlexBoxLayout();
        header.setAlignItems(Alignment.CENTER);
        header.add(UIUtils.createH5Label(getTranslation("registration.details.match-contact.company-contacts-found")));
        header.add(btnRefresh);

        RegistrationSmbContactsGrid contactsGrid = new RegistrationSmbContactsGrid(contacts, this::setContactBean);

        companyContactsComponent = new FlexBoxLayout(header, contactsGrid);
        companyContactsComponent.setPadding(Top.S);
        companyContactsComponent.setFlexDirection(FlexDirection.COLUMN);

        addComponentAtIndex(indexOf(companyForm) + 1, companyContactsComponent);
    }

    private void buildMainInfoForm() {
        mainInfoForm = new RegistrationMainInfoForm(binder, userBinder, sambaBinder);
        mainInfoForm.setUserIdClickListener(event -> {
            MainLayout.get().navigate(UserDetails.getTabLabel(registration.getUser()), UserDetails.class,
                    UserDetails.getNaviParam(registration.getUser()));
        });
        mainInfoForm.setCreateContactListener(event -> {
            if (sambaBinder.getBean() != null) {
                UIUtils.showNotification(
                        getTranslation("registration.details.create-contact.contact-already-selected-msg"));
            } else if (sambaCompanyBinder.getBean() == null) {
                UIUtils.showNotification(
                        getTranslation("registration.details.create-contact.company-not-selected-msg"));
            } else {
                ConfirmDialog dialog = ConfirmDialog
                        .createQuestion()
                        .withCaption(getTranslation("registration.details.confirm-contact-creation-caption"))
                        .withMessage(getTranslation("registration.details.confirm-contact-creation-msg"))
                        .withCancelButton(ButtonOption.caption(getTranslation("commons.no")));
                dialog.withOkButton(() -> {
                    try {
                        SmbContactData newContact = smbRegistrationService.createContact(registration,
                                sambaCompanyBinder.getBean().getId());
                        setContactBean(newContact);
                        dialog.close();
                    } catch (Exception e) {
                        log.error("failed to create samba contact for registration " + registration.getId(), e);
                        UIUtils.showNotification(e.getMessage());
                        dialog.getButton(ButtonType.OK).setEnabled(true);
                    }
                }, ButtonOption.focus(), ButtonOption.caption(getTranslation("commons.yes")),
                        ButtonOption.closeOnClick(false));
                dialog.getButton(ButtonType.OK).setDisableOnClick(true);
                dialog.open();
            }
        });
        mainInfoForm.setLinkContactListener(event -> {
            new BindSmbContactDlg(sambaBinder.getBean(), contactUiFetcher, this::setContactBean).open();
        });
        mainInfoForm.addStatusChangeListener(event -> {
            boolean isManualReviewPending = event.getValue() == RegistrationStatus.MANUAL_REVIEW_PENDING;
            companyForm.setLinkCompanyBtnVisibility(isManualReviewPending);
        });
        mainInfoForm.setSaveMemoListener(event -> {
            if (binder.writeBeanIfValid(registration)) {
                registrationService.updateMemo(registration.getToken(), registration.getMemo());
            }
        });
        mainInfoForm.setDiscardMemoListener(event -> {
            binder.readBean(registration);
        });
    }

    private void buildCompanyForm() {
        companyForm = new RegistrationCompanyForm(binder, userBinder, sambaCompanyBinder);
        companyForm.setLinkCompanyListener(event -> {
            new BindSmbCompanyDlg(sambaCompanyBinder.getBean(), companyUiFetcher, this::setCompanyBean).open();
        });
    }

    private void fetchCompanyContacts(Consumer<List<SmbContactData>> onResult) {
        SmbCompanyData company = sambaCompanyBinder.getBean();
        if (company == null) {
            return;
        }
        new Thread(() -> {
            try {
                List<SmbContactData> contacts = smbRegistrationService.getCompanyContacts(company.getId(),
                        registration.getFirstName(), registration.getLastName());
                ui.access(() -> onResult.accept(contacts));
            } catch (Exception ex) {
                log.warn("failed to load contacts of company " + company.getId(), ex);
                ui.access(() -> {
                    onResult.accept(Collections.emptyList());
                    UIUtils.showNotification(
                            getTranslation("registration.details.match-contact.company-contacts-failed-msg",
                                    ex.getMessage()));
                });
            }
        }).start();
    }
}
