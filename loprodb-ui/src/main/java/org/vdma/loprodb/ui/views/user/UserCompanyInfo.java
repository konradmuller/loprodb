package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.model.SmbPostAddressData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.LabelField;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_CLASS_NAME;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;
import static org.vdma.loprodb.ui.views.user.UserDetails.HEADER_CLASS_NAME;
import org.vdma.loprodb.ui.views.user.components.UserPropFormItem;

class UserCompanyInfo extends FlexBoxLayout {

    private final Binder<UserData> binder;
    private final Binder<SmbContactData> sambaBinder;

    UserCompanyInfo(Binder<UserData> binder, Binder<SmbContactData> sambaBinder) {
        this.binder = binder;
        this.sambaBinder = sambaBinder;
        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);
        buildContent();
    }

    private void buildContent() {
        FormLayout form = new FormLayout();
        UIUtils.setTheme(FORM_THEME, form);
        form.addClassName(FORM_CLASS_NAME);
        form.setWidthFull();
        UIUtils.setPadding(form, Bottom.L, Top.S);
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        TextField tfCompanyName = new TextField();
        binder.forField(tfCompanyName).bind(u -> u.getCompany().getName(), null);
        TextField tfSmbCompanyName = new TextField();
        sambaBinder.forField(tfSmbCompanyName).bind(c -> c.getCompany().getName(), null);

        TextField tfCompanyName1 = new TextField();
        binder.forField(tfCompanyName1).bind(u -> u.getCompany().getName1(), null);
        TextField tfSmbCompanyName1 = new TextField();
        sambaBinder.forField(tfSmbCompanyName1).bind(c -> c.getCompany().getName1(), null);

        TextField tfCompanyName2 = new TextField();
        binder.forField(tfCompanyName2).bind(u -> u.getCompany().getName2(), null);
        TextField tfSmbCompanyName2 = new TextField();
        sambaBinder.forField(tfSmbCompanyName2).bind(c -> c.getCompany().getName2(), null);

        TextField tfCompanyName3 = new TextField();
        binder.forField(tfCompanyName3).bind(u -> u.getCompany().getName3(), null);
        TextField tfSmbCompanyName3 = new TextField();
        sambaBinder.forField(tfSmbCompanyName3).bind(c -> c.getCompany().getName3(), null);

        TextField tfCountry = new TextField();
        binder.forField(tfCountry).bind(u -> u.getCompany().getCountry(), null);
        TextField tfSmbCountry = new TextField();
        sambaBinder.forField(tfSmbCountry).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? UIUtils.getDisplayCountry(a.getCountryCode()) : null;
        }, null);

        TextField tfZip = new TextField();
        binder.forField(tfZip).bind(u -> u.getCompany().getZip(), null);
        TextField tfSmbZip = new TextField();
        sambaBinder.forField(tfSmbZip).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? a.getZip() : null;
        }, null);

        TextField tfLocation = new TextField();
        binder.forField(tfLocation).bind(u -> u.getCompany().getLocation(), null);
        TextField tfSmbLocation = new TextField();
        sambaBinder.forField(tfSmbLocation).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? a.getLocation() : null;
        }, null);

        TextField tfDistrict = new TextField();
        binder.forField(tfDistrict).bind(u -> u.getCompany().getDistrict(), null);
        TextField tfSmbDistrict = new TextField();
        sambaBinder.forField(tfSmbDistrict).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? a.getDistrict() : null;
        }, null);

        TextField tfCity = new TextField();
        binder.forField(tfCity).bind(u -> u.getCompany().getCity(), null);
        TextField tfSmbCity = new TextField();
        sambaBinder.forField(tfSmbCity).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? a.getCity() : null;
        }, null);

        TextField tfStreet = new TextField();
        binder.forField(tfStreet).bind(u -> u.getCompany().getStreet(), null);
        TextField tfSmbStreet = new TextField();
        sambaBinder.forField(tfSmbStreet).bind(c -> {
            SmbPostAddressData a = c.getCompany().getAddress();
            return a != null ? a.getStreet() : null;
        }, null);

        TextField tfHouseNumber = new TextField();
        binder.forField(tfHouseNumber).bind(u -> u.getCompany().getHouseNumber(), null);

        form.add(buildHeader());
        form.add(new UserPropFormItem(getTranslation("user.details.company.company-name"), tfCompanyName,
                tfSmbCompanyName));
        form.add(new UserPropFormItem(getTranslation("user.details.company.company-name1"), tfCompanyName1,
                tfSmbCompanyName1));
        form.add(new UserPropFormItem(getTranslation("user.details.company.company-name2"), tfCompanyName2,
                tfSmbCompanyName2));
        form.add(new UserPropFormItem(getTranslation("user.details.company.company-name3"), tfCompanyName3,
                tfSmbCompanyName3));
        form.add(new UserPropFormItem(getTranslation("commons.country"), tfCountry, tfSmbCountry));
        form.add(new UserPropFormItem(getTranslation("commons.zip"), tfZip, tfSmbZip));
        form.add(new UserPropFormItem(getTranslation("commons.city"), tfCity, tfSmbCity));
        form.add(new UserPropFormItem(getTranslation("commons.location"), tfLocation, tfSmbLocation));
        form.add(new UserPropFormItem(getTranslation("commons.district"), tfDistrict, tfSmbDistrict));
        form.add(new UserPropFormItem(getTranslation("commons.street"), tfStreet, tfSmbStreet));
        form.add(new UserPropFormItem(getTranslation("commons.house-number"), tfHouseNumber, new Div()));

        add(form);
    }

    private Component buildHeader() {
        Label lbHeaderProfile = UIUtils.createH5Label(getTranslation("user.details.company.company-data"));
        lbHeaderProfile.addClassName(LumoStyles.Margin.Right.AUTO);

        Label lblUserId = UIUtils.createH5Label("");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblUserId);
        LabelField<Long> lfUserId = new LabelField<>(lblUserId);
        lfUserId.setPrefix(getTranslation("user.details.user-id"));
        binder.forField(lfUserId).bind(UserData::getId, null);

        FlexBoxLayout headerLeft = new FlexBoxLayout(lbHeaderProfile, lfUserId);
        headerLeft.setFlexDirection(FlexDirection.ROW);
        headerLeft.setBoxSizing(BoxSizing.BORDER_BOX);
        headerLeft.setAlignItems(FlexComponent.Alignment.CENTER);
        headerLeft.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

        Label lbHeaderSamba = UIUtils.createH5Label(getTranslation("user.details.company.samba-company-data"));

        Label lblSambaId = UIUtils.createH5Label("");
        lblSambaId.getElement().getStyle().set("text-decoration", "underline");
        lblSambaId.getElement().getStyle().set("cursor", "pointer");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblSambaId);
        LabelField<Long> lfSambaId = new LabelField<>(lblSambaId);
        lfSambaId.setPrefix(getTranslation("user.details.samba-addr-id"));
        sambaBinder.forField(lfSambaId).bind(c -> c.getCompany().getId(), null);

        AnchorField sambaLink = new AnchorField(lfSambaId);
        sambaBinder.forField(sambaLink).bind(c -> c.getCompany().getUrl(), null);

        FlexBoxLayout headerRight = new FlexBoxLayout(lbHeaderSamba, sambaLink);
        headerRight.setFlexDirection(FlexDirection.ROW);
        headerRight.setSpacing(Right.XL);
        headerRight.setBoxSizing(BoxSizing.BORDER_BOX);
        headerRight.setAlignItems(FlexComponent.Alignment.CENTER);

        UserPropFormItem userPropFormItem = new UserPropFormItem("", headerLeft, headerRight);
        userPropFormItem.addClassName(HEADER_CLASS_NAME);
        return userPropFormItem;
    }
}
