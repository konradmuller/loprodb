package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import lombok.Getter;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

public class UserPropFormItem extends FormLayout.FormItem {

    public static final String THEME = "user-prop-form-item";

    @Getter
    private final FlexBoxLayout field;

    public UserPropFormItem(Component label, Component first, Component... other) {
        field = new FlexBoxLayout();
        field.add(first);
        field.add(other);
        field.setFlexGrow(3, first);
        field.setFlexGrow(4, other);
        field.setFlexBasis("0", first);
        field.setFlexBasis("0", other);
        field.setBoxSizing(BoxSizing.BORDER_BOX);
        field.setSpacing(Right.XL);
        field.setPadding(Horizontal.S, Vertical.XS);
        field.setAlignItems(FlexComponent.Alignment.CENTER);
        add(field);
        addToLabel(label);
        UIUtils.setTheme(THEME, this);
    }

    public UserPropFormItem(String label, Component first, Component... other) {
        this(new Label(label), first, other);
    }

    public UserPropFormItem asRequired() {
        UIUtils.asRequired(this);
        return this;
    }
}
