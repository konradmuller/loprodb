package org.vdma.loprodb.ui.views.registration.components;

import com.vaadin.flow.component.customfield.CustomField;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.util.UIUtils;

public class RegistrationStatusField extends CustomField<RegistrationStatus> {

    @Override
    protected RegistrationStatus generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(RegistrationStatus status) {
        getChildren().forEach(this::remove);
        Badge badge = RegistrationStatusBadge.createForStatus(status);
        if (badge != null) {
            UIUtils.setPadding(badge, Horizontal.L);
            add(badge);
        }
    }
}
