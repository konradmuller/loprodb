package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.views.AbstractAuditTrail;
import org.vdma.loprodb.ui.views.user.components.UserStatusField;

@RequiredArgsConstructor
class UserAuditTrail extends AbstractAuditTrail<UserData> {

    private final Long userId;
    private final UserMapper userMapper;
    private final AuditService auditService;

    @Override
    protected SpringDataProviderBuilder<AuditData<UserData>, ObjectListParam> auditProviderBuilder() {
        return SpringDataProviderBuilder.userAudit(userId, userMapper, auditService);
    }

    @Override
    protected String getDetailsDrawerHeaderTitle(AuditData<UserData> audit) {
        return audit.getEntityData().getFullName();
    }

    @Override
    protected Component createDetails(AuditData<UserData> audit) {
        Binder<UserData> binder = new Binder<>();

        UserStatusField badgeStatus = new UserStatusField();
        badgeStatus.setWidthFull();
        binder.forField(badgeStatus).bind(UserData::getStatus, null);

        TextField tfSambaId = new TextField();
        tfSambaId.setWidthFull();
        binder.forField(tfSambaId).bind(user -> user.getSambaId() == null ? "" : user.getSambaId().toString(), null);

        TextField tfAimId = new TextField();
        tfAimId.setWidthFull();
        binder.forField(tfAimId).bind(user -> user.getAimId() == null ? "" : user.getAimId().toString(), null);

        TextField tfDeactivateOn = new TextField();
        binder.forField(tfDeactivateOn).bind(user -> UIUtils.formatDate(user.getAutoDeactivationDate()), null);

        Checkbox chbDeactivateOn = new Checkbox();
        binder.forField(chbDeactivateOn).bind(UserData::isAutoDeactivation, null);

        FlexBoxLayout fblDeactivate = new FlexBoxLayout(chbDeactivateOn, tfDeactivateOn);
        fblDeactivate.setFlexGrow(1, tfDeactivateOn);
        fblDeactivate.setAlignSelf(Alignment.CENTER, chbDeactivateOn);
        fblDeactivate.setSpacing(Right.XL);

        TextField tfSalutation = new TextField();
        tfSalutation.setWidthFull();
        binder.forField(tfSalutation)
                .bind(user -> getTranslation(Gender.class.getSimpleName() + "." + user.getGender().name()), null);

        TextField tfTitle = new TextField();
        tfTitle.setWidthFull();
        binder.forField(tfTitle).bind(UserData::getTitle, null);

        TextField tfFirstName = new TextField();
        tfFirstName.setWidthFull();
        binder.forField(tfFirstName).bind(UserData::getFirstName, null);

        TextField tfLastName = new TextField();
        tfLastName.setWidthFull();
        binder.forField(tfLastName).bind(UserData::getLastName, null);

        EmailField efEmail = new EmailField();
        efEmail.setWidthFull();
        binder.forField(efEmail).bind(UserData::getEmail, null);

        TextField tfPhone = new TextField();
        tfPhone.setWidthFull();
        binder.forField(tfPhone).bind(UserData::getPhone, null);

        TextField tfFunction = new TextField();
        tfFunction.setWidthFull();
        binder.forField(tfFunction).bind(UserData::getFunction, null);

        TextField tfPosition = new TextField();
        tfPosition.setWidthFull();
        binder.forField(tfPosition).bind(user -> user.getPosition().getNameByLocale(getLocale()), null);

        TextField tfArea = new TextField();
        tfArea.setWidthFull();
        binder.forField(tfArea).bind(user -> user.getBusinessArea().getNameByLocale(getLocale()), null);

        TextField tfDepartment = new TextField();
        tfDepartment.setWidthFull();
        binder.forField(tfDepartment).bind(UserData::getDepartment, null);

        TextField tfBirthday = new TextField();
        tfBirthday.setWidthFull();
        binder.forField(tfBirthday).bind(u -> UIUtils.formatDate(u.getBirthday()), null);

        TextField tfPublicPhone = new TextField();
        tfPublicPhone.setWidthFull();
        binder.forField(tfPublicPhone).bind(UserData::getPublicPhone, null);

        TextField tfPublicMobile = new TextField();
        tfPublicMobile.setWidthFull();
        binder.forField(tfPublicMobile).bind(UserData::getPublicMobile, null);

        EmailField efPublicEmail = new EmailField();
        efPublicEmail.setWidthFull();
        binder.forField(efPublicEmail).bind(UserData::getPublicEmail, null);

        TextField tfDsVersion = new TextField();
        tfDsVersion.setWidthFull();
        UIUtils.setTheme("bold", tfDsVersion);
        binder.forField(tfDsVersion).bind(UserData::getDsVersion, null);

        TextField tfLang = new TextField();
        tfLang.setWidthFull();
        binder.forField(tfLang)
                .bind(user -> getTranslation(Language.class.getSimpleName() + "." + user.getLanguage().name()), null);

        TextField tfPhoto = new TextField();
        tfPhoto.setWidthFull();
        binder.forField(tfPhoto).bind(user -> UIUtils.formatDate(user.getPhotoChangedOn()), null);

        TextArea taTags = new TextArea();
        taTags.setWidthFull();
        binder.forField(taTags).bind(user -> String.join("\n", user.getTags()), null);

        TextField tfCompanyName = new TextField();
        tfCompanyName.setWidthFull();
        binder.forField(tfCompanyName).bind(u -> u.getCompany().getName(), null);

        TextField tfCompanyName1 = new TextField();
        tfCompanyName1.setWidthFull();
        binder.forField(tfCompanyName1).bind(u -> u.getCompany().getName1(), null);

        TextField tfCompanyName2 = new TextField();
        tfCompanyName2.setWidthFull();
        binder.forField(tfCompanyName2).bind(u -> u.getCompany().getName2(), null);

        TextField tfCompanyName3 = new TextField();
        tfCompanyName3.setWidthFull();
        binder.forField(tfCompanyName3).bind(u -> u.getCompany().getName3(), null);

        TextField tfLocation = new TextField();
        tfLocation.setWidthFull();
        binder.forField(tfLocation).bind(u -> u.getCompany().getLocation(), null);

        TextField tfDistrict = new TextField();
        tfDistrict.setWidthFull();
        binder.forField(tfDistrict).bind(u -> u.getCompany().getDistrict(), null);

        TextField tfCountry = new TextField();
        tfCountry.setWidthFull();
        binder.forField(tfCountry).bind(u -> u.getCompany().getCountry(), null);

        TextField tfZip = new TextField();
        tfZip.setWidthFull();
        binder.forField(tfZip).bind(u -> u.getCompany().getZip(), null);

        TextField tfCity = new TextField();
        tfCity.setWidthFull();
        binder.forField(tfCity).bind(u -> u.getCompany().getCity(), null);

        TextField tfStreet = new TextField();
        tfStreet.setWidthFull();
        binder.forField(tfStreet).bind(u -> u.getCompany().getStreet(), null);

        TextField tfHouseNumber = new TextField();
        tfHouseNumber.setWidthFull();
        binder.forField(tfHouseNumber).bind(u -> u.getCompany().getHouseNumber(), null);

        TextArea taRoles = new TextArea();
        taRoles.setWidthFull();
        binder.forField(taRoles).bind(user -> user.getApps().stream().map(app -> {
            StringBuilder sb = new StringBuilder(app.getAppName());
            if (!app.getRoles().isEmpty()) {
                sb.append(": ");
                sb.append(app.getRoles().stream().map(AppRoleData::getName).collect(Collectors.joining(", ")));
            }
            return sb.toString();
        }).collect(Collectors.joining("\n\n")), null);

        TextArea taTopics = new TextArea();
        taTopics.setWidthFull();
        binder.forField(taTopics).bind(user -> user.getTopics().stream()
                .map(topic -> topic.getNameByLocale(getLocale())).sorted()
                .collect(Collectors.joining(", ")), null);

        TextField regPwdChangedDate = new TextField();
        regPwdChangedDate.setWidthFull();
        binder.forField(regPwdChangedDate).bind(user -> UIUtils.formatDate(user.getRegPwdChangedDate()), null);

        FormLayout form = new FormLayout();
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addClassNames(LumoStyles.Padding.Bottom.L, LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        form.addFormItem(tfSambaId, getTranslation("user.list.filter.samba-id"));
        form.addFormItem(badgeStatus, getTranslation("user.details.basic.status"));
        form.addFormItem(tfAimId, getTranslation("user.details.basic.aim-id"));
        form.addFormItem(fblDeactivate, getTranslation("user.details.basic.automatic-deactivation"));
        form.addFormItem(tfSalutation, getTranslation("user.details.basic.salutation"));
        form.addFormItem(tfTitle, getTranslation("user.details.basic.title"));
        form.addFormItem(tfFirstName, getTranslation("user.details.basic.first-name"));
        form.addFormItem(tfLastName, getTranslation("user.details.basic.last-name"));
        form.addFormItem(tfBirthday, getTranslation("user.details.basic.birthday"));
        form.addFormItem(efEmail, getTranslation("user.details.basic.email"));
        form.addFormItem(tfPhone, getTranslation("user.details.basic.phone"));
        form.addFormItem(tfPublicPhone, getTranslation("user.details.basic.public-phone"));
        form.addFormItem(efPublicEmail, getTranslation("user.details.basic.public-email"));
        form.addFormItem(tfPublicMobile, getTranslation("user.details.basic.public-mobile"));
        form.addFormItem(tfFunction, getTranslation("user.details.basic.function"));
        form.addFormItem(tfPosition, getTranslation("user.details.basic.position"));
        form.addFormItem(tfArea, getTranslation("user.details.basic.business-area"));
        form.addFormItem(tfDepartment, getTranslation("user.details.basic.department"));
        form.addFormItem(tfDsVersion, getTranslation("user.details.basic.ds-version"));
        form.addFormItem(tfLang, getTranslation("commons.lang"));
        form.addFormItem(tfPhoto, getTranslation("user.details.basic.photo-changed-on"));
        form.addFormItem(taTags, getTranslation("user.details.basic.tags"));
        form.addFormItem(tfCompanyName, getTranslation("user.details.company.company-name"));
        form.addFormItem(tfCompanyName1, getTranslation("user.details.company.company-name1"));
        form.addFormItem(tfCompanyName2, getTranslation("user.details.company.company-name2"));
        form.addFormItem(tfCompanyName3, getTranslation("user.details.company.company-name3"));
        form.addFormItem(tfCountry, getTranslation("commons.country"));
        form.addFormItem(tfZip, getTranslation("commons.zip"));
        form.addFormItem(tfCity, getTranslation("commons.city"));
        form.addFormItem(tfLocation, getTranslation("commons.location"));
        form.addFormItem(tfDistrict, getTranslation("commons.district"));
        form.addFormItem(tfStreet, getTranslation("commons.street"));
        form.addFormItem(tfHouseNumber, getTranslation("commons.house-number"));
        form.addFormItem(taRoles, getTranslation("user.details.apps.app") + "/" +
                getTranslation("user.details.apps.roles"));
        form.addFormItem(taTopics, getTranslation("user.details.topics"));
        form.addFormItem(regPwdChangedDate, getTranslation("user.details.audit.reg-pwd-changed-date"));

        binder.readBean(audit.getEntityData());
        return form;
    }
}
