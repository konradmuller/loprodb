package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.RouterLink;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.views.LoginView;

public class Header extends FlexBoxLayout {
    public Header() {
        Image logo = new Image(UIUtils.IMG_PATH + "logo-with-title.svg", "Login and Profile DB logo");
        logo.setWidth("100px");
        logo.addClickListener(event -> UI.getCurrent().navigate(LoginView.class));

        RouterLink link = new RouterLink("", LoginView.class);
        link.getElement().getStyle().set("display", "flex");
        link.getElement().getStyle().set("margin-left", "calc(" + Left.RESPONSIVE_X.getVariable() + " * 2)");
        link.add(logo);

        setPadding(Horizontal.RESPONSIVE_X, Vertical.RESPONSIVE_M);
        setBackgroundColor("var(--lumo-primary-color)");
        add(link);
        setWidthFull();
    }
}
