package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.HasErrorParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import javax.servlet.http.HttpServletResponse;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.exceptions.AccessDeniedException;

@PageTitle("Access denied")
@Route(value = "denied", layout = MainLayout.class)
public class AccessDeniedView extends ViewFrame implements AfterNavigationObserver,
        HasErrorParameter<AccessDeniedException>, PublicView {

    public AccessDeniedView() {
        setViewContent(createContent());
    }

    private Component createContent() {
        VerticalLayout result = new VerticalLayout(new Label("You have no rights to see this view."));
        result.setPadding(true);
        return result;
    }

    @Override
    public int setErrorParameter(BeforeEnterEvent event, ErrorParameter<AccessDeniedException> parameter) {
        return HttpServletResponse.SC_FORBIDDEN;
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        MainLayout.get().getAppBar().setTitle("Access denied");
    }
}
