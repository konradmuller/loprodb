package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.ListItem;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.views.user.components.SmbAddressStatusField;

class RegistrationSmbContactsGrid extends Grid<SmbContactData> {

    RegistrationSmbContactsGrid(List<SmbContactData> contacts, Consumer<SmbContactData> contactSelectListener) {

        contacts.sort(Comparator.comparing(SmbContactData::getDocumentsCount, Comparator.reverseOrder()));

        setHeightByRows(true);
        setSelectionMode(Grid.SelectionMode.NONE);
        UIUtils.setMargin(this, Top.S);
        setDataProvider(DataProvider.ofCollection(contacts));
        setWidthFull();
        addColumn(new ComponentRenderer<>(this::createStatusColumn))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader(getTranslation("user.list.status"));

        addColumn(new ComponentRenderer<>(this::createNameColumn))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.list.name"));

        addColumn(new ComponentRenderer<>(this::createCompanyNameColumn))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setHeader(getTranslation("user.list.company"));

        addColumn(contact -> {
            if (!contact.getAreaPositions().isEmpty()) {
                return contact.getAreaPositions().get(0).getPosition().getNameByLocale(getLocale());
            } else {
                return "";
            }
        }).setAutoWidth(true)
                .setFlexGrow(1)
                .setHeader(getTranslation("user.details.basic.position"));

        addColumn(SmbContactData::getFunction)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setHeader(getTranslation("user.details.basic.function"));

        addColumn(new ComponentRenderer<>(this::createIdWithLinkColumn))
                .setAutoWidth(true)
                .setHeader(getTranslation("user.list.filter.samba-id"));

        addColumn(new ComponentRenderer<>(c -> createSelectColumn(c, contactSelectListener)))
                .setAutoWidth(true)
                .setTextAlign(ColumnTextAlign.END)
                .setFlexGrow(1);
    }

    private Button createSelectColumn(SmbContactData contact, Consumer<SmbContactData> contactSelectListener) {
        Button btnSelect = UIUtils.createOutlinedButton(getTranslation("registration.details.match-contact.choose"));
        btnSelect.addClickListener(event -> contactSelectListener.accept(contact));
        return btnSelect;
    }

    private AnchorField createIdWithLinkColumn(SmbContactData contact) {
        Label lblId = new Label(contact.getId().toString());
        lblId.getElement().getStyle().set("cursor", "pointer");
        AnchorField sambaLink = new AnchorField(lblId);
        sambaLink.setValue(contact.getUrl());
        return sambaLink;
    }

    private ListItem createCompanyNameColumn(SmbContactData contact) {
        ListItem item = new ListItem(contact.getCompany().getName(), contact.getCompany().getAddress().getCity());
        item.setPadding(Vertical.XS);
        return item;
    }

    private ListItem createNameColumn(SmbContactData contact) {
        ListItem item = new ListItem(contact.getFullName(), contact.getEmail());
        item.setPadding(Vertical.XS);
        return item;
    }

    private SmbAddressStatusField createStatusColumn(SmbContactData contact) {
        SmbAddressStatusField smbStatus = new SmbAddressStatusField();
        smbStatus.setValue(contact.getStatus().name());
        smbStatus.setWidthFull();
        return smbStatus;
    }
}
