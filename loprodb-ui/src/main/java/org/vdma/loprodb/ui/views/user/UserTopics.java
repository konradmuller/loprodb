package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.binder.Binder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.util.css.FlexDirection;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_CLASS_NAME;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;

class UserTopics extends FlexBoxLayout {

    private final Binder<UserData> binder;

    UserTopics(Binder<UserData> binder) {
        this.binder = binder;

        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);
        buildContent();
    }

    private void buildContent() {
        TopicField topics = new TopicField();
        binder.forField(topics).bind(UserData::getTopics, null);
        add(topics);
    }

    private static final class TopicField extends CustomField<Collection<ReferenceData>> {

        private FormLayout form;

        TopicField() {
            form = new FormLayout();
            UIUtils.setTheme(FORM_THEME, form);
            form.addClassName(FORM_CLASS_NAME);
            form.setWidthFull();
            UIUtils.setPadding(form, Bottom.L, Top.S);
            form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                    FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
            add(form);
        }

        @Override
        protected Collection<ReferenceData> generateModelValue() {
            return getValue();
        }

        @Override
        protected void setPresentationValue(Collection<ReferenceData> newPresentationValue) {
            form.removeAll();
            if (newPresentationValue != null && !newPresentationValue.isEmpty()) {
                // to get correct highlighting
                form.add(new Span(), new Span());

                List<ReferenceData> topics = new ArrayList<>(newPresentationValue);
                topics.sort(Comparator.comparing(referenceData -> referenceData.getNameByLocale(getLocale())));

                FlexBoxLayout item = new FlexBoxLayout();
                for (int i = 0; i < topics.size(); i++) {
                    if ((i % 2) == 0) {
                        item = new FlexBoxLayout();
                        item.setBoxSizing(BoxSizing.BORDER_BOX);
                        item.setPadding(Horizontal.XL);
                        form.add(item);
                    }
                    Checkbox chbTopic = new Checkbox(topics.get(i).getNameByLocale(getLocale()));
                    chbTopic.setValue(true);
                    chbTopic.setReadOnly(true);
                    item.add(chbTopic);
                    item.setFlexGrow(1, chbTopic);
                    item.setFlexBasis("0", chbTopic);
                }
            }
        }
    }
}
