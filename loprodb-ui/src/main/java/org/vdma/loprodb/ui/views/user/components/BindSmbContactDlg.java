package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import java.util.function.Consumer;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.ui.components.CustomDialog;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.ImageField;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Uniform;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.user.SmbContactUiFetcher;

public class BindSmbContactDlg extends CustomDialog {

    private static final String PHOTO_WIDTH = "175px";

    public BindSmbContactDlg(SmbContactData initial, SmbContactUiFetcher contactUiFetcher,
            Consumer<SmbContactData> onResult) {
        Binder<SmbContactData> binder = new Binder<>();
        binder.setBean(initial);

        NumberField nfSambaId = new NumberField();
        nfSambaId.setWidthFull();
        nfSambaId.setMin(1);
        nfSambaId.setStep(1);
        if (initial != null) {
            nfSambaId.setValue(initial.getId().doubleValue());
        }

        Button btnQuery = UIUtils.createOutlinedButton(getTranslation("user.details.basic.fetch-samba-address"));
        btnQuery.setWidth(PHOTO_WIDTH);
        btnQuery.addClickListener(e -> {
            if (nfSambaId.getValue() != null) {
                btnQuery.setEnabled(false);
                contactUiFetcher.fetch(nfSambaId.getValue().longValue(), c -> {
                    binder.setBean(c);
                    btnQuery.setEnabled(true);
                });
            }
        });

        FormLayout sambaIdForm = new FormLayout();
        sambaIdForm.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
        UIUtils.setTheme(UserPropFormItem.THEME,
                sambaIdForm.addFormItem(nfSambaId, getTranslation("user.list.filter.samba-id")));

        FlexBoxLayout flbSambaId = new FlexBoxLayout(sambaIdForm, btnQuery);
        flbSambaId.setSpacing(Right.XL);
        flbSambaId.setBoxSizing(BoxSizing.BORDER_BOX);
        flbSambaId.setFlexGrow(1, sambaIdForm);

        TextField tfSalutation = new TextField();
        tfSalutation.setWidthFull();
        binder.forField(tfSalutation).bind(c -> {
            if (c.getSalutation() != null) {
                return c.getSalutation().getNameByLocale(getLocale());
            } else {
                return null;
            }
        }, null);

        TextField tfTitle = new TextField();
        tfTitle.setWidthFull();
        binder.forField(tfTitle).bind(SmbContactData::getTitle, null);

        TextField tfFirstName = new TextField();
        tfFirstName.setWidthFull();
        binder.forField(tfFirstName).bind(SmbContactData::getFirstName, null);

        TextField tfLastName = new TextField();
        tfLastName.setWidthFull();
        binder.forField(tfLastName).bind(SmbContactData::getLastName, null);

        TextField tfEmail = new TextField();
        tfEmail.setWidthFull();
        binder.forField(tfEmail).bind(SmbContactData::getEmail, null);

        TextField tfPhone = new TextField();
        tfPhone.setWidthFull();
        binder.forField(tfPhone).bind(SmbContactData::getPhone, null);

        TextField tfCompany = new TextField();
        tfCompany.setWidthFull();
        binder.forField(tfCompany).bind(c -> c.getCompany().getName(), null);

        TextField tfFunc = new TextField();
        tfFunc.setWidthFull();
        binder.forField(tfFunc).bind(SmbContactData::getFunction, null);

        TextField tfPosition = new TextField();
        tfPosition.setWidthFull();
        binder.forField(tfPosition).bind(c -> {
            if (!c.getAreaPositions().isEmpty()) {
                return c.getAreaPositions().get(0).getPosition().getNameByLocale(getLocale());
            } else {
                return null;
            }
        }, null);

        ImageField imgPhoto = new ImageField();
        imgPhoto.setWidth(PHOTO_WIDTH);
        binder.forField(imgPhoto).bind(SmbContactData::getPicture100, null);

        FormLayout detailsForm = new FormLayout();
        detailsForm.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        detailsForm.addFormItem(tfSalutation, getTranslation("user.details.basic.salutation"));
        detailsForm.addFormItem(tfTitle, getTranslation("user.details.basic.title"));
        detailsForm.addFormItem(tfFirstName, getTranslation("user.details.basic.first-name"));
        detailsForm.addFormItem(tfLastName, getTranslation("user.details.basic.last-name"));
        detailsForm.addFormItem(tfEmail, getTranslation("user.details.basic.email"));
        detailsForm.addFormItem(tfPhone, getTranslation("user.details.basic.phone"));
        detailsForm.addFormItem(tfCompany, getTranslation("user.details.basic.company-name"));
        detailsForm.addFormItem(tfFunc, getTranslation("user.details.basic.function"));
        detailsForm.addFormItem(tfPosition, getTranslation("user.details.basic.position"));

        detailsForm.getChildren().forEach(fi -> UIUtils.setTheme(UserPropFormItem.THEME, fi));

        FlexBoxLayout flbDetails = new FlexBoxLayout(detailsForm, new Div(imgPhoto));
        flbDetails.setSpacing(Right.XL);
        flbDetails.setBoxSizing(BoxSizing.BORDER_BOX);

        FlexBoxLayout flbContent = new FlexBoxLayout(flbSambaId, new Hr(), flbDetails);
        flbContent.setBoxSizing(BoxSizing.BORDER_BOX);
        flbContent.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        flbContent.setPadding(Uniform.RESPONSIVE_M);

        setCaption(getTranslation("user.details.basic.edit-samba-link"));
        setWidth("1000px");
        addOkListener(event -> {
            if (nfSambaId.getValue() == null) {
                onResult.accept(null);
                close();
            } else if (binder.getBean() != null && binder.getBean().getId().equals(nfSambaId.getValue().longValue())) {
                onResult.accept(binder.getBean());
                close();
            } else {
                getOk().setEnabled(false);
                getCancel().setEnabled(false);
                contactUiFetcher.fetch(nfSambaId.getValue().longValue(), c -> {
                    onResult.accept(c);
                    close();
                });
            }
        });
        addCancelListener(event -> close());
        setContent(flbContent);
    }
}
