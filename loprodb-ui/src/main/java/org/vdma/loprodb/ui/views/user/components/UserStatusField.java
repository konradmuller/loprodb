package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.customfield.CustomField;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.util.UIUtils;

public class UserStatusField extends CustomField<UserStatus> {

    @Override
    protected UserStatus generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(UserStatus userStatus) {
        getChildren().forEach(this::remove);
        Badge badge = UserStatusBadge.createForStatus(userStatus);
        if (badge != null) {
            UIUtils.setPadding(badge, Horizontal.L);
            add(badge);
        }
    }
}
