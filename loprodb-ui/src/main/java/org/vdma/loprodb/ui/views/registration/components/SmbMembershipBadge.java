package org.vdma.loprodb.ui.views.registration.components;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import org.vdma.loprodb.samba.model.SmbMembershipData;
import org.vdma.loprodb.ui.util.LumoStyles;

public class SmbMembershipBadge extends Div {

    public SmbMembershipBadge(SmbMembershipData membership) {
        String mText = membership.getMemberId() != null ? membership.getMemberId() + " / " : "";
        mText += membership.getOuShortName();

        add(new Label(mText));
        if (membership.getMembershipColor() != null) {
            getElement().getStyle().set("background-color", membership.getMembershipColor());
        }
        addClassName(LumoStyles.Padding.Uniform.S);
    }
}
