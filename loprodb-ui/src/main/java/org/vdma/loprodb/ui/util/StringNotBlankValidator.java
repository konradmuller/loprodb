package org.vdma.loprodb.ui.util;

import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.validator.AbstractValidator;
import org.apache.commons.lang3.StringUtils;

public class StringNotBlankValidator extends AbstractValidator<String> {

    public StringNotBlankValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public ValidationResult apply(String value, ValueContext context) {
        return toResult(value, StringUtils.isNotBlank(value));
    }
}
