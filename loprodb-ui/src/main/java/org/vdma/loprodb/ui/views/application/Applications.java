package org.vdma.loprodb.ui.views.application;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import org.apache.commons.lang3.StringUtils;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.service.param.application.UpdateAppPasswordParam;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.CustomDialog;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawer;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerFooter;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerHeader;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.SplitViewFrame;

@PageTitle("Applications")
@Route(value = "apps", layout = MainLayout.class)
public class Applications extends SplitViewFrame implements EqualCmp {

    private Grid<ApplicationData> grid;
    private DataProvider<ApplicationData, ObjectListParam> dataProvider;
    private ListDataProvider<AppRoleData> rolesDataProvider;

    private DetailsDrawer detailsDrawer;
    private DetailsDrawerHeader detailsDrawerHeader;
    private DetailsDrawerFooter detailsDrawerFooter;

    private Binder<ApplicationData> binder;
    private Binder<UpdateAppPasswordParam> passwordBinder;

    private ApplicationData selectedApp;
    private UpdateAppPasswordParam updatePassword;

    private final IApplicationService appService;

    @Autowired
    public Applications(IApplicationService appService) {
        this.appService = appService;
        buildContent();
    }

    private void buildContent() {
        Button btnNewApp = UIUtils.createNewObjButton(getTranslation("app.new"), this::onNewApp);

        FlexBoxLayout content = new FlexBoxLayout(btnNewApp, buildGrid());
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setHeightFull();
        content.setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        content.setAlignSelf(FlexComponent.Alignment.START, btnNewApp);

        setViewContent(content);
        setViewDetails(createDetailsDrawer());
    }

    private void onNewApp() {
        showDetails(new ApplicationData());
        grid.deselectAll();
    }

    private Grid buildGrid() {
        dataProvider = SpringDataProviderBuilder.applications(appService).withDefaultFilter(new ObjectListParam())
                .build();

        grid = new Grid<>();
        UIUtils.setMargin(grid, Top.RESPONSIVE_M);
        grid.addSelectionListener(event -> event.getFirstSelectedItem().ifPresent(this::showDetails));
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();
        grid.addColumn(ApplicationData::getName)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("app.name"));
        grid.addColumn(ApplicationData::getCode)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("app.code"));
        grid.addColumn(new ComponentRenderer<>(this::createItemRoles))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("app.roles"));
        return grid;
    }

    private Component createItemRoles(ApplicationData app) {
        FlexBoxLayout c = new FlexBoxLayout();
        c.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        app.getRoles().stream().sorted(Comparator.comparing(AppRoleData::getName, String.CASE_INSENSITIVE_ORDER))
                .map(role -> new Span(role.getName())).forEach(c::add);
        return c;
    }

    private DetailsDrawer createDetailsDrawer() {
        detailsDrawer = new DetailsDrawer(DetailsDrawer.Position.RIGHT);

        // Header
        detailsDrawerHeader = new DetailsDrawerHeader("");
        detailsDrawerHeader.addCloseListener(buttonClickEvent -> {
            detailsDrawer.hide();
            grid.deselectAll();
        });
        detailsDrawer.setHeader(detailsDrawerHeader);

        // Footer
        detailsDrawerFooter = new DetailsDrawerFooter();
        detailsDrawerFooter.addSaveListener(e -> {
            if ((selectedApp.getId() != null || passwordBinder.writeBeanIfValid(updatePassword)) &&
                    binder.writeBeanIfValid(selectedApp)) {
                selectedApp.setRoles(new HashSet<>(rolesDataProvider.getItems()));
                if (selectedApp.getId() == null) {
                    CreateAppParam createAppParam = new CreateAppParam();
                    createAppParam.setPassword(updatePassword.getPassword());
                    createAppParam.setCode(selectedApp.getCode());
                    createAppParam.setName(selectedApp.getName());
                    createAppParam.setRoles(selectedApp.getRoles());
                    selectedApp = appService.create(createAppParam);
                    dataProvider.refreshAll();
                } else {
                    selectedApp = appService.update(selectedApp.getId(), selectedApp);
                    if (passwordBinder.writeBeanIfValid(updatePassword)) {
                        selectedApp = appService.updatePassword(selectedApp.getId(), updatePassword);
                    }
                    dataProvider.refreshItem(selectedApp);
                    grid.deselect(selectedApp);
                }
                grid.select(selectedApp);
                UIUtils.showNotification(getTranslation("commons.changes-saved-msg"));
            } else {
                UIUtils.showNotification(getTranslation("commons.validation.error-msg"));
            }
        });
        detailsDrawerFooter.addRemoveListener(() -> ConfirmDialog
                .createQuestion()
                .withCaption(getTranslation("commons.confirm-deletion"))
                .withMessage(getTranslation("app.confirm-deletion-msg"))
                .withOkButton(() -> {
                    appService.delete(selectedApp.getId());
                    UIUtils.showNotification(getTranslation("app.deletion-successful-msg"));
                    dataProvider.refreshAll();
                    detailsDrawer.hide();
                    grid.deselectAll();
                    selectedApp = null;
                }, ButtonOption.focus(), ButtonOption.caption(getTranslation("commons.yes")))
                .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                .open());
        detailsDrawerFooter.addCancelListener(e -> showDetails(selectedApp));
        detailsDrawer.setFooter(detailsDrawerFooter);

        return detailsDrawer;
    }

    private void showDetails(ApplicationData app) {
        selectedApp = app;
        if (app.getId() == null) {
            detailsDrawerHeader.setTitle(getTranslation("app.new"));
        } else {
            detailsDrawerHeader.setTitle(app.getName());
        }
        detailsDrawerFooter.setRemoveVisible(app.getId() != null);
        FormLayout details = createDetails(app);
        detailsDrawer.setContent(details);
        detailsDrawer.show();
    }

    private FormLayout createDetails(ApplicationData app) {
        binder = new Binder<>();

        TextField code = new TextField();
        code.setWidthFull();

        TextField name = new TextField();
        name.setValueChangeMode(ValueChangeMode.ON_BLUR);
        name.addValueChangeListener(event -> {
            if (StringUtils.isBlank(code.getValue()) && StringUtils.isNotBlank(name.getValue())) {
                code.setValue(name.getValue().toLowerCase().trim().replaceAll("\\s+", "-"));
            }
        });
        name.setWidthFull();

        Grid<AppRoleData> rolesGrid = createRolesGrid(app);

        binder.forField(name).asRequired()
                .withValidator((value) -> appService.isNameAvailable(value, selectedApp.getId()),
                        getTranslation("app.validation.name-used"))
                .bind(ApplicationData::getName, ApplicationData::setName);
        binder.forField(code).asRequired()
                .withValidator((value) -> appService.isCodeAvailable(value, selectedApp.getId()),
                        getTranslation("app.validation.code-used"))
                .bind(ApplicationData::getCode, ApplicationData::setCode);

        FormLayout form = new FormLayout();
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addClassNames(LumoStyles.Padding.Bottom.L, LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        UIUtils.asRequired(form.addFormItem(name, getTranslation("app.name")));
        UIUtils.asRequired(form.addFormItem(code, getTranslation("app.code")));
        form.addFormItem(rolesGrid, getTranslation("app.roles"));

        binder.readBean(app);

        PasswordField password = new PasswordField();
        password.setWidthFull();

        updatePassword = new UpdateAppPasswordParam();
        passwordBinder = new Binder<>();
        Binder.BindingBuilder<UpdateAppPasswordParam, String> passwordBindingBuilder = passwordBinder.forField(
                password);
        if (app.getId() == null) {
            passwordBindingBuilder.asRequired();
        }
        passwordBindingBuilder.bind(UpdateAppPasswordParam::getPassword, UpdateAppPasswordParam::setPassword);
        passwordBinder.readBean(updatePassword);

        FormLayout.FormItem fiPassword = form.addFormItem(password, getTranslation("app.token"));
        if (app.getId() == null) {
            UIUtils.asRequired(fiPassword);
        }

        return form;
    }

    private Grid<AppRoleData> createRolesGrid(ApplicationData app) {
        rolesDataProvider = DataProvider.ofCollection(new ArrayList<>(app.getRoles()));
        rolesDataProvider.setSortComparator(
                (r1, r2) -> String.CASE_INSENSITIVE_ORDER.compare(r1.getName(), r2.getName()));

        Grid<AppRoleData> rolesGrid = new Grid<>();
        rolesGrid.setWidthFull();
        rolesGrid.setHeightByRows(true);
        rolesGrid.setDataProvider(rolesDataProvider);
        rolesGrid.setSelectionMode(Grid.SelectionMode.NONE);
        rolesGrid.addItemDoubleClickListener(event -> openRoleDlg(event.getItem()));

        Grid.Column<AppRoleData> nameColumn =
                rolesGrid.addColumn(AppRoleData::getName)
                        .setFlexGrow(1);

        rolesGrid.addComponentColumn(roleData -> {
            Button edit = new Button(UIUtils.createPrimaryIcon(VaadinIcon.EDIT));
            edit.addClickListener(event -> openRoleDlg(roleData));

            Button remove = new Button(UIUtils.createPrimaryIcon(VaadinIcon.CLOSE));
            remove.addClickListener(event -> {
                rolesDataProvider.getItems().remove(roleData);
                rolesDataProvider.refreshAll();
            });

            FlexBoxLayout content = new FlexBoxLayout(edit, remove);
            content.setSpacing(Right.XS);
            return content;
        }).setFlexGrow(0);

        Button newRole = UIUtils.createTertiaryButton(getTranslation("app.new-role"));
        newRole.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        newRole.setSizeUndefined();
        newRole.addClickListener(e -> openRoleDlg(new AppRoleData()));
        newRole.addClickShortcut(Key.KEY_N, KeyModifier.ALT);
        newRole.getElement().getStyle().set("margin", "0");

        HeaderRow topRow = rolesGrid.prependHeaderRow();
        topRow.getCell(nameColumn).setComponent(newRole);
        return rolesGrid;
    }

    private void openRoleDlg(AppRoleData role) {
        Binder<AppRoleData> binder = new Binder<>(AppRoleData.class);

        TextField tfName = new TextField();
        tfName.setAutofocus(true);
        tfName.setWidthFull();

        binder.forField(tfName).asRequired().bind(AppRoleData::getName, AppRoleData::setName);

        binder.readBean(role);

        FormLayout formLayout = new FormLayout();
        formLayout.addClassNames(LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));

        UIUtils.asRequired(formLayout.addFormItem(tfName, getTranslation("app.role-name")));

        CustomDialog dialog = new CustomDialog();
        dialog.setWidth("300px");
        dialog.setContent(formLayout);
        dialog.addCancelListener(e -> dialog.close());
        dialog.addOkListener(e -> {
            if (binder.writeBeanIfValid(role)) {
                if (!rolesDataProvider.getItems().contains(role)) {
                    rolesDataProvider.getItems().add(role);
                }
                rolesDataProvider.refreshAll();
                dialog.close();
            }
        });
        dialog.open();
    }
}
