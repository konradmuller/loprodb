package org.vdma.loprodb.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.security.SecurityUtils;
import org.vdma.loprodb.ui.exceptions.AccessDeniedException;
import org.vdma.loprodb.ui.views.LoginView;

@Component
public class ConfigureUIServiceInitListener implements VaadinServiceInitListener {

    @Value("${login.url:login}")
    private String loginUrl;

    @Override
    public void serviceInit(ServiceInitEvent event) {
        event.getSource().addUIInitListener(uiEvent -> {
            final UI ui = uiEvent.getUI();
            ui.addBeforeEnterListener(this::beforeEnter);
        });

        RouteConfiguration configuration = RouteConfiguration.forApplicationScope();
        configuration.setRoute(loginUrl, LoginView.class);
    }

    /**
     * Reroutes the user if (s)he is not authorized to access the view.
     *
     * @param event
     *            before navigation event with event details
     */
    private void beforeEnter(BeforeEnterEvent event) {
        if (!SecurityUtils.isAccessGranted(event.getNavigationTarget())) {
            if (SecurityUtils.isUserLoggedIn()) {
                event.rerouteToError(AccessDeniedException.class);
            } else {
                event.rerouteTo(LoginView.class);
            }
        }
    }
}
