package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import lombok.Getter;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.FontWeight;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

public class CustomDialog extends Dialog {

    private FlexBoxLayout content;

    @Getter
    private Button ok;

    @Getter
    private Button cancel;

    private Label lblCaption;

    public CustomDialog() {
        setCloseOnOutsideClick(false);
        addDialogCloseActionListener(e -> cancel.click());
        UIUtils.setTheme(UIUtils.NO_PADDING_DIALOG_THEME, this);

        ok = UIUtils.createPrimaryButton(getTranslation("commons.ok"));
        ok.addClickShortcut(Key.ENTER);
        cancel = UIUtils.createTertiaryButton(getTranslation("commons.cancel"));

        FlexBoxLayout actions = new FlexBoxLayout(cancel, ok);
        actions.setBackgroundColor(LumoStyles.Color.Contrast._5);
        actions.setPadding(Horizontal.RESPONSIVE_L, Vertical.S);
        actions.setSpacing(Right.S);
        actions.setWidthFull();
        actions.setBoxSizing(BoxSizing.BORDER_BOX);
        actions.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

        content = new FlexBoxLayout();
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);

        add(content, actions);
    }

    public void setCaption(String caption) {
        if (lblCaption == null) {
            lblCaption = UIUtils.createH5Label(caption);
            lblCaption.getElement().getStyle().set("color", LumoStyles.Color.BASE_COLOR);
            UIUtils.setPadding(lblCaption, Horizontal.RESPONSIVE_M, Vertical.S);
            UIUtils.setBackgroundColor(LumoStyles.Color.Primary._100, lblCaption);
            UIUtils.setFontWeight(FontWeight.BOLD, lblCaption);
            addComponentAsFirst(lblCaption);
        }
        lblCaption.setText(caption);
    }

    public void setContent(Component... content) {
        this.content.removeAll();
        this.content.add(content);
    }

    public void addOkListener(ComponentEventListener<ClickEvent<Button>> eventComponentEventListener) {
        ok.addClickListener(eventComponentEventListener);
    }

    public void addCancelListener(ComponentEventListener<ClickEvent<Button>> eventComponentEventListener) {
        cancel.addClickListener(eventComponentEventListener);
    }
}
