package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.util.Collection;
import java.util.Collections;
import java.util.WeakHashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.param.ReferenceListParam;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.CustomDialog;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

@PageTitle("Email black list")
@Route(value = "email-black-list", layout = MainLayout.class)
@Slf4j
public class EmailBlackList extends ViewFrame {

    private DataProvider<ReferenceData, ReferenceListParam> dataProvider;

    private final IReferenceService referenceService;
    private Grid<ReferenceData> grid;

    @Autowired
    public EmailBlackList(IReferenceService referenceService) {
        this.referenceService = referenceService;
        setId("email-black-list");
        setViewContent(buildContent());
    }

    private Component buildContent() {
        Button btnNewVal = UIUtils.createNewObjButton(getTranslation("commons.new-value"), () -> {
            grid.getEditor().cancel();
            openNewValueDlg();
        });

        Grid<ReferenceData> grid = buildGrid();

        FlexBoxLayout content = new FlexBoxLayout(btnNewVal, grid);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setHeightFull();
        content.setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        content.setAlignSelf(FlexComponent.Alignment.START, btnNewVal);
        return content;
    }

    private Grid<ReferenceData> buildGrid() {
        dataProvider = SpringDataProviderBuilder.references(referenceService)
                .withDefaultFilter(new ReferenceListParam(RefType.EMAIL_BLACK_LIST))
                .build();

        grid = new Grid<>();
        grid.setHeightByRows(true);
        grid.setDataProvider(dataProvider);
        grid.setWidth("50%");
        grid.setSelectionMode(Grid.SelectionMode.NONE);
        UIUtils.setMargin(grid, Top.RESPONSIVE_M);

        Grid.Column<ReferenceData> valueColumn = grid.addColumn(ReferenceData::getNameDe)
                .setHeader(getTranslation("commons.value")).setFlexGrow(1);

        Binder<ReferenceData> binder = new Binder<>(ReferenceData.class);
        Editor<ReferenceData> editor = grid.getEditor();
        editor.setBinder(binder);
        editor.setBuffered(true);

        TextField tfValue = new TextField();
        binder.forField(tfValue).asRequired().bind(ReferenceData::getNameDe, ReferenceData::setNameDe);
        valueColumn.setEditorComponent(tfValue);

        Collection<Button> editButtons = Collections.newSetFromMap(new WeakHashMap<>());

        Grid.Column<ReferenceData> editorColumn = grid.addComponentColumn(refData -> {
            Button edit = new Button(UIUtils.createPrimaryIcon(VaadinIcon.EDIT));
            edit.addClickListener(e -> {
                editor.editItem(refData);
                tfValue.focus();
            });
            edit.setEnabled(!editor.isOpen());
            editButtons.add(edit);

            Button remove = new Button(UIUtils.createPrimaryIcon(VaadinIcon.CLOSE));
            remove.addClickListener(event -> {
                referenceService.delete(refData.getId());
                dataProvider.refreshAll();
            });
            editButtons.add(remove);

            FlexBoxLayout content = new FlexBoxLayout(edit, remove);
            content.setSpacing(Right.XS);
            return content;
        }).setFlexGrow(0);

        editor.addOpenListener(e -> editButtons.forEach(button -> button.setEnabled(!editor.isOpen())));
        editor.addCloseListener(e -> editButtons.forEach(button -> button.setEnabled(!editor.isOpen())));

        Button save = new Button(UIUtils.createPrimaryIcon(VaadinIcon.CHECK), e -> editor.save());
        Button cancel = new Button(UIUtils.createPrimaryIcon(VaadinIcon.CLOSE), e -> editor.cancel());

        // Add a keypress listener that listens for an escape key up event.
        // Note! some browsers return key as Escape and some as Esc
        grid.getElement().addEventListener("keyup", event -> editor.cancel())
                .setFilter("event.key === 'Escape' || event.key === 'Esc'");

        FlexBoxLayout buttons = new FlexBoxLayout(save, cancel);
        buttons.setSpacing(Right.XS);
        editorColumn.setEditorComponent(buttons);

        editor.addSaveListener(event -> referenceService.update(event.getItem().getId(), event.getItem()));

        return grid;
    }

    private void openNewValueDlg() {
        ReferenceData newVal = new ReferenceData();
        newVal.setRefType(RefType.EMAIL_BLACK_LIST);

        Binder<ReferenceData> binder = new Binder<>(ReferenceData.class);

        TextField tfVal = new TextField();
        tfVal.setAutofocus(true);
        tfVal.setWidthFull();

        binder.forField(tfVal).asRequired().bind(ReferenceData::getNameDe, ReferenceData::setNameDe);

        binder.readBean(newVal);

        FormLayout formLayout = new FormLayout();
        formLayout.addClassNames(LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));

        UIUtils.asRequired(formLayout.addFormItem(tfVal, getTranslation("commons.value")));

        CustomDialog dialog = new CustomDialog();
        dialog.setWidth("300px");
        dialog.setContent(formLayout);
        dialog.addCancelListener(e -> dialog.close());
        dialog.addOkListener(e -> {
            if (binder.writeBeanIfValid(newVal)) {
                referenceService.create(newVal);
                dataProvider.refreshAll();
                dialog.close();
            }
        });
        dialog.open();
    }
}
