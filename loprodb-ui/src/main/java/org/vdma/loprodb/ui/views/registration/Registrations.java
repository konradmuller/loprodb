package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyPressEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.entity.AdActivationStatus;
import static org.vdma.loprodb.entity.AdActivationStatus.FAILED;
import static org.vdma.loprodb.entity.AdActivationStatus.OK;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.RegistrationListParam;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.ListItem;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawer;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerHeader;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.util.css.lumo.BadgeColor;
import static org.vdma.loprodb.ui.util.css.lumo.BadgeColor.ERROR;
import static org.vdma.loprodb.ui.util.css.lumo.BadgeColor.SUCCESS;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.SplitViewFrame;
import org.vdma.loprodb.ui.views.registration.components.RegistrationStatusBadge;
import org.vdma.loprodb.ui.views.registration.components.RegistrationStatusField;

@PageTitle("Registrations")
@Route(value = "registrations", layout = MainLayout.class)
public class Registrations extends SplitViewFrame implements EqualCmp {

    private Grid<RegistrationData> grid;
    private ConfigurableFilterDataProvider<RegistrationData, Void, RegistrationListParam> dataProvider;
    private RegistrationListParam filter = new RegistrationListParam();

    private DetailsDrawer detailsDrawer;
    private DetailsDrawerHeader detailsDrawerHeader;

    private final IRegistrationService registrationService;

    @Autowired
    public Registrations(IRegistrationService registrationService) {
        this.registrationService = registrationService;
        buildContent();
    }

    private void buildContent() {
        FlexBoxLayout content = new FlexBoxLayout(buildFilter(), buildGrid());
        content.setAlignItems(FlexComponent.Alignment.START);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setHeightFull();
        content.setPadding(Horizontal.RESPONSIVE_X, Vertical.RESPONSIVE_M);

        setViewContent(content);
        setViewDetails(createDetailsDrawer());
    }

    private Component buildFilter() {
        Button btnSearch = UIUtils.createPrimaryButton(getTranslation("registration.list.filter.search"));
        ComponentEventListener<KeyPressEvent> enterListener = e -> btnSearch.click();

        TextField tfEmail = new TextField();
        tfEmail.addKeyPressListener(Key.ENTER, enterListener);
        tfEmail.setClearButtonVisible(true);
        tfEmail.setWidthFull();

        TextField tfPhone = new TextField();
        tfPhone.addKeyPressListener(Key.ENTER, enterListener);
        tfPhone.setClearButtonVisible(true);
        tfPhone.setWidthFull();

        TextField tfFirstName = new TextField();
        tfFirstName.addKeyPressListener(Key.ENTER, enterListener);
        tfFirstName.setClearButtonVisible(true);
        tfFirstName.setWidthFull();

        TextField tfLastName = new TextField();
        tfLastName.addKeyPressListener(Key.ENTER, enterListener);
        tfLastName.setClearButtonVisible(true);
        tfLastName.setWidthFull();

        TextField tfCompany = new TextField();
        tfCompany.addKeyPressListener(Key.ENTER, enterListener);
        tfCompany.setClearButtonVisible(true);
        tfCompany.setWidthFull();

        TextField tfCity = new TextField();
        tfCity.addKeyPressListener(Key.ENTER, enterListener);
        tfCity.setClearButtonVisible(true);
        tfCity.setWidthFull();

        FlexBoxLayout fblStatus = new FlexBoxLayout();
        fblStatus.setBoxSizing(BoxSizing.BORDER_BOX);
        fblStatus.setFlexWrap(FlexLayout.FlexWrap.WRAP);

        for (RegistrationStatus s : RegistrationStatus.values()) {
            Checkbox chbStatus = new Checkbox(
                    getTranslation(RegistrationStatus.class.getSimpleName() + "." + s.name()));
            if (s == RegistrationStatus.MANUAL_REVIEW_PENDING) {
                chbStatus.setValue(true);
            }
            chbStatus.addValueChangeListener(event -> {
                if (event.getValue()) {
                    filter.getStatuses().add(s);
                } else {
                    filter.getStatuses().remove(s);
                }
            });
            fblStatus.add(chbStatus);
            fblStatus.setFlex("0 0 50%", chbStatus);
        }

        btnSearch.addClickListener(event -> {
            filter.setEmail(tfEmail.getValue());
            filter.setPhone(tfPhone.getValue());
            filter.setFirstName(tfFirstName.getValue());
            filter.setLastName(tfLastName.getValue());
            filter.setCompanyName(tfCompany.getValue());
            filter.setCompanyCity(tfCity.getValue());
            dataProvider.setFilter(filter);
        });
        FlexBoxLayout fblSearchBtnWrapper = new FlexBoxLayout(btnSearch);
        fblSearchBtnWrapper.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        fblSearchBtnWrapper.setAlignItems(FlexComponent.Alignment.END);

        FormLayout formLeft = new FormLayout();
        formLeft.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("400px", 2, FormLayout.ResponsiveStep.LabelsPosition.TOP));
        formLeft.addFormItem(tfFirstName, getTranslation("registration.list.filter.first-name"));
        formLeft.addFormItem(tfLastName, getTranslation("registration.list.filter.last-name"));
        formLeft.addFormItem(tfEmail, getTranslation("registration.list.filter.email"));
        formLeft.addFormItem(tfPhone, getTranslation("registration.list.filter.phone"));
        formLeft.addFormItem(tfCompany, getTranslation("registration.list.filter.company"));
        formLeft.addFormItem(tfCity, getTranslation("registration.list.filter.city"));
        formLeft.getChildren().forEach(c -> UIUtils.setMargin(c, Vertical.ZERO));

        FormLayout formRight = new FormLayout();
        formRight.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP));
        formRight.addFormItem(fblStatus, getTranslation("registration.list.filter.status"));
        formRight.add(fblSearchBtnWrapper);
        formRight.getChildren().forEach(c -> UIUtils.setMargin(c, Vertical.ZERO));

        FlexBoxLayout result = new FlexBoxLayout(formLeft, formRight);
        result.setSpacing(Right.L);
        result.setBoxSizing(BoxSizing.BORDER_BOX);
        result.setPadding(Horizontal.M, Vertical.S);
        result.setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        result.setFlex("4", formLeft);
        result.setFlex("3", formRight);
        UIUtils.setBorder(result);

        return result;
    }

    private Grid buildGrid() {
        filter.getStatuses().add(RegistrationStatus.MANUAL_REVIEW_PENDING);

        dataProvider = SpringDataProviderBuilder.registrations(registrationService).build().withConfigurableFilter();
        dataProvider.setFilter(filter);

        grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addItemDoubleClickListener(event -> openRegistrationDetailsTab(event.getItem()));
        grid.addSelectionListener(event -> event.getFirstSelectedItem().ifPresent(this::showDetails));
        UIUtils.setMargin(grid, Top.RESPONSIVE_M);
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();
        grid.addColumn(new ComponentRenderer<>(this::createItemStatus))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader(getTranslation("registration.list.status"));
        grid.addColumn(new ComponentRenderer<>(this::createItemName))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setSortProperty("lastName", "firstName")
                .setHeader(getTranslation("registration.list.name"));
        grid.addColumn(new ComponentRenderer<>(this::createItemCompany))
                .setAutoWidth(false)
                .setFlexGrow(1)
                .setFrozen(true)
                .setSortProperty("company.name")
                .setHeader(getTranslation("registration.list.company"));
        grid.addColumn(new LocalDateTimeRenderer<>(RegistrationData::getCreatedDate,
                DateTimeFormatter.ofPattern(UIUtils.DATE_TIME_FORMAT_PATTERN)))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setSortProperty("createdDate")
                .setHeader(getTranslation("registration.list.registration-date"));
        grid.addColumn(new ComponentRenderer<>(this::createItemActivationStatus))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader(getTranslation("registration.list.activation"));
        grid.addColumn(new ComponentRenderer<>(this::createItemEditor))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader(getTranslation("registration.list.editor"));
        grid.addColumn(new ComponentRenderer<>(this::createItemEdit))
                .setAutoWidth(true)
                .setFlexGrow(0);
        return grid;
    }

    private Component createItemEdit(RegistrationData registrationData) {
        Button edit = UIUtils.createOutlinedButton(getTranslation("commons.edit"));
        edit.addClickListener(event -> openRegistrationDetailsTab(registrationData));
        return edit;
    }

    private void openRegistrationDetailsTab(RegistrationData registrationData) {
        if (grid.getSelectionModel().isSelected(registrationData)) {
            detailsDrawer.hide();
        }
        MainLayout.get().navigate(RegistrationDetails.getTabLabel(registrationData), RegistrationDetails.class,
                RegistrationDetails.getNaviParam(registrationData));
    }

    private Component createItemCompany(RegistrationData registrationData) {
        ListItem item = new ListItem(registrationData.getCompany().getName(), registrationData.getCompany().getCity());
        item.setPadding(Vertical.XS);
        return item;
    }

    private Component createItemName(RegistrationData registrationData) {
        ListItem item = new ListItem(registrationData.getFullName(), registrationData.getEmail());
        item.setPadding(Vertical.XS);
        return item;
    }

    private Component createItemStatus(RegistrationData registrationData) {
        Badge badge = RegistrationStatusBadge.createForStatus(registrationData.getStatus());
        return badge != null ? badge : new Span();
    }

    private Component createItemActivationStatus(RegistrationData registrationData) {
        if (registrationData.getUser() == null) {
            return new Span();
        }
        AdActivationStatus activationStatus = registrationData.getUser().getRecentUserAd()
                .map(ad -> OK).orElse(FAILED);
        BadgeColor badgeColor = activationStatus == OK ? SUCCESS : ERROR;
        return new Badge(
                getTranslation(AdActivationStatus.class.getSimpleName() + "." + activationStatus.name()),
                badgeColor);
    }

    private Component createItemEditor(RegistrationData registrationData) {
        if (registrationData.getEditor() == null) {
            return new Span();
        }
        ListItem item = new ListItem(registrationData.getEditor().getFullName());
        item.setPadding(Vertical.XS);
        return item;
    }

    private DetailsDrawer createDetailsDrawer() {
        detailsDrawer = new DetailsDrawer(DetailsDrawer.Position.RIGHT);

        // Header
        detailsDrawerHeader = new DetailsDrawerHeader("");
        detailsDrawerHeader.addCloseListener(buttonClickEvent -> {
            detailsDrawer.hide();
            grid.deselectAll();
        });
        detailsDrawer.setHeader(detailsDrawerHeader);
        return detailsDrawer;
    }

    private void showDetails(RegistrationData registration) {
        detailsDrawerHeader.setTitle(registration.getFullName());
        FormLayout details = createDetails(registration);
        detailsDrawer.setContent(details);
        detailsDrawer.show();
    }

    private FormLayout createDetails(RegistrationData registration) {
        Binder<RegistrationData> binder = new Binder<>();

        RegistrationStatusField regStatus = new RegistrationStatusField();
        binder.forField(regStatus).bind(RegistrationData::getStatus, RegistrationData::setStatus);

        ComboBox<Gender> cbSalutation = new ComboBox<>(null, Gender.values());
        cbSalutation.setWidthFull();
        cbSalutation.setItemLabelGenerator(
                gender -> getTranslation(Gender.class.getSimpleName() + "." + gender.name()));
        binder.forField(cbSalutation).bind(RegistrationData::getGender, null);

        TextField tfTitle = new TextField();
        tfTitle.setWidthFull();
        binder.forField(tfTitle).bind(RegistrationData::getTitle, null);

        TextField tfFirstName = new TextField();
        tfFirstName.setWidthFull();
        binder.forField(tfFirstName).bind(RegistrationData::getFirstName, null);

        TextField tfLastName = new TextField();
        tfLastName.setWidthFull();
        binder.forField(tfLastName).bind(RegistrationData::getLastName, null);

        TextField tfEmail = new TextField();
        tfEmail.setWidthFull();
        binder.forField(tfEmail).bind(RegistrationData::getEmail, null);

        TextField tfPhone = new TextField();
        tfPhone.setWidthFull();
        binder.forField(tfPhone).bind(RegistrationData::getPhone, null);

        TextField tfMobile = new TextField();
        tfMobile.setWidthFull();
        binder.forField(tfMobile).bind(RegistrationData::getMobile, null);

        TextField tfFunction = new TextField();
        tfFunction.setWidthFull();
        binder.forField(tfFunction).bind(RegistrationData::getFunction, null);

        TextField tfPosition = new TextField();
        binder.forField(tfPosition).bind(reg -> reg.getPosition().getNameByLocale(getLocale()), null);

        TextField tfArea = new TextField();
        binder.forField(tfArea).bind(reg -> reg.getBusinessArea().getNameByLocale(getLocale()), null);

        ComboBox<Language> cbLang = new ComboBox<>(null, Language.values());
        cbLang.setWidthFull();
        cbLang.setItemLabelGenerator(lang -> getTranslation(Language.class.getSimpleName() + "." + lang.name()));
        binder.forField(cbLang).bind(RegistrationData::getLanguage, null);

        FormLayout form = new FormLayout();
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addClassNames(LumoStyles.Padding.Bottom.L, LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        form.addFormItem(regStatus, getTranslation("registration.details.status"));
        form.addFormItem(cbSalutation, getTranslation("registration.details.salutation"));
        form.addFormItem(tfTitle, getTranslation("registration.details.title"));
        form.addFormItem(tfFirstName, getTranslation("registration.details.first-name"));
        form.addFormItem(tfLastName, getTranslation("registration.details.last-name"));
        form.addFormItem(tfEmail, getTranslation("registration.details.email"));
        form.addFormItem(tfPhone, getTranslation("registration.details.phone"));
        form.addFormItem(tfMobile, getTranslation("registration.details.mobile"));
        form.addFormItem(tfFunction, getTranslation("registration.details.function"));
        form.addFormItem(tfPosition, getTranslation("registration.details.position"));
        form.addFormItem(tfArea, getTranslation("registration.details.business-area"));
        form.addFormItem(cbLang, getTranslation("commons.lang"));

        binder.readBean(registration);

        return form;
    }
}
