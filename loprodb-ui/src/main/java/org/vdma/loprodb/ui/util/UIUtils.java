package org.vdma.loprodb.ui.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Pre;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.VaadinSession;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Currency;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Size;
import org.vdma.loprodb.ui.util.css.AlignSelf;
import org.vdma.loprodb.ui.util.css.BorderRadius;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.util.css.Overflow;
import org.vdma.loprodb.ui.util.css.PointerEvents;
import org.vdma.loprodb.ui.util.css.Shadow;
import org.vdma.loprodb.ui.util.css.TextAlign;
import org.vdma.loprodb.ui.util.css.TextOverflow;
import org.vdma.loprodb.ui.util.css.WhiteSpace;

public class UIUtils {

    private UIUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static final String IMG_PATH = "images/";

    public static final String NO_IMG = IMG_PATH + "no_image.png";

    public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    public static final String DATE_TIME_FORMAT_PATTERN = "dd.MM.yyyy HH:mm:ss";
    public static final String DATE_FORMAT_WITH_MONTH_PATTERN = "MMM dd, YYYY";

    public static final String DECIMAL_FORMAT_PATTERN = "###,##0.00";

    public static final String NO_PADDING_DIALOG_THEME = "no-padding-dialog-overlay";
    private static final int NO_AUTO_CLOSE_NOTIFICATION_DURATION = 0;
    private static final int NOTIFICATION_DURATION = 3000;

    private static Map<String, String> countries;
    private static Map<String, String> currencies;

    private static final ThreadLocal<SimpleDateFormat> simpleDateTimeFormat =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_TIME_FORMAT_PATTERN));

    private static final ThreadLocal<DateTimeFormatter> dateFormat = ThreadLocal
            .withInitial(() -> DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN));

    private static final ThreadLocal<DateTimeFormatter> dateTimeFormat = ThreadLocal
            .withInitial(() -> DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN));

    /**
     * formatters.
     */
    public static DateTimeFormatter applyLocale(DateTimeFormatter formatter) {
        return formatter.withLocale(getSessionLocale());
    }

    public static DecimalFormat applyLocale(String pattern) {
        return new DecimalFormat(pattern, DecimalFormatSymbols.getInstance(getSessionLocale()));
    }

    public static DecimalFormat decimalFormat() {
        return applyLocale(DECIMAL_FORMAT_PATTERN);
    }

    public static DateTimeFormatter dateFormat() {
        return applyLocale(DateTimeFormatter.ofPattern(DATE_FORMAT_WITH_MONTH_PATTERN));
    }

    public static DateTimeFormatter dateShortFormat() {
        return applyLocale(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
    }

    public static DateTimeFormatter dateTimeShortFormat() {
        return applyLocale(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.MEDIUM));
    }

    public static void setFavIcon(InitialPageSettings settings) {
        settings.addFavIcon("icon", "icons/favicon.ico", "16x16");
    }

    /* ==== LOCALES ==== */

    public static Locale getSessionLocale() {
        return VaadinSession.getCurrent().getLocale();
    }

    public static Locale getApplicationDefaultLocale() {
        return Locale.GERMANY;
    }

    /* ==== BUTTONS ==== */

    // Styles

    public static Button createPrimaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createPrimaryButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createPrimaryButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createTertiaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_TERTIARY);
    }

    public static Button createTertiaryButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_TERTIARY);
    }

    public static Button createTertiaryButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_TERTIARY);
    }

    public static Button createTertiaryInlineButton(String text) {
        return createButton(text, ButtonVariant.LUMO_TERTIARY_INLINE);
    }

    public static Button createTertiaryInlineButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_TERTIARY_INLINE);
    }

    public static Button createTertiaryInlineButton(String text,
            VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_TERTIARY_INLINE);
    }

    public static Button createSuccessButton(String text) {
        return createButton(text, ButtonVariant.LUMO_SUCCESS);
    }

    public static Button createSuccessButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_SUCCESS);
    }

    public static Button createSuccessButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_SUCCESS);
    }

    public static Button createSuccessPrimaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_SUCCESS,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createSuccessPrimaryButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_SUCCESS,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createSuccessPrimaryButton(String text,
            VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_SUCCESS,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createErrorButton(String text) {
        return createButton(text, ButtonVariant.LUMO_ERROR);
    }

    public static Button createErrorTertiaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY);
    }

    public static Button createErrorButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_ERROR);
    }

    public static Button createErrorButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_ERROR);
    }

    public static Button createErrorPrimaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createErrorPrimaryButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createErrorPrimaryButton(String text,
            VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createContrastButton(String text) {
        return createButton(text, ButtonVariant.LUMO_CONTRAST);
    }

    public static Button createContrastButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_CONTRAST);
    }

    public static Button createContrastButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_CONTRAST);
    }

    public static Button createContrastPrimaryButton(String text) {
        return createButton(text, ButtonVariant.LUMO_CONTRAST,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createContrastPrimaryButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_CONTRAST,
                ButtonVariant.LUMO_PRIMARY);
    }

    public static Button createContrastPrimaryButton(String text,
            VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_CONTRAST,
                ButtonVariant.LUMO_PRIMARY);
    }

    // Size

    public static Button createSmallButton(String text) {
        return createButton(text, ButtonVariant.LUMO_SMALL);
    }

    public static Button createSmallButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_SMALL);
    }

    public static Button createSmallButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_SMALL);
    }

    public static Button createLargeButton(String text) {
        return createButton(text, ButtonVariant.LUMO_LARGE);
    }

    public static Button createLargeButton(VaadinIcon icon) {
        return createButton(icon, ButtonVariant.LUMO_LARGE);
    }

    public static Button createLargeButton(String text, VaadinIcon icon) {
        return createButton(text, icon, ButtonVariant.LUMO_LARGE);
    }

    public static Button createOutlinedButton(String text) {
        Button button = createButton(text);
        setTheme("outlined", button);
        return button;
    }

    // Text

    public static Button createButton(String text, ButtonVariant... variants) {
        Button button = new Button(text);
        button.addThemeVariants(variants);
        button.getElement().setAttribute("aria-label", text);
        return button;
    }

    // Icon

    public static Button createButton(VaadinIcon icon,
            ButtonVariant... variants) {
        Button button = new Button(new Icon(icon));
        button.addThemeVariants(variants);
        return button;
    }

    // Text and icon

    public static Button createButton(String text, VaadinIcon icon,
            ButtonVariant... variants) {
        Icon i = new Icon(icon);
        i.getElement().setAttribute("slot", "prefix");
        Button button = new Button(text, i);
        button.addThemeVariants(variants);
        return button;
    }

    public static Button createNewObjButton(String text, Runnable onClick) {
        Button newObj = createPrimaryButton(text);
        newObj.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        newObj.setSizeUndefined();
        newObj.addClickListener(e -> onClick.run());
        newObj.addClickShortcut(Key.KEY_N, KeyModifier.ALT);
        newObj.getElement().getStyle().set("margin", "0");
        return newObj;
    }

    /* ==== TEXTFIELDS ==== */

    public static TextField createSmallTextField() {
        TextField textField = new TextField();
        textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        return textField;
    }

    public static TextField createSearchTextField(Consumer<String> onSearch) {
        TextField search = new TextField();
        search.setPlaceholder(search.getTranslation("commons.search"));
        search.addValueChangeListener(event -> onSearch.accept(event.getValue()));
        search.setValueChangeMode(ValueChangeMode.LAZY);
        search.setValueChangeTimeout(300);
        Icon icon = VaadinIcon.SEARCH.create();
        search.setPrefixComponent(icon);
        search.getElement().getStyle().set("padding", "0");
        UIUtils.setTheme("search", search);
        return search;
    }

    public static FlexBoxLayout createSearchAndNewObjHeader(String btnText, Runnable onNew, Consumer<String> onSearch) {
        TextField search = createSearchTextField(onSearch);
        search.getElement().getStyle().set("margin-right", Right.RESPONSIVE_M.getVariable());

        Button newObj = createNewObjButton(btnText, onNew);

        FlexBoxLayout header = new FlexBoxLayout(search, newObj);
        header.setFlexGrow(1, search);
        header.setFlexWrap(FlexLayout.FlexWrap.WRAP);
        header.setMargin(Bottom.RESPONSIVE_M);
        return header;
    }

    /* ==== LABELS ==== */

    public static Label createLabel(FontSize size, TextColor color,
            String text) {
        Label label = new Label(text);
        setFontSize(size, label);
        setTextColor(color, label);
        return label;
    }

    public static Label createLabel(FontSize size, String text) {
        return createLabel(size, TextColor.BODY, text);
    }

    public static Label createLabel(TextColor color, String text) {
        return createLabel(FontSize.M, color, text);
    }

    public static Label createH1Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H1);
        return label;
    }

    public static Label createH2Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H2);
        return label;
    }

    public static Label createH3Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H3);
        return label;
    }

    public static Label createH4Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H4);
        return label;
    }

    public static Label createH5Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H5);
        return label;
    }

    public static Label createH6Label(String text) {
        Label label = new Label(text);
        label.addClassName(LumoStyles.Heading.H6);
        return label;
    }

    public static Pre createLabelPreformatted(FontSize size, TextColor color,
            String text) {
        Pre pre = new Pre(text);
        setFontSize(size, pre);
        setTextColor(color, pre);
        setBackgroundColor("transparent", pre);
        pre.getElement().getStyle().set("font-family", "var(--lumo-font-family)");
        pre.getElement().getStyle().set("margin", "0");

        return pre;
    }

    public static Select<String> createCurrencyComboBox() {
        Select<String> currency = new Select<>();
        currency.setWidthFull();
        currency.setItems(getCurrencies().keySet());
        currency.setItemLabelGenerator(code -> getCurrencies().get(code));
        return currency;
    }

    public static Select<String> createCountryComboBox() {
        Select<String> country = new Select<>();
        country.setWidthFull();
        country.setItems(getCountries().keySet());
        country.setItemLabelGenerator(code -> getCountries().get(code));
        return country;
    }

    /* === MISC === */

    public static Button createFloatingActionButton(VaadinIcon icon) {
        Button button = createPrimaryButton(icon);
        button.addThemeName("fab");
        return button;
    }

    /* === NUMBERS === */

    public static String formatAmount(Double amount) {
        return decimalFormat().format(amount);
    }

    public static String formatAmount(BigDecimal amount) {
        return decimalFormat().format(amount);
    }

    public static String formatAmount(int amount) {
        return decimalFormat().format(amount);
    }

    public static Label createAmountLabel(double amount) {
        Label label = createH5Label(formatAmount(amount));
        label.addClassName(LumoStyles.FontFamily.MONOSPACE);
        return label;
    }

    public static Label createAmountLabel(BigDecimal amount) {
        Label label = createH5Label(amount != null ? formatAmount(amount) : "");
        label.addClassName(LumoStyles.FontFamily.MONOSPACE);
        return label;
    }

    public static String formatUnits(int units) {
        return NumberFormat.getIntegerInstance().format(units);
    }

    public static Label createUnitsLabel(int units) {
        Label label = new Label(formatUnits(units));
        label.addClassName(LumoStyles.FontFamily.MONOSPACE);
        return label;
    }

    /* === ICONS === */

    public static Icon createPrimaryIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.PRIMARY, i);
        return i;
    }

    public static Icon createSecondaryIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.SECONDARY, i);
        return i;
    }

    public static Icon createTertiaryIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.TERTIARY, i);
        return i;
    }

    public static Icon createDisabledIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.DISABLED, i);
        return i;
    }

    public static Icon createSuccessIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.SUCCESS, i);
        return i;
    }

    public static Icon createErrorIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        setTextColor(TextColor.ERROR, i);
        return i;
    }

    public static Icon createSmallIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        i.addClassName(IconSize.S.getClassName());
        return i;
    }

    public static Icon createLargeIcon(VaadinIcon icon) {
        Icon i = new Icon(icon);
        i.addClassName(IconSize.L.getClassName());
        return i;
    }

    // Combinations

    public static Icon createIcon(IconSize size, TextColor color,
            VaadinIcon icon) {
        Icon i = new Icon(icon);
        i.addClassNames(size.getClassName());
        setTextColor(color, i);
        return i;
    }

    /* === DATES === */

    public static String formatDate(LocalDate date) {
        return date != null ? dateFormat.get().format(date) : "";
    }

    public static String formatDate(LocalDateTime dateTime) {
        return dateTime != null ? dateTimeFormat.get().format(dateTime) : "";
    }

    public static String formatDate(Date date) {
        return date != null ? simpleDateTimeFormat.get().format(date) : "";
    }

    /* === NOTIFICATIONS === */

    public static void showNotification(String text) {
        Notification.show(text, NOTIFICATION_DURATION, Notification.Position.BOTTOM_CENTER);
    }

    public static void showErrorNotification(@NotNull String message, @Nullable String stacktrace) {
        Icon closeIcon = createIcon(IconSize.S, TextColor.PRIMARY, VaadinIcon.CLOSE);
        setMinWidth("15px", closeIcon);
        setWidth("15px", closeIcon);

        HorizontalLayout firstRow = new HorizontalLayout();
        firstRow.addAndExpand(new Span(message));
        firstRow.add(closeIcon);
        VerticalLayout content = new VerticalLayout(firstRow);
        content.setPadding(false);

        if (StringUtils.isNotBlank(stacktrace)) {
            TextArea taStacktrace = new TextArea();
            taStacktrace.setWidthFull();
            taStacktrace.setValue(stacktrace);
            taStacktrace.setReadOnly(true);
            taStacktrace.setHeight("400px");
            Details details = new Details(taStacktrace.getTranslation("commons.details"), taStacktrace);
            details.addThemeVariants(DetailsVariant.FILLED);
            setWidth("100%", details);
            content.add(details);
        }

        Notification notification = new Notification(content);
        notification.setDuration(NO_AUTO_CLOSE_NOTIFICATION_DURATION);
        notification.setPosition(Notification.Position.BOTTOM_CENTER);
        closeIcon.addClickListener(event -> notification.close());
        notification.open();
    }

    /* === CSS UTILITIES === */

    public static void setAlignSelf(AlignSelf alignSelf,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("align-self",
                    alignSelf.getValue());
        }
    }

    public static void setBackgroundColor(String backgroundColor,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("background-color",
                    backgroundColor);
        }
    }

    public static void removeBackgroundColor(Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().remove("background-color");
        }
    }

    public static void setBorderRadius(BorderRadius borderRadius,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("border-radius",
                    borderRadius.getValue());
        }
    }

    public static void setBorder(Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("border", "1px solid var(--lumo-contrast-20pct)");
        }
    }

    public static void setBoxSizing(BoxSizing boxSizing,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("box-sizing",
                    boxSizing.getValue());
        }
    }

    public static void setColSpan(int span, Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("colspan",
                    Integer.toString(span));
        }
    }

    public static void setFontSize(FontSize fontSize,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("font-size",
                    fontSize.getValue());
        }
    }

    public static void setFontWeight(FontWeight fontWeight,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("font-weight",
                    fontWeight.getValue());
        }
    }

    public static void setLineHeight(LineHeight lineHeight,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("line-height",
                    lineHeight.getValue());
        }
    }

    public static void setLineHeight(String value,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("line-height",
                    value);
        }
    }

    public static void setMaxWidth(String value, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("max-width", value);
        }
    }

    public static void setMinWidth(String value, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("min-width", value);
        }
    }

    public static void setOverflow(Overflow overflow, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("overflow",
                    overflow.getValue());
        }
    }

    public static void setPointerEvents(PointerEvents pointerEvents, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("pointer-events",
                    pointerEvents.getValue());
        }
    }

    public static void setShadow(Shadow shadow, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("box-shadow",
                    shadow.getValue());
        }
    }

    public static void setTextAlign(TextAlign textAlign,
            Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("text-align",
                    textAlign.getValue());
        }
    }

    public static void setTextColor(TextColor textColor, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("color", textColor.getValue());
        }
    }

    public static void setTextColor(String textColor, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("color", textColor);
        }
    }

    public static void setTextOverflow(TextOverflow textOverflow, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("text-overflow", textOverflow.getValue());
        }
    }

    public static void setTheme(String theme, Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("theme", theme);
        }
    }

    public static void setTooltip(String tooltip, Component... components) {
        for (Component component : components) {
            component.getElement().setProperty("title", tooltip);
        }
    }

    public static void setWhiteSpace(WhiteSpace whiteSpace,
            Component... components) {
        for (Component component : components) {
            component.getElement().setProperty("white-space",
                    whiteSpace.getValue());
        }
    }

    public static void setWidth(String value, Component... components) {
        for (Component component : components) {
            component.getElement().getStyle().set("width", value);
        }
    }

    public static void asRequired(Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("required", "true");
        }
    }

    public static void setPadding(Component component, Size... sizes) {
        removePadding(component);
        for (Size size : sizes) {
            for (String attribute : size.getPaddingAttributes()) {
                component.getElement().getStyle().set(attribute, size.getVariable());
            }
        }
    }

    public static void removePadding(Component component) {
        Style style = component.getElement().getStyle();
        style.remove("padding");
        style.remove("padding-bottom");
        style.remove("padding-left");
        style.remove("padding-right");
        style.remove("padding-top");
    }

    public static void setMargin(Component component, Size... sizes) {
        removeMargin(component);
        for (Size size : sizes) {
            for (String attribute : size.getMarginAttributes()) {
                component.getElement().getStyle().set(attribute, size.getVariable());
            }
        }
    }

    public static void removeMargin(Component component) {
        Style style = component.getElement().getStyle();
        style.remove("margin");
        style.remove("margin-bottom");
        style.remove("margin-left");
        style.remove("margin-right");
        style.remove("margin-top");
    }

    /* === ACCESSIBILITY === */

    public static void setAriaLabel(String value, Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("aria-label", value);
        }
    }

    public static String getDisplayCountry(String isoCode) {
        return getCountries().get(isoCode);
    }

    public static String getDisplayCurrency(String code) {
        return getCurrencies().get(code);
    }

    private static Map<String, String> getCountries() {
        if (countries == null) {
            initCountries();
        }
        return countries;
    }

    private static Map<String, String> getCurrencies() {
        if (currencies == null) {
            initCurrencies();
        }
        return currencies;
    }

    private static void initCountries() {
        countries = Stream.of(Locale.getISOCountries())
                .collect(Collectors.toMap(Function.identity(), code -> new Locale("", code).getDisplayCountry()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    private static void initCurrencies() {
        currencies = Currency.getAvailableCurrencies().stream()
                .collect(Collectors.toMap(Currency::getCurrencyCode, Currency::getDisplayName))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    /* ==== MENUBAR ==== */

    public static MenuBar createMenuBar(boolean openOnHover) {
        MenuBar menuBar = new MenuBar();
        menuBar.setOpenOnHover(openOnHover);
        return menuBar;
    }
}
