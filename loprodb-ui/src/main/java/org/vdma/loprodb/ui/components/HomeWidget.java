package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.FontSize;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.TextColor;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

@CssImport("./styles/components/home_widget.css")
public class HomeWidget extends Anchor {

    public static final String CLASS_NAME = "home-widget";
    private static final String CLASS_NAME_TWO = "two";
    private static final String CLASS_NAME_THREE = "three";

    public HomeWidget(String href, String titleText, String descriptionText, String buttonText, String imageName) {
        this.setHref(href);
        this.add(createContent(titleText, descriptionText, buttonText, imageName));
    }

    public HomeWidget(String href, String titleText, String descriptionText, String buttonText) {
        this.setHref(href);
        this.add(createContent(titleText, descriptionText, buttonText));
    }

    private Component createContent(String titleText, String descriptionText, String buttonText) {
        FlexBoxLayout content = new FlexBoxLayout(getTitle(titleText), getDescription(descriptionText),
                getSpace(), getButton(buttonText));
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setPadding(Horizontal.M, Vertical.M);
        content.setHeightFull();

        return content;
    }

    private Component createContent(String titleText, String descriptionText, String buttonText, String imageName) {
        FlexBoxLayout layout = new FlexBoxLayout(getTitle(titleText), getDescription(descriptionText),
                getSpace(), getButton(buttonText));
        layout.setBoxSizing(BoxSizing.BORDER_BOX);
        layout.setFlexDirection(FlexLayout.FlexDirection.COLUMN);

        FlexBoxLayout content = new FlexBoxLayout(layout, getSpace(), getImage(imageName));
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setPadding(Left.M, Right.XL, Vertical.M);
        content.setSizeFull();

        return content;
    }

    private Component getTitle(String titleText) {
        return UIUtils.createH4Label(titleText);
    }

    private Component getDescription(String descriptionText) {
        Label description = UIUtils.createLabel(FontSize.S, TextColor.SECONDARY, descriptionText);
        description.addClassNames(LumoStyles.Margin.Top.S);
        return description;
    }

    private Component getButton(String buttonText) {
        Button button = UIUtils.createContrastButton(buttonText, VaadinIcon.CHEVRON_RIGHT_SMALL);
        button.setIconAfterText(true);
        button.addClassName(CLASS_NAME + "__button");

        FlexBoxLayout buttonLayout = new FlexBoxLayout(button, getSpace());
        buttonLayout.setWidthFull();
        buttonLayout.setBoxSizing(BoxSizing.BORDER_BOX);
        return buttonLayout;
    }

    private Component getSpace() {
        Div space = new Div();
        space.getStyle().set("flex", "auto");
        return space;
    }

    private Component getImage(String imageName) {
        Image image = new Image();
        image.setSrc(UIUtils.IMG_PATH + imageName);
        return image;
    }

    public void colspan(int colspan) {
        switch (colspan) {
        case 2:
            addClassName(CLASS_NAME_TWO);
            break;
        case 3:
            addClassName(CLASS_NAME_THREE);
            break;
        default:
            break;
        }
    }
}
