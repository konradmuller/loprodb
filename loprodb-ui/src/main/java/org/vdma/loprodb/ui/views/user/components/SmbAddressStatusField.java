package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.customfield.CustomField;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.lumo.BadgeColor;

public class SmbAddressStatusField extends CustomField<String> {

    @Override
    protected String generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(String status) {
        getChildren().forEach(this::remove);
        if (status != null) {
            Badge badge = new Badge(UI.getCurrent().getTranslation("SambaAddressStatus." + status),
                    BadgeColor.NORMAL_PRIMARY);
            UIUtils.setPadding(badge, Horizontal.L);
            add(badge);
        }
    }
}
