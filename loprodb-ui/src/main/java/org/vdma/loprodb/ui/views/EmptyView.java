package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import org.vdma.loprodb.ui.MainLayout;

@Route(value = "empty", layout = MainLayout.class)
public class EmptyView extends ViewFrame {

    public EmptyView() {
        setId("empty");
        setViewContent(new Span());
    }
}
