package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.UI;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.ui.util.UIUtils;

@RequiredArgsConstructor
@Slf4j
public class SmbContactUiFetcher {

    private final UI ui;
    private final SmbContactService smbContactService;

    public void fetch(Long contactId, Consumer<SmbContactData> onResult) {
        if (contactId == null) {
            onResult.accept(null);
            return;
        }
        new Thread(() -> {
            try {
                SmbContactData contactData = smbContactService.getContactById(contactId);
                ui.access(() -> onResult.accept(contactData));
            } catch (HttpStatusCodeException ex) {
                ui.access(() -> {
                    if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                        UIUtils.showNotification(ui.getTranslation("user.details.samba-contact-not-found-msg"));
                    } else {
                        log.warn("failed to request samba contact info for id " + contactId, ex);
                    }
                    onResult.accept(null);
                });
            }
        }).start();
    }

}
