package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import lombok.Getter;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawer;
import org.vdma.loprodb.ui.components.detailsdrawer.DetailsDrawerHeader;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

public abstract class AbstractAuditTrail<AD> extends FlexBoxLayout {

    private static final String TIMESTAMP_PROP = "timestamp";

    @Getter
    private DetailsDrawer detailsDrawer;

    private Grid<AuditData<AD>> grid;
    private DetailsDrawerHeader detailsDrawerHeader;

    private ObjectListParam filter = new ObjectListParam();
    private ConfigurableFilterDataProvider<AuditData<AD>, Void, ObjectListParam> dataProvider;

    public AbstractAuditTrail() {
        setFlexDirection(FlexDirection.COLUMN);
        setBoxSizing(BoxSizing.BORDER_BOX);
        setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setHeightFull();
        createDetailsDrawer();
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        if (getComponentCount() == 0) {
            buildContent();
        }
    }

    protected abstract SpringDataProviderBuilder<AuditData<AD>, ObjectListParam> auditProviderBuilder();

    protected abstract String getDetailsDrawerHeaderTitle(AuditData<AD> audit);

    protected abstract Component createDetails(AuditData<AD> audit);

    private void onSearch(String term) {
        filter.setSearchTerm(term);
        dataProvider.setFilter(filter);
    }

    private void buildContent() {
        TextField header = UIUtils.createSearchTextField(this::onSearch);
        UIUtils.setMargin(header, Bottom.RESPONSIVE_M);

        add(header, buildGrid());
    }

    private void createDetailsDrawer() {
        detailsDrawer = new DetailsDrawer(DetailsDrawer.Position.RIGHT);

        // Header
        detailsDrawerHeader = new DetailsDrawerHeader("");
        detailsDrawerHeader.addCloseListener(buttonClickEvent -> {
            detailsDrawer.hide();
            grid.deselectAll();
        });
        detailsDrawer.setHeader(detailsDrawerHeader);
    }

    private void showDetails(AuditData<AD> audit) {
        detailsDrawerHeader.setTitle(getDetailsDrawerHeaderTitle(audit));
        detailsDrawer.setContent(createDetails(audit));
        detailsDrawer.show();
    }

    private Grid buildGrid() {
        dataProvider = auditProviderBuilder().build().withConfigurableFilter();
        dataProvider.setFilter(filter);

        grid = new Grid<>();
        grid.addSelectionListener(event -> event.getFirstSelectedItem().ifPresent(this::showDetails));
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();
        Grid.Column<AuditData<AD>> timestampColumn =
                grid.addColumn(audit -> UIUtils.formatDate(audit.getRevInfo().getTimestamp()))
                        .setAutoWidth(true)
                        .setFlexGrow(1)
                        .setFrozen(true)
                        .setSortProperty(TIMESTAMP_PROP)
                        .setHeader(getTranslation("user.details.audit.date-time"));
        grid.addColumn(audit -> audit.getRevInfo().getUsername())
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.audit.author"));
        grid.sort(new GridSortOrderBuilder<AuditData<AD>>().thenDesc(timestampColumn).build());

        return grid;
    }
}
