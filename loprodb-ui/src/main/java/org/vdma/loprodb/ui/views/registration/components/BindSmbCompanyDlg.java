package org.vdma.loprodb.ui.views.registration.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import java.util.function.Consumer;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbPostAddressData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.CustomDialog;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.LabelField;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Uniform;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.registration.SmbCompanyUiFetcher;
import org.vdma.loprodb.ui.views.user.components.SmbAddressStatusField;
import org.vdma.loprodb.ui.views.user.components.UserPropFormItem;

public class BindSmbCompanyDlg extends CustomDialog {

    public BindSmbCompanyDlg(SmbCompanyData initial, SmbCompanyUiFetcher companyUiFetcher,
            Consumer<SmbCompanyData> onResult) {
        Binder<SmbCompanyData> binder = new Binder<>();
        binder.setBean(initial);

        NumberField nfSambaId = new NumberField();
        nfSambaId.setWidthFull();
        nfSambaId.setMin(1);
        nfSambaId.setStep(1);
        if (initial != null) {
            nfSambaId.setValue(initial.getId().doubleValue());
        }

        Button btnQuery = UIUtils.createOutlinedButton(getTranslation("user.details.basic.fetch-samba-address"));
        btnQuery.setWidth("175px");
        btnQuery.addClickListener(e -> {
            if (nfSambaId.getValue() != null) {
                btnQuery.setEnabled(false);
                companyUiFetcher.fetch(nfSambaId.getValue().longValue(), c -> {
                    binder.setBean(c);
                    btnQuery.setEnabled(true);
                });
            }
        });

        FormLayout sambaIdForm = new FormLayout();
        sambaIdForm.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
        UIUtils.setTheme(UserPropFormItem.THEME,
                sambaIdForm.addFormItem(nfSambaId, getTranslation("user.list.filter.samba-id")));

        FlexBoxLayout flbSambaId = new FlexBoxLayout(sambaIdForm, btnQuery);
        flbSambaId.setSpacing(Right.XL);
        flbSambaId.setBoxSizing(BoxSizing.BORDER_BOX);
        flbSambaId.setFlexGrow(1, sambaIdForm);

        SmbAddressStatusField statusField = new SmbAddressStatusField();
        binder.forField(statusField).bind(c -> c.getStatus().name(), null);

        TextField tfType = new TextField();
        tfType.setWidthFull();
        binder.forField(tfType).bind(c -> getTranslation(c.getType().getTranslateKey()), null);

        TextField tfName = new TextField();
        tfName.setWidthFull();
        binder.forField(tfName).bind(SmbCompanyData::getName, null);

        TextField tfName1 = new TextField();
        tfName1.setWidthFull();
        binder.forField(tfName1).bind(SmbCompanyData::getName1, null);

        TextField tfName2 = new TextField();
        tfName2.setWidthFull();
        binder.forField(tfName2).bind(SmbCompanyData::getName2, null);

        TextField tfName3 = new TextField();
        tfName3.setWidthFull();
        binder.forField(tfName3).bind(SmbCompanyData::getName3, null);

        TextField tfCountry = new TextField();
        tfCountry.setWidthFull();
        binder.forField(tfCountry).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? UIUtils.getDisplayCountry(a.getCountryCode()) : null;
        }, null);

        TextField tfZip = new TextField();
        tfZip.setWidthFull();
        binder.forField(tfZip).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getZip() : null;
        }, null);

        TextField tfCity = new TextField();
        tfCity.setWidthFull();
        binder.forField(tfCity).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getCity() : null;
        }, null);

        TextField tfStreet = new TextField();
        tfStreet.setWidthFull();
        binder.forField(tfStreet).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getStreet() : null;
        }, null);

        Label lblHomepage = new Label();
        lblHomepage.getElement().getStyle().set("text-decoration", "underline");
        lblHomepage.getElement().getStyle().set("cursor", "pointer");
        LabelField<String> lfHomepage = new LabelField<>(lblHomepage);
        binder.forField(lfHomepage).bind(SmbCompanyData::getHomepage, null);

        AnchorField homepageLink = new AnchorField(lfHomepage);
        binder.forField(homepageLink).bind(SmbCompanyData::getHomepage, null);

        SmbMembershipsField membershipsField = new SmbMembershipsField();
        binder.forField(membershipsField).bind(SmbCompanyData::getMemberships, null);

        FormLayout detailsForm = new FormLayout();
        detailsForm.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        detailsForm.addFormItem(statusField, getTranslation("commons.status"));
        detailsForm.addFormItem(tfType, getTranslation("commons.type"));
        detailsForm.addFormItem(tfName, getTranslation("user.details.basic.company-name"));
        detailsForm.addFormItem(tfName1, getTranslation("user.details.company.company-name1"));
        detailsForm.addFormItem(tfName2, getTranslation("user.details.company.company-name2"));
        detailsForm.addFormItem(tfName3, getTranslation("user.details.company.company-name3"));
        detailsForm.addFormItem(tfCountry, getTranslation("commons.country"));
        detailsForm.addFormItem(tfZip, getTranslation("commons.zip"));
        detailsForm.addFormItem(tfCity, getTranslation("commons.city"));
        detailsForm.addFormItem(tfStreet, getTranslation("commons.street"));
        detailsForm.addFormItem(homepageLink, getTranslation("commons.homepage"));
        detailsForm.addFormItem(membershipsField, getTranslation("commons.memberships"));

        detailsForm.getChildren().forEach(fi -> UIUtils.setTheme(UserPropFormItem.THEME, fi));

        FlexBoxLayout flbDetails = new FlexBoxLayout(detailsForm);
        flbDetails.setSpacing(Right.XL);
        flbDetails.setBoxSizing(BoxSizing.BORDER_BOX);

        FlexBoxLayout flbContent = new FlexBoxLayout(flbSambaId, new Hr(), flbDetails);
        flbContent.setBoxSizing(BoxSizing.BORDER_BOX);
        flbContent.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        flbContent.setPadding(Uniform.RESPONSIVE_M);

        setCaption(getTranslation("user.details.basic.edit-samba-link"));
        setWidth("1000px");
        addOkListener(event -> {
            if (nfSambaId.getValue() == null) {
                onResult.accept(null);
                close();
            } else if (binder.getBean() != null && binder.getBean().getId().equals(nfSambaId.getValue().longValue())) {
                onResult.accept(binder.getBean());
                close();
            } else {
                getOk().setEnabled(false);
                getCancel().setEnabled(false);
                companyUiFetcher.fetch(nfSambaId.getValue().longValue(), c -> {
                    onResult.accept(c);
                    close();
                });
            }
        });
        addCancelListener(event -> close());
        setContent(flbContent);
    }
}
