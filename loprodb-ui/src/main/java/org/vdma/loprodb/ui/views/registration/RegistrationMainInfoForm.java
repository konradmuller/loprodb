package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import static java.util.Optional.ofNullable;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.Clickable;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.LabelField;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.registration.components.RegistrationStatusField;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_CLASS_NAME;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;
import static org.vdma.loprodb.ui.views.user.UserDetails.HEADER_CLASS_NAME;
import org.vdma.loprodb.ui.views.user.components.SmbAddressStatusField;
import org.vdma.loprodb.ui.views.user.components.UserPropFormItem;
import org.vdma.loprodb.ui.views.user.components.UserStatusField;

class RegistrationMainInfoForm extends FormLayout {

    private Button btnLinkContact;
    private Button btnCreateContact;
    private Button btnSaveMemo;
    private Button btnDiscardMemo;

    private LabelField<Long> lfSambaContactId;
    private final RegistrationStatusField regStatus;
    private Clickable userIdClickable;

    RegistrationMainInfoForm(Binder<RegistrationData> binder, Binder<UserData> userBinder,
                             Binder<SmbContactData> sambaBinder) {

        regStatus = new RegistrationStatusField();
        UIUtils.setMargin(regStatus, Right.AUTO);
        regStatus.addValueChangeListener(event -> {
            boolean isManualReviewPending = event.getValue() == RegistrationStatus.MANUAL_REVIEW_PENDING;
            btnLinkContact.setVisible(isManualReviewPending);
            btnCreateContact.setVisible(isManualReviewPending);
        });
        binder.forField(regStatus).bind(RegistrationData::getStatus, RegistrationData::setStatus);

        SmbAddressStatusField smbStatus = new SmbAddressStatusField();
        smbStatus.setWidthFull();
        sambaBinder.forField(smbStatus).bind(c -> c.getStatus().name(), null);

        UserStatusField userStatus = new UserStatusField();
        UIUtils.setMargin(userStatus, Right.AUTO);
        userBinder.forField(userStatus).bind(UserData::getStatus, null);

        TextField tfEditor = new TextField();
        tfEditor.setWidthFull();
        binder.forField(tfEditor).bind(reg -> ofNullable(reg.getEditor()).map(UserData::getFullName).orElse(null), null);
        FlexBoxLayout fbEditor = new FlexBoxLayout(tfEditor);
        fbEditor.setAlignSelf(FlexComponent.Alignment.START);
        
        TextArea taMemo = new TextArea();
        taMemo.setWidthFull();
        binder.forField(taMemo).bind(RegistrationData::getMemo, RegistrationData::setMemo);
        
        btnSaveMemo = UIUtils.createPrimaryButton(getTranslation("registration.details.save-memo"));
        UIUtils.setMargin(btnSaveMemo, Left.S);
        
        btnDiscardMemo = UIUtils.createOutlinedButton(getTranslation("registration.details.discard-memo"));
        UIUtils.setMargin(btnDiscardMemo, Left.S);
        
        TextField tfSalutation = new TextField();
        tfSalutation.setWidthFull();
        binder.forField(tfSalutation)
                .bind(reg -> getTranslation(Gender.class.getSimpleName() + "." + reg.getGender().name()), null);

        TextField tfSmbSalutation = new TextField();
        tfSmbSalutation.setWidthFull();
        sambaBinder.forField(tfSmbSalutation).bind(
                c -> c.getSalutation() != null ? c.getSalutation().getNameByLocale(getLocale()) : "", null);

        TextField tfUserSalutation = new TextField();
        tfUserSalutation.setWidthFull();
        userBinder.forField(tfUserSalutation)
                .bind(user -> getTranslation(Gender.class.getSimpleName() + "." + user.getGender().name()), null);

        TextField tfTitle = new TextField();
        binder.forField(tfTitle).bind(RegistrationData::getTitle, null);

        TextField tfSmbTitle = new TextField();
        tfSmbTitle.setWidthFull();
        sambaBinder.forField(tfSmbTitle).bind(SmbContactData::getTitle, null);

        TextField tfUserTitle = new TextField();
        userBinder.forField(tfUserTitle).bind(UserData::getTitle, null);

        TextField tfFirstName = new TextField();
        binder.forField(tfFirstName).bind(RegistrationData::getFirstName, null);

        TextField tfSmbFirstName = new TextField();
        tfSmbFirstName.setWidthFull();
        sambaBinder.forField(tfSmbFirstName).bind(SmbContactData::getFirstName, null);

        TextField tfUserFirstName = new TextField();
        userBinder.forField(tfUserFirstName).bind(UserData::getFirstName, null);

        TextField tfLastName = new TextField();
        binder.forField(tfLastName).bind(RegistrationData::getLastName, null);

        TextField tfSmbLastName = new TextField();
        tfSmbLastName.setWidthFull();
        sambaBinder.forField(tfSmbLastName).bind(SmbContactData::getLastName, null);

        TextField tfUserLastName = new TextField();
        userBinder.forField(tfUserLastName).bind(UserData::getLastName, null);

        TextField tfEmail = new TextField();
        binder.forField(tfEmail).bind(RegistrationData::getEmail, null);

        TextField tfSmbEmail = new TextField();
        tfSmbEmail.setWidthFull();
        sambaBinder.forField(tfSmbEmail).bind(SmbContactData::getEmail, null);

        TextField tfUserEmail = new TextField();
        userBinder.forField(tfUserEmail).bind(UserData::getEmail, null);

        TextField tfPhone = new TextField();
        binder.forField(tfPhone).bind(RegistrationData::getPhone, null);

        TextField tfSmbPhone = new TextField();
        tfSmbPhone.setWidthFull();
        sambaBinder.forField(tfSmbPhone).bind(SmbContactData::getPhone, null);

        TextField tfUserPhone = new TextField();
        userBinder.forField(tfUserPhone).bind(UserData::getPhone, null);

        TextField tfMobile = new TextField();
        binder.forField(tfMobile).bind(RegistrationData::getMobile, null);

        TextField tfSmbMobile = new TextField();
        tfSmbPhone.setWidthFull();
        sambaBinder.forField(tfSmbMobile).bind(SmbContactData::getPublicMobile, null);

        TextField tfUserMobile = new TextField();
        userBinder.forField(tfUserMobile).bind(UserData::getPublicMobile, null);
        
        TextField tfFunc = new TextField();
        binder.forField(tfFunc).bind(RegistrationData::getFunction, null);

        TextField tfSmbFunc = new TextField();
        tfSmbFunc.setWidthFull();
        sambaBinder.forField(tfSmbFunc).bind(SmbContactData::getFunction, null);

        TextField tfUserFunc = new TextField();
        userBinder.forField(tfUserFunc).bind(UserData::getFunction, null);

        TextField tfPosition = new TextField();
        binder.forField(tfPosition).bind(reg -> reg.getPosition().getNameByLocale(getLocale()), null);

        TextField tfSmbPosition = new TextField();
        tfSmbPosition.setWidthFull();
        sambaBinder.forField(tfSmbPosition).bind(c -> {
            if (c.getAreaPositions().isEmpty()) {
                return "";
            } else {
                return c.getAreaPositions().get(0).getPosition().getNameByLocale(getLocale());
            }
        }, null);

        TextField tfUserPosition = new TextField();
        userBinder.forField(tfUserPosition).bind(user -> user.getPosition().getNameByLocale(getLocale()), null);

        TextField tfArea = new TextField();
        binder.forField(tfArea).bind(reg -> reg.getBusinessArea().getNameByLocale(getLocale()), null);

        TextField tfSmbArea = new TextField();
        tfSmbArea.setWidthFull();
        sambaBinder.forField(tfSmbArea).bind(c -> {
            if (c.getAreaPositions().isEmpty()) {
                return "";
            } else {
                return c.getAreaPositions().get(0).getArea().getNameByLocale(getLocale());
            }
        }, null);

        TextField tfUserArea = new TextField();
        userBinder.forField(tfUserArea).bind(user -> user.getBusinessArea().getNameByLocale(getLocale()), null);

        TextArea taTags = new TextArea();
        taTags.setWidthFull();
        binder.forField(taTags).bind(reg -> String.join("\n", reg.getTags()), null);

        TextArea taUserTags = new TextArea();
        taUserTags.setWidthFull();
        userBinder.forField(taUserTags).bind(user -> String.join("\n", user.getTags()), null);

        UIUtils.setTheme(FORM_THEME, this);
        addClassName(FORM_CLASS_NAME);
        setWidthFull();
        UIUtils.setPadding(this, Bottom.L, Top.S);
        setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        add(buildMainInfoHeader(userBinder, sambaBinder));
        add(new UserPropFormItem(getTranslation("registration.details.status"),
                new FlexBoxLayout(regStatus), new FlexBoxLayout(smbStatus), new FlexBoxLayout(userStatus)));
        add(new UserPropFormItem(getTranslation("registration.details.editor-memo"), 
                fbEditor, new FlexBoxLayout(taMemo), new FlexBoxLayout(btnSaveMemo, btnDiscardMemo)));
        add(new UserPropFormItem(getTranslation("registration.details.salutation"), tfSalutation,
                tfSmbSalutation, tfUserSalutation));
        add(new UserPropFormItem(getTranslation("registration.details.title"), tfTitle, tfSmbTitle,
                tfUserTitle));
        add(new UserPropFormItem(getTranslation("registration.details.first-name"), tfFirstName,
                tfSmbFirstName, tfUserFirstName));
        add(new UserPropFormItem(getTranslation("registration.details.last-name"), tfLastName,
                tfSmbLastName,
                tfUserLastName));
        add(new UserPropFormItem(getTranslation("registration.details.email"), tfEmail, tfSmbEmail,
                tfUserEmail));
        add(new UserPropFormItem(getTranslation("registration.details.phone"), tfPhone, tfSmbPhone,
                tfUserPhone));
        add(new UserPropFormItem(getTranslation("registration.details.mobile"), tfMobile, tfSmbMobile,
                tfUserMobile));
        add(new UserPropFormItem(getTranslation("registration.details.function"), tfFunc, tfSmbFunc,
                tfUserFunc));
        add(new UserPropFormItem(getTranslation("registration.details.position"), tfPosition,
                tfSmbPosition,
                tfUserPosition));
        add(new UserPropFormItem(getTranslation("registration.details.business-area"), tfArea, tfSmbArea,
                tfUserArea));
        add(new UserPropFormItem(getTranslation("registration.details.tags"), taTags, new Div(),
                taUserTags));
    }

    private Component buildMainInfoHeader(Binder<UserData> userBinder,
            Binder<SmbContactData> sambaBinder) {
        Label lbHeaderRegistration = UIUtils.createH5Label(getTranslation("registration.details.registration"));

        Label lblSambaId = UIUtils.createH5Label("");
        lblSambaId.getElement().getStyle().set("text-decoration", "underline");
        lblSambaId.getElement().getStyle().set("cursor", "pointer");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblSambaId);
        lfSambaContactId = new LabelField<>(lblSambaId);
        lfSambaContactId.setPrefix(getTranslation("user.details.samba-addr-id"));
        userBinder.forField(lfSambaContactId).bind(UserData::getSambaId, null);

        AnchorField sambaLink = new AnchorField(lfSambaContactId);
        sambaBinder.forField(sambaLink).bind(SmbContactData::getUrl, null);

        Label lbHeaderSamba = UIUtils.createH5Label(getTranslation("commons.samba"));

        FlexBoxLayout headerSambaPart = new FlexBoxLayout(lbHeaderSamba, sambaLink);
        headerSambaPart.setFlexDirection(FlexLayout.FlexDirection.ROW);
        headerSambaPart.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        headerSambaPart.setBoxSizing(BoxSizing.BORDER_BOX);
        headerSambaPart.setAlignItems(FlexComponent.Alignment.CENTER);

        Label lbHeaderProfile = UIUtils.createH5Label(getTranslation("user.details.basic.user-profile"));
        lbHeaderProfile.setVisible(false);

        Label lblUserId = UIUtils.createH5Label("");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblUserId);
        lblUserId.getElement().getStyle().set("text-decoration", "underline");
        lblUserId.getElement().getStyle().set("cursor", "pointer");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblUserId);
        LabelField<Long> lfUserId = new LabelField<>(lblUserId);
        lfUserId.addValueChangeListener(event -> lbHeaderProfile.setVisible(event.getValue() != null));
        lfUserId.setPrefix(getTranslation("user.details.user-id"));
        userBinder.forField(lfUserId).bind(UserData::getId, null);

        userIdClickable = new Clickable(lfUserId);
        userIdClickable.addClassName(LumoStyles.Margin.Left.AUTO);

        btnLinkContact = UIUtils.createOutlinedButton(getTranslation("registration.details.link-contact-manually"));

        btnCreateContact = UIUtils.createOutlinedButton(getTranslation("registration.details.create-contact"));

        FlexBoxLayout flbActions = new FlexBoxLayout(btnCreateContact, btnLinkContact);
        flbActions.setFlexDirection(FlexLayout.FlexDirection.COLUMN);

        FlexBoxLayout headerUserPart = new FlexBoxLayout(lbHeaderProfile, userIdClickable, flbActions);
        headerUserPart.setFlexDirection(FlexLayout.FlexDirection.ROW);
        headerUserPart.setBoxSizing(BoxSizing.BORDER_BOX);
        headerUserPart.setAlignItems(FlexComponent.Alignment.CENTER);
        headerUserPart.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);

        UserPropFormItem userPropFormItem = new UserPropFormItem("", lbHeaderRegistration, headerSambaPart,
                headerUserPart);
        userPropFormItem.addClassName(HEADER_CLASS_NAME);
        return userPropFormItem;
    }

    void setUserIdClickListener(ComponentEventListener<ClickEvent<Div>> listener) {
        userIdClickable.addClickListener(listener);
    }

    void setLinkContactListener(ComponentEventListener<ClickEvent<Button>> listener) {
        btnLinkContact.addClickListener(listener);
    }

    void setCreateContactListener(ComponentEventListener<ClickEvent<Button>> listener) {
        btnCreateContact.addClickListener(listener);
    }

    void addStatusChangeListener(HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<
            CustomField<RegistrationStatus>, RegistrationStatus>> listener) {
        regStatus.addValueChangeListener(listener);
    }

    void setCreateContactBtnVisibility(boolean visible) {
        btnCreateContact.setVisible(visible);
    }

    Long getSambaContactId() {
        return lfSambaContactId.getValue();
    }

    void setSambaContactId(Long id) {
        lfSambaContactId.setValue(id);
    }
    
    void setSaveMemoListener(ComponentEventListener<ClickEvent<Button>> listener) {
        btnSaveMemo.addClickListener(listener);
    }
    
    void setDiscardMemoListener(ComponentEventListener<ClickEvent<Button>> listener) {
        btnDiscardMemo.addClickListener(listener);
    }
}
