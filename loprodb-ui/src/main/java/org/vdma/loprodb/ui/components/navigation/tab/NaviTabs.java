package org.vdma.loprodb.ui.components.navigation.tab;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.HasUrlParameter;
import java.util.function.Predicate;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.Overflow;
import org.vdma.loprodb.ui.views.EmptyView;
import org.vdma.loprodb.ui.views.Home;

/**
 * NaviTabs supports tabs that can be closed, and that can navigate to a
 * specific target when clicked.
 */
@Slf4j
public class NaviTabs extends Tabs {

    private ComponentEventListener<SelectedChangeEvent> listener = (ComponentEventListener<
            SelectedChangeEvent>) selectedChangeEvent -> navigateToSelectedTab();

    private HasChanges hasChangesDelegate;

    public NaviTabs(HasChanges hasChangesDelegate) {
        this.hasChangesDelegate = hasChangesDelegate;
        addSelectedChangeListener(listener);
        getElement().setAttribute("overflow", "end");
        UIUtils.setOverflow(Overflow.HIDDEN, this);
    }

    /**
     * When adding the first tab, the selection change event is triggered. This
     * will cause the app to navigate to that tab's navigation target (if any).
     * This constructor allows you to add the tabs before the event listener is
     * set.
     */
    public NaviTabs(HasChanges hasChangesDelegate, NaviTab... naviTabs) {
        this(hasChangesDelegate);
        add(naviTabs);
    }

    /**
     * Creates a regular tab without any click listeners.
     */
    public Tab addTab(String text) {
        Tab tab = new Tab(text);
        add(tab);
        return tab;
    }

    /**
     * Creates a tab that when clicked navigates to the specified target.
     */
    public Tab addTab(String text, Class<? extends Component> navigationTarget, boolean closable) {
        NaviTab tab = new NaviTab(text, navigationTarget);
        configureTab(tab, closable);
        add(tab);
        return tab;
    }

    public <T, C extends Component & HasUrlParameter<T>> Tab addTab(String text, Class<? extends C> navigationTarget,
            T param, boolean closable) {
        ParamNaviTab tab = new ParamNaviTab<>(text, navigationTarget, param);
        configureTab(tab, closable);
        add(tab);
        return tab;
    }

    private void configureTab(BaseNaviTab tab, boolean closable) {
        tab.closable(closable);
        if (closable) {
            tab.getCloseButton().addClickListener(event -> {
                if (hasChangesDelegate.hasChanges()) {
                    ConfirmDialog
                            .createQuestion()
                            .withCaption("Unsaved changes")
                            .withMessage(new Div(
                                    new Paragraph(
                                            "You have unsaved changes made to this tab which will be lost if you close it. "),
                                    new Paragraph("Would you like to discard them?")))
                            .withOkButton(() -> closeTab(tab), ButtonOption.focus(), ButtonOption.caption("Yes"))
                            .withCancelButton(ButtonOption.caption("No"))
                            .open();
                } else {
                    closeTab(tab);
                }
            });
        }
    }

    private void closeTab(BaseNaviTab tab) {
        remove(tab);
        navigateToSelectedTab();
        MainLayout.get().onCloseTab(tab);
    }

    /**
     * Navigates to the selected tab's navigation target if available.
     */
    public void navigateToSelectedTab() {
        if (getSelectedTab() instanceof BaseNaviTab) {
            try {

                //
                // DON'T DELETE
                // RESETS CURRENT ROUTING TO ENABLE REROUTE TO SAME VIEW
                //
                if (getSelectedTab() instanceof ParamNaviTab) {
                    UI.getCurrent().navigate(EmptyView.class);
                }
                ((BaseNaviTab) getSelectedTab()).navigate();
            } catch (Exception e) {
                // @todo this is an code flow by exception anti-pattern. Either
                // handle the case without the exception, or
                // @todo at least document meticulously why this can't be done
                // any other way and what kind of exceptions are we catching
                // @todo and when they can occur.
                // @todo this block consumes all exceptions, even
                // backend-originated, and may result in exceptions disappearing
                // mysteriously.

                // If the right-most tab is closed, the Tabs component does not
                // auto-select tabs on the left.
                if (getTabCount() > 0) {
                    setSelectedIndex(getTabCount() - 1);
                } else {
                    UI.getCurrent().navigate(Home.class);
                }
            }
        }
    }

    /**
     * Updates the current tab's name and navigation target.
     */
    public void updateSelectedTab(String text, Class<? extends Component> navigationTarget) {
        Tab tab = getSelectedTab();
        tab.setLabel(text);

        if (tab instanceof NaviTab) {
            ((NaviTab) tab).setNavigationTarget(navigationTarget);
        }

        navigateToSelectedTab();
    }

    /**
     * Returns the number of tabs.
     */
    public int getTabCount() {
        return Math.toIntExact(getChildren()
                .filter(component -> component instanceof Tab).count());
    }

    public void closeSelectedTab() {
        Tab tab = getSelectedTab();
        if (tab instanceof BaseNaviTab) {
            ((BaseNaviTab) tab).getCloseButton().click();
        }
    }

    public void closeTab(Predicate<Tab> predicate) {
        getChildren().filter(component -> component instanceof Tab)
                .map(c -> (Tab) c)
                .filter(predicate)
                .forEach(tab -> {
                    if (tab instanceof BaseNaviTab) {
                        ((BaseNaviTab) tab).getCloseButton().click();
                    }
                });
    }

    public Stream<Tab> getTabs() {
        return getChildren().filter(component -> component instanceof Tab).map(component -> (Tab) component);
    }

}
