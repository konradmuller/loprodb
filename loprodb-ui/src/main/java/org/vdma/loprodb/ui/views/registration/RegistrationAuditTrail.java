package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.mapper.RegistrationMapper;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.views.AbstractAuditTrail;
import org.vdma.loprodb.ui.views.registration.components.RegistrationStatusField;

@RequiredArgsConstructor
class RegistrationAuditTrail extends AbstractAuditTrail<RegistrationData> {

    private final Long registrationId;
    private final RegistrationMapper mapper;
    private final AuditService auditService;

    @Override
    protected SpringDataProviderBuilder<AuditData<RegistrationData>, ObjectListParam> auditProviderBuilder() {
        return SpringDataProviderBuilder.registrationAudit(registrationId, mapper, auditService);
    }

    @Override
    protected String getDetailsDrawerHeaderTitle(AuditData<RegistrationData> audit) {
        return audit.getEntityData().getFullName();
    }

    @Override
    protected Component createDetails(AuditData<RegistrationData> audit) {
        Binder<RegistrationData> binder = new Binder<>();

        RegistrationStatusField regStatus = new RegistrationStatusField();
        binder.forField(regStatus).bind(RegistrationData::getStatus, RegistrationData::setStatus);

        ComboBox<Gender> cbSalutation = new ComboBox<>(null, Gender.values());
        cbSalutation.setWidthFull();
        cbSalutation.setItemLabelGenerator(
                gender -> getTranslation(Gender.class.getSimpleName() + "." + gender.name()));
        binder.forField(cbSalutation).bind(RegistrationData::getGender, null);

        TextField tfTitle = new TextField();
        tfTitle.setWidthFull();
        binder.forField(tfTitle).bind(RegistrationData::getTitle, null);

        TextField tfFirstName = new TextField();
        tfFirstName.setWidthFull();
        binder.forField(tfFirstName).bind(RegistrationData::getFirstName, null);

        TextField tfLastName = new TextField();
        tfLastName.setWidthFull();
        binder.forField(tfLastName).bind(RegistrationData::getLastName, null);

        TextField tfEmail = new TextField();
        tfEmail.setWidthFull();
        binder.forField(tfEmail).bind(RegistrationData::getEmail, null);

        TextField tfPhone = new TextField();
        tfPhone.setWidthFull();
        binder.forField(tfPhone).bind(RegistrationData::getPhone, null);

        TextField tfMobile = new TextField();
        tfMobile.setWidthFull();
        binder.forField(tfMobile).bind(RegistrationData::getMobile, null);
        
        TextField tfFunction = new TextField();
        tfFunction.setWidthFull();
        binder.forField(tfFunction).bind(RegistrationData::getFunction, null);

        TextField tfPosition = new TextField();
        tfPosition.setWidthFull();
        binder.forField(tfPosition).bind(reg -> reg.getPosition().getNameByLocale(getLocale()), null);

        TextField tfArea = new TextField();
        tfArea.setWidthFull();
        binder.forField(tfArea).bind(reg -> reg.getBusinessArea().getNameByLocale(getLocale()), null);

        ComboBox<Language> cbLang = new ComboBox<>(null, Language.values());
        cbLang.setWidthFull();
        cbLang.setItemLabelGenerator(lang -> getTranslation(Language.class.getSimpleName() + "." + lang.name()));
        binder.forField(cbLang).bind(RegistrationData::getLanguage, null);

        TextArea taTags = new TextArea();
        taTags.setWidthFull();
        binder.forField(taTags).bind(reg -> String.join("\n", reg.getTags()), null);

        TextField tfCompany = new TextField();
        tfCompany.setWidthFull();
        binder.forField(tfCompany).bind(reg -> reg.getCompany().getName(), null);

        TextField tfCountry = new TextField();
        tfCountry.setWidthFull();
        binder.forField(tfCountry).bind(reg -> reg.getCompany().getCountry(), null);

        TextField tfZip = new TextField();
        tfZip.setWidthFull();
        binder.forField(tfZip).bind(reg -> reg.getCompany().getZip(), null);

        TextField tfCity = new TextField();
        tfCity.setWidthFull();
        binder.forField(tfCity).bind(reg -> reg.getCompany().getCity(), null);

        TextField tfStreet = new TextField();
        tfStreet.setWidthFull();
        binder.forField(tfStreet).bind(reg -> reg.getCompany().getStreet(), null);

        TextField tfHouseNumber = new TextField();
        tfHouseNumber.setWidthFull();
        binder.forField(tfHouseNumber).bind(reg -> reg.getCompany().getHouseNumber(), null);

        FormLayout form = new FormLayout();
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));
        form.addClassNames(LumoStyles.Padding.Bottom.L, LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        form.addFormItem(regStatus, getTranslation("registration.details.status"));
        form.addFormItem(cbSalutation, getTranslation("registration.details.salutation"));
        form.addFormItem(tfTitle, getTranslation("registration.details.title"));
        form.addFormItem(tfFirstName, getTranslation("registration.details.first-name"));
        form.addFormItem(tfLastName, getTranslation("registration.details.last-name"));
        form.addFormItem(tfEmail, getTranslation("registration.details.email"));
        form.addFormItem(tfPhone, getTranslation("registration.details.phone"));
        form.addFormItem(tfMobile, getTranslation("registration.details.mobile"));
        form.addFormItem(tfFunction, getTranslation("registration.details.function"));
        form.addFormItem(tfPosition, getTranslation("registration.details.position"));
        form.addFormItem(tfArea, getTranslation("registration.details.business-area"));
        form.addFormItem(cbLang, getTranslation("commons.lang"));
        form.addFormItem(taTags, getTranslation("registration.details.tags"));
        form.addFormItem(tfCompany, getTranslation("registration.details.company-name"));
        form.addFormItem(tfCountry, getTranslation("commons.country"));
        form.addFormItem(tfZip, getTranslation("commons.zip"));
        form.addFormItem(tfCity, getTranslation("commons.city"));
        form.addFormItem(tfStreet, getTranslation("commons.street"));
        form.addFormItem(tfHouseNumber, getTranslation("commons.house-number"));

        binder.readBean(audit.getEntityData());
        return form;
    }
}
