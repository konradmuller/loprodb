package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.backend.rest.LoprodbApiException;
import org.vdma.loprodb.backend.rest.LoprodbRegistrationService;
import org.vdma.loprodb.dto.RegistrationData;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_CONFIRMED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUAL_REVIEW_PENDING;
import static org.vdma.loprodb.entity.RegistrationStatus.OPT_IN_PENDING;
import org.vdma.loprodb.samba.service.SmbCompanyService;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationService;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserAdService;
import org.vdma.loprodb.service.mapper.RegistrationMapper;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.navigation.bar.AppBar;
import org.vdma.loprodb.ui.components.navigation.tab.HasChanges;
import org.vdma.loprodb.ui.components.navigation.tab.ParamNaviTab;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.ParamRoute;
import org.vdma.loprodb.ui.views.SplitViewFrame;

@PageTitle("Registration details")
@Route(value = "registrations", layout = MainLayout.class)
@CssImport("./styles/user-details.css")
@CssImport(value = "./styles/components/form-layout.css", themeFor = "vaadin-form-layout")
@Slf4j
public class RegistrationDetails extends SplitViewFrame implements HasUrlParameter<String>,
        ParamRoute<String, RegistrationDetails>, HasChanges, EqualCmp {

    private static final String BASIC_INFO_TAB = "registration-basic-info";
    private static final String AUDIT_TRAIL_TAB = "registration-audit-trail";

    private int tab = -1;

    private RegistrationData registration;

    private RegistrationBasicInfo basicInfoTab;
    private RegistrationAuditTrail auditTrailTab;
    private Button btnConfirmByUser;
    private Button btnConfirm;
    private Button btnDecline;
    private Button btnResend;
    private Button btnResetToManualCheck;

    private final IRegistrationService registrationService;
    private final SmbContactService smbContactService;
    private final SmbCompanyService smbCompanyService;
    private final SmbRegistrationService smbRegistrationService;
    private final RegistrationMapper registrationMapper;
    private final AuditService auditService;
    private final LoprodbRegistrationService loprodbRegistrationService;
    private final IUserAdService userAdService;

    @Autowired
    public RegistrationDetails(IRegistrationService registrationService,
                               SmbContactService smbContactService, SmbCompanyService smbCompanyService,
                               SmbRegistrationService smbRegistrationService,
                               RegistrationMapper registrationMapper, AuditService auditService,
                               LoprodbRegistrationService loprodbRegistrationService,
                               IUserAdService userAdService) {
        this.registrationService = registrationService;
        this.smbContactService = smbContactService;
        this.smbCompanyService = smbCompanyService;
        this.smbRegistrationService = smbRegistrationService;
        this.registrationMapper = registrationMapper;
        this.auditService = auditService;
        this.loprodbRegistrationService = loprodbRegistrationService;
        this.userAdService = userAdService;
        setId("registration_details");
    }

    @Override
    public void setParameter(BeforeEvent event, String token) {
        registration = registrationService.getRegistration(token);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        if (isContentEmpty()) {
            buildContent(attachEvent.getUI());
        }
        initAppBar();
    }

    private void buildContent(UI ui) {
        setViewFooter(buildActions());

        basicInfoTab = new RegistrationBasicInfo(smbContactService, smbCompanyService, smbRegistrationService,
                registrationService, registration, ui);
        auditTrailTab = new RegistrationAuditTrail(registration.getId(), registrationMapper, auditService);
    }

    private void initAppBar() {
        AppBar appBar = MainLayout.get().getAppBar();
        appBar.addTab(getTranslation("registration.details.registration-data")).setId(BASIC_INFO_TAB);
        appBar.addTab(getTranslation("user.details.audit")).setId(AUDIT_TRAIL_TAB);
        appBar.getTabs().setSelectedIndex(tab);
        appBar.addTabSelectionListener(e -> {
            clearViewDetails();
            String selectedTabId = MainLayout.get().getAppBar().getSelectedTab().getId().get();
            if (AUDIT_TRAIL_TAB.equals(selectedTabId)) {
                tab = 2;
                setViewContent(auditTrailTab);
                setViewDetails(auditTrailTab.getDetailsDrawer());
            } else {
                tab = 0;
                setViewContent(basicInfoTab);
            }
        });
        if (tab < 0) {
            tab = 0;
            appBar.getTabs().setSelectedIndex(tab);
        }
        appBar.centerTabs();
    }

    private FlexBoxLayout buildActions() {
        btnConfirmByUser = UIUtils.createPrimaryButton(getTranslation("registration.details.confirm-by-user"));
        UIUtils.setMargin(btnConfirmByUser, Left.S);
        btnConfirmByUser.addClickListener(event -> {
            ConfirmDialog
                    .createQuestion()
                    .withCaption(getTranslation("registration.details.confirm-registration"))
                    .withMessage(getTranslation("registration.details.confirm-confirmation-msg"))
                    .withOkButton(this::confirmByUser, ButtonOption.focus(),
                            ButtonOption.caption(getTranslation("commons.yes")))
                    .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                    .open();
        });

        btnConfirm = UIUtils.createPrimaryButton(getTranslation("registration.details.confirm-registration"));
        UIUtils.setMargin(btnConfirm, Left.S);
        btnConfirm.addClickListener(event -> {
            if (basicInfoTab.getLinkedSambaContactId() == null) {
                UIUtils.showNotification(getTranslation("registration.details.samba-contact-not-linked-error-msg"));
            } else {
                ConfirmDialog
                        .createQuestion()
                        .withCaption(getTranslation("registration.details.confirm-registration"))
                        .withMessage(getTranslation("registration.details.confirm-confirmation-msg"))
                        .withOkButton(this::confirm, ButtonOption.focus(),
                                ButtonOption.caption(getTranslation("commons.yes")))
                        .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                        .open();
            }
        });

        btnDecline = UIUtils.createOutlinedButton(getTranslation("registration.details.decline-registration"));
        UIUtils.setMargin(btnDecline, Left.S);
        btnDecline.addClickListener(event -> {
            ConfirmDialog
                    .createQuestion()
                    .withCaption(getTranslation("registration.details.decline-registration"))
                    .withMessage(getTranslation("registration.details.confirm-decline-msg"))
                    .withOkButton(this::reject, ButtonOption.focus(),
                            ButtonOption.caption(getTranslation("commons.yes")))
                    .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                    .open();
        });

        btnResend = UIUtils.createPrimaryButton(getTranslation("registration.resend"));
        btnResend.addClickListener(event -> ConfirmDialog
                .createQuestion()
                .withCaption(getTranslation("registration.details.resend"))
                .withMessage(getTranslation("registration.confirm-resend-msg"))
                .withOkButton(this::resend, ButtonOption.focus(),
                        ButtonOption.caption(getTranslation("commons.yes")))
                .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                .open());

        Button btnDelete = UIUtils.createErrorPrimaryButton(getTranslation("registration.delete"));
        UIUtils.setMargin(btnDelete, Left.S);
        btnDelete.addClickListener(event -> {
            ConfirmDialog
                    .createQuestion()
                    .withCaption(getTranslation("commons.confirm-deletion"))
                    .withMessage(getTranslation("registration.confirm-deletion-msg"))
                    .withOkButton(this::delete, ButtonOption.focus(),
                            ButtonOption.caption(getTranslation("commons.yes")))
                    .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                    .open();
        });
        
        btnResetToManualCheck = 
                UIUtils.createOutlinedButton(getTranslation("registration.reset-to-manual-review"));
        UIUtils.setMargin(btnDelete, Left.S);
        btnResetToManualCheck.addClickListener(event -> {
            ConfirmDialog
                    .createQuestion()
                    .withCaption(getTranslation("commons.confirm-action"))
                    .withMessage(getTranslation("registration.confirm-reset-to-manual-review"))
                    .withOkButton(this::resetToManualReview, ButtonOption.focus(),
                            ButtonOption.caption(getTranslation("commons.yes")))
                    .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                    .open();
        });

        setActionsVisible();

        FlexBoxLayout actions = new FlexBoxLayout(
                btnResend, btnResetToManualCheck, btnDelete, btnDecline, btnConfirm, btnConfirmByUser);
        actions.setAlignItems(FlexComponent.Alignment.CENTER);
        actions.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        actions.setBoxSizing(BoxSizing.BORDER_BOX);
        actions.setPadding(Horizontal.RESPONSIVE_X);
        return actions;
    }

    @Override
    public boolean hasChanges() {
        return false;
    }

    @Override
    public Class<RegistrationDetails> getRouteType() {
        return RegistrationDetails.class;
    }

    @Override
    public String getRouteParam() {
        return getNaviParam(registration);
    }

    @Override
    public String getTabLabel() {
        return getTabLabel(registration);
    }

    @Override
    public boolean eq(HasElement other) {
        return EqualCmp.super.eq(other) &&
                ((RegistrationDetails) other).registration.getToken().equals(registration.getToken());
    }

    private void confirmByUser() {
        try {
            loprodbRegistrationService.confirmByUser(registration.getToken());
            registration = registrationService.getRegistration(registration.getToken());
            basicInfoTab.setRegistration(registration);
            setActionsVisible();
            UIUtils.showNotification(getTranslation("registration.details.confirm.successful-msg"));
        } catch (LoprodbApiException ex) {
            UIUtils.showErrorNotification(getTranslation("registration.details.confirm.failed-msg",
                    ex.getApiError().getMessage()), ex.getApiError().getTrace());
        } catch (Exception ex) {
            log.error(String.format("Failed to confirm registration %s by user", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.details.confirm.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void confirm() {
        try {
            registration = registrationService.confirm(registration.getToken(), MANUALLY_CONFIRMED,
                    null, basicInfoTab.getLinkedSambaContactId(), basicInfoTab.isLinkedSambaContactVdmaMember());
            basicInfoTab.setRegistration(registration);
            setActionsVisible();
            UIUtils.showNotification(getTranslation("registration.details.confirm.successful-msg"));
        } catch (IllegalArgumentException ex) {
            UIUtils.showErrorNotification(getTranslation("registration.details.confirm.failed-msg",
                    ex.getMessage()), null);
        } catch (Exception ex) {
            log.error(String.format("Failed to confirm registration %s", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.details.confirm.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void reject() {
        try {
            registration = registrationService.reject(registration.getToken(), MANUALLY_REJECTED);
            basicInfoTab.setRegistration(registration);
            setActionsVisible();
            UIUtils.showNotification(getTranslation("registration.details.decline.successful-msg"));
        } catch (Exception ex) {
            log.error(String.format("Failed to reject registration %s", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.details.decline.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void delete() {
        try {
            registrationService.delete(registration.getToken());
            UIUtils.showNotification(getTranslation("registration.delete.successful-msg"));
            close(registration);
        } catch (Exception ex) {
            log.error(String.format("Failed to delete registration %s", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.details.delete.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void resetToManualReview() {
        try {
            registration = registrationService.resetToManualReview(registration.getToken());
            basicInfoTab.setRegistration(registration);
            setActionsVisible();
            UIUtils.showNotification(getTranslation("registration.reset-to-manual-review.successful-msg"));
        } catch (Exception ex) {
            log.error(String.format("Failed to reset registration %s to manual review", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.reset-to-manual-review.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void resend() {
        try {
            userAdService.setLoginEmailSent(registration.getUser().getRecentUserAd().get(), false);
            UIUtils.showNotification(getTranslation("registration.resend.successful-msg"));
            close(registration);
        } catch (Exception ex) {
            log.error(String.format("Failed to send activation for registration %s", registration.getToken()), ex);
            UIUtils.showErrorNotification(getTranslation("registration.details.delete.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    private void setActionsVisible() {
        btnConfirm.setVisible(registration.getStatus() == MANUAL_REVIEW_PENDING);
        btnDecline.setVisible(registration.getStatus() == MANUAL_REVIEW_PENDING ||
                registration.getStatus() == OPT_IN_PENDING);
        btnConfirmByUser.setVisible(registration.getStatus() == OPT_IN_PENDING);
        btnResend.setVisible((registration.getStatus() == AUTO_CONFIRMED_MEMBER ||
                registration.getStatus() == AUTO_CONFIRMED_NON_MEMBER ||
                registration.getStatus() == MANUALLY_CONFIRMED) &&
                registration.getUser() != null
                && registration.getUser().getRecentUserAd().isPresent());
        btnResetToManualCheck.setVisible(registration.getStatus() == AUTO_REJECTED ||
                registration.getStatus() == MANUALLY_REJECTED);
    }

    static String getTabLabel(RegistrationData registration) {
        return UI.getCurrent().getTranslation("registration.details.registration") + ": " + registration.getFullName();
    }

    static String getNaviParam(RegistrationData registration) {
        return registration.getToken();
    }

    public static void close(RegistrationData registration) {
        ParamNaviTab.close(RegistrationDetails.class, getNaviParam(registration));
    }
}
