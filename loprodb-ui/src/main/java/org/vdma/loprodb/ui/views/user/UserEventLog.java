package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import java.time.format.DateTimeFormatter;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.util.css.FlexDirection;

public class UserEventLog extends FlexBoxLayout {

    public static final String EVENT_DATE_PROP = "eventDate";
    private final Long userId;
    private final IEventLogService eventLogService;

    private ObjectListParam filter = new ObjectListParam();
    private ConfigurableFilterDataProvider<EventLogData, Void, ObjectListParam> dataProvider;

    public UserEventLog(Long userId, IEventLogService eventLogService) {
        this.userId = userId;
        this.eventLogService = eventLogService;

        setFlexDirection(FlexDirection.COLUMN);
        setBoxSizing(BoxSizing.BORDER_BOX);
        setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setHeightFull();

        buildContent();
    }

    private void onSearch(String term) {
        filter.setSearchTerm(term);
        dataProvider.setFilter(filter);
    }

    private void buildContent() {
        TextField header = UIUtils.createSearchTextField(this::onSearch);
        UIUtils.setMargin(header, Bottom.RESPONSIVE_M);

        add(header, buildGrid());
    }

    private Grid buildGrid() {
        dataProvider = SpringDataProviderBuilder.userEventLog(userId, eventLogService).build().withConfigurableFilter();
        dataProvider.setFilter(filter);

        Grid<EventLogData> grid = new Grid<>();
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();

        Grid.Column<EventLogData> eventDateColumn =
                grid.addColumn(new LocalDateTimeRenderer<>(EventLogData::getEventDate,
                        DateTimeFormatter.ofPattern(UIUtils.DATE_TIME_FORMAT_PATTERN)))
                        .setAutoWidth(true)
                        .setFrozen(true)
                        .setSortProperty(EVENT_DATE_PROP)
                        .setHeader(getTranslation("user.details.audit.date-time"));

        grid.addColumn(EventLogData::getAppName)
                .setAutoWidth(true)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.app"));

        grid.addColumn(EventLogData::getMessage)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.app-message"));

        grid.sort(new GridSortOrderBuilder<EventLogData>().thenDesc(eventDateColumn).build());

        return grid;
    }
}
