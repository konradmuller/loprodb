package org.vdma.loprodb.ui.views;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.param.ReferenceListParam;

public class ReferenceComboBox extends ComboBox<ReferenceData> {

    public ReferenceComboBox(IReferenceService referenceService, RefType refType) {
        String sortProperty;
        if (getLocale().getLanguage().equalsIgnoreCase("de")) {
            sortProperty = "nameDe";
        } else {
            sortProperty = "nameEn";
        }
        ConfigurableFilterDataProvider<ReferenceData, String, ReferenceListParam> provider =
                SpringDataProviderBuilder.references(referenceService)
                        .withDefaultSort(sortProperty, SortDirection.ASCENDING)
                        .build()
                        .withConfigurableFilter((text, filter) -> {
                            filter.setSearchTerm(text);
                            return filter;
                        });
        provider.setFilter(new ReferenceListParam(refType));

        setDataProvider(provider);
        setItemLabelGenerator(ref -> ref.getNameByLocale(getLocale()));
    }
}
