package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.dto.UserData;
import static org.vdma.loprodb.event.user.UpdateSource.LOPRODB;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.navigation.bar.AppBar;
import org.vdma.loprodb.ui.components.navigation.tab.HasChanges;
import org.vdma.loprodb.ui.components.navigation.tab.ParamNaviTab;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.ParamRoute;
import org.vdma.loprodb.ui.views.SplitViewFrame;
import org.vdma.loprodb.ui.views.registration.RegistrationDetails;

@PageTitle("User details")
@Route(value = "users", layout = MainLayout.class)
@CssImport("./styles/user-details.css")
@CssImport(value = "./styles/components/form-layout.css", themeFor = "vaadin-form-layout")
@Slf4j
public class UserDetails extends SplitViewFrame implements HasUrlParameter<String>,
        ParamRoute<String, UserDetails>, HasChanges, EqualCmp {

    public static final String HEADER_CLASS_NAME = "user-details-header";
    public static final String FORM_CLASS_NAME = "user-props-form";
    public static final String FORM_THEME = "user-props-form";

    private static final String BASIC_INFO_TAB = "user-basic-info";
    private static final String COMPANY_INFO_TAB = "user-company-info";
    private static final String APP_ROLES_TAB = "user-app-roles";
    private static final String TOPICS_TAB = "user-topics";
    private static final String AUDIT_TRAIL_TAB = "user-audit-trail";
    private static final String EVENT_LOG_TAB = "user-event-log";
    private static final String MS_ACCOUNT_TAB = "user-ms-account";

    private int tab = -1;

    private UserData user;
    private Binder<UserData> binder;
    private Binder<SmbContactData> sambaBinder;

    private UserBasicInfo basicInfoTab;
    private UserCompanyInfo companyInfoTab;
    private UserApplicationRoles applicationRolesTab;
    private UserTopics topicsTab;
    private UserAuditTrail auditTrailTab;
    private UserEventLog eventLogTab;
    private UserMsAccountInfo msAccountInfoTab;

    private SmbContactUiFetcher contactUiFetcher;

    private final IApplicationService appService;
    private final IUserService userService;
    private final IReferenceService referenceService;
    private final AuditService auditService;
    private final IEventLogService eventLogService;
    private final SmbContactService smbContactService;
    private final IRegistrationService registrationService;
    private final UserMapper userMapper;

    @Autowired
    public UserDetails(IApplicationService appService, IUserService userService, IReferenceService referenceService,
                       AuditService auditService, IEventLogService eventLogService, SmbContactService smbContactService,
                       UserMapper userMapper, IRegistrationService registrationService) {
        this.appService = appService;
        this.userService = userService;
        this.referenceService = referenceService;
        this.auditService = auditService;
        this.eventLogService = eventLogService;
        this.smbContactService = smbContactService;
        this.userMapper = userMapper;
        this.registrationService = registrationService;
        setId("user_details");
    }

    @Override
    public void setParameter(BeforeEvent event, String userExternalId) {
        user = userService.getUser(userExternalId);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        if (isContentEmpty()) {
            contactUiFetcher = new SmbContactUiFetcher(attachEvent.getUI(), smbContactService);
            buildContent();
        }
        initAppBar();
    }

    private void buildContent() {
        setViewFooter(buildActions());

        binder = new Binder<>();
        sambaBinder = new Binder<>();
        basicInfoTab = new UserBasicInfo(binder, sambaBinder, user, userService, referenceService, contactUiFetcher);
        companyInfoTab = new UserCompanyInfo(binder, sambaBinder);
        applicationRolesTab = new UserApplicationRoles(appService, user, eventLogService, binder);
        topicsTab = new UserTopics(binder);
        auditTrailTab = new UserAuditTrail(user.getId(), userMapper, auditService);
        eventLogTab = new UserEventLog(user.getId(), eventLogService);
        msAccountInfoTab = new UserMsAccountInfo(user.getRecentUserAd().orElse(null));
        binder.readBean(user);
        fetchSambaContact(user.getSambaId());
    }

    private void fetchSambaContact(Long contactId) {
        sambaBinder.setBean(null);
        contactUiFetcher.fetch(contactId, sambaBinder::setBean);
    }

    private FlexBoxLayout buildActions() {
        Button save = UIUtils.createPrimaryButton(getTranslation("commons.save"));
        save.addClickListener(this::save);
        UIUtils.setMargin(save, Left.S);
        Button cancel = UIUtils.createOutlinedButton(getTranslation("commons.cancel"));
        cancel.addClickListener(event -> {
            binder.readBean(user);
            fetchSambaContact(user.getSambaId());
            applicationRolesTab.setUser(user);
        });
        Button delete = UIUtils.createErrorPrimaryButton(getTranslation("user.details.delete-user"));
        delete.addClickListener(event -> {
            ConfirmDialog
                    .createQuestion()
                    .withCaption(getTranslation("commons.confirm-deletion"))
                    .withMessage(getTranslation("user.confirm-deletion-msg"))
                    .withOkButton(this::delete, ButtonOption.focus(),
                            ButtonOption.caption(getTranslation("commons.yes")))
                    .withCancelButton(ButtonOption.caption(getTranslation("commons.no")))
                    .open();
        });
        delete.addClassName(LumoStyles.Margin.Horizontal.AUTO);

        FlexBoxLayout rightActions = new FlexBoxLayout(delete, cancel, save);
        rightActions.setAlignItems(FlexComponent.Alignment.CENTER);
        rightActions.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        rightActions.setBoxSizing(BoxSizing.BORDER_BOX);

        Button updateEvent = UIUtils.createOutlinedButton(getTranslation("user.details.update-event"));
        Div leftActions = new Div(updateEvent);

        FlexBoxLayout actions = new FlexBoxLayout(leftActions, rightActions);
        actions.setWidthFull();
        actions.setBoxSizing(BoxSizing.BORDER_BOX);
        actions.setAlignItems(FlexComponent.Alignment.CENTER);
        actions.setFlexGrow(1, leftActions, rightActions);
        actions.setPadding(Horizontal.RESPONSIVE_X);
        return actions;
    }

    private void initAppBar() {
        AppBar appBar = MainLayout.get().getAppBar();
        appBar.addTab(getTranslation("user.details.basic-info")).setId(BASIC_INFO_TAB);
        appBar.addTab(getTranslation("user.details.company-info")).setId(COMPANY_INFO_TAB);
        appBar.addTab(getTranslation("user.details.apps-roles")).setId(APP_ROLES_TAB);
        appBar.addTab(getTranslation("user.details.topics")).setId(TOPICS_TAB);
        appBar.addTab(getTranslation("user.details.audit")).setId(AUDIT_TRAIL_TAB);
        appBar.addTab(getTranslation("user.details.event-log")).setId(EVENT_LOG_TAB);
        appBar.addTab(getTranslation("user.details.ms-account")).setId(MS_ACCOUNT_TAB);
        appBar.getTabs().setSelectedIndex(tab);
        appBar.addTabSelectionListener(e -> {
            clearViewDetails();
            String selectedTabId = MainLayout.get().getAppBar().getSelectedTab().getId().get();
            switch (selectedTabId) {
                case COMPANY_INFO_TAB:
                    tab = 1;
                    setViewContent(companyInfoTab);
                    break;
                case APP_ROLES_TAB:
                    tab = 2;
                    setViewContent(applicationRolesTab);
                    break;
                case TOPICS_TAB:
                    tab = 3;
                    setViewContent(topicsTab);
                    break;
                case AUDIT_TRAIL_TAB:
                    tab = 4;
                    setViewContent(auditTrailTab);
                    setViewDetails(auditTrailTab.getDetailsDrawer());
                    break;
                case EVENT_LOG_TAB:
                    tab = 5;
                    setViewContent(eventLogTab);
                    break;
                case MS_ACCOUNT_TAB:
                    tab = 6;
                    setViewContent(msAccountInfoTab);
                    break;
                default:
                    tab = 0;
                    setViewContent(basicInfoTab);
            }
        });
        if (tab < 0) {
            tab = 0;
            appBar.getTabs().setSelectedIndex(tab);
        }
        appBar.centerTabs();
    }

    private void save(ClickEvent<Button> event) {
        Long sambaIdBefore = user.getSambaId();
        if (binder.writeBeanIfValid(user)) {
            try {
                user.setApps(applicationRolesTab.getRoles());
                if (!Objects.equals(sambaIdBefore, user.getSambaId())) {
                    // samba ID changed
                    user.setSambaContactVersion(null);
                }
                user = userService.update(user.getExternalId(), user, LOPRODB);
                binder.readBean(user);
                applicationRolesTab.setUser(user);
                UIUtils.showNotification(getTranslation("commons.changes-saved-msg"));
            } catch (Exception ex) {
                log.error(String.format("Failed to save user %s", user.getExternalId()), ex);
                UIUtils.showErrorNotification(getTranslation("user.details.save.failed-msg",
                        ex.getMessage()), ExceptionUtils.getStackTrace(ex));
            }
        } else {
            UIUtils.showNotification(getTranslation("commons.validation.error-msg"));
        }
    }

    private void delete() {
        try {
            registrationService.getRegistrationByUser(user.getExternalId()).ifPresent(RegistrationDetails::close);
            close(user);
            userService.delete(user.getExternalId());
            UIUtils.showNotification(getTranslation("user.deletion-successful-msg"));
        } catch (Exception ex) {
            log.error(String.format("Failed to delete user %s", user.getExternalId()), ex);
            UIUtils.showErrorNotification(getTranslation("user.details.delete.failed-msg",
                    ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
    }

    @Override
    public boolean hasChanges() {
        return binder.hasChanges();
    }

    @Override
    public Class<UserDetails> getRouteType() {
        return UserDetails.class;
    }

    @Override
    public String getRouteParam() {
        return getNaviParam(user);
    }

    @Override
    public String getTabLabel() {
        return getTabLabel(user);
    }

    @Override
    public boolean eq(HasElement other) {
        return EqualCmp.super.eq(other) && ((UserDetails) other).user.getExternalId().equals(user.getExternalId());
    }

    public static String getTabLabel(UserData user) {
        return user.getFullName();
    }

    public static String getNaviParam(UserData user) {
        return user.getExternalId();
    }

    public static void close(UserData user) {
        ParamNaviTab.close(UserDetails.class, getNaviParam(user));
    }
}
