package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.backend.providers.SpringDataProviderBuilder;
import org.vdma.loprodb.dto.AppMessageData;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.dto.UserAppData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

class UserApplicationRoles extends FlexBoxLayout {

    private final IApplicationService appService;
    private final IEventLogService eventLogService;
    private final Binder<UserData> binder;

    private Grid<ApplicationData> grid;

    private Map<ApplicationData, AppAccess> accessMap = new HashMap<>();

    private UserData user;

    private Set<UserAppData> apps;

    private List<AppMessageData> appMessages;

    UserApplicationRoles(IApplicationService appService, UserData user, IEventLogService eventLogService,
            Binder<UserData> binder) {
        this.appService = appService;
        this.eventLogService = eventLogService;
        this.binder = binder;
        setUser(user);

        setFlexDirection(FlexDirection.COLUMN);
        setBoxSizing(BoxSizing.BORDER_BOX);
        setPadding(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setHeightFull();

        buildContent();
    }

    Set<UserAppData> getRoles() {
        // if tab wasn't opened then return current state of apps and roles
        if (accessMap.isEmpty()) {
            return apps;
        }
        return accessMap.values().stream().filter(AppAccess::isAccessAllowed)
                .map(access -> {
                    UserAppData userAppData = access.getUserAppData();
                    userAppData.setRoles(
                            access.getRoleCheckboxesMap().entrySet().stream()
                                    .filter(roleEntry -> roleEntry.getValue().getValue())
                                    .map(Map.Entry::getKey)
                                    .collect(Collectors.toSet()));
                    return userAppData;
                }).collect(Collectors.toSet());
    }

    public void setUser(UserData user) {
        this.user = user;
        this.apps = new HashSet<>(user.getApps());
        accessMap.clear();
        appMessages = null;
        if (grid != null) {
            grid.getDataProvider().refreshAll();
        }
    }

    private void buildContent() {
        Checkbox chbSyncVdmaRoles = new Checkbox(getTranslation("user.details.apps.sync-vdma-roles"));
        UIUtils.setMargin(chbSyncVdmaRoles, Bottom.RESPONSIVE_M);
        binder.forField(chbSyncVdmaRoles).bind(UserData::isVdmaRolesSynchronized, UserData::setVdmaRolesSynchronized);

        add(chbSyncVdmaRoles, buildGrid());
    }

    private Grid buildGrid() {
        DataProvider<ApplicationData, ObjectListParam> dataProvider =
                SpringDataProviderBuilder.applications(appService).withDefaultFilter(new ObjectListParam()).build();

        grid = new Grid<>();
        grid.setDataProvider(dataProvider);
        grid.setHeightFull();
        grid.setWidthFull();
        grid.setSelectionMode(Grid.SelectionMode.NONE);
        grid.addColumn(ApplicationData::getName)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.app"));
        grid.addColumn(new ComponentRenderer<>(this::createItemAccess))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.access"));
        grid.addColumn(new ComponentRenderer<>(this::createItemRoles))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.roles"));
        grid.addColumn(this::createMessageItem)
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setFrozen(true)
                .setHeader(getTranslation("user.details.apps.app-message"));

        return grid;
    }

    private String createMessageItem(ApplicationData app) {
        return getAppMessages().stream().filter(m -> Objects.equals(m.getAppId(), app.getId()))
                .findFirst().map(AppMessageData::getMessage).orElse("");
    }

    private Component createItemAccess(ApplicationData app) {
        AppAccess access = accessMap.computeIfAbsent(app, this::createAppAccess);

        RadioButtonGroup<String> rgAccess = new RadioButtonGroup<>();
        String yesItem = getTranslation("commons.yes");
        String noItem = getTranslation("commons.no");
        rgAccess.setItems(yesItem, noItem);
        rgAccess.setValue(access.isAccessAllowed() ? yesItem : noItem);
        rgAccess.addValueChangeListener(event -> {
            boolean accessAllowed = yesItem.equals(event.getValue());
            access.getRoleCheckboxesMap().forEach((role, checkbox) -> {
                checkbox.setEnabled(accessAllowed);
                if (!accessAllowed) {
                    checkbox.setValue(false);
                }
            });
            access.setAccessAllowed(accessAllowed);
        });

        return rgAccess;
    }

    private AppAccess createAppAccess(ApplicationData app) {
        AppAccess access = new AppAccess();
        Optional<UserAppData> appData = apps.stream()
                .filter(userAppData -> userAppData.getAppId().equals(app.getId()))
                .findFirst();
        access.setAccessAllowed(appData.isPresent());
        access.setUserAppData(appData.orElseGet(() -> new UserAppData(app.getId())));
        return access;
    }

    private Component createItemRoles(ApplicationData app) {
        AppAccess access = accessMap.computeIfAbsent(app, this::createAppAccess);
        FlexBoxLayout c = new FlexBoxLayout();
        c.setFlexDirection(FlexDirection.COLUMN);
        app.getRoles().forEach(role -> {
            Checkbox chbRole = new Checkbox(role.getName());
            chbRole.setEnabled(access.isAccessAllowed());

            Checkbox old = access.getRoleCheckboxesMap().get(role);
            if (old != null) {
                chbRole.setValue(old.getValue());
            } else {
                chbRole.setValue(access.getUserAppData().getRoles().stream()
                        .anyMatch(ur -> ur.getId().equals(role.getId())));
            }
            accessMap.get(app).getRoleCheckboxesMap().put(role, chbRole);
            c.add(chbRole);
        });
        return c;
    }

    private List<AppMessageData> getAppMessages() {
        if (appMessages == null) {
            appMessages = eventLogService.getLastAppMessagesForUser(user.getId());
        }
        return appMessages;
    }

    @Getter
    @Setter
    private static class AppAccess {
        private UserAppData userAppData;
        private boolean accessAllowed;
        private Map<AppRoleData, Checkbox> roleCheckboxesMap = new HashMap<>();
    }
}
