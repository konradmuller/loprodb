package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.UI;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.util.css.lumo.BadgeColor;

public class UserStatusBadge {

    private UserStatusBadge() {
    }

    public static Badge createForStatus(UserStatus status) {
        if (status == null) {
            return null;
        }
        BadgeColor badgeColor;
        switch (status) {
        case BLOCKED:
            badgeColor = BadgeColor.ERROR;
            break;
        case INACTIVE:
            badgeColor = BadgeColor.NORMAL;
            break;
        default:
            badgeColor = BadgeColor.SUCCESS;
        }

        return new Badge(UI.getCurrent().getTranslation(UserStatus.class.getSimpleName() + "." + status.name()),
                badgeColor);
    }
}
