package org.vdma.loprodb.ui.components.navigation.bar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.HasUrlParameter;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.navigation.tab.HasChanges;
import org.vdma.loprodb.ui.components.navigation.tab.NaviTabs;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.views.Home;

@CssImport("./styles/components/tab-bar.css")
public class TabBar extends FlexBoxLayout {

    private String CLASS_NAME = "tab-bar";

    private Button menuIcon;
    private NaviTabs tabs;
    private Button addTab;

    public TabBar(UserData currentUser, HasChanges contentDelegate) {
        setClassName(CLASS_NAME);

        menuIcon = UIUtils.createTertiaryInlineButton(VaadinIcon.MENU);
        menuIcon.addClassName(CLASS_NAME + "__navi-icon");
        menuIcon.addClickListener(e -> MainLayout.get().getNaviDrawer().toggle());

        Div avatar = new Div();
        avatar.setClassName(CLASS_NAME + "__avatar");
        avatar.setText(currentUser.getInitials());

        ContextMenu contextMenu = new ContextMenu(avatar);
        contextMenu.setOpenOnClick(true);
        contextMenu.addItem("Log Out", e -> {
            UI.getCurrent().getPage().setLocation("/logout");
        });

        addTab = UIUtils.createSmallButton(VaadinIcon.PLUS);
        addTab.addClickListener(e -> tabs.setSelectedTab(addTab("New Tab", Home.class, true)));
        addTab.setClassName(CLASS_NAME + "__add-tab");

        tabs = new NaviTabs(contentDelegate);
        tabs.setClassName(CLASS_NAME + "__tabs");

        add(menuIcon, tabs, addTab, avatar);
    }

    /* === MENU ICON === */

    public Button getMenuIcon() {
        return menuIcon;
    }

    /* === TABS === */

    public void centerTabs() {
        tabs.addClassName(LumoStyles.Margin.Horizontal.AUTO);
    }

    private void configureTab(Tab tab) {
        tab.addClassName(CLASS_NAME + "__tab");
    }

    public Tab addTab(String text) {
        Tab tab = tabs.addTab(text);
        configureTab(tab);
        return tab;
    }

    public Tab addTab(String text, Class<? extends Component> navigationTarget, boolean closable) {
        Tab tab = tabs.addTab(text, navigationTarget, closable);
        configureTab(tab);
        return tab;
    }

    public <T, C extends Component & HasUrlParameter<T>> Tab addTab(String text, Class<? extends C> navigationTarget,
            T param, boolean closable) {
        Tab tab = tabs.addTab(text, navigationTarget, param, closable);
        configureTab(tab);
        return tab;
    }

    public Tab getSelectedTab() {
        return tabs.getSelectedTab();
    }

    public void closeSelectedTab() {
        tabs.closeSelectedTab();
    }

    public void setSelectedTab(Tab selectedTab) {
        tabs.setSelectedTab(selectedTab);
    }

    public void closeTab(Predicate<Tab> predicate) {
        tabs.closeTab(predicate);
    }

    public Stream<Tab> getTabs() {
        return tabs.getTabs();
    }

    public void updateSelectedTab(String text,
            Class<? extends Component> navigationTarget) {
        tabs.updateSelectedTab(text, navigationTarget);
    }

    public void addTabSelectionListener(
            ComponentEventListener<Tabs.SelectedChangeEvent> listener) {
        tabs.addSelectedChangeListener(listener);
    }

    public int getTabCount() {
        return tabs.getTabCount();
    }

    public void removeAllTabs() {
        tabs.removeAll();
    }

    /* === ADD TAB BUTTON === */

    public void setAddTabVisible(boolean visible) {
        addTab.setVisible(visible);
    }
}
