package org.vdma.loprodb.ui.views.registration.components;

import com.vaadin.flow.component.UI;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.ui.components.Badge;
import org.vdma.loprodb.ui.util.css.lumo.BadgeColor;

public class RegistrationStatusBadge {

    private RegistrationStatusBadge() {
    }

    public static Badge createForStatus(RegistrationStatus status) {
        if (status == null) {
            return null;
        }
        String badgeText = UI.getCurrent().getTranslation(
                RegistrationStatus.class.getSimpleName() + "." + status.name());
        Badge badge;
        switch (status) {
        case OPT_IN_PENDING:
            badge = new Badge(badgeText, BadgeColor.CONTRAST);
            badge.getElement().getStyle().set("background-color", "var(--lumo-contrast-20pct)");
            return badge;
        case MANUAL_REVIEW_PENDING:
            return new Badge(badgeText, BadgeColor.SUCCESS_PRIMARY);
        case AUTO_REJECTED:
        case MANUALLY_REJECTED:
            return new Badge(badgeText, BadgeColor.ERROR_PRIMARY);
        case AUTO_CONFIRMED_MEMBER:
        case AUTO_CONFIRMED_NON_MEMBER:
        case MANUALLY_CONFIRMED:
            return new Badge(badgeText, BadgeColor.NORMAL_PRIMARY);
        default:
            return new Badge(badgeText, BadgeColor.ERROR);
        }
    }
}
