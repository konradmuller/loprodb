package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.textfield.TextField;
import static java.util.Optional.ofNullable;
import lombok.extern.slf4j.Slf4j;
import org.vdma.loprodb.dto.UserAdData;
import org.vdma.loprodb.entity.CreationType;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.layout.size.Vertical;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;
import static org.vdma.loprodb.ui.views.user.components.UserPropFormItem.THEME;

@Slf4j
class UserMsAccountInfo extends FlexBoxLayout {

    private final UserAdData userAd;

    UserMsAccountInfo(UserAdData userAd) {
        this.userAd = userAd;
        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);
        buildContent();
    }

    private void buildContent() {

        FormLayout form = new FormLayout();
        UIUtils.setTheme(FORM_THEME, form);
        form.setWidthFull();
        UIUtils.setPadding(form, Bottom.L, Top.S);
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        if (userAd != null) {
            form.add(getPropertyComponent("user.details.ms-account.ad-user-id", userAd.getAdUserId()));
            form.add(getPropertyComponent("user.details.ms-account.ad-principal-name", userAd.getAdUserPrincipalName()));
            form.add(getPropertyComponent("user.details.ms-account.created-date", UIUtils.formatDate(userAd.getCreatedDate())));
            form.add(getPropertyComponent("user.details.ms-account.email", userAd.getEmail()));
            form.add(getPropertyComponent("user.details.ms-account.creation-type", 
                    ofNullable(userAd.getCreationType()).map(CreationType::name).orElse("")));
        } else {
            form.add(new Label(getTranslation("user.details.ms-account.no-info-found")));
        }

        add(form);
    }

    private Component getPropertyComponent(String key, String value) {

        Label label = new Label(getTranslation(key));
        label.setWidth("30%");

        TextField field = new TextField();
        field.setValue(value);
        field.setWidth("70%");
        field.setReadOnly(true);

        FlexBoxLayout row = new FlexBoxLayout();
        row.add(label);
        row.add(field);
        row.setBoxSizing(BoxSizing.BORDER_BOX);
        row.setSpacing(Right.XL);
        row.setPadding(Horizontal.S, Vertical.XS);
        row.setAlignItems(FlexComponent.Alignment.CENTER);
        UIUtils.setTheme(THEME, this);

        return row;
    }
}
