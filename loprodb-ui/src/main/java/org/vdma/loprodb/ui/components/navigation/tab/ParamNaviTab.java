package org.vdma.loprodb.ui.components.navigation.tab;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.HasUrlParameter;
import java.util.Objects;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.ui.MainLayout;

@Getter
@EqualsAndHashCode(of = { "navigationTarget", "param" }, callSuper = false)
public class ParamNaviTab<T, C extends Component & HasUrlParameter<T>> extends BaseNaviTab {

    private Class<? extends C> navigationTarget;

    @Setter
    private T param;

    public ParamNaviTab(String label, Class<? extends C> navigationTarget, T param) {
        super(label);
        setNavigationTarget(navigationTarget);
        this.param = param;
    }

    public void setNavigationTarget(Class<? extends C> navigationTarget) {
        this.navigationTarget = Objects.requireNonNull(navigationTarget);
    }

    @Override
    public void navigate() {
        UI.getCurrent().navigate(navigationTarget, param);
    }

    public static void close(Class navigationTarget, Object param) {
        MainLayout.get().getTabBar().closeTab(tab -> {
            if (tab instanceof ParamNaviTab) {
                ParamNaviTab paramNaviTab = (ParamNaviTab) tab;
                return paramNaviTab.getNavigationTarget() == navigationTarget &&
                        Objects.equals(paramNaviTab.getParam(), param);
            } else {
                return false;
            }
        });
    }
}
