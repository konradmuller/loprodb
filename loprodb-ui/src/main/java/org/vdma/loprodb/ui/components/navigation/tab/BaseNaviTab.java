package org.vdma.loprodb.ui.components.navigation.tab;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import lombok.Getter;
import org.vdma.loprodb.ui.util.FontSize;
import org.vdma.loprodb.ui.util.UIUtils;

@Getter
public abstract class BaseNaviTab extends Tab {

    private final Button closeButton;

    public BaseNaviTab(String label) {
        super(label);

        closeButton = UIUtils.createButton(VaadinIcon.CLOSE, ButtonVariant.LUMO_TERTIARY_INLINE);
        // ButtonVariant.LUMO_SMALL isn't small enough.
        UIUtils.setFontSize(FontSize.XXS, closeButton);
        add(closeButton);

        closable(true);
    }

    public void closable(boolean closable) {
        closeButton.setVisible(closable);
        if (closable) {
            getElement().setAttribute("closable", true);
        } else {
            getElement().removeAttribute("closable");
        }
    }

    public abstract void navigate();
}
