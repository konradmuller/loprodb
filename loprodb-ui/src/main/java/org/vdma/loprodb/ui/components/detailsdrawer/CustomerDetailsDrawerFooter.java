package org.vdma.loprodb.ui.components.detailsdrawer;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.shared.Registration;
import org.vdma.loprodb.ui.util.UIUtils;

public class CustomerDetailsDrawerFooter extends DetailsDrawerFooter {

    private MenuItem removeItem;
    private MenuItem duplicate;
    private MenuBar menuBarMore;

    @Override
    protected Component addMoreActions() {
        menuBarMore = UIUtils.createMenuBar(true);
        MenuItem more = menuBarMore.addItem("More");
        duplicate = more.getSubMenu().addItem("Duplicate...");
        removeItem = more.getSubMenu().addItem("Delete...");
        return menuBarMore;
    }

    public Registration addDuplicateListener(ComponentEventListener<ClickEvent<MenuItem>> listener) {
        return duplicate.addClickListener(listener);
    }

    @Override
    public Registration addRemoveListener(Runnable listener) {
        return removeItem.addClickListener(event -> listener.run());
    }

    public void setDuplicateVisible(boolean visible) {
        duplicate.setVisible(visible);
    }

    public void setMoreVisible(boolean visible) {
        menuBarMore.setVisible(visible);
    }

    @Override
    public void setRemoveVisible(boolean visible) {
        removeItem.setVisible(visible);
    }
}
