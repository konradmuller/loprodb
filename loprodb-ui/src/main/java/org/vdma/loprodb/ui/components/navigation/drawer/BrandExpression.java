package org.vdma.loprodb.ui.components.navigation.drawer;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import org.vdma.loprodb.ui.util.UIUtils;

@CssImport("./styles/components/brand-expression.css")
public class BrandExpression extends Div {

    private static final String DEFAULT_LOGO = UIUtils.IMG_PATH + "logo-with-title.svg";
    private static final String RAIL_LOGO = UIUtils.IMG_PATH + "logo.svg";

    private String CLASS_NAME = "brand-expression";

    private Image logo;

    public BrandExpression(String text) {
        setClassName(CLASS_NAME);

        logo = new Image(DEFAULT_LOGO, "");
        logo.setAlt(text + " logo");
        logo.setClassName(CLASS_NAME + "__logo");

        add(logo);
    }

    public void toggleRailMode(boolean rail) {
        if (rail) {
            logo.setSrc(RAIL_LOGO);
        } else {
            logo.setSrc(DEFAULT_LOGO);
        }
    }

}
