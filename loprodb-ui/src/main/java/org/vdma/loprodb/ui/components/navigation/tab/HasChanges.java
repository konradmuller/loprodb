package org.vdma.loprodb.ui.components.navigation.tab;

public interface HasChanges {
    boolean hasChanges();
}
