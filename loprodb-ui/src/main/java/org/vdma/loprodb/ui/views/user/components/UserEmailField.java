package org.vdma.loprodb.ui.views.user.components;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.EmailValidator;
import java.util.function.Predicate;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.ui.components.CustomDialog;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;

public class UserEmailField extends EmailField {

    private final Predicate<String> uniqueValidator;
    private final boolean safe;

    public UserEmailField(Predicate<String> uniqueValidator, boolean safe) {
        this.uniqueValidator = uniqueValidator;
        this.safe = safe;
        if (safe) {
            setReadOnly(true);
            Icon edit = UIUtils.createPrimaryIcon(VaadinIcon.EDIT);
            edit.addClickListener(event -> openEditEmailDlg(this));
            setSuffixComponent(edit);
        }
    }

    public void bind(Binder<UserData> binder) {
        Binder.BindingBuilder<UserData, String> builder = binder.forField(this).asRequired();
        if (!safe) {
            builder.withValidator(new EmailValidator(getTranslation("commons.validation.invalid-email")))
                    .withValidator(uniqueValidator::test, getTranslation("commons.validation.used"));
        }
        builder.bind(UserData::getEmail, UserData::setEmail);
    }

    private void openEditEmailDlg(EmailField origin) {
        UserData userData = new UserData();
        userData.setEmail(origin.getValue());
        Binder<UserData> emailBinder = new Binder<>();

        UserEmailField efEmail = new UserEmailField(uniqueValidator, false);
        efEmail.setWidthFull();
        efEmail.bind(emailBinder);

        emailBinder.readBean(userData);

        FormLayout formLayout = new FormLayout();
        formLayout.addClassNames(LumoStyles.Padding.Horizontal.L, LumoStyles.Padding.Top.S);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1,
                FormLayout.ResponsiveStep.LabelsPosition.TOP));

        UIUtils.asRequired(formLayout.addFormItem(efEmail, getTranslation("user.details.basic.email")));

        CustomDialog dialog = new CustomDialog();
        dialog.setWidth("300px");
        dialog.addCancelListener(event -> dialog.close());
        dialog.addOkListener(event -> {
            if (emailBinder.writeBeanIfValid(userData)) {
                origin.setValue(userData.getEmail());
                dialog.close();
            }
        });
        dialog.setContent(formLayout);
        dialog.open();
    }
}
