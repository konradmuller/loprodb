package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Image;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.vdma.loprodb.ui.util.UIUtils;

public class ImageField extends CustomField<String> {

    private Image image;

    @Getter
    @Setter
    private String noImagePath = UIUtils.NO_IMG;

    public ImageField() {
        image = new Image(noImagePath, "");
        add(image);
        UIUtils.setLineHeight("0", this);
    }

    @Override
    protected String generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(String newPresentationValue) {
        if (StringUtils.isNotBlank(newPresentationValue)) {
            image.setSrc(newPresentationValue);
        } else {
            image.setSrc(noImagePath);
        }
    }
}
