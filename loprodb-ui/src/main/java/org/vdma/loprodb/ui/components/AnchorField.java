package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Anchor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

public class AnchorField extends CustomField<String> {

    @Getter
    private Anchor anchor;

    public AnchorField(Component... components) {
        anchor = new Anchor();
        anchor.setTarget("_blank");
        anchor.add(components);
        add(anchor);
    }

    @Override
    protected String generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(String newPresentationValue) {
        anchor.setHref(StringUtils.defaultIfBlank(newPresentationValue, "#"));
    }
}
