package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;

public class Clickable extends Div implements ClickNotifier<Div> {

    public Clickable(Component... components) {
        super(components);
    }
}
