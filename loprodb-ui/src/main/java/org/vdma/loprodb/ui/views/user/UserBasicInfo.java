package org.vdma.loprodb.ui.views.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import java.util.Collections;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.ImageField;
import org.vdma.loprodb.ui.components.ImageUploader;
import org.vdma.loprodb.ui.components.LabelField;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Horizontal;
import org.vdma.loprodb.ui.layout.size.Left;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.StringNotBlankValidator;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;
import org.vdma.loprodb.ui.views.ReferenceComboBox;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_CLASS_NAME;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;
import static org.vdma.loprodb.ui.views.user.UserDetails.HEADER_CLASS_NAME;
import org.vdma.loprodb.ui.views.user.components.BindSmbContactDlg;
import org.vdma.loprodb.ui.views.user.components.SmbAddressStatusField;
import org.vdma.loprodb.ui.views.user.components.UserEmailField;
import org.vdma.loprodb.ui.views.user.components.UserPropFormItem;
import org.vdma.loprodb.ui.views.user.components.UserStatusField;

@Slf4j
class UserBasicInfo extends FlexBoxLayout {

    private static final String PHOTO_WIDTH = "175px";

    private final Binder<UserData> binder;
    private final Binder<SmbContactData> sambaBinder;
    private final IUserService userService;
    private final UserData user;
    private final IReferenceService referenceService;
    private final SmbContactUiFetcher contactUiFetcher;

    UserBasicInfo(Binder<UserData> binder, Binder<SmbContactData> sambaBinder, UserData user,
            IUserService userService, IReferenceService referenceService, SmbContactUiFetcher contactUiFetcher) {
        this.binder = binder;
        this.sambaBinder = sambaBinder;
        this.referenceService = referenceService;
        this.contactUiFetcher = contactUiFetcher;
        this.userService = userService;
        this.user = user;
        UIUtils.setBorder(this);
        setFlexDirection(FlexDirection.COLUMN);
        setMargin(Horizontal.RESPONSIVE_X, Top.RESPONSIVE_M);
        setPadding(Horizontal.RESPONSIVE_X);
        setBackgroundColor(LumoStyles.Color.BASE_COLOR);
        setBoxSizing(BoxSizing.BORDER_BOX);
        buildContent();
    }

    private void buildContent() {
        Button btActivate = UIUtils.createOutlinedButton(getTranslation("user.details.basic.activate"));
        UIUtils.setMargin(btActivate, Left.XS);
        Button btDeactivate = UIUtils.createOutlinedButton(getTranslation("user.details.basic.deactivate"));
        UIUtils.setMargin(btDeactivate, Left.XS);
        Button btLock = UIUtils.createOutlinedButton(getTranslation("user.details.basic.lock"));
        UIUtils.setMargin(btLock, Left.XS);

        UserStatusField userStatusField = new UserStatusField();
        UIUtils.setMargin(userStatusField, Right.AUTO);
        userStatusField.addValueChangeListener(event -> {
            btActivate.setVisible(event.getValue() != UserStatus.ACTIVE);
            btDeactivate.setVisible(event.getValue() != UserStatus.INACTIVE);
            btLock.setVisible(event.getValue() != UserStatus.BLOCKED);
        });

        btDeactivate.addClickListener(e -> userStatusField.setValue(UserStatus.INACTIVE));
        btLock.addClickListener(e -> userStatusField.setValue(UserStatus.BLOCKED));
        btActivate.addClickListener(e -> userStatusField.setValue(UserStatus.ACTIVE));

        binder.forField(userStatusField).asRequired().bind(UserData::getStatus, UserData::setStatus);

        SmbAddressStatusField smbStatus = new SmbAddressStatusField();
        smbStatus.setWidthFull();
        sambaBinder.forField(smbStatus).bind(c -> c.getStatus().name(), null);

        FlexBoxLayout fblStatus = new FlexBoxLayout(userStatusField, btActivate, btDeactivate, btLock);
        fblStatus.setJustifyContentMode(JustifyContentMode.END);
        fblStatus.setAlignSelf(FlexComponent.Alignment.CENTER, userStatusField);

        NumberField nfAimId = new NumberField();
        nfAimId.setStep(1);
        nfAimId.setMin(1);
        binder.forField(nfAimId).bind(user -> user.getAimId() == null ? null : user.getAimId().doubleValue(), null);

        TextField tfSmbAimId = new TextField();
        tfSmbAimId.setWidthFull();
        sambaBinder.forField(tfSmbAimId).bind(
                contact -> contact.getAimId() != null ? contact.getAimId().toString() : "", null);

        DatePicker dtpDeactivateOn = new DatePicker();
        dtpDeactivateOn.setLocale(Locale.GERMAN);
        dtpDeactivateOn.setClearButtonVisible(true);
        dtpDeactivateOn.setEnabled(false);
        binder.forField(dtpDeactivateOn).bind(UserData::getAutoDeactivationDate, UserData::setAutoDeactivationDate);
        Checkbox chbDeactivateOn = new Checkbox();
        chbDeactivateOn.addValueChangeListener(event -> dtpDeactivateOn.setEnabled(event.getValue()));
        binder.forField(chbDeactivateOn).bind(UserData::isAutoDeactivation, UserData::setAutoDeactivation);
        FlexBoxLayout fblDeactivate = new FlexBoxLayout(chbDeactivateOn, dtpDeactivateOn);
        fblDeactivate.setFlexGrow(1, dtpDeactivateOn);
        fblDeactivate.setAlignSelf(Alignment.CENTER, chbDeactivateOn);
        fblDeactivate.setSpacing(Right.XL);

        ComboBox<Gender> cbSalutation = new ComboBox<>(null, Gender.values());
        cbSalutation.setItemLabelGenerator(
                gender -> getTranslation(Gender.class.getSimpleName() + "." + gender.name()));
        binder.forField(cbSalutation).asRequired().bind(UserData::getGender, UserData::setGender);
        TextField tfSmbSalutation = new TextField();
        tfSmbSalutation.setWidthFull();
        sambaBinder.forField(tfSmbSalutation).bind(
                c -> c.getSalutation() != null ? c.getSalutation().getNameByLocale(getLocale()) : "", null);

        TextField tfTitle = new TextField();
        binder.forField(tfTitle).bind(UserData::getTitle, UserData::setTitle);
        TextField tfSmbTitle = new TextField();
        tfSmbTitle.setWidthFull();
        sambaBinder.forField(tfSmbTitle).bind(SmbContactData::getTitle, null);

        TextField tfFirstName = new TextField();
        binder.forField(tfFirstName).asRequired()
                .withValidator(new StringNotBlankValidator(getTranslation("commons.field.required")))
                .bind(UserData::getFirstName, UserData::setFirstName);
        TextField tfSmbFirstName = new TextField();
        tfSmbFirstName.setWidthFull();
        sambaBinder.forField(tfSmbFirstName).bind(SmbContactData::getFirstName, null);

        TextField tfLastName = new TextField();
        binder.forField(tfLastName).asRequired()
                .withValidator(new StringNotBlankValidator(getTranslation("commons.field.required")))
                .bind(UserData::getLastName, UserData::setLastName);
        TextField tfSmbLastName = new TextField();
        tfSmbLastName.setWidthFull();
        sambaBinder.forField(tfSmbLastName).bind(SmbContactData::getLastName, null);

        UserEmailField efEmail = new UserEmailField(val -> !userService.emailExists(val, user.getExternalId()), true);
        efEmail.setWidthFull();
        efEmail.bind(binder);
        TextField tfSmbEmail = new TextField();
        tfSmbEmail.setWidthFull();
        sambaBinder.forField(tfSmbEmail).bind(SmbContactData::getEmail, null);

        TextField tfPhone = new TextField();
        binder.forField(tfPhone).bind(UserData::getPhone, UserData::setPhone);
        TextField tfSmbPhone = new TextField();
        tfSmbPhone.setWidthFull();
        sambaBinder.forField(tfSmbPhone).bind(SmbContactData::getPhone, null);

        TextField tfCompanyName = new TextField();
        binder.forField(tfCompanyName).bind(u -> u.getCompany().getName(), null);
        TextField tfSmbCompanyName = new TextField();
        tfSmbCompanyName.setWidthFull();
        sambaBinder.forField(tfSmbCompanyName).bind(c -> c.getCompany().getName(), null);

        TextField tfFunction = new TextField();
        binder.forField(tfFunction).asRequired()
                .withValidator(new StringNotBlankValidator(getTranslation("commons.field.required")))
                .bind(UserData::getFunction, UserData::setFunction);
        TextField tfSmbFunction = new TextField();
        tfSmbFunction.setWidthFull();
        sambaBinder.forField(tfSmbFunction).bind(SmbContactData::getFunction, null);

        ComboBox<ReferenceData> cbPosition = new ReferenceComboBox(referenceService, RefType.USER_POSITION);
        binder.forField(cbPosition).asRequired().bind(UserData::getPosition, UserData::setPosition);
        TextField tfSmbPosition = new TextField();
        tfSmbPosition.setWidthFull();
        sambaBinder.forField(tfSmbPosition).bind(c -> {
            if (c.getAreaPositions().isEmpty()) {
                return "";
            } else {
                return c.getAreaPositions().get(0).getPosition().getNameByLocale(getLocale());
            }
        }, null);

        ComboBox<ReferenceData> cbBusinessArea = new ReferenceComboBox(referenceService, RefType.USER_BUSINESS_AREA);
        binder.forField(cbBusinessArea).asRequired().bind(UserData::getBusinessArea, UserData::setBusinessArea);
        TextField tfSmbBusinessArea = new TextField();
        tfSmbBusinessArea.setWidthFull();
        sambaBinder.forField(tfSmbBusinessArea).bind(c -> {
            if (c.getAreaPositions().isEmpty()) {
                return "";
            } else {
                return c.getAreaPositions().get(0).getArea().getNameByLocale(getLocale());
            }
        }, null);

        TextField tfDepartment = new TextField();
        binder.forField(tfDepartment).bind(UserData::getDepartment, UserData::setDepartment);
        TextField tfSmbDepartment = new TextField();
        sambaBinder.forField(tfSmbDepartment).bind(SmbContactData::getDepartment, null);

        DatePicker dtpBirthday = new DatePicker();
        dtpBirthday.setLocale(Locale.GERMAN);
        dtpBirthday.setClearButtonVisible(true);
        binder.forField(dtpBirthday).bind(UserData::getBirthday, UserData::setBirthday);
        TextField tfSmbBirthday = new TextField();
        tfSmbBirthday.setWidthFull();
        sambaBinder.forField(tfSmbBirthday).bind(sambaContact -> UIUtils.formatDate(sambaContact.getBirthday()), null);

        TextField tfPublicPhone = new TextField();
        binder.forField(tfPublicPhone).bind(UserData::getPublicPhone, UserData::setPublicPhone);
        TextField tfSmbPublicPhone = new TextField();
        sambaBinder.forField(tfSmbPublicPhone).bind(SmbContactData::getPublicPhone, null);

        TextField tfPublicMobile = new TextField();
        binder.forField(tfPublicMobile).bind(UserData::getPublicMobile, UserData::setPublicMobile);
        TextField tfSmbPublicMobile = new TextField();
        sambaBinder.forField(tfSmbPublicMobile).bind(SmbContactData::getPublicMobile, null);

        EmailField efPublicEmail = new EmailField();
        binder.forField(efPublicEmail).bind(UserData::getPublicEmail, UserData::setPublicEmail);
        TextField tfSmbPublicEmail = new TextField();
        sambaBinder.forField(tfSmbPublicEmail).bind(SmbContactData::getPublicEmail, null);

        TextField tfDsVersion = new TextField();
        tfDsVersion.setReadOnly(true);
        UIUtils.setTheme("bold", tfDsVersion);
        binder.forField(tfDsVersion).bind(UserData::getDsVersion, null);

        ComboBox<Language> cbLang = new ComboBox<>(null, Language.values());
        cbLang.setItemLabelGenerator(lang -> getTranslation(Language.class.getSimpleName() + "." + lang.name()));
        binder.forField(cbLang).asRequired().bind(UserData::getLanguage, UserData::setLanguage);

        ImageUploader imgPhoto = new ImageUploader();
        imgPhoto.setWidth(PHOTO_WIDTH);
        binder.forField(imgPhoto).bind(UserData::getPhoto, UserData::setPhoto);

        ImageField imgSmbPhoto = new ImageField();
        imgSmbPhoto.setWidth(PHOTO_WIDTH);
        sambaBinder.forField(imgSmbPhoto).bind(SmbContactData::getPicture100, null);

        TextField tfPhotoChangedOn = new TextField();
        tfPhotoChangedOn.setWidthFull();
        binder.forField(tfPhotoChangedOn).bind(user -> UIUtils.formatDate(user.getPhotoChangedOn()), null);
        TextField tfSmbPhotoChangedOn = new TextField();
        tfSmbPhotoChangedOn.setWidthFull();
        sambaBinder.forField(tfSmbPhotoChangedOn)
                .bind(sambaContact -> UIUtils.formatDate(sambaContact.getPhotoChangedOn()), null);

        TextArea taTags = new TextArea();
        taTags.setPlaceholder("Provide multiple values in new lines");
        taTags.setWidthFull();
        binder.forField(taTags).bind(user -> String.join("\n", user.getTags()), (user, value) -> {
            if (StringUtils.isBlank(value)) {
                user.setTags(Collections.emptySet());
            } else {
                user.setTags(Stream.of(value.split("\n")).map(String::trim).collect(Collectors.toSet()));
            }
        });

        FormLayout form = new FormLayout();
        UIUtils.setTheme(FORM_THEME, form);
        form.addClassName(FORM_CLASS_NAME);
        form.setWidthFull();
        UIUtils.setPadding(form, Bottom.L, Top.S);
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        form.add(buildHeader());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.status"), fblStatus,
                new FlexBoxLayout(smbStatus)).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.aim-id"), nfAimId, tfSmbAimId));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.automatic-deactivation"), fblDeactivate,
                new Div()));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.salutation"), cbSalutation,
                tfSmbSalutation).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.title"), tfTitle, tfSmbTitle));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.first-name"), tfFirstName,
                tfSmbFirstName).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.last-name"), tfLastName, tfSmbLastName)
                .asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.birthday"), dtpBirthday, tfSmbBirthday));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.email"), efEmail, tfSmbEmail).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.phone"), tfPhone, tfSmbPhone));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.public-phone"), tfPublicPhone,
                tfSmbPublicPhone));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.public-email"), efPublicEmail,
                tfSmbPublicEmail));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.public-mobile"), tfPublicMobile,
                tfSmbPublicMobile));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.company-name"), tfCompanyName,
                tfSmbCompanyName));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.function"), tfFunction, tfSmbFunction)
                .asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.position"), cbPosition, tfSmbPosition)
                .asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.business-area"), cbBusinessArea,
                tfSmbBusinessArea).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.department"), tfDepartment, tfSmbDepartment));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.ds-version"), tfDsVersion, new Div()));
        form.add(new UserPropFormItem(getTranslation("commons.lang"), cbLang, new Div()).asRequired());
        form.add(new UserPropFormItem(getTranslation("user.details.basic.photo"), imgPhoto,
                new FlexBoxLayout(imgSmbPhoto)));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.photo-changed-on"), tfPhotoChangedOn,
                tfSmbPhotoChangedOn));
        form.add(new UserPropFormItem(getTranslation("user.details.basic.tags"), taTags, new Div()));

        add(form);
    }

    private Component buildHeader() {
        Label lbHeaderProfile = UIUtils.createH5Label(getTranslation("user.details.basic.user-profile"));
        lbHeaderProfile.addClassName(LumoStyles.Margin.Right.AUTO);

        Label lblUserId = UIUtils.createH5Label("");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblUserId);
        LabelField<Long> lfUserId = new LabelField<>(lblUserId);
        lfUserId.setPrefix(getTranslation("user.details.user-id"));
        binder.forField(lfUserId).bind(UserData::getId, null);

        FlexBoxLayout headerLeft = new FlexBoxLayout(lbHeaderProfile, lfUserId);
        headerLeft.setFlexDirection(FlexDirection.ROW);
        headerLeft.setBoxSizing(BoxSizing.BORDER_BOX);
        headerLeft.setAlignItems(FlexComponent.Alignment.CENTER);
        headerLeft.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

        Label lblSambaId = UIUtils.createH5Label("");
        lblSambaId.getElement().getStyle().set("text-decoration", "underline");
        lblSambaId.getElement().getStyle().set("cursor", "pointer");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblSambaId);
        LabelField<Long> lfSambaId = new LabelField<>(lblSambaId);
        lfSambaId.setPrefix(getTranslation("user.details.samba-addr-id"));

        Button btSync = UIUtils.createOutlinedButton(getTranslation("user.details.basic.synchronize"));
        btSync.setVisible(false);
        btSync.addClickListener(event -> {
            btSync.setEnabled(false);
            contactUiFetcher.fetch(lfSambaId.getValue(), c -> {
                btSync.setEnabled(true);
                sambaBinder.setBean(c);
            });
        });

        lfSambaId.addValueChangeListener(event -> btSync.setVisible(event.getValue() != null));
        binder.forField(lfSambaId).bind(UserData::getSambaId, UserData::setSambaId);

        AnchorField sambaLink = new AnchorField(lfSambaId);
        sambaBinder.forField(sambaLink).bind(SmbContactData::getUrl, null);

        Label lbHeaderSamba = UIUtils.createH5Label(getTranslation("commons.samba"));
        Button changeSambaUser = UIUtils.createOutlinedButton(getTranslation("commons.edit"));
        changeSambaUser.addClickListener(e -> {
            new BindSmbContactDlg(sambaBinder.getBean(), contactUiFetcher, c -> {
                lfSambaId.setValue(c != null ? c.getId() : null);
                sambaBinder.setBean(c);
            }).open();
        });
        changeSambaUser.addClassName(LumoStyles.Margin.Left.AUTO);

        FlexBoxLayout headerRight = new FlexBoxLayout(lbHeaderSamba, sambaLink, btSync, changeSambaUser);
        headerRight.setFlexDirection(FlexDirection.ROW);
        headerRight.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        headerRight.setBoxSizing(BoxSizing.BORDER_BOX);
        headerRight.setSpacing(Right.L);
        headerRight.setAlignItems(FlexComponent.Alignment.CENTER);

        UserPropFormItem userPropFormItem = new UserPropFormItem("", headerLeft, headerRight);
        userPropFormItem.addClassName(HEADER_CLASS_NAME);
        return userPropFormItem;
    }
}
