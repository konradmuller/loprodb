package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import java.util.Comparator;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import java.util.List;
import java.util.function.Consumer;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.views.registration.components.SmbMembershipBadge;
import org.vdma.loprodb.ui.views.user.components.SmbAddressStatusField;

class RegistrationSmbCompaniesGrid extends Grid<SmbCompanyData> {

    private static final String ITEM_FIRST_ROW_HEIGHT = "25px";

    RegistrationSmbCompaniesGrid(List<SmbCompanyData> companies, Consumer<SmbCompanyData> selectListener) {
        setHeightByRows(true);
        setSelectionMode(SelectionMode.NONE);
        UIUtils.setMargin(this, Top.S);
        setDataProvider(DataProvider.ofCollection(companies));
        setWidthFull();

        addColumn(new ComponentRenderer<>(this::createStatusAndTypeColumn))
                .setAutoWidth(true)
                .setFlexGrow(0);

        addColumn(new ComponentRenderer<>(this::createNameAndMembershipsColumn))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setComparator(comparing(SmbCompanyData::getName1))
                .setHeader(getTranslation("registration.company.list.name"));

        addColumn(new ComponentRenderer<>(this::createCountryAndHomepageColumn))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setComparator(Comparator.<SmbCompanyData, String>comparing(c -> c.getAddress().getCountryCode())
                        .thenComparing(SmbCompanyData::getHomepage, nullsLast(String::compareToIgnoreCase)))
                .setHeader(getTranslation("registration.company.list.country-homepage"));

        Grid.Column<SmbCompanyData> locationColumn = addColumn(new ComponentRenderer<>(this::createCityAndAddressColumn))
                .setAutoWidth(true)
                .setFlexGrow(1)
                .setComparator(Comparator.<SmbCompanyData, String>comparing(c -> c.getAddress().getCity())
                        .thenComparing(c -> c.getAddress().getStreet()))
                .setHeader(getTranslation("registration.company.list.city-address"));

        addColumn(new ComponentRenderer<>(this::createIdWithLinkColumn))
                .setAutoWidth(true)
                .setFlexGrow(0);

        addColumn(new ComponentRenderer<>(c -> createSelectColumn(c, selectListener)))
                .setAutoWidth(true)
                .setFlexGrow(0);
        
        sort(new GridSortOrderBuilder<SmbCompanyData>().thenAsc(locationColumn).build());
    }

    private Button createSelectColumn(SmbCompanyData company, Consumer<SmbCompanyData> companySelectListener) {
        Button btnSelect = UIUtils.createOutlinedButton(getTranslation("registration.details.match-contact.choose"));
        btnSelect.addClickListener(event -> companySelectListener.accept(company));
        return btnSelect;
    }

    private AnchorField createIdWithLinkColumn(SmbCompanyData company) {
        Label lblId = new Label(company.getId().toString());
        lblId.getElement().getStyle().set("cursor", "pointer");
        AnchorField sambaLink = new AnchorField(lblId);
        sambaLink.setValue(company.getUrl());
        return sambaLink;
    }

    private FlexBoxLayout createCountryAndHomepageColumn(SmbCompanyData company) {
        Label lblCountry = new Label(UIUtils.getDisplayCountry(company.getAddress().getCountryCode()));

        FlexBoxLayout flbCountryHomepage = new FlexBoxLayout(lblCountry, new Label(company.getHomepage()));
        flbCountryHomepage.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        flbCountryHomepage.setSpacing(Bottom.S);

        return flbCountryHomepage;
    }

    private FlexBoxLayout createCityAndAddressColumn(SmbCompanyData company) {

        FlexBoxLayout flbStreetCity = new FlexBoxLayout(
                new Label(company.getAddress().getCity()),
                new Label(company.getAddress().getStreet()));
        flbStreetCity.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        flbStreetCity.setSpacing(Bottom.S);

        return flbStreetCity;
    }

    private FlexBoxLayout createStatusAndTypeColumn(SmbCompanyData company) {
        Div type = new Div(new Label(getTranslation(company.getType().getTranslateKey())));
        type.getElement().getStyle().set("background-color", "#d7d7d7");
        type.addClassNames(LumoStyles.Padding.Vertical.S, LumoStyles.Padding.Horizontal.L);

        SmbAddressStatusField statusField = new SmbAddressStatusField();
        statusField.setValue(company.getStatus().name());
        UIUtils.setLineHeight("0", statusField);
        statusField.setHeight(ITEM_FIRST_ROW_HEIGHT);

        FlexBoxLayout flb = new FlexBoxLayout(statusField, type);
        flb.setSpacing(Bottom.S);
        flb.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        return flb;
    }

    private FlexBoxLayout createNameAndMembershipsColumn(SmbCompanyData company) {
        Label lblName1 = UIUtils.createH5Label(company.getName1());
        Label lblName2 = UIUtils.createH5Label(company.getName2());

        FlexBoxLayout flbNames = new FlexBoxLayout(lblName1, lblName2);
        flbNames.setSpacing(Right.S);
        flbNames.setHeight(ITEM_FIRST_ROW_HEIGHT);
        flbNames.setAlignItems(FlexComponent.Alignment.CENTER);

        FlexBoxLayout flb = new FlexBoxLayout(flbNames);
        flb.setSpacing(Bottom.S);
        flb.setFlexDirection(FlexLayout.FlexDirection.COLUMN);

        if (company.getMemberships() != null && !company.getMemberships().isEmpty()) {
            FlexBoxLayout flbMembership = new FlexBoxLayout();
            flbMembership.setSpacing(Right.S);
            company.getMemberships().forEach(membership -> flbMembership.add(new SmbMembershipBadge(membership)));
            flb.add(flbMembership);
        }

        return flb;
    }
}
