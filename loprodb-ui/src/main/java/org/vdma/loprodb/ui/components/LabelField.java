package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Label;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class LabelField<T> extends CustomField<T> {

    private final Label lbl;

    private String prefix;
    private String suffix;

    public LabelField(Label lbl) {
        this.lbl = lbl;
        add(lbl);
    }

    @Override
    protected T generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(T newPresentationValue) {
        if (newPresentationValue == null) {
            lbl.setText("");
        } else {
            String val = newPresentationValue.toString();
            if (StringUtils.isNotBlank(prefix)) {
                val = prefix + " " + val;
            }
            if (StringUtils.isNotBlank(suffix)) {
                val = val + " " + suffix;
            }
            lbl.setText(val.trim());
        }
    }
}
