package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.timepicker.TimePicker;
import java.time.Duration;
import java.time.LocalDateTime;
import org.vdma.loprodb.ui.layout.size.Right;

public class DateTimePicker extends CustomField<LocalDateTime> {

    private final DatePicker date = new DatePicker();
    private final TimePicker time = new TimePicker();

    public DateTimePicker(String label) {
        this();
        setLabel(label);
    }
    public DateTimePicker() {
        date.setClearButtonVisible(true);
        date.setSizeUndefined();

        time.setClearButtonVisible(true);
        time.setStep(Duration.ofSeconds(1));

        FlexBoxLayout content = new FlexBoxLayout(date, time);
        content.setSpacing(Right.XS);
        content.setFlexGrow(1, date, time);
        add(content);
    }

    @Override
    protected LocalDateTime generateModelValue() {
        if (date.getValue() == null || time.getValue() == null) {
            return null;
        }
        return LocalDateTime.of(date.getValue(), time.getValue());
    }

    @Override
    protected void setPresentationValue(LocalDateTime newPresentationValue) {
        if (newPresentationValue == null) {
            date.setValue(null);
            time.setValue(null);
        } else {
            date.setValue(newPresentationValue.toLocalDate());
            time.setValue(newPresentationValue.toLocalTime());
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        date.setReadOnly(readOnly);
        time.setReadOnly(readOnly);
    }

    @Override
    public boolean isReadOnly() {
        return date.isReadOnly();
    }
}
