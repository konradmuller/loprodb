package org.vdma.loprodb.ui.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.server.StreamResource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.vdma.loprodb.ui.layout.size.Right;
import org.vdma.loprodb.ui.util.UIUtils;
import org.vdma.loprodb.ui.util.css.BoxSizing;

@CssImport(value = "./styles/upload.css", themeFor = "vaadin-upload")
@Slf4j
public class ImageUploader extends CustomField<byte[]> {

    private Image image;

    @Getter
    @Setter
    private String noImagePath = UIUtils.NO_IMG;

    private final Button delete;

    public ImageUploader() {
        Button change = new Button(UIUtils.createPrimaryIcon(VaadinIcon.EDIT));

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setDropAllowed(false);
        UIUtils.setTheme("no-file-list", upload);
        upload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        upload.setUploadButton(change);
        upload.addSucceededListener(event -> {
            try {
                setValue(IOUtils.toByteArray(buffer.getInputStream()));
            } catch (IOException e) {
                log.warn("Failed to upload image", e);
            }
        });

        image = new Image(noImagePath, "");

        delete = new Button(UIUtils.createPrimaryIcon(VaadinIcon.CLOSE));
        delete.addClickListener(e -> {
            setValue(null);
            upload.getElement().executeJs("this.files=[]");
        });
        delete.setVisible(false);

        Div actions = new Div(upload, delete);
        UIUtils.setLineHeight("0", actions);

        FlexBoxLayout content = new FlexBoxLayout(image, actions);
        content.setSpacing(Right.XS);
        content.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        content.setBoxSizing(BoxSizing.BORDER_BOX);
        content.setAlignItems(FlexComponent.Alignment.CENTER);
        add(content);
    }

    @Override
    protected byte[] generateModelValue() {
        return getValue();
    }

    @Override
    protected void setPresentationValue(byte[] imageBytes) {
        delete.setVisible(imageBytes != null);

        if (imageBytes != null) {
            image.setSrc(new StreamResource("image", () -> new ByteArrayInputStream(imageBytes)));
        } else {
            image.setSrc(noImagePath);
        }
    }

    @Override
    public void setWidth(String width) {
        image.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        image.setHeight(height);
    }
}
