package org.vdma.loprodb.ui.views.emailtemplate;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.entity.EmailTemplateType;
import org.vdma.loprodb.service.IEmailTemplateService;
import org.vdma.loprodb.ui.MainLayout;
import org.vdma.loprodb.ui.components.navigation.bar.AppBar;
import org.vdma.loprodb.ui.views.EqualCmp;
import org.vdma.loprodb.ui.views.ViewFrame;

@PageTitle("Email templates")
@Route(value = "email-templates", layout = MainLayout.class)
public class EmailTemplates extends ViewFrame implements EqualCmp {

    private int tab = -1;

    private final IEmailTemplateService emailTemplateService;

    private List<EmailTemplateDetails> tabs = new ArrayList<>();

    @Autowired
    public EmailTemplates(IEmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        if (isContentEmpty()) {
            buildContent();
        }
        initAppBar();
    }

    private void initAppBar() {
        AppBar appBar = MainLayout.get().getAppBar();
        for (int i = 0; i < tabs.size(); i++) {
            EmailTemplateType templateType = tabs.get(i).getEmailTemplate().getTemplateType();
            appBar.addTab(getTranslation(EmailTemplateType.class.getSimpleName() + "." + templateType.name()))
                    .setId("email_template_" + i);
        }
        appBar.getTabs().setSelectedIndex(tab);
        appBar.addTabSelectionListener(e -> {
            String selectedTabId = MainLayout.get().getAppBar().getSelectedTab().getId().get();
            tab = Integer.parseInt(selectedTabId.substring(selectedTabId.lastIndexOf("_") + 1));
            setViewContent(tabs.get(tab));
        });
        if (tab < 0) {
            tab = 0;
            appBar.getTabs().setSelectedIndex(tab);
        }
        appBar.centerTabs();
    }

    private void buildContent() {
        tabs.addAll(emailTemplateService.getAll().stream()
                .map(template -> new EmailTemplateDetails(template, emailTemplateService))
                .collect(Collectors.toList()));
    }

}
