package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.UI;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.service.SmbCompanyService;
import org.vdma.loprodb.ui.util.UIUtils;

@RequiredArgsConstructor
@Slf4j
public class SmbCompanyUiFetcher {

    private final UI ui;
    private final SmbCompanyService smbCompanyService;

    public void fetch(Long companyId, Consumer<SmbCompanyData> onResult) {
        if (companyId == null) {
            onResult.accept(null);
            return;
        }
        new Thread(() -> {
            try {
                SmbCompanyData company = smbCompanyService.getCompanyById(companyId);
                ui.access(() -> onResult.accept(company));
            } catch (HttpStatusCodeException ex) {
                ui.access(() -> {
                    if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                        UIUtils.showNotification(ui.getTranslation("registration.details.samba-company-not-found-msg"));
                    } else {
                        log.warn("failed to request samba company by id " + companyId, ex);
                    }
                    onResult.accept(null);
                });
            }
        }).start();
    }

}
