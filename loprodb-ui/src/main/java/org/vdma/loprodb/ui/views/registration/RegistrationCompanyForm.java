package org.vdma.loprodb.ui.views.registration;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbPostAddressData;
import org.vdma.loprodb.ui.components.AnchorField;
import org.vdma.loprodb.ui.components.FlexBoxLayout;
import org.vdma.loprodb.ui.components.LabelField;
import org.vdma.loprodb.ui.layout.size.Bottom;
import org.vdma.loprodb.ui.layout.size.Top;
import org.vdma.loprodb.ui.util.LumoStyles;
import org.vdma.loprodb.ui.util.UIUtils;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_CLASS_NAME;
import static org.vdma.loprodb.ui.views.user.UserDetails.FORM_THEME;
import static org.vdma.loprodb.ui.views.user.UserDetails.HEADER_CLASS_NAME;
import org.vdma.loprodb.ui.views.user.components.UserPropFormItem;

class RegistrationCompanyForm extends FormLayout {

    private Button btnLinkCompany;

    RegistrationCompanyForm(Binder<RegistrationData> binder,
            Binder<UserData> userBinder,
            Binder<SmbCompanyData> sambaCompanyBinder) {
        TextField tfCountry = new TextField();
        binder.forField(tfCountry).bind(reg -> reg.getCompany().getCountry(), null);

        TextField tfSmbCountry = new TextField();
        sambaCompanyBinder.forField(tfSmbCountry).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? UIUtils.getDisplayCountry(a.getCountryCode()) : null;
        }, null);

        TextField tfUserCountry = new TextField();
        userBinder.forField(tfUserCountry).bind(user -> user.getCompany().getCountry(), null);

        TextField tfCity = new TextField();
        binder.forField(tfCity).bind(reg -> reg.getCompany().getCity(), null);

        TextField tfSmbCity = new TextField();
        sambaCompanyBinder.forField(tfSmbCity).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getCity() : null;
        }, null);

        TextField tfUserCity = new TextField();
        userBinder.forField(tfUserCity).bind(u -> u.getCompany().getCity(), null);

        TextField tfZip = new TextField();
        binder.forField(tfZip).bind(reg -> reg.getCompany().getZip(), null);

        TextField tfSmbZip = new TextField();
        sambaCompanyBinder.forField(tfSmbZip).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getZip() : null;
        }, null);

        TextField tfUserZip = new TextField();
        userBinder.forField(tfUserZip).bind(user -> user.getCompany().getZip(), null);

        TextField tfStreet = new TextField();
        binder.forField(tfStreet).bind(reg -> reg.getCompany().getStreet(), null);

        TextField tfSmbStreet = new TextField();
        sambaCompanyBinder.forField(tfSmbStreet).bind(c -> {
            SmbPostAddressData a = c.getAddress();
            return a != null ? a.getStreet() : null;
        }, null);

        TextField tfUserStreet = new TextField();
        userBinder.forField(tfUserStreet).bind(u -> u.getCompany().getStreet(), null);

        TextField tfHouseNumber = new TextField();
        binder.forField(tfHouseNumber).bind(reg -> reg.getCompany().getHouseNumber(), null);

        TextField tfUserHouseNumber = new TextField();
        userBinder.forField(tfUserHouseNumber).bind(u -> u.getCompany().getHouseNumber(), null);

        UIUtils.setTheme(FORM_THEME, this);
        addClassName(FORM_CLASS_NAME);
        setWidthFull();
        UIUtils.setPadding(this, Bottom.L, Top.S);
        setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1,
                        FormLayout.ResponsiveStep.LabelsPosition.ASIDE));

        add(buildCompanyInfoHeader(binder, userBinder, sambaCompanyBinder));
        add(new UserPropFormItem(getTranslation("commons.country"), tfCountry, tfSmbCountry,
                tfUserCountry));
        add(new UserPropFormItem(getTranslation("commons.zip"), tfZip, tfSmbZip, tfUserZip));
        add(new UserPropFormItem(getTranslation("commons.city"), tfCity, tfSmbCity, tfUserCity));
        add(new UserPropFormItem(getTranslation("commons.street"), tfStreet, tfSmbStreet, tfUserStreet));
        add(new UserPropFormItem(getTranslation("commons.house-number"), tfHouseNumber, new Div(),
                tfUserHouseNumber));
    }

    private Component buildCompanyInfoHeader(Binder<RegistrationData> binder,
            Binder<UserData> userBinder,
            Binder<SmbCompanyData> sambaCompanyBinder) {
        Label lblCompanyName = UIUtils.createH5Label("");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblCompanyName);
        LabelField<String> lfCompanyName = new LabelField<>(lblCompanyName);
        binder.forField(lfCompanyName).bind(reg -> reg.getCompany().getName(), null);

        Label lblSambaCompanyName = UIUtils.createH5Label("");
        lblSambaCompanyName.getElement().getStyle().set("text-decoration", "underline");
        lblSambaCompanyName.getElement().getStyle().set("cursor", "pointer");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblSambaCompanyName);
        LabelField<String> lfSambaCompanyName = new LabelField<>(lblSambaCompanyName);
        sambaCompanyBinder.forField(lfSambaCompanyName).bind(c -> {
            String name = c.getName();
            if (c.getVdmaMemberId() != null) {
                name += " [" + c.getVdmaMemberId() + "]";
            }
            return name;
        }, null);

        AnchorField sambaCompanyLink = new AnchorField(lfSambaCompanyName);
        sambaCompanyBinder.forField(sambaCompanyLink).bind(SmbCompanyData::getUrl, null);

        Label lblUserCompanyName = UIUtils.createH5Label("");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblUserCompanyName);
        LabelField<String> lfUserCompanyName = new LabelField<>(lblUserCompanyName);
        userBinder.forField(lfUserCompanyName).bind(user -> user.getCompany().getName(), null);

        Label lblCompany = UIUtils.createH5Label(getTranslation("registration.details.company-name"));
        lblCompany.getElement().getStyle().set("display", "inline");
        UIUtils.setTextColor(LumoStyles.Color.BASE_COLOR, lblCompany);

        btnLinkCompany = UIUtils.createOutlinedButton(getTranslation("registration.details.link-company-manually"));
        btnLinkCompany.addClassName(LumoStyles.Margin.Left.AUTO);

        UserPropFormItem userPropFormItem = new UserPropFormItem(lblCompany, lfCompanyName, sambaCompanyLink,
                new FlexBoxLayout(lfUserCompanyName, btnLinkCompany));
        userPropFormItem.addClassName(HEADER_CLASS_NAME);
        return userPropFormItem;
    }

    void setLinkCompanyListener(ComponentEventListener<ClickEvent<Button>> listener) {
        btnLinkCompany.addClickListener(listener);
    }

    void setLinkCompanyBtnVisibility(boolean visible) {
        btnLinkCompany.setVisible(visible);
    }
}
