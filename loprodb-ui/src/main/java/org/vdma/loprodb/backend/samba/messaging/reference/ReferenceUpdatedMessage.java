package org.vdma.loprodb.backend.samba.messaging.reference;

import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.messaging.ActionType;

@Getter
@Setter
public class ReferenceUpdatedMessage {
    private Long id;
    private ActionType actionType;
}
