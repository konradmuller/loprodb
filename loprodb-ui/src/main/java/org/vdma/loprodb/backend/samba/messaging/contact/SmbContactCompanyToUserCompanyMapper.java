package org.vdma.loprodb.backend.samba.messaging.contact;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.vdma.loprodb.entity.ExtendedCompanyInfo;
import org.vdma.loprodb.samba.model.SmbCompanyData;

@Mapper
public interface SmbContactCompanyToUserCompanyMapper {

    @Mapping(target = "street", source = "address.street")
    @Mapping(target = "location", source = "address.location")
    @Mapping(target = "district", source = "address.district")
    @Mapping(target = "zip", source = "address.zip")
    @Mapping(target = "city", source = "address.city")
    @Mapping(target = "country", source = "address.country")
    ExtendedCompanyInfo map(SmbCompanyData company);
}
