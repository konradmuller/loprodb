package org.vdma.loprodb.backend.providers;

import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.ToLongFunction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.RegistrationListParam;
import org.vdma.loprodb.service.mapper.RegistrationMapper;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.service.param.ReferenceListParam;
import org.vdma.loprodb.service.param.UserListParam;

public class SpringDataProviderBuilder<T, F> {
    private final BiFunction<Pageable, F, Page<T>> queryFunction;
    private final ToLongFunction<F> lengthFunction;
    private final List<QuerySortOrder> defaultSortOrders = new ArrayList<>();

    private F defaultFilter = null;

    public SpringDataProviderBuilder(BiFunction<Pageable, F, Page<T>> queryFunction, ToLongFunction<F> lengthFunction) {
        this.queryFunction = queryFunction;
        this.lengthFunction = lengthFunction;
    }

    public SpringDataProviderBuilder<T, F> withDefaultSort(String column, SortDirection direction) {
        defaultSortOrders.add(new QuerySortOrder(column, direction));
        return this;
    }

    public SpringDataProviderBuilder<T, F> withDefaultFilter(F defaultFilter) {
        this.defaultFilter = defaultFilter;
        return this;
    }

    public DataProvider<T, F> build() {
        return new PageableDataProvider<T, F>() {
            @Override
            protected Page<T> fetchFromBackEnd(Query<T, F> query, Pageable pageable) {
                return queryFunction.apply(pageable, query.getFilter().orElse(defaultFilter));
            }

            @Override
            protected List<QuerySortOrder> getDefaultSortOrders() {
                return defaultSortOrders;
            }

            @Override
            protected int sizeInBackEnd(Query<T, F> query) {
                return (int) lengthFunction.applyAsLong(query.getFilter().orElse(defaultFilter));
            }
        };
    }

    public static SpringDataProviderBuilder<ApplicationData, ObjectListParam> applications(
            IApplicationService appService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> appService.filter(filter.initFromPageable(pageable)),
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return appService.filter(filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<UserData, UserListParam> users(
            IUserService userService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> {
                    filter.initFromPageable(pageable);
                    return userService.filter(filter);
                },
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return userService.filter(filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<RegistrationData, RegistrationListParam> registrations(
            IRegistrationService registrationService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> {
                    filter.initFromPageable(pageable);
                    return registrationService.filter(filter);
                },
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return registrationService.filter(filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<AuditData<UserData>, ObjectListParam> userAudit(Long userId,
            UserMapper mapper, AuditService auditService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> auditService.filter(User.class, userId, mapper::map,
                        filter.initFromPageable(pageable)),
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return auditService.filter(User.class, userId, mapper::map, filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<AuditData<RegistrationData>, ObjectListParam> registrationAudit(Long regId,
            RegistrationMapper mapper, AuditService auditService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> auditService.filter(Registration.class, regId, mapper::map,
                        filter.initFromPageable(pageable)),
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return auditService.filter(Registration.class, regId, mapper::map, filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<EventLogData, ObjectListParam> userEventLog(Long userId,
            IEventLogService eventLogService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> eventLogService.getForUser(userId, filter.initFromPageable(pageable)),
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return eventLogService.getForUser(userId, filter).getTotalElements();
                });
    }

    public static SpringDataProviderBuilder<ReferenceData, ReferenceListParam> references(
            IReferenceService referenceService) {
        return new SpringDataProviderBuilder<>(
                (pageable, filter) -> {
                    filter.initFromPageable(pageable);
                    return referenceService.filter(filter);
                },
                filter -> {
                    filter.setPage(0);
                    filter.setPageSize(1);
                    return referenceService.filter(filter).getTotalElements();
                });
    }

    //
    // public static SpringDataProviderBuilder<TransactionData, TransactionListParam> transactions(String orgId,
    // String customerId, ITransactionService transactionService) {
    // return new SpringDataProviderBuilder<>(
    // (pageable, filter) -> {
    // filter.initFromPageable(pageable);
    // return transactionService.filter(orgId, customerId, filter);
    // },
    // filter -> {
    // filter.setPage(0);
    // filter.setPageSize(1);
    // return transactionService.filter(orgId, customerId, filter).getTotalElements();
    // });
    // }

}
