package org.vdma.loprodb.backend.samba.messaging.reference;

import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.RefType;
import static org.vdma.loprodb.entity.RefType.USER_BUSINESS_AREA;
import static org.vdma.loprodb.entity.RefType.USER_POSITION;
import org.vdma.loprodb.samba.model.SmbRefType;
import static org.vdma.loprodb.samba.model.SmbRefType.CONTACT_AREA;
import static org.vdma.loprodb.samba.model.SmbRefType.CONTACT_POSITION;
import org.vdma.loprodb.samba.model.SmbReference;
import org.vdma.loprodb.service.IReferenceService;

@Slf4j
@Service
@RequiredArgsConstructor
public class SmbReferenceSynchronizer {

    private final IReferenceService referenceService;

    private final Map<SmbRefType, RefType> refTypesMapping = new HashMap<SmbRefType, RefType>() {
        {
            put(CONTACT_AREA, USER_BUSINESS_AREA);
            put(CONTACT_POSITION, USER_POSITION);
        }
    };

    public void synchronize(SmbReference smbReference) {
        RefType refType = refTypesMapping.get(smbReference.getRefType());
        if (refType == null) {
            log.info("Ignore reference type {}", smbReference.getRefType());
            return;
        }
        ReferenceData reference = referenceService.getBySambaId(smbReference.getId()).orElseGet(() -> {
            ReferenceData referenceData =
                    referenceService.getByAnyValueAndType(smbReference.getValueDe(), refType);
            if (referenceData == null) {
                referenceData = new ReferenceData();
                referenceData.setRefType(refType);
            }
            referenceData.setSambaId(smbReference.getId());
            return referenceData;
        });
        reference.setNameDe(smbReference.getValueDe());
        reference.setNameEn(smbReference.getValueEn());
        if (reference.getId() == null) {
            referenceService.create(reference);
        } else {
            referenceService.update(reference.getId(), reference);
        }
    }

}
