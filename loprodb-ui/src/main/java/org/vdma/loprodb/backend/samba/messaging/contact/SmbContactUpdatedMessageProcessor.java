package org.vdma.loprodb.backend.samba.messaging.contact;

import static java.lang.String.format;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.entity.listener.user.UserSmbSyncContextHolder;
import static org.vdma.loprodb.event.user.UpdateSource.SAMBA;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.service.IUserService;

@Component
@Slf4j
@RequiredArgsConstructor
public class SmbContactUpdatedMessageProcessor {

    private final IUserService userService;
    private final SmbContactService smbContactService;
    private final SmbContactSynchronizer synchronizer;

    // todo: concurrency = "1" : samba can send multiple messages if contact updated when contact person is also updated
    @JmsListener(destination = "${jms.queue.samba.contact-updated}", concurrency = "1")
    public void onContactUpdated(Message<String> message) {
        log.info("Samba contact {} updated message", message.getPayload());
        try {
            UserData userData = userService.getUserByIdType(UserIdType.SAMBA_ID, message.getPayload());
            SmbContactData sambaContact = smbContactService.getContactById(userData.getSambaId());

            if (Objects.equals(userData.getSambaContactVersion(), sambaContact.getLoprodbSyncVersion())) {
                log.info(format(
                        "Sync version (%s) didn't change, ignoring update...", userData.getSambaContactVersion()));
                return;
            }

            log.info("Starting synchronization for user {}", userData.getExternalId());

            UserSmbSyncContextHolder.getContext().setUserVersionInSamba(sambaContact.getLoprodbUserVersion());

            Authentication authentication = new UsernamePasswordAuthenticationToken("samba_sync", "");
            SecurityContextHolder.getContext().setAuthentication(authentication);

            userData = synchronizer.syncWithSmbContact(userData, sambaContact);
            userService.update(userData.getExternalId(), userData, SAMBA);

            log.info("Synchronization for user {} successfully finished", userData.getExternalId());
        } catch (Exception e) {
            log.error("Failed to synchronize user with samba contact " + message.getPayload(), e);
        } finally {
            UserSmbSyncContextHolder.clearContext();
        }
    }
}
