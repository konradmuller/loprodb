package org.vdma.loprodb.backend.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.vdma.loprodb.rest.ApiError;

@RequiredArgsConstructor
@Slf4j
public class LoprodbApiErrorHandler extends DefaultResponseErrorHandler {

    private final ObjectMapper objectMapper;

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().is4xxClientError() || response.getStatusCode().is5xxServerError()) {
            String body = IOUtils.toString(response.getBody(), StandardCharsets.UTF_8);
            try {
                ApiError apiError = objectMapper.readValue(body, ApiError.class);
                throw new LoprodbApiException(apiError);
            } catch (JsonProcessingException e) {
                log.error("Failed to parse Loprodb API error", e);
                throw new RuntimeException(e);
            }
        }
    }
}
