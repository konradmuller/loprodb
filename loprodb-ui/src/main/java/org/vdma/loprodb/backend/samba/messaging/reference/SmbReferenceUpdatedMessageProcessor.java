package org.vdma.loprodb.backend.samba.messaging.reference;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import static org.vdma.loprodb.messaging.ActionType.DELETED;
import org.vdma.loprodb.samba.model.SmbReference;
import org.vdma.loprodb.samba.service.SmbReferenceService;
import org.vdma.loprodb.service.IReferenceService;

@Component
@Slf4j
@RequiredArgsConstructor
public class SmbReferenceUpdatedMessageProcessor {

    private final ObjectMapper objectMapper;
    private final SmbReferenceService smbReferenceService;
    private final IReferenceService referenceService;
    private final SmbReferenceSynchronizer referenceSynchronizer;

    @JmsListener(destination = "${jms.queue.samba.reference-updated}")
    public void onReferenceUpdated(Message<String> message) throws Exception {
        log.info("Samba reference updated message, payload = {}", message.getPayload());
        try {
            ReferenceUpdatedMessage msg = objectMapper.readValue(message.getPayload(), ReferenceUpdatedMessage.class);
            if (msg.getActionType() == DELETED) {
                referenceService.getBySambaId(msg.getId())
                        .ifPresent(referenceData -> referenceService.delete(referenceData.getId()));
            } else {
                SmbReference smbReference = smbReferenceService.getReferenceById(msg.getId());
                referenceSynchronizer.synchronize(smbReference);
            }
            log.info("Samba reference {} successfully synchronized", msg.getId());
        } catch (Exception e) {
            log.error("Failed to process samba reference update", e);
        }
    }
}
