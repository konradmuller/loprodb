package org.vdma.loprodb.backend.rest;

import lombok.Getter;
import org.vdma.loprodb.rest.ApiError;

public class LoprodbApiException extends RuntimeException {

    @Getter
    private final ApiError apiError;

    public LoprodbApiException(ApiError apiError) {
        super();
        this.apiError = apiError;
    }
}
