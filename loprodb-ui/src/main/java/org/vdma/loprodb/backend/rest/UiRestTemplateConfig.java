package org.vdma.loprodb.backend.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.vdma.loprodb.config.AppConfigProps;

@Configuration
public class UiRestTemplateConfig {

    @Bean
    public RestTemplate loprodbRestTemplate(@Qualifier("loprodbObjectMapper") ObjectMapper objectMapper,
            AppConfigProps appConfigProps) {
        return new RestTemplateBuilder()
                .messageConverters(new MappingJackson2HttpMessageConverter(objectMapper))
                .basicAuthentication(appConfigProps.getInternal().getCode(), appConfigProps.getInternal().getToken())
                .errorHandler(new LoprodbApiErrorHandler(objectMapper))
                .build();
    }
}
