package org.vdma.loprodb.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vdma.loprodb.rest.ConfirmRegistrationResult;

@Service
@Slf4j
public class LoprodbRegistrationService {

    @Value("${rest.api.url.loprodb-registration-service.confirm}")
    private String confirmUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public LoprodbRegistrationService(@Qualifier("loprodbRestTemplate") RestTemplate loprodbRestTemplate) {
        this.restTemplate = loprodbRestTemplate;
    }

    public ConfirmRegistrationResult confirmByUser(String token) {
        return restTemplate.postForObject(confirmUrl, null, ConfirmRegistrationResult.class, token);
    }
}
