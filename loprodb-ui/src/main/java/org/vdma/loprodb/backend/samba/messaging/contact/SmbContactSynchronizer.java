package org.vdma.loprodb.backend.samba.messaging.contact;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.config.AppConfigProps;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserAppData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.RefType;
import static org.vdma.loprodb.entity.UserStatus.INACTIVE;
import static org.vdma.loprodb.samba.model.SmbAddressStatus.DECEASED;
import static org.vdma.loprodb.samba.model.SmbAddressStatus.LEFT_COMPANY;
import org.vdma.loprodb.samba.model.SmbAreaPosition;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.model.SmbReference;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IReferenceService;

@Mapper(uses = { SmbContactCompanyToUserCompanyMapper.class }, nullValuePropertyMappingStrategy = IGNORE)
@Slf4j
public abstract class SmbContactSynchronizer {

    private static final String HERR = "Herr";
    private static final String FRAU = "Frau";

    @Autowired
    private IReferenceService referenceService;

    @Autowired
    private IApplicationService applicationService;

    @Autowired
    private SmbContactService smbContactService;

    @Autowired
    private AppConfigProps appConfigProps;

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "photoChangedOn", ignore = true)
    @Mapping(target = "photoCheckSum", ignore = true)
    @Mapping(target = "phone", ignore = true)
    @Mapping(target = "publicPhone", ignore = true)
    @Mapping(target = "publicMobile", ignore = true)
    @Mapping(target = "gender", source = "salutation", qualifiedByName = "salutationToGender")
    @Mapping(target = "sambaContactVersion", source = "loprodbSyncVersion")
    @Mapping(target = "email", ignore = true)
    public abstract UserData syncWithSmbContact(@MappingTarget UserData userData, SmbContactData sambaContact);

    @Named("salutationToGender")
    public Gender salutationToGender(SmbReference salutation) {
        if (HERR.equalsIgnoreCase(salutation.getValueDe())) {
            return Gender.MALE;
        } else if (FRAU.equalsIgnoreCase(salutation.getValueDe())) {
            return Gender.FEMALE;
        } else {
            log.warn("Cannot map salutation {} to any gender", salutation.getValueDe());
            return null;
        }
    }

    @AfterMapping
    public void afterMapping(SmbContactData smbContact, @MappingTarget UserData user) {
        syncAreaPosition(smbContact, user);
        syncPhoto(smbContact, user);
        if (smbContact.getStatus() == LEFT_COMPANY || smbContact.getStatus() == DECEASED) {
            user.setStatus(INACTIVE);
        }
        try {
            syncVdmaRoles(smbContact, user);
        } catch (Exception e) {
            log.warn("Failed to sync VDMA roles for " + user.getExternalId(), e);
        }
    }

    private void syncVdmaRoles(SmbContactData smbContact, UserData user) {
        if (user.isVdmaRolesSynchronized()) {
            List<UserAppData> apps = new ArrayList<>(user.getApps());

            List<String> addRoles = new ArrayList<>();
            List<String> removeRoles = new ArrayList<>();

            if (smbContact.isVdmaMember()) {
                addRoles.add(appConfigProps.getVdma().getRoleMember());
            } else {
                removeRoles.add(appConfigProps.getVdma().getRoleMember());
            }
            if (smbContact.isVdmaEmployee()) {
                addRoles.add(appConfigProps.getVdma().getRoleEmployee());
            } else {
                removeRoles.add(appConfigProps.getVdma().getRoleEmployee());
            }

            if (!removeRoles.isEmpty()) {
                apps.stream()
                        .filter(app -> app.getAppCode().equalsIgnoreCase(appConfigProps.getVdma().getCode()))
                        .findFirst()
                        .ifPresent(vdmaApp -> vdmaApp.getRoles().removeIf(r -> removeRoles.contains(r.getName())));
            }
            if (!addRoles.isEmpty()) {
                ApplicationData vdmaApp = applicationService.getApplication(appConfigProps.getVdma().getCode());
                List<AppRoleData> vdmaRoles = new ArrayList<>();
                for (String role : addRoles) {
                    AppRoleData vdmaRole = vdmaApp.getRoles().stream()
                            .filter(appRole -> appRole.getName().equalsIgnoreCase(role))
                            .findFirst().orElse(null);
                    if (vdmaRole != null) {
                        vdmaRoles.add(vdmaRole);
                    } else {
                        log.warn("Cannot find VDMA role {}", role);
                    }
                }
                if (!vdmaRoles.isEmpty()) {
                    UserAppData userVdmaApp = apps.stream()
                            .filter(app -> app.getAppId().equals(vdmaApp.getId()))
                            .findFirst().orElseGet(() -> {
                                UserAppData newVdmaApp = new UserAppData(vdmaApp.getId());
                                apps.add(newVdmaApp);
                                return newVdmaApp;
                            });
                    for (AppRoleData vdmaRole : vdmaRoles) {
                        if (userVdmaApp.getRoles().stream().noneMatch(r -> r.getId().equals(vdmaRole.getId()))) {
                            userVdmaApp.getRoles().add(vdmaRole);
                        }
                    }
                }
            }
            user.setApps(new HashSet<>(apps));
        }
    }

    private void syncPhoto(SmbContactData smbContact, UserData user) {
        if (smbContact.getPhotoCheckSum() == null || smbContact.getPictureOriginal() == null) {
            log.info("Samba contact {} has no photo", smbContact.getId());
            return;
        }
        if (smbContact.getPhotoCheckSum().equals(user.getPhotoCheckSum())) {
            log.info("Photos are equal, no need to be synchronized");
            return;
        }
        user.setPhoto(smbContactService.getContactPhoto(smbContact.getPictureOriginal()));
    }

    private void syncAreaPosition(SmbContactData smbContact, UserData user) {
        if (smbContact.getAreaPositions().isEmpty()) {
            log.info("Contact {} has no area/positions, skip mapping", smbContact.getId());
        } else {
            for (SmbAreaPosition ap : smbContact.getAreaPositions()) {
                ReferenceData pos = referenceService.getByAnyValueAndType(ap.getPosition().getValueDe(),
                        RefType.USER_POSITION);
                if (pos != null) {
                    ReferenceData area = referenceService.getByAnyValueAndType(ap.getArea().getValueDe(),
                            RefType.USER_BUSINESS_AREA);
                    if (area != null) {
                        user.setPosition(pos);
                        user.setBusinessArea(area);
                        break;
                    }
                }
            }
        }
    }

}
