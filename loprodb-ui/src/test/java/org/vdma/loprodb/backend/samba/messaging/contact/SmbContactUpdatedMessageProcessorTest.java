package org.vdma.loprodb.backend.samba.messaging.contact;

import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import static org.mockito.Mockito.never;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.vdma.loprodb.config.AppConfigProps;
import org.vdma.loprodb.entity.AppRole;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.messaging.user.UserUpdatedMessage;
import org.vdma.loprodb.messaging.user.UserUpdatedMessageSender;
import org.vdma.loprodb.repository.AppRoleRepository;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegPwdChangeRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbContactService;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.impl.ApplicationServiceImpl;
import org.vdma.loprodb.service.impl.ReferenceServiceImpl;
import org.vdma.loprodb.service.impl.UserServiceImpl;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.AppRoleMapperImpl;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapperImpl;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.mapper.ReferenceMapperImpl;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.mapper.UserMapperImpl;
import org.vdma.loprodb.service.patch.BeanPatcher;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { SmbContactUpdatedMessageProcessorTest.Cfg.class, AppConfigProps.class })
@TestPropertySource(locations = "classpath:test.properties")
@EnableConfigurationProperties
public class SmbContactUpdatedMessageProcessorTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private IReferenceService referenceService;

    @MockBean
    private ApplicationRepository applicationRepository;

    @MockBean
    private AppRoleRepository appRoleRepository;

    @MockBean
    private SmbContactService smbContactService;

    @MockBean
    private JmsTemplate jmsTemplate;

    @Autowired
    private SmbContactUpdatedMessageProcessor messageProcessor;

    @Autowired
    private UserUpdatedMessageSender sender;

    @Autowired
    private AppConfigProps appConfigProps;

    private User user;
    private Application vdmaApp;
    private AppRole roleEmployee;

    @Before
    public void prepare() {
        vdmaApp = new Application();
        vdmaApp.setId(1L);
        vdmaApp.setCode(appConfigProps.getVdma().getCode());

        AppRole roleMember = new AppRole();
        roleMember.setId(2L);
        roleMember.setApplication(vdmaApp);
        roleMember.setName(appConfigProps.getVdma().getRoleMember());
        vdmaApp.getRoles().add(roleMember);

        roleEmployee = new AppRole();
        roleEmployee.setId(3L);
        roleEmployee.setApplication(vdmaApp);
        roleEmployee.setName(appConfigProps.getVdma().getRoleEmployee());
        vdmaApp.getRoles().add(roleEmployee);

        user = new User();
        user.setEmail("victor@email.com");
        user.setExternalId("abc");
        user.setStatus(UserStatus.ACTIVE);
        user.setGender(Gender.MALE);
        user.setFirstName("Victor");
        user.setLastName("Frankenstein");
        user.setPhone("999");
        user.setFunction("immortal");
        user.setLanguage(Language.DE);
        user.setSambaId(1L);
        user.setSambaSyncVersion(1000L);
        user.setPosition(new Reference());
        user.setBusinessArea(new Reference());
        user.setVdmaRolesSynchronized(true);

        SmbContactData smbContactData = new SmbContactData();
        smbContactData.setId(1L);
        smbContactData.setFirstName("Bob");
        smbContactData.setLastName("Marley");
        smbContactData.setEmail("bob@mail.com");
        smbContactData.setPicture100("pic100");
        smbContactData.setPicture500("pic500");
        smbContactData.setPictureOriginal("picOriginal");
        smbContactData.setPhotoCheckSum("123");
        smbContactData.setLoprodbSyncVersion(10L);
        smbContactData.setLoprodbUserVersion(1000L);
        smbContactData.setVdmaMember(true);
        smbContactData.setVdmaEmployee(false);
        SmbCompanyData companyData = new SmbCompanyData();
        companyData.setName("Siemens");
        smbContactData.setCompany(companyData);

        Mockito.when(smbContactService.getContactById(1L)).thenReturn(smbContactData);
        Mockito.when(smbContactService.getContactPhoto(Mockito.anyString())).thenReturn(new byte[1]);

        Mockito.when(referenceService.findReference(Mockito.anyLong(), Mockito.any())).thenReturn(new Reference());
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(new User());
        Mockito.when(applicationRepository.findByCode(appConfigProps.getVdma().getCode())).thenReturn(vdmaApp);
        Mockito.when(applicationRepository.findById(1L)).thenReturn(Optional.of(vdmaApp));
        Mockito.when(appRoleRepository.findByIdAndApplication(2L, vdmaApp)).thenReturn(roleMember);
        Mockito.when(appRoleRepository.findByIdAndApplication(3L, vdmaApp)).thenReturn(roleEmployee);

        Mockito.when(userRepository.findByExternalId(Mockito.anyString())).thenReturn(user);
        Mockito.when(userRepository.findBySambaId(1L)).thenReturn(user);

        ReflectionTestUtils.setField(sender, "userUpdatedLiferayQueue", "liferay");
        ReflectionTestUtils.setField(sender, "userUpdatedSambaQueue", "samba");
    }

    @Test
    public void testSync() {
        messageProcessor.onContactUpdated(new GenericMessage<>("1"));

        Mockito.verify(jmsTemplate).convertAndSend(eq("liferay"), Mockito.any(UserUpdatedMessage.class));
        Mockito.verify(jmsTemplate, never()).convertAndSend(eq("samba"), Mockito.any(UserUpdatedMessage.class));

        Assert.assertEquals("Bob", user.getFirstName());
        Assert.assertEquals("Marley", user.getLastName());
        Assert.assertTrue(user.getPhoto() != null && user.getPhoto().length > 0);
        // check if null value wasn't set
        Assert.assertEquals("immortal", user.getFunction());
    }

    @Test
    public void testSync_AddVdmaRoles() {
        messageProcessor.onContactUpdated(new GenericMessage<>("1"));

        Assert.assertTrue(user.getApps().stream()
                .anyMatch(a -> appConfigProps.getVdma().getCode().equalsIgnoreCase(a.getCode())));
        Assert.assertTrue(user.getAppRoles().stream()
                .anyMatch(r -> appConfigProps.getVdma().getRoleMember().equalsIgnoreCase(r.getName())));
    }

    @Test
    public void testSync_RemoveVdmaRoles() {
        user.getApps().add(vdmaApp);
        user.getAppRoles().add(roleEmployee);

        messageProcessor.onContactUpdated(new GenericMessage<>("1"));

        Assert.assertTrue(user.getAppRoles().stream()
                .noneMatch(r -> appConfigProps.getVdma().getRoleEmployee().equalsIgnoreCase(r.getName())));
    }

    @Test
    public void testSync_VdmaRolesNotSynchronized() {
        user.setVdmaRolesSynchronized(false);

        messageProcessor.onContactUpdated(new GenericMessage<>("1"));

        Assert.assertTrue(user.getAppRoles().stream()
                .noneMatch(r -> appConfigProps.getVdma().getRoleMember().equalsIgnoreCase(r.getName())));
        Assert.assertTrue(user.getAppRoles().stream()
                .noneMatch(r -> appConfigProps.getVdma().getRoleEmployee().equalsIgnoreCase(r.getName())));
    }

    public static class Cfg {

        @MockBean
        private CreateUserPwdTokenRepository createUserPwdTokenRepo;

        @MockBean
        private RegPwdChangeRepository regPwdChangeRepo;

        @Bean
        public UserUpdatedMessageSender userUpdatedMessageSender(JmsTemplate jmsTemplate) {
            return new UserUpdatedMessageSender(jmsTemplate);
        }

        @Bean
        public SmbContactUpdatedMessageProcessor messageProcessor(IUserService userService,
                SmbContactService smbContactService, SmbContactSynchronizer synchronizer) {
            return new SmbContactUpdatedMessageProcessor(userService, smbContactService, synchronizer);
        }

        @Bean
        public SmbContactCompanyToUserCompanyMapper companyMapper() {
            return new SmbContactCompanyToUserCompanyMapperImpl();
        }

        @Bean
        public SmbContactSynchronizer synchronizer() {
            return new SmbContactSynchronizerImpl();
        }

        @Bean
        public ApplicationMapper applicationMapper() {
            return new ApplicationMapperImpl();
        }

        @Bean
        public AppRoleMapper appRoleMapper() {
            return new AppRoleMapperImpl();
        }

        @Bean
        public UserMapper userMapper() {
            return new UserMapperImpl();
        }

        @Bean
        public ReferenceMapper referenceMapper() {
            return new ReferenceMapperImpl();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(11);
        }

        @Bean
        public BeanPatcher beanPatcher() {
            return new BeanPatcher();
        }

        @Bean
        public IReferenceService referenceService(ReferenceRepository referenceRepository,
                ReferenceMapper referenceMapper) {
            return new ReferenceServiceImpl(referenceRepository, referenceMapper);
        }

        @Bean
        public IApplicationService applicationService(ApplicationRepository appRepo, ApplicationMapper appMapper,
                AppRoleMapper appRoleMapper, PasswordEncoder passwordEncoder) {
            return new ApplicationServiceImpl(appRepo, appMapper, appRoleMapper, passwordEncoder);
        }

        @Bean
        public IUserService userService(UserMapper userMapper, UserRepository userRepo, AppRoleRepository appRoleRepo,
                IApplicationService appService, IReferenceService referenceService,
                BeanPatcher beanPatcher, ApplicationEventPublisher eventPublisher,
                CreateUserPwdTokenRepository createUserPwdTokenRepo,
                RegPwdChangeRepository regPwdChangeRepo) {
            return new UserServiceImpl(userMapper, userRepo, appRoleRepo, appService, referenceService, beanPatcher,
                    eventPublisher, createUserPwdTokenRepo, regPwdChangeRepo);
        }

    }

}
