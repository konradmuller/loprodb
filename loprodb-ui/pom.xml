<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <parent>
    <artifactId>loprodb</artifactId>
    <groupId>org.vdma</groupId>
    <version>0.1</version>
  </parent>
  <modelVersion>4.0.0</modelVersion>

  <artifactId>loprodb-ui</artifactId>
  <name>loprodb-ui</name>
  <version>0.1</version>
  <packaging>jar</packaging>

  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <vaadin.version>14.3.0</vaadin.version>

    <drivers.dir>${project.basedir}/drivers</drivers.dir>
    <drivers.downloader.phase>pre-integration-test</drivers.downloader.phase>
  </properties>

  <repositories>
    <repository>
      <id>vaadin-addons</id>
      <url>https://maven.vaadin.com/vaadin-addons</url>
    </repository>
  </repositories>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-bom</artifactId>
        <version>${vaadin.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
      <groupId>org.claspina</groupId>
      <artifactId>confirm-dialog</artifactId>
      <version>2.0.0</version>
    </dependency>
    <dependency>
      <artifactId>loprodb-lib</artifactId>
      <groupId>org.vdma</groupId>
      <version>0.1</version>
    </dependency>
    <dependency>
      <artifactId>loprodb-email</artifactId>
      <groupId>org.vdma</groupId>
      <version>0.1</version>
    </dependency>
    <dependency>
      <groupId>org.vdma</groupId>
      <artifactId>smb-ws</artifactId>
      <version>0.1</version>
    </dependency>
    <dependency>
      <groupId>org.vdma</groupId>
      <artifactId>loprodb-schema</artifactId>
      <version>0.1</version>
    </dependency>
    <dependency>
      <groupId>com.oracle.database.jdbc</groupId>
      <artifactId>ojdbc8</artifactId>
      <version>${oracle.version}</version>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
    </dependency>
    <dependency>
      <groupId>org.liquibase</groupId>
      <artifactId>liquibase-core</artifactId>
      <version>${liquibase.version}</version>
    </dependency>
    <dependency>
      <groupId>org.vaadin.gatanaso</groupId>
      <artifactId>multiselect-combo-box-flow</artifactId>
      <version>2.4.1.rc1</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-cas</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-config</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>${commons-io.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.onelogin</groupId>
      <artifactId>java-saml</artifactId>
      <version>2.9.0</version>
    </dependency>

  </dependencies>

  <build>
    <defaultGoal>spring-boot:run</defaultGoal>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <!-- Clean build and startup time for Vaadin apps sometimes may exceed
                     the default Spring Boot's 30sec timeout.  -->
        <configuration>
          <wait>500</wait>
          <maxAttempts>240</maxAttempts>
        </configuration>
      </plugin>

      <!--
          Take care of synchronizing java dependencies and imports in
          package.json and main.js files.
          It also creates webpack.config.js if not exists yet.
      -->
      <plugin>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-maven-plugin</artifactId>
        <version>${vaadin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>prepare-frontend</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>1.8</version>
        <executions>
          <execution>
            <phase>package</phase>
            <configuration>
              <target>
                <copy file="target/${project.artifactId}-${project.version}.jar" tofile="target/${project.artifactId}"/>
              </target>
            </configuration>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.5.1</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
          <annotationProcessorPaths>
            <path>
              <groupId>org.mapstruct</groupId>
              <artifactId>mapstruct-processor</artifactId>
              <version>${org.mapstruct.version}</version>
            </path>
            <path>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <version>${lombok.version}</version>
            </path>
          </annotationProcessorPaths>
          <compilerArgs>
            <compilerArg>
              -Amapstruct.defaultComponentModel=spring
            </compilerArg>
          </compilerArgs>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <!-- Production mode is activated using -Pproduction -->
      <id>production</id>
      <properties>
        <vaadin.productionMode>true</vaadin.productionMode>
      </properties>

      <dependencies>
        <dependency>
          <groupId>com.vaadin</groupId>
          <artifactId>flow-server-production-mode</artifactId>
        </dependency>
      </dependencies>

      <build>
        <plugins>
          <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
              <jvmArguments>-Dvaadin.productionMode</jvmArguments>
              <executable>true</executable>
            </configuration>
          </plugin>
          <plugin>
            <groupId>com.vaadin</groupId>
            <artifactId>vaadin-maven-plugin</artifactId>
            <executions>
              <execution>
                <goals>
                  <goal>build-frontend</goal>
                </goals>
                <phase>compile</phase>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

  </profiles>


</project>