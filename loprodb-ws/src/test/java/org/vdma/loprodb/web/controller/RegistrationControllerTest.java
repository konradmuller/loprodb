package org.vdma.loprodb.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Configuration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.email.EmailTemplateSender;
import org.vdma.loprodb.email.RegistrationEmailSender;
import org.vdma.loprodb.entity.BaseCompanyInfo;
import org.vdma.loprodb.entity.EmailTemplate;
import org.vdma.loprodb.entity.EmailTemplateType;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.Registration;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUAL_REVIEW_PENDING;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_2;
import static org.vdma.loprodb.entity.RegistrationVariant.V2;
import static org.vdma.loprodb.entity.RegistrationVariant.V3;
import static org.vdma.loprodb.entity.RegistrationVariant.V6_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V6_2;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.repository.EmailTemplateRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegistrationRepository;
import org.vdma.loprodb.samba.model.SmbAddressStatus;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.model.SmbEmailType;
import org.vdma.loprodb.samba.model.SmbPostAddressData;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationMatchContactResult;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationService;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEmailTemplateService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.web.IntegrationTest;

@RunWith(SpringRunner.class)
@IntegrationTest
@Import(RegistrationControllerTest.RestControllerTestConfig.class)
@TestPropertySource(locations = "classpath:test.properties")
public class RegistrationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private IApplicationService applicationService;

    @Autowired
    private ReferenceRepository referenceRepo;
    
    @Autowired
    private RegistrationRepository registrationRepo;

    @Autowired
    private IRegistrationService registrationService;

    @Autowired
    private EmailTemplateRepository emailTemplateRepo;

    @Autowired
    private IUserService userService;

    @MockBean
    private JavaMailSender mailSender;

    @MockBean
    private SmbRegistrationService smbRegistrationService;

    private Reference pos;
    private Reference businessArea;

    @Before
    public void prepare() {
        CreateAppParam app = new CreateAppParam();
        app.setName("app name");
        app.setCode("app");
        app.setPassword("app");
        app.setRoles(new HashSet<>(Arrays.asList(new AppRoleData("admin"), new AppRoleData("client"))));
        applicationService.create(app);

        pos = new Reference();
        pos.setNameDe("pos");
        pos.setNameEn("pos");
        pos.setSambaId(1000L);
        pos.setRefType(RefType.USER_POSITION);
        pos = referenceRepo.save(pos);

        businessArea = new Reference();
        businessArea.setNameDe("area");
        businessArea.setNameEn("area");
        businessArea.setSambaId(2000L);
        businessArea.setRefType(RefType.USER_BUSINESS_AREA);
        businessArea = referenceRepo.save(businessArea);

        EmailTemplate optInTemplate = new EmailTemplate();
        optInTemplate.setTemplateType(EmailTemplateType.OPT_IN);
        optInTemplate.setBodyDe("bodyDe");
        optInTemplate.setBodyEn("bodyEn");
        optInTemplate.setSubjectDe("subjDe");
        optInTemplate.setSubjectEn("subjEn");
        emailTemplateRepo.save(optInTemplate);

        EmailTemplate rejectedTemplate = new EmailTemplate();
        rejectedTemplate.setTemplateType(EmailTemplateType.REGISTRATION_REJECTED);
        rejectedTemplate.setBodyDe("rejected de");
        rejectedTemplate.setBodyEn("rejected en");
        rejectedTemplate.setSubjectDe("rejected de");
        rejectedTemplate.setSubjectEn("rejected en");
        emailTemplateRepo.save(rejectedTemplate);
    }

    @Test
    @DirtiesContext
    public void testCreate() throws Exception {
        RegistrationData reg = createRegistrationData();

        mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(reg))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.position.nameDe", equalTo("pos")))
                .andExpect(jsonPath("$.businessArea.nameDe", equalTo("area")))
                .andExpect(jsonPath("$.dsVersion", equalTo("1.0.0")))
                .andExpect(jsonPath("$.email", equalTo("email@email.com")))
                .andExpect(jsonPath("$.firstName", equalTo("first")))
                .andExpect(jsonPath("$.lastName", equalTo("last")))
                .andExpect(jsonPath("$.function", equalTo("func")))
                .andExpect(jsonPath("$.gender", equalTo("MALE")))
                .andExpect(jsonPath("$.phone", equalTo("111")))
                .andExpect(jsonPath("$.mobile", equalTo("222")))
                .andExpect(jsonPath("$.title", equalTo("title")))
                .andExpect(jsonPath("$.language", equalTo("DE")))
                .andExpect(jsonPath("$.company.city", equalTo("Berlin")))
                .andExpect(jsonPath("$.company.country", equalTo("Germany")))
                .andExpect(jsonPath("$.company.street", equalTo("Berger strasse")))
                .andExpect(jsonPath("$.company.houseNumber", equalTo("1A")))
                .andExpect(jsonPath("$.company.zip", equalTo("111")))
                .andExpect(jsonPath("$.company.name", equalTo("VDMA")))
                .andExpect(jsonPath("$.token", not(empty())))
                .andExpect(jsonPath("$.createdDate", notNullValue()))
                .andExpect(jsonPath("$.lastModifiedDate", notNullValue()))
                .andExpect(jsonPath("$.createdBy", equalTo("app")))
                .andExpect(jsonPath("$.lastModifiedBy", equalTo("app")))
                .andExpect(jsonPath("$.status", equalTo("OPT_IN_PENDING")));

        // verify opt-in email sending

        TestTransaction.flagForCommit();
        TestTransaction.end();

        ArgumentCaptor<MimeMessagePreparator> preparatorCaptor = ArgumentCaptor.forClass(MimeMessagePreparator.class);
        Mockito.verify(mailSender).send(preparatorCaptor.capture());
        MimeMessage mimeMessage = new MimeMessage((Session) null);
        preparatorCaptor.getValue().prepare(mimeMessage);
        Assert.assertTrue(
                Arrays.stream(mimeMessage.getAllRecipients()).anyMatch(a -> a.toString().contains(reg.getEmail())));
    }

    @Test
    public void testCreate_Unauthenticated() throws Exception {
        mvc.perform(post("/s2s/registration")
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreate_WrongCredentials() throws Exception {
        mvc.perform(post("/s2s/registration")
                .with(httpBasic("app1", "app1"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreate_RequiredFieldEmpty() throws Exception {
        RegistrationData reg = createRegistrationData();
        reg.setFirstName(null);

        mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(reg))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testConfirm_SingleSambaContact_VdmaMember() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact = createContactData();
        contact.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact.setId(1L);
        contact.setStatus(SmbAddressStatus.LEFT_COMPANY);
        contact.setVdmaMember(true);

        contact.getCompany().setStatus(SmbAddressStatus.ACTIVE);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(AUTO_CONFIRMED_MEMBER.name())))
                .andExpect(jsonPath("$.user.createPasswordToken", not(blankOrNullString())))
                .andExpect(jsonPath("$.user.firstName", equalTo(regData.getFirstName())))
                .andExpect(jsonPath("$.user.lastName", equalTo(regData.getLastName())))
                .andExpect(jsonPath("$.user.sambaId", equalTo(1)))
                .andExpect(jsonPath("$.user.email", equalTo(regData.getEmail())))
                .andExpect(jsonPath("$.user.tags[0]", equalTo(V1_1.getTag())));

        Registration reg = registrationService.findRegistration(regData.getToken());

        Assert.assertSame(reg.getStatus(), AUTO_CONFIRMED_MEMBER);
        Assert.assertNotNull(reg.getUser());
        Assert.assertEquals(reg.getUser().getEmail(), regData.getEmail());
        Assert.assertEquals(reg.getRegistrationVariant(), V1_1);
        Assert.assertTrue(reg.getTags().contains(V1_1.getTag()));

        mvc.perform(get("/s2s/users/{userId}", regData.getEmail())
                .param("userIdType", "email")
                .with(httpBasic("vdma", "vdma"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo(reg.getFirstName())))
                .andExpect(jsonPath("$.lastName", equalTo(reg.getLastName())))
                .andExpect(jsonPath("$.gender", equalTo(reg.getGender().name())))
                .andExpect(jsonPath("$.title", equalTo(reg.getTitle())))
                .andExpect(jsonPath("$.phone", equalTo(reg.getPhone())))
                .andExpect(jsonPath("$.publicMobile", equalTo(reg.getMobile())))
                .andExpect(jsonPath("$.language", equalTo(reg.getLanguage().name())))
                .andExpect(jsonPath("$.function", equalTo(reg.getFunction())))
                .andExpect(jsonPath("$.dsVersion", equalTo(reg.getDsVersion())))
                .andExpect(jsonPath("$.sambaId", equalTo(1)))
                .andExpect(jsonPath("$.position.id", equalTo(reg.getPosition().getId().intValue())))
                .andExpect(jsonPath("$.businessArea.id", equalTo(reg.getBusinessArea().getId().intValue())))
                .andExpect(jsonPath("$.company.name", equalTo(reg.getCompany().getName())))
                .andExpect(jsonPath("$.company.zip", equalTo(reg.getCompany().getZip())))
                .andExpect(jsonPath("$.company.city", equalTo(reg.getCompany().getCity())))
                .andExpect(jsonPath("$.company.street", equalTo(reg.getCompany().getStreet())))
                .andExpect(jsonPath("$.company.country", equalTo(reg.getCompany().getCountry())))
                .andExpect(jsonPath("$.company.houseNumber", equalTo(reg.getCompany().getHouseNumber())))
                .andExpect(jsonPath("$.apps[?(@.appCode == 'vdma')]").exists())
                .andExpect(jsonPath("$.apps[?(@.appCode == 'vdma')].roles[?(@.name == 'member')]").exists())
                .andExpect(jsonPath("$.tags[0]", equalTo(V1_1.getTag())));
    }

    @Test
    public void test_SingleSambaContact_VdmaMember_NoFullMatch() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contactData = createContactData();
        contactData.getCompany().getAddress().setStreet("CompletelyWrongStreet 112");
        
        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contactData));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())));

        Registration reg = registrationService.findRegistration(regData.getToken());

        Assert.assertSame(reg.getStatus(), MANUAL_REVIEW_PENDING);
    }

    @Test
    public void testConfirm_SingleSambaContact_VdmaMember_EmailNotPersonal() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact = new SmbContactData();
        contact.setEmailType(SmbEmailType.SECRETARIAT_EMAIL);
        contact.setId(1L);
        contact.setStatus(SmbAddressStatus.ACTIVE);
        contact.setVdmaMember(true);

        SmbCompanyData company = new SmbCompanyData();
        company.setStatus(SmbAddressStatus.ACTIVE);

        contact.setCompany(company);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());
    }

    @Test
    public void testConfirm_SingleSambaContact_NotVdmaMember() throws Exception {
        createVdmaApp(false);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact = createContactData();
        contact.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact.setStatus(SmbAddressStatus.ACTIVE);
        contact.setId(20L);
        contact.setVdmaMember(false);

        contact.getCompany().setStatus(SmbAddressStatus.ACTIVE);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(AUTO_CONFIRMED_NON_MEMBER.name())))
                .andExpect(jsonPath("$.user.createPasswordToken", not(blankOrNullString())))
                .andExpect(jsonPath("$.user.firstName", equalTo(regData.getFirstName())))
                .andExpect(jsonPath("$.user.lastName", equalTo(regData.getLastName())))
                .andExpect(jsonPath("$.user.sambaId", equalTo(20)))
                .andExpect(jsonPath("$.user.email", equalTo(regData.getEmail())))
                .andExpect(jsonPath("$.user.tags[0]", equalTo(V1_2.getTag())))
                .andExpect(jsonPath("$.user.apps[?(@.appCode == 'vdma')]").exists());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V1_2);
        Assert.assertTrue(reg.getTags().contains(V1_2.getTag()));
    }

    @Test
    @DirtiesContext
    public void testConfirm_SingleSambaContact_ParentCompanyNotActive() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact = new SmbContactData();
        contact.setStatus(SmbAddressStatus.ACTIVE);
        contact.setId(20L);
        contact.setVdmaMember(false);

        SmbCompanyData company = new SmbCompanyData();
        company.setStatus(SmbAddressStatus.IN_LIQUIDATION);

        contact.setCompany(company);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V1_2);
    }

    @Test
    public void testConfirm_SeveralSambaContacts_NoneVdmaMembers() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = new SmbContactData();
        contact1.setId(20L);

        SmbContactData contact2 = new SmbContactData();
        contact2.setId(30L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Arrays.asList(contact1, contact2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V2);
        Assert.assertTrue(reg.getTags().contains(V2.getTag()));
    }

    @Test
    public void testConfirm_SeveralSambaContacts_OneVdmaMember() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = new SmbContactData();
        contact1.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact1.setVdmaMember(true);
        contact1.setId(22L);

        SmbContactData contact2 = new SmbContactData();
        contact2.setId(30L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Arrays.asList(contact1, contact2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())));

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V2);
        Assert.assertTrue(reg.getTags().contains(V2.getTag()));
    }

    @Test
    public void testConfirm_SeveralSambaContacts_OneVdmaMember_EmailNotPersonal() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = new SmbContactData();
        contact1.setEmailType(SmbEmailType.SECRETARIAT_EMAIL);
        contact1.setVdmaMember(true);
        contact1.setId(22L);

        SmbContactData contact2 = new SmbContactData();
        contact2.setId(30L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Arrays.asList(contact1, contact2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V2);
        Assert.assertTrue(reg.getTags().contains(V2.getTag()));
    }

    @Test
    public void testConfirm_SeveralSambaContacts_SeveralVdmaMembers_OneWithSameName() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = new SmbContactData();
        contact1.setVdmaMember(true);
        contact1.setFirstName("");
        contact1.setLastName("");
        contact1.setId(22L);

        SmbContactData contact2 = new SmbContactData();
        contact2.setVdmaMember(true);
        contact2.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact2.setFirstName(regData.getFirstName());
        contact2.setLastName(regData.getLastName());
        contact2.setId(33L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Arrays.asList(contact1, contact2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())));

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V2);
        Assert.assertTrue(reg.getTags().contains(V2.getTag()));
    }

    @Test
    public void testConfirm_SeveralSambaContacts_SeveralVdmaMembers_SeveralWithSameName() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = new SmbContactData();
        contact1.setVdmaMember(true);
        contact1.setFirstName(regData.getFirstName());
        contact1.setLastName(regData.getLastName());
        contact1.setId(22L);

        SmbContactData contact2 = new SmbContactData();
        contact2.setVdmaMember(true);
        contact2.setFirstName(regData.getFirstName());
        contact2.setLastName(regData.getLastName());
        contact2.setId(33L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Arrays.asList(contact1, contact2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V2);
        Assert.assertTrue(reg.getTags().contains(V2.getTag()));
    }

    @Test
    public void testConfirm_OneActiveVdmaMemberCompany() throws Exception {
        createVdmaApp(true);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbCompanyData company1 = new SmbCompanyData();
        company1.setVdmaMember(true);
        company1.setStatus(SmbAddressStatus.ACTIVE);
        company1.setId(100L);

        SmbCompanyData company2 = new SmbCompanyData();
        company2.setVdmaMember(true);
        company2.setStatus(SmbAddressStatus.IN_LIQUIDATION);
        company2.setId(101L);

        SmbContactData newContact = new SmbContactData();
        newContact.setFirstName(regData.getFirstName());
        newContact.setLastName(regData.getLastName());
        newContact.setEmail(regData.getEmail());
        newContact.setId(123L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setCompanies(Arrays.asList(company1, company2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);
        Mockito.when(smbRegistrationService.createContact(Mockito.any(RegistrationData.class), Mockito.eq(100L)))
                .thenReturn(newContact);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(AUTO_CONFIRMED_MEMBER.name())))
                .andExpect(jsonPath("$.user.createPasswordToken", not(blankOrNullString())))
                .andExpect(jsonPath("$.user.firstName", equalTo(regData.getFirstName())))
                .andExpect(jsonPath("$.user.lastName", equalTo(regData.getLastName())))
                .andExpect(jsonPath("$.user.sambaId", equalTo(123)))
                .andExpect(jsonPath("$.user.email", equalTo(regData.getEmail())))
                .andExpect(jsonPath("$.user.tags[0]", equalTo(V3.getTag())))
                .andExpect(jsonPath("$.user.apps[?(@.appCode == 'vdma')]").exists())
                .andExpect(jsonPath("$.user.apps[?(@.appCode == 'vdma')].roles[?(@.name == 'member')]").exists());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V3);
        Assert.assertTrue(reg.getTags().contains(V3.getTag()));
    }

    @Test
    @DirtiesContext
    public void testConfirm_NoActiveCompanies() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbCompanyData company1 = new SmbCompanyData();
        company1.setVdmaMember(true);
        company1.setStatus(SmbAddressStatus.DECEASED);
        company1.setId(100L);

        SmbCompanyData company2 = new SmbCompanyData();
        company2.setVdmaMember(true);
        company2.setStatus(SmbAddressStatus.BANKRUPTCY);
        company2.setId(101L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setCompanies(Arrays.asList(company1, company2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V3);
    }

    @Test
    public void testConfirm_SeveralCompanies_ActiveExist() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbCompanyData company1 = new SmbCompanyData();
        company1.setVdmaMember(true);
        company1.setStatus(SmbAddressStatus.BANKRUPTCY);
        company1.setId(100L);

        SmbCompanyData company2 = new SmbCompanyData();
        company2.setStatus(SmbAddressStatus.ACTIVE);
        company2.setId(101L);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setCompanies(Arrays.asList(company1, company2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V3);
        Assert.assertTrue(reg.getTags().contains(V3.getTag()));
    }

    @Test
    public void testConfirm_RelatedCompaniesFound() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbCompanyData company1 = new SmbCompanyData();
        company1.setId(100L);
        SmbRegistrationMatchContactResult.SmbScoredRelatedCompany related1 =
                new SmbRegistrationMatchContactResult.SmbScoredRelatedCompany();
        related1.setCompany(company1);

        SmbCompanyData company2 = new SmbCompanyData();
        company2.setId(101L);
        SmbRegistrationMatchContactResult.SmbScoredRelatedCompany related2 =
                new SmbRegistrationMatchContactResult.SmbScoredRelatedCompany();
        related2.setCompany(company2);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setRelatedCompanies(Arrays.asList(related1, related2));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V6_2);
        Assert.assertTrue(reg.getTags().contains(V6_2.getTag()));
    }

    @Test
    public void testConfirm_MatchResultEmpty() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(new SmbRegistrationMatchContactResult());

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", equalTo(MANUAL_REVIEW_PENDING.name())))
                .andExpect(jsonPath("$.user").doesNotExist());

        Registration reg = registrationService.findRegistration(regData.getToken());
        Assert.assertEquals(reg.getRegistrationVariant(), V6_1);
        Assert.assertTrue(reg.getTags().contains(V6_1.getTag()));
    }

    @Test
    public void testConfirm_AlreadyConfirmedFailed() throws Exception {
        
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(new SmbRegistrationMatchContactResult());
        
        RegistrationData registration = objectMapper.readValue(responseJson, RegistrationData.class);

        mvc.perform(post("/s2s/registration/{token}/confirm", registration.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isOk());

        Registration regEntity = registrationRepo.findByToken(registration.getToken());
        regEntity.setCreatedDate(LocalDateTime.now().minusDays(3));
        registrationRepo.save(regEntity);
        
        mvc.perform(post("/s2s/registration/{token}/confirm", registration.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConfirm_TokenNotFound() throws Exception {
        mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(new SmbRegistrationMatchContactResult());

        mvc.perform(post("/s2s/registration/{token}/confirm", "123")
                .with(httpBasic("app", "app")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConfirm_VdmaAppNotFound() throws Exception {
        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = createContactData();
        contact1.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact1.setId(20L);
        contact1.setVdmaMember(true);

        contact1.getCompany().setStatus(SmbAddressStatus.ACTIVE);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact1));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConfirm_VdmaMemberRoleNotFound() throws Exception {
        createVdmaApp(false);

        String responseJson = mvc.perform(post("/s2s/registration")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(createRegistrationData()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegistrationData regData = objectMapper.readValue(responseJson, RegistrationData.class);

        SmbContactData contact1 = createContactData();
        contact1.setId(20L);
        contact1.setEmailType(SmbEmailType.PERSONAL_EMAIL);
        contact1.setVdmaMember(true);

        contact1.getCompany().setStatus(SmbAddressStatus.ACTIVE);

        SmbRegistrationMatchContactResult smbMatchResult = new SmbRegistrationMatchContactResult();
        smbMatchResult.setContacts(Collections.singletonList(contact1));
        Mockito.when(smbRegistrationService.matchContact(Mockito.any(RegistrationData.class)))
                .thenReturn(smbMatchResult);

        mvc.perform(post("/s2s/registration/{token}/confirm", regData.getToken())
                .with(httpBasic("app", "app")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testValidateEmail() throws Exception {
        Reference black1 = new Reference();
        black1.setRefType(RefType.EMAIL_BLACK_LIST);
        black1.setNameDe("info@");
        referenceRepo.save(black1);

        Reference black2 = new Reference();
        black2.setRefType(RefType.EMAIL_BLACK_LIST);
        black2.setNameDe("@hotmail.com");
        referenceRepo.save(black2);

        Reference black3 = new Reference();
        black3.setRefType(RefType.EMAIL_BLACK_LIST);
        black3.setNameDe("noreply@gmx.com");
        referenceRepo.save(black3);

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "info@email.com"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("black list")));

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "email@hotmail.com"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("black list")));

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "noreply@gmx.com"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("black list")));

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "email@vdma.org"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("vdma email")));

        createUser("test@email.com");

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "test@email.com"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("email exists")));

        Mockito.when(smbRegistrationService.getSimilarContactEmails("uncle.bob@siemens.org"))
                .thenReturn(Collections.emptySet());
        Mockito.when(smbRegistrationService.getSimilarContactEmails("t1000@bmw.com"))
                .thenReturn(new HashSet<>(Arrays.asList("t1000@bmw.net", "t1000@bmw.tv")));

        createUser("uncle.bob@siemens.com");

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "uncle.bob@siemens.org"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("similar email found")));

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "t1000@bmw.com"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("similar email found")));

        Mockito.when(smbRegistrationService.getSimilarContactEmails(Mockito.anyString())).thenReturn(Collections
                .emptySet());

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "leo@barca.com"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsStringIgnoringCase("OK")));

        createUser("steve@apple.co.uk");

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "steve@apple.com"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsStringIgnoringCase("ok")));

        mvc.perform(get("/s2s/registration/validate-email")
                .with(httpBasic("app", "app"))
                .param("email", "steve@123hotmail.com"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsStringIgnoringCase("ok")));
    }

    private void createVdmaApp(boolean withRoleMember) {
        CreateAppParam createVdmaApp = new CreateAppParam();
        createVdmaApp.setName("VDMA app");
        createVdmaApp.setCode("vdma");
        createVdmaApp.setPassword("vdma");
        if (withRoleMember) {
            createVdmaApp.setRoles(new HashSet<>(Collections.singletonList(new AppRoleData("member"))));
        }
        applicationService.create(createVdmaApp);
    }

    private void createUser(String email) {
        UserData user = new UserData();
        user.setEmail(email);
        user.setStatus(UserStatus.ACTIVE);
        user.setGender(Gender.MALE);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPhone("111");
        user.setPublicMobile("222");
        user.setFunction("func");
        user.setPosition(new ReferenceData(pos.getId()));
        user.setBusinessArea(new ReferenceData(businessArea.getId()));
        user.setLanguage(Language.DE);
        userService.create(user);
    }

    private RegistrationData createRegistrationData() {
        ReferenceData position = new ReferenceData();
        position.setSambaId(1000L);

        ReferenceData businessArea = new ReferenceData();
        businessArea.setSambaId(2000L);

        RegistrationData reg = new RegistrationData();
        reg.setPosition(position);
        reg.setBusinessArea(businessArea);
        reg.setDsVersion("1.0.0");
        reg.setEmail("email@email.com");
        reg.setFirstName("first");
        reg.setLastName("last");
        reg.setFunction("func");
        reg.setGender(Gender.MALE);
        reg.setPhone("111");
        reg.setMobile("222");
        reg.setTitle("title");
        reg.setLanguage(Language.DE);

        BaseCompanyInfo company = new BaseCompanyInfo();
        company.setCity("Berlin");
        company.setCountry("Germany");
        company.setStreet("Berger strasse");
        company.setHouseNumber("1A");
        company.setZip("111");
        company.setName("VDMA");

        reg.setCompany(company);
        return reg;
    }

    private SmbContactData createContactData() {
        SmbContactData contactData = new SmbContactData();
        contactData.setEmail("email@email.com");
        contactData.setFirstName("first");
        contactData.setLastName("last");
        SmbPostAddressData addressData = new SmbPostAddressData();
        addressData.setStreet("Berger str 1A");
        addressData.setCity("Berlin");
        SmbCompanyData companyData = new SmbCompanyData();
        companyData.setAddress(addressData);
        companyData.setName("VDMA");
        contactData.setCompany(companyData);
        return contactData;
    }

    public static class RestControllerTestConfig {
        @Bean
        public EmailTemplateSender emailTemplateSender(JavaMailSender mailSender,
                                                       IEmailTemplateService emailTemplateService, Configuration configuration) {
            return new EmailTemplateSender(mailSender, emailTemplateService, configuration);
        }

        @Bean
        public RegistrationEmailSender registrationEmailSender(EmailTemplateSender emailTemplateSender) {
            return new RegistrationEmailSender(emailTemplateSender);
        }
    }
}
