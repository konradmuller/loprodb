package org.vdma.loprodb.web;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.WsAsyncConfig;
import org.vdma.loprodb.config.JmsConfig;
import org.vdma.loprodb.email.EmailTemplateSender;
import org.vdma.loprodb.email.RegistrationEmailSender;
import org.vdma.loprodb.messaging.user.UserUpdatedMessageSender;

@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@WebMvcTest
@ComponentScan(basePackages = "org.vdma.loprodb",
        excludeFilters = { @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                value = { JmsConfig.class, UserUpdatedMessageSender.class, RegistrationEmailSender.class,
                    WsAsyncConfig.class, EmailTemplateSender.class }) })
@Transactional
@AutoConfigureCache
@AutoConfigureDataJpa
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@ActiveProfiles({ "test" })
public @interface IntegrationTest {
}
