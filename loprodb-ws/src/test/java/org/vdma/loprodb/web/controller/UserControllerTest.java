package org.vdma.loprodb.web.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserAppData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.CreateUserPwdToken;
import org.vdma.loprodb.entity.ExtendedCompanyInfo;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;
import static org.vdma.loprodb.event.user.UpdateSource.LOPRODB;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.web.IntegrationTest;

@RunWith(SpringRunner.class)
@IntegrationTest
@TestPropertySource(locations = "classpath:test.properties")
public class UserControllerTest {

    private static final String USER_EMAIL = "user@email.com";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private IApplicationService applicationService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ReferenceRepository referenceRepo;

    @Autowired
    private IEventLogService eventLogService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CreateUserPwdTokenRepository createUserPwdTokenRepo;

    private UserData user;

    @Before
    public void prepare() {
        CreateAppParam createAppParam = new CreateAppParam();
        createAppParam.setName("app name");
        createAppParam.setCode("app");
        createAppParam.setPassword("app");
        createAppParam.setRoles(new HashSet<>(Arrays.asList(new AppRoleData("admin"), new AppRoleData("client"))));

        ApplicationData app = applicationService.create(createAppParam);

        Reference pos = new Reference();
        pos.setNameDe("pos");
        pos.setNameEn("pos");
        pos.setRefType(RefType.USER_POSITION);
        pos.setSambaId(1000L);
        pos = referenceRepo.save(pos);

        Reference businessArea = new Reference();
        businessArea.setNameDe("area");
        businessArea.setNameEn("area");
        businessArea.setRefType(RefType.USER_BUSINESS_AREA);
        businessArea.setSambaId(2000L);
        businessArea = referenceRepo.save(businessArea);

        user = new UserData();
        user.setEmail(USER_EMAIL);
        user.setStatus(UserStatus.ACTIVE);
        user.setGender(Gender.MALE);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPhone("123");
        user.setFunction("func");
        user.setPosition(new ReferenceData(pos.getId()));
        user.setBusinessArea(new ReferenceData(businessArea.getId()));
        user.setLanguage(Language.DE);
        user.setSambaId(1L);

        ExtendedCompanyInfo companyInfo = new ExtendedCompanyInfo();
        companyInfo.setCity("Berlin");
        companyInfo.setName("Siemens");

        user.setCompany(companyInfo);

        UserAppData userAppData = new UserAppData(app.getId());
        userAppData.setRoles(app.getRoles().stream()
                .filter(ar -> "admin".equals(ar.getName())).collect(Collectors.toSet()));
        user.setApps(Collections.singleton(userAppData));
        user = userService.create(user);
    }

    @Test
    public void testGet_OK() throws Exception {
        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .param("userIdType", "email")
                .param("appMessage", "message 1")
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get("/s2s/users/{userId}", user.getExternalId())
                .param("userIdType", "profdbId")
                .param("appMessage", "message 2")
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get("/s2s/users/{userId}", 1)
                .param("userIdType", "sambaId")
                .param("appMessage", "message 3")
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apps[0].appCode", equalTo("app")))
                .andExpect(jsonPath("$.apps[0].roles", hasSize(1)))
                .andExpect(jsonPath("$.apps[0].roles[0].name", equalTo("admin")));

        List<EventLogData> events = eventLogService.getForUser(user.getId(), new ObjectListParam()).getContent();
        Assert.assertEquals(3, events.size());
        Assert.assertTrue(events.stream().anyMatch(
                logData -> "message 1".equals(logData.getMessage()) && "app name".equals(logData.getAppName())));
    }

    @Test
    public void testGet_IncorrectIdType() throws Exception {
        mvc.perform(get("/s2s/users/{userId}", 1)
                .param("userIdType", "wrongId")
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        mvc.perform(get("/s2s/users/{userId}", "example@email.com")
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Unauthenticated() throws Exception {
        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGet_WrongCredentials() throws Exception {
        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .with(httpBasic("app1", "app1"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGet_StatusNotActive() throws Exception {
        user.setStatus(UserStatus.BLOCKED);
        userService.update(user.getExternalId(), user, LOPRODB);

        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.LOCKED.value()))
                .andExpect(jsonPath("$.message",
                        Matchers.containsString(UserStatus.BLOCKED.name())));
    }

    @Test
    public void testPatch() throws Exception {
        Reference pos = new Reference();
        pos.setNameDe("pos2");
        pos.setNameEn("pos2");
        pos.setRefType(RefType.USER_POSITION);
        pos.setSambaId(100L);
        pos = referenceRepo.save(pos);

        Reference businessArea = new Reference();
        businessArea.setNameDe("area2");
        businessArea.setNameEn("area2");
        businessArea.setRefType(RefType.USER_BUSINESS_AREA);
        businessArea = referenceRepo.save(businessArea);

        Reference topic1 = new Reference();
        topic1.setNameDe("topic1");
        topic1.setNameEn("topic1");
        topic1.setRefType(RefType.USER_TOPIC);
        topic1 = referenceRepo.save(topic1);

        Reference topic2 = new Reference();
        topic2.setNameDe("topic2");
        topic2.setNameEn("topic2");
        topic2.setRefType(RefType.USER_TOPIC);
        topic2 = referenceRepo.save(topic2);

        Map<String, Number> positionData = new HashMap<>();
        positionData.put("sambaId", pos.getSambaId());

        Map<String, Object> patchData = new HashMap<>();
        patchData.put("aimId", 999);
        patchData.put("sambaId", 777);
        patchData.put("autoDeactivation", true);
        patchData.put("autoDeactivationDate", LocalDate.of(2021, 1, 1));
        patchData.put("title", "new title");
        patchData.put("function", "func2");
        patchData.put("position", positionData);
        patchData.put("department", "depart");
        patchData.put("birthday", LocalDate.of(1990, 12, 1));
        patchData.put("publicPhone", "111");
        patchData.put("publicEmail", "email@public.com");
        patchData.put("publicMobile", "222");
        patchData.put("businessArea", businessArea);
        patchData.put("topics", Arrays.asList(topic1.getId(), topic2.getId()));
        patchData.put("status", UserStatus.INACTIVE);
        patchData.put("gender", Gender.FEMALE);
        patchData.put("lastName", "hound");
        patchData.put("phone", "333");
        patchData.put("language", Language.EN);
        patchData.put("dsVersion", "10.0.0");
        patchData.put("externalId", "abcde");

        Map<String, Object> companyInfo = new HashMap<>();
        companyInfo.put("name", "Siemens new name");
        companyInfo.put("name2", "Siemens 2");
        companyInfo.put("name3", "Siemens 3");
        companyInfo.put("city", "München");
        companyInfo.put("zip", "0000");
        companyInfo.put("street", "str.");
        companyInfo.put("houseNumber", "12a");
        companyInfo.put("country", "US");
        companyInfo.put("location", "company location");
        companyInfo.put("district", "company district");

        patchData.put("company", companyInfo);

        mvc.perform(patch("/s2s/users/{userId}", USER_EMAIL)
                .param("appMessage", "message")
                .with(httpBasic("app", "app"))
                .content(objectMapper.writeValueAsString(patchData))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.function", equalTo("func2")))
                .andExpect(jsonPath("$.position.nameDe", equalTo("pos2")))
                .andExpect(jsonPath("$.businessArea.nameDe", equalTo("area2")))
                .andExpect(jsonPath("$.topics", hasSize(2)))
                .andExpect(jsonPath("$.topics[?(@.nameDe == 'topic1')]").exists())
                .andExpect(jsonPath("$.topics[?(@.nameDe == 'topic2')]").exists())
                .andExpect(jsonPath("$.status", equalTo("INACTIVE")))
                .andExpect(jsonPath("$.gender", equalTo("FEMALE")))
                .andExpect(jsonPath("$.aimId", equalTo(999)))
                .andExpect(jsonPath("$.sambaId", equalTo(777)))
                .andExpect(jsonPath("$.autoDeactivation", equalTo(true)))
                .andExpect(jsonPath("$.autoDeactivationDate", equalTo("2021-01-01")))
                .andExpect(jsonPath("$.title", equalTo("new title")))
                .andExpect(jsonPath("$.department", equalTo("depart")))
                .andExpect(jsonPath("$.birthday", equalTo("1990-12-01")))
                .andExpect(jsonPath("$.publicPhone", equalTo("111")))
                .andExpect(jsonPath("$.publicEmail", equalTo("email@public.com")))
                .andExpect(jsonPath("$.publicMobile", equalTo("222")))
                .andExpect(jsonPath("$.phone", equalTo("333")))
                .andExpect(jsonPath("$.language", equalTo("EN")))
                .andExpect(jsonPath("$.dsVersion", equalTo("10.0.0")))
                // check if other properties (untouched and prohibited) haven't been changed
                .andExpect(jsonPath("$.company.name", equalTo("Siemens")))
                .andExpect(jsonPath("$.externalId", equalTo(user.getExternalId())))
                .andExpect(jsonPath("$.email", equalTo(USER_EMAIL)))
                .andExpect(jsonPath("$.lastName", equalTo("last")))
                .andExpect(jsonPath("$.firstName", equalTo("first")))
                .andExpect(jsonPath("$.company.name", equalTo("Siemens")))
                .andExpect(jsonPath("$.company.name2").doesNotExist())
                .andExpect(jsonPath("$.company.name3").doesNotExist())
                .andExpect(jsonPath("$.company.zip").doesNotExist())
                .andExpect(jsonPath("$.company.city", equalTo("Berlin")))
                .andExpect(jsonPath("$.company.street").doesNotExist())
                .andExpect(jsonPath("$.company.houseNumber").doesNotExist())
                .andExpect(jsonPath("$.company.country").doesNotExist())
                .andExpect(jsonPath("$.company.location").doesNotExist())
                .andExpect(jsonPath("$.company.district").doesNotExist());

        List<EventLogData> events = eventLogService.getForUser(user.getId(), new ObjectListParam()).getContent();
        Assert.assertEquals(1, events.size());
    }

    @Test
    public void testChangeRegPassword() throws Exception {
        mvc.perform(post("/s2s/users/{userId}/change-pwd", USER_EMAIL)
                .param("userIdType", "wrongId")
                .param("password", "567")
                .with(httpBasic("app", "app")))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/s2s/users/{userId}/change-pwd", USER_EMAIL)
                .param("password", "123"))
                .andExpect(status().isUnauthorized());

        mvc.perform(post("/s2s/users/{userId}/change-pwd", USER_EMAIL)
                .with(httpBasic("app1", "app1"))
                .param("password", "123"))
                .andExpect(status().isUnauthorized());

        mvc.perform(post("/s2s/users/{userId}/change-pwd", "bob@siemens.com")
                .with(httpBasic("app", "app"))
                .param("password", "123"))
                .andExpect(status().isNotFound());

        mvc.perform(post("/s2s/users/{userId}/change-pwd", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .param("password", "123"))
                .andExpect(status().isOk());

        User u = userService.findUser(user.getId());

        Assert.assertNotNull(u.getRegPwdChangedDate());
        Assert.assertNotNull(u.getRegPwdChange());
        Assert.assertEquals("123", u.getRegPwdChange().getPassword());

        mvc.perform(post("/s2s/users/{userId}/change-pwd", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .param("password", "567"))
                .andExpect(status().isOk());

        Assert.assertEquals("567", u.getRegPwdChange().getPassword());
    }

    @Test
    public void testCreateRegPassword() throws Exception {
        CreateUserPwdToken token = new CreateUserPwdToken(userService.findUser(user.getId()));
        createUserPwdTokenRepo.save(token);

        mvc.perform(post("/s2s/users/{userId}/create-pwd", user.getEmail())
                .with(httpBasic("app", "app"))
                .param("password", "pwd")
                .param("token", "wrong token"))
                .andExpect(status().isNotFound());

        mvc.perform(post("/s2s/users/{userId}/create-pwd", user.getEmail())
                .with(httpBasic("app", "app"))
                .param("password", "pwd")
                .param("token", token.getToken()))
                .andExpect(status().isOk());

        // same token twice -> not found
        mvc.perform(post("/s2s/users/{userId}/create-pwd", user.getEmail())
                .with(httpBasic("app", "app"))
                .param("password", "pwd2")
                .param("token", token.getToken()))
                .andExpect(status().isNotFound());

        User u = userService.findUser(user.getId());
        Assert.assertEquals("pwd", u.getRegPwdChange().getPassword());
    }

    @Test
    public void testPhoto_Upload_Delete() throws Exception {
        mvc.perform(multipart("/s2s/users/{userId}/photo", USER_EMAIL)
                .file("file", new byte[] { 1, 2, 3 })
                .with(httpBasic("app", "app"))
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(status().isNoContent());

        String userJson = mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.photoCheckSum").isNotEmpty())
                .andExpect(jsonPath("$.photoChangedOn").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        TypeReference<Map<String, Object>> token = new TypeReference<Map<String, Object>>() {
        };
        Map<String, Object> user = objectMapper.readValue(userJson, token);

        mvc.perform(delete("/s2s/users/{userId}/photo", USER_EMAIL)
                .with(httpBasic("app", "app")))
                .andExpect(status().isNoContent());

        mvc.perform(get("/s2s/users/{userId}", USER_EMAIL)
                .with(httpBasic("app", "app"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.photoCheckSum").doesNotExist())
                .andExpect(jsonPath("$.photoChangedOn", not(equalTo(user.get("photoChangedOn")))));
    }
}
