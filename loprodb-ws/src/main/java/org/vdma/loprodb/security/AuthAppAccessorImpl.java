package org.vdma.loprodb.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.web.IAppAccessor;

@Component
public class AuthAppAccessorImpl implements IAppAccessor {
    @Override
    public String getCurrentApp() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        throw new ApplicationNotFoundException("Application not found in security context!");
    }
}
