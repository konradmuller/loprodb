package org.vdma.loprodb.security;

import java.util.Collections;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.repository.ApplicationRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppUserDetailsService implements UserDetailsService {

    private final ApplicationRepository applicationRepo;

    @Override
    public UserDetails loadUserByUsername(String appCode) throws UsernameNotFoundException {
        Application app = applicationRepo.findByCode(appCode);
        if (app == null) {
            log.warn("No application found with code: {}", appCode);
            throw new UsernameNotFoundException("No app found with email: " + appCode);
        }
        log.debug("app: {} found", appCode);
        return new AppUserDetails(app.getId(), app.getName(), app.getCode(), app.getPassword(),
                true, true, true, true,
                Collections.emptyList());
    }
}
