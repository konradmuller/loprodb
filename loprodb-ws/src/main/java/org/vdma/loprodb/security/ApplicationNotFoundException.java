package org.vdma.loprodb.security;

import org.springframework.security.core.AuthenticationException;

public class ApplicationNotFoundException extends AuthenticationException {
    public ApplicationNotFoundException(String msg) {
        super(msg);
    }
}
