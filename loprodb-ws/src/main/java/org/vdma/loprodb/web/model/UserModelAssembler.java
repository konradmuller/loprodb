package org.vdma.loprodb.web.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.web.controller.PhotoController;

@Component
public class UserModelAssembler implements SimpleRepresentationModelAssembler<UserData> {

    @Override
    public void addLinks(EntityModel<UserData> resource) {
        UserData userData = resource.getContent();
        if (userData != null && StringUtils.isNotBlank(userData.getPhotoCheckSum())) {
            resource.add(linkTo(methodOn(PhotoController.class).getUserPhoto(userData.getExternalId()))
                    .withRel("photo"));
        }
    }

    @Override
    public void addLinks(CollectionModel<EntityModel<UserData>> resources) {

    }
}
