package org.vdma.loprodb.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.vdma.loprodb.service.IUserService;

@RestController
@RequestMapping("/photos")
@Api(value = "Photos", tags = { "Photos" })
@RequiredArgsConstructor
public class PhotoController {

    private final IUserService userService;

    @GetMapping(value = "/users/{userId}", produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(value = "Get user photo", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getUserPhoto(
            @ApiParam(value = "ProfDB ID", required = true) @PathVariable("userId") String userExternalId) {
        byte[] photo = userService.getUserPhoto(userExternalId);
        if (photo == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(photo, HttpStatus.OK);
    }

}
