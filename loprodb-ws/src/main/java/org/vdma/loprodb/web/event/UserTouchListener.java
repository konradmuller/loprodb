package org.vdma.loprodb.web.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.vdma.loprodb.service.IEventLogService;

@Component
@RequiredArgsConstructor
@Slf4j
public class UserTouchListener {

    public final IEventLogService eventLogService;

    @Async
    @EventListener
    public void onUserTouch(UserTouchEvent event) {
        log.debug("User {} touched by the app {} via web service", event.getUserId(), event.getAppCode());
        if (StringUtils.isNotBlank(event.getAppMsg())) {
            log.debug("logging app message for the user {}...", event.getUserId());
            eventLogService.createLogForUser(event.getUserId(), event.getAppCode(), event.getAppMsg());
        }
    }
}
