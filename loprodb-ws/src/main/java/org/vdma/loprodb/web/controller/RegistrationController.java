package org.vdma.loprodb.web.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.persistence.EntityExistsException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.Views;
import org.vdma.loprodb.entity.RegistrationStatus;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUAL_REVIEW_PENDING;
import static org.vdma.loprodb.entity.RegistrationStatus.OPT_IN_PENDING;
import org.vdma.loprodb.entity.RegistrationVariant;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_2;
import static org.vdma.loprodb.entity.RegistrationVariant.V2;
import static org.vdma.loprodb.entity.RegistrationVariant.V3;
import static org.vdma.loprodb.entity.RegistrationVariant.V6_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V6_2;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.rest.ConfirmRegistrationResult;
import org.vdma.loprodb.samba.model.SmbCompanyData;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationMatchContactResult;
import org.vdma.loprodb.samba.service.registration.SmbRegistrationService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserService;

@RestController
@RequestMapping("/s2s/registration")
@Api(value = "Registration", tags = {"Registration"})
@RequiredArgsConstructor
public class RegistrationController {

    private static final String VDMA_EMAIL_SUFFIX = "@vdma.org";
    public static final String STREET_PATTERN = "(?i:%s)([ ]?)(?i:strasse|straße|street|str|str\\.)([ ]?)(%s)";
    public static final String COMPANY_NAME_PATTERN = "(%s)(?:[ ]?)(?i:gmbh|ag)?";

    private final IRegistrationService registrationService;
    private final IUserService userService;
    private final SmbRegistrationService smbRegistrationService;

    @JsonView(Views.Read.class)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, response = RegistrationData.class, message = "OK")
    })
    @ApiOperation(value = "${ctrl.registration.create}", notes = "${ctrl.registration.create.notes}",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RegistrationData create(
            @RequestBody @JsonView(Views.Modify.class) @Valid RegistrationData registrationData) {
        registrationService.findByEmail(registrationData.getEmail())
                .ifPresent(reg -> {
                    if (reg.getStatus() == MANUAL_REVIEW_PENDING) {
                        throw new EntityExistsException(String.format(
                                "Registration for email '%s' already exists and being verified",
                                registrationData.getEmail()));
                    }
                });
        return registrationService.create(registrationData);
    }

    @PostMapping(value = "/{token}/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ConfirmRegistrationResult.class)
    })
    @ApiOperation(value = "${ctrl.registration.confirm}", notes = "${ctrl.registration.confirm.notes}")
    public ConfirmRegistrationResult confirm(@ApiParam(
            value = "${ctrl.registration.confirm.param.token}") @PathVariable("token") String token) {

        RegistrationData reg = registrationService.getRegistration(token);
        
        Set<RegistrationStatus> statusesAllowedToConfirm = new HashSet<>();
        statusesAllowedToConfirm.add(MANUAL_REVIEW_PENDING);
        statusesAllowedToConfirm.add(AUTO_CONFIRMED_MEMBER);
        statusesAllowedToConfirm.add(AUTO_CONFIRMED_NON_MEMBER);
        
        // If the registration is not older than 48 hours and is in one of statuses
        // which can be set after the successful confirmation, then return success
        // (allowing the confirmation link to be clicked more than once)
        if (LocalDateTime.now().isAfter(reg.getCreatedDate().plusDays(2))) {
            throw new EntityNotExistsException("Registration is older than 48 hours");
        } else if (statusesAllowedToConfirm.contains(reg.getStatus())) {
            return new ConfirmRegistrationResult(reg.getStatus(), reg.getUser(), true);
        } else if (reg.getStatus() != OPT_IN_PENDING) {
            throw new EntityNotExistsException(
                    "Registration is not in OPT_IN_PENDING and not in one of the statuses which allow confirming again");
        }

        SmbRegistrationMatchContactResult smbMatchContact = smbRegistrationService.matchContact(reg);

        RegistrationData processed;
        List<SmbContactData> contacts = smbMatchContact.getContacts();
        if (contacts.size() == 1) {
            SmbContactData contact = contacts.get(0);
            RegistrationStatus status = contact.isVdmaMember() ? AUTO_CONFIRMED_MEMBER : AUTO_CONFIRMED_NON_MEMBER;
            RegistrationVariant variant = contact.isVdmaMember() ? V1_1 : V1_2;
            if (contact.getCompany().isActive() && contact.isEmailPersonal() && matchesFully(reg, contact)) {
                processed = registrationService.confirm(reg.getToken(), status, variant, contact.getId(),
                        contact.isVdmaMember());
            } else {
                processed = registrationService.sendToManualReview(reg.getToken(), variant);
            }
        } else if (!contacts.isEmpty()) {
            processed = registrationService.sendToManualReview(reg.getToken(), V2);
        } else if (!smbMatchContact.getCompanies().isEmpty()) {
            List<SmbCompanyData> activeVdmaMembers = smbMatchContact.getCompanies().stream()
                    .filter(SmbCompanyData::isVdmaMember)
                    .filter(SmbCompanyData::isActive)
                    .collect(Collectors.toList());
            if (activeVdmaMembers.size() == 1) {
                SmbContactData contact = smbRegistrationService.createContact(reg, activeVdmaMembers.get(0).getId());
                processed = registrationService.confirm(reg.getToken(), AUTO_CONFIRMED_MEMBER, V3, contact.getId(),
                        true);
            } else {
                processed = registrationService.sendToManualReview(reg.getToken(), V3);
            }
        } else if (!smbMatchContact.getRelatedCompanies().isEmpty()) {
            processed = registrationService.sendToManualReview(reg.getToken(), V6_2);
        } else {
            processed = registrationService.sendToManualReview(reg.getToken(), V6_1);
        }
        return new ConfirmRegistrationResult(processed.getStatus(), processed.getUser(), false);
    }

    @GetMapping(value = "/validate-email")
    @ApiOperation(value = "${ctrl.registration.validate-email}", notes = "${ctrl.registration.validate-email.notes}")
    public ResponseEntity<String> validateEmail(@RequestParam(name = "email") String email) {
        if (registrationService.isBlackListEmail(email)) {
            return ResponseEntity.badRequest().body("black list");
        }
        if (email.endsWith(VDMA_EMAIL_SUFFIX)) {
            return ResponseEntity.badRequest().body("vdma email");
        }
        if (userService.emailExists(email, null)) {
            return ResponseEntity.badRequest().body("email exists");
        }
        if (!userService.getSimilarEmails(email).isEmpty() ||
                !smbRegistrationService.getSimilarContactEmails(email).isEmpty()) {
            return ResponseEntity.ok("similar email found");
        }
        return ResponseEntity.ok("OK");
    }

    private boolean matchesFully(RegistrationData registrationData, SmbContactData contactData) {
        Pattern companyNamePattern =
                Pattern.compile(String.format(COMPANY_NAME_PATTERN, registrationData.getCompany().getName()));
        return equalsTrimIgnoreCase(registrationData.getEmail(), contactData.getEmail()) &&
                equalsTrimIgnoreCase(registrationData.getFirstName(), contactData.getFirstName()) &&
                equalsTrimIgnoreCase(registrationData.getLastName(), contactData.getLastName()) &&
                Pattern.compile(String.format(STREET_PATTERN,
                        normalizeStreet(registrationData.getCompany().getStreet()),
                        registrationData.getCompany().getHouseNumber()))
                        .matcher(
                                normalizeStreet(contactData.getCompany().getAddress().getStreet()))
                        .matches() &&
                (companyNamePattern.matcher(trimToEmpty(contactData.getCompany().getName())).matches() ||
                        companyNamePattern.matcher(trimToEmpty(contactData.getCompany().getName1())).matches() ||
                        companyNamePattern.matcher(trimToEmpty(contactData.getCompany().getName2())).matches() ||
                        companyNamePattern.matcher(trimToEmpty(contactData.getCompany().getName3())).matches());
    }

    private boolean equalsTrimIgnoreCase(String first, String last) {
        return trimToEmpty(first).equalsIgnoreCase(trimToEmpty(last));
    }

    private String normalizeStreet(final String original) {
        return trimToEmpty(original).replaceAll("\\s*stra(ß|ss)e$|\\s*str(eet|\\.)?$", "").trim();
    }
}
