package org.vdma.loprodb.web;

public interface IAppAccessor {
    String getCurrentApp();
}
