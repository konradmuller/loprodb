package org.vdma.loprodb.web;

import java.io.PrintWriter;
import java.io.StringWriter;
import javax.persistence.EntityExistsException;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.rest.ApiError;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {

    private static final String API_EXCEPTION = "API exception";

    @ExceptionHandler({ IllegalArgumentException.class, MethodArgumentNotValidException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ApiError handle(HttpServletRequest request, Exception exception) {
        log.warn(API_EXCEPTION, exception);
        return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(EntityNotExistsException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ApiError handle(HttpServletRequest request, EntityNotExistsException exception) {
        log.warn(API_EXCEPTION + " - requested entity not found", exception);
        return new ApiError(HttpStatus.NOT_FOUND, exception.getMessage());
    }
    
    @ExceptionHandler(EntityExistsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ApiError handle(HttpServletRequest request, EntityExistsException exception) {
        log.warn(API_EXCEPTION + " - entity already exists", exception);
        return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ApiError> handle(HttpServletRequest request, ResponseStatusException exception) {
        log.warn(API_EXCEPTION, exception);
        return new ResponseEntity<>(new ApiError(exception.getStatus(), exception.getReason()),
                exception.getStatus());
    }

    @ExceptionHandler({ AuthenticationException.class, AccessDeniedException.class })
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ApiError handleAccessDenied(HttpServletRequest request, Throwable exception) {
        log.warn(API_EXCEPTION + " - access denied", exception);
        return new ApiError(HttpStatus.UNAUTHORIZED, exception.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handle500(HttpServletRequest request, Throwable exception) {
        log.warn(API_EXCEPTION + " - internal server error", exception);

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        stackTrace.flush();

        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage(), stackTrace.toString());
    }
}
