package org.vdma.loprodb.web.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.vdma.loprodb.entity.RegistrationVariant;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_2;
import org.vdma.loprodb.event.user.UserCreatedEvent;
import org.vdma.loprodb.samba.model.SmbContactData;
import org.vdma.loprodb.samba.service.SmbContactService;

@Component
@RequiredArgsConstructor
@Slf4j
public class RegistrationAutoConfirmedListener {

    private final SmbContactService smbContactService;

    @Order(100)
    @TransactionalEventListener(condition = "#event.fromRegistration != null")
    public void onConfirmed(UserCreatedEvent event) {
        RegistrationVariant variant = event.getFromRegistration().getRegistrationVariant();
        if (variant == V1_1 || variant == V1_2) {
            try {
                SmbContactData contact = smbContactService.getContactById(event.getUser().getSambaId());
                if (contact.isNotActive() && contact.getCompany().isActive()) {
                    smbContactService.reactivate(contact.getId());
                    log.info("Samba contact {} has been reactivated!", contact.getId());
                }
            } catch (Exception e) {
                log.warn("Failed to reactivate contact " + event.getUser().getSambaId(), e);
            }
        }
    }

}
