package org.vdma.loprodb.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.vdma.loprodb.config.AppConfigProps;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.web.IAppAccessor;
import org.vdma.loprodb.web.event.UserTouchEvent;
import org.vdma.loprodb.web.model.UserModelAssembler;

@RestController
@RequestMapping("/s2s/users")
@Api(value = "Users", tags = { "Users" })
@RequiredArgsConstructor
public class UserController {

    private final IUserService userService;
    private final IAppAccessor appAccessor;
    private final UserModelAssembler userModelAssembler;
    private final ApplicationEventPublisher eventPublisher;
    private final AppConfigProps appConfigProps;

    @GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${ctrl.users.get}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EntityModel<UserData> get(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.param.app-message}") @RequestParam(name = "appMessage",
                    required = false) String appMessage,
            @ApiParam(value = "${ctrl.users.param.update-last-login-date}") @RequestParam(name = "updateLastLoginDate",
                    required = false, defaultValue = "false") boolean updateLastLoginDate) {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        UserData user = userService.getUserByIdType(idType, userId);
        if (user.getStatus() != UserStatus.ACTIVE) {
            throw new ResponseStatusException(HttpStatus.LOCKED, "User status is " + user.getStatus().name());
        }
        if (Objects.equals(appConfigProps.getVdma().getCode(), appAccessor.getCurrentApp()) && updateLastLoginDate) {
            userService.updateLastSeenDate(user.getId());
        }
        eventPublisher.publishEvent(new UserTouchEvent(appAccessor.getCurrentApp(), user.getId(), appMessage));
        return userModelAssembler.toModel(user);
    }

    @PatchMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${ctrl.users.update}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public EntityModel<UserData> update(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.param.app-message}") @RequestParam(name = "appMessage",
                    required = false) String appMessage,
            @RequestBody Map<String, Object> params) {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        if (params.containsKey("photoBase64")) {
            final String photoBase64 = (String) params.get("photoBase64");
            params.put("photo", isBlank(photoBase64) ? null : Base64.getDecoder().decode(photoBase64));
        }
        UserData user = userService.update(idType, userId, params);
        eventPublisher.publishEvent(new UserTouchEvent(appAccessor.getCurrentApp(), user.getId(), appMessage));
        return userModelAssembler.toModel(user);
    }

    @PostMapping(value = "/{userId}/change-pwd", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${ctrl.users.change-reg-password}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void changeRegPassword(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.change-reg-password.param.password}") @RequestParam(
                    name = "password") String password) {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        UserData user = userService.getUserByIdType(idType, userId);
        userService.changeRegPassword(user.getExternalId(), password);
    }

    @PostMapping(value = "/{userId}/create-pwd", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${ctrl.users.create-reg-password}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void createRegPassword(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.create-reg-password.param.token}") @RequestParam(
                    name = "token") String createPasswordToken,
            @ApiParam(value = "${ctrl.users.change-reg-password.param.password}") @RequestParam(
                    name = "password") String password) {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        UserData user = userService.getUserByIdType(idType, userId);
        userService.createRegPassword(user.getExternalId(), createPasswordToken, password);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(value = "/{userId}/photo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "${ctrl.users.upload-photo}")
    public void uploadPhoto(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.param.app-message}") @RequestParam(name = "appMessage",
                    required = false) String appMessage,
            @RequestParam(name = "file") MultipartFile file) throws Exception {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        userService.setUserPhoto(idType, userId, file.getBytes());
        UserData user = userService.getUserByIdType(idType, userId);
        eventPublisher.publishEvent(new UserTouchEvent(appAccessor.getCurrentApp(), user.getId(), appMessage));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{userId}/photo")
    @ApiOperation(value = "${ctrl.users.delete-photo}")
    public void deletePhoto(
            @ApiParam(value = "${ctrl.users.param.user-id}", required = true) @PathVariable("userId") String userId,
            @ApiParam(value = "${ctrl.users.param.user-id-type}",
                    allowableValues = "email,sambaId,profdbId") @RequestParam(name = "userIdType",
                            required = false, defaultValue = "email") String userIdType,
            @ApiParam(value = "${ctrl.users.param.app-message}") @RequestParam(name = "appMessage",
                    required = false) String appMessage) {
        UserIdType idType = UserIdType.createFromExternalName(userIdType);
        userService.setUserPhoto(idType, userId, null);
        UserData user = userService.getUserByIdType(idType, userId);
        eventPublisher.publishEvent(new UserTouchEvent(appAccessor.getCurrentApp(), user.getId(), appMessage));
    }
}
