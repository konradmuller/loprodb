package org.vdma.loprodb.web.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserTouchEvent {
    private String appCode;
    private Long userId;
    private String appMsg;
}
