package org.vdma.loprodb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.MediaType;
import org.springframework.plugin.core.OrderAwarePluginRegistry;
import org.springframework.plugin.core.PluginRegistry;

@Configuration
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class WsConfig {

    // @Bean
    // @Primary
    // public LinkDiscoverers linkDiscoverer() {
    // List<LinkDiscoverer> plugins = new ArrayList<>();
    // plugins.add(new CollectionJsonLinkDiscoverer());
    // return new LinkDiscoverers(SimplePluginRegistry.create(plugins));
    // }

    // !!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!
    // needed to add this bean due to Spring HATEOAS + springfox startup issue
    // above commented code commonly suggested to fix this problem doesn't work if we add HypermediaType.HAL support
    // maybe in future it will be fixed and we will be able to delete this bean
    // https://stackoverflow.com/questions/58431876/spring-boot-2-2-0-spring-hateoas-startup-issue
    // https://github.com/spring-projects/spring-hateoas/issues/1094
    // https://github.com/OpenAPITools/openapi-generator/issues/5325
    @Bean
    public PluginRegistry<LinkDiscoverer, MediaType> discoverers(
            OrderAwarePluginRegistry<LinkDiscoverer, MediaType> relProviderPluginRegistry) {
        return relProviderPluginRegistry;
    }
}
