package org.vdma.loprodb.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "org.vdma.loprodb")
public class WsStarter {
    public static void main(String[] args) {
        SpringApplication.run(WsStarter.class, args);
    }
}
