package org.vdma.loprodb.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.vdma.loprodb.dto.AppMessageData;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.repository.AppRoleRepository;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.EventLogRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegPwdChangeRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.service.impl.ApplicationServiceImpl;
import org.vdma.loprodb.service.impl.EventLogServiceImpl;
import org.vdma.loprodb.service.impl.ReferenceServiceImpl;
import org.vdma.loprodb.service.impl.UserServiceImpl;
import org.vdma.loprodb.service.mapper.AppMessageMapper;
import org.vdma.loprodb.service.mapper.AppMessageMapperImpl;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.AppRoleMapperImpl;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapperImpl;
import org.vdma.loprodb.service.mapper.EventLogMapper;
import org.vdma.loprodb.service.mapper.EventLogMapperImpl;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.mapper.ReferenceMapperImpl;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.mapper.UserMapperImpl;
import org.vdma.loprodb.service.patch.BeanPatcher;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaAuditing
public class EventLogServiceTest {

    @Autowired
    public IEventLogService eventLogService;

    @Autowired
    public UserRepository userRepo;

    @Autowired
    public ReferenceRepository referenceRepo;

    @Autowired
    public ApplicationRepository applicationRepo;

    private User user;
    private Application app1;
    private Application app2;

    @Before
    public void prepare() {
        Reference pos = referenceRepo.save(new Reference("pos", "pos", RefType.USER_POSITION));
        Reference area = referenceRepo.save(new Reference("area", "area", RefType.USER_BUSINESS_AREA));

        app1 = applicationRepo.save(new Application("app1", "app1", "app1"));
        app2 = applicationRepo.save(new Application("app2", "app2", "app2"));

        user = new User();
        user.setEmail("email@email.com");
        user.setStatus(UserStatus.ACTIVE);
        user.setGender(Gender.MALE);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPhone("000");
        user.setFunction("func");
        user.setPosition(pos);
        user.setBusinessArea(area);
        user.setLanguage(Language.DE);
        user.setSambaId(1L);
        user.setApps(new HashSet<>(Arrays.asList(app1, app2)));
        user = userRepo.save(user);
    }

    @Test
    public void testLastAppMessagesForUser() {
        eventLogService.createLogForUser(user.getId(), "app1", "message 1 from app1");
        eventLogService.createLogForUser(user.getId(), "app2", "message 1 from app2");
        eventLogService.createLogForUser(user.getId(), "app1", "message 2 from app1");
        eventLogService.createLogForUser(user.getId(), "app2", "message 2 from app2");
        eventLogService.createLogForUser(user.getId(), "app1", "message 3 from app1");
        eventLogService.createLogForUser(user.getId(), "app2", "message 3 from app2");

        List<AppMessageData> logs = eventLogService.getLastAppMessagesForUser(user.getId());
        Assert.assertEquals(2, logs.size());

        Assert.assertTrue(logs.stream().anyMatch(msg -> msg.getAppId().equals(app1.getId()) &&
                "message 3 from app1".equals(msg.getMessage())));

        Assert.assertTrue(logs.stream().anyMatch(msg -> msg.getAppId().equals(app2.getId()) &&
                "message 3 from app2".equals(msg.getMessage())));
    }

    @TestConfiguration
    public static class Cfg {

        @Bean
        public IEventLogService eventLogService(EventLogRepository eventLogRepo, EventLogMapper eventLogMapper,
                AppMessageMapper messageMapper, IUserService userService,
                IApplicationService appService) {
            return new EventLogServiceImpl(eventLogRepo, eventLogMapper, messageMapper, userService, appService);
        }

        @Bean
        public EventLogMapper eventLogMapper() {
            return new EventLogMapperImpl();
        }

        @Bean
        public AppMessageMapper appMessageMapper() {
            return new AppMessageMapperImpl();
        }

        @Bean
        public ApplicationMapper applicationMapper() {
            return new ApplicationMapperImpl();
        }

        @Bean
        public AppRoleMapper appRoleMapper() {
            return new AppRoleMapperImpl();
        }

        @Bean
        public UserMapper userMapper() {
            return new UserMapperImpl();
        }

        @Bean
        public ReferenceMapper referenceMapper() {
            return new ReferenceMapperImpl();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(11);
        }

        @Bean
        public BeanPatcher beanPatcher() {
            return new BeanPatcher();
        }

        @Bean
        public IReferenceService referenceService(ReferenceRepository referenceRepository,
                ReferenceMapper referenceMapper) {
            return new ReferenceServiceImpl(referenceRepository, referenceMapper);
        }

        @Bean
        public IApplicationService applicationService(ApplicationRepository appRepo, ApplicationMapper appMapper,
                AppRoleMapper appRoleMapper, PasswordEncoder passwordEncoder) {
            return new ApplicationServiceImpl(appRepo, appMapper, appRoleMapper, passwordEncoder);
        }

        @Bean
        public IUserService userService(UserMapper userMapper, UserRepository userRepo, AppRoleRepository appRoleRepo,
                IApplicationService appService, IReferenceService referenceService,
                BeanPatcher beanPatcher, ApplicationEventPublisher eventPublisher,
                CreateUserPwdTokenRepository createUserPwdTokenRepo,
                RegPwdChangeRepository regPwdChangeRepo) {
            return new UserServiceImpl(userMapper, userRepo, appRoleRepo, appService, referenceService, beanPatcher,
                    eventPublisher, createUserPwdTokenRepo, regPwdChangeRepo);
        }

    }
}
