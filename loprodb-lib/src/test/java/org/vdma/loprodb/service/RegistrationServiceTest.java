package org.vdma.loprodb.service;

import java.util.Collections;
import java.util.HashSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vdma.loprodb.config.AppConfigProps;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.BaseCompanyInfo;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_CONFIRMED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUAL_REVIEW_PENDING;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_1;
import static org.vdma.loprodb.entity.RegistrationVariant.V1_2;
import static org.vdma.loprodb.entity.RegistrationVariant.V2;
import static org.vdma.loprodb.entity.RegistrationVariant.V3;
import static org.vdma.loprodb.entity.RegistrationVariant.V6_2;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.AppRoleRepository;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.EventLogRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegPwdChangeRepository;
import org.vdma.loprodb.repository.RegistrationRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.service.impl.ApplicationServiceImpl;
import org.vdma.loprodb.service.impl.EventLogServiceImpl;
import org.vdma.loprodb.service.impl.ReferenceServiceImpl;
import org.vdma.loprodb.service.impl.RegistrationServiceImpl;
import org.vdma.loprodb.service.impl.UserServiceImpl;
import org.vdma.loprodb.service.mapper.AppMessageMapper;
import org.vdma.loprodb.service.mapper.AppMessageMapperImpl;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.AppRoleMapperImpl;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapperImpl;
import org.vdma.loprodb.service.mapper.EventLogMapper;
import org.vdma.loprodb.service.mapper.EventLogMapperImpl;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.mapper.ReferenceMapperImpl;
import org.vdma.loprodb.service.mapper.RegistrationMapper;
import org.vdma.loprodb.service.mapper.RegistrationMapperImpl;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.mapper.UserMapperImpl;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.service.patch.BeanPatcher;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaAuditing
@Import({ AppConfigProps.class })
@TestPropertySource(locations = "classpath:test.properties")
public class RegistrationServiceTest {

    @Autowired
    public ReferenceRepository referenceRepo;

    @Autowired
    public IApplicationService applicationService;

    @Autowired
    public IRegistrationService registrationService;

    @Autowired
    private AppConfigProps appConfigProps;

    @Autowired
    private IUserService userService;

    private Reference pos;

    private Reference area;

    private RegistrationData registration;

    @Before
    public void prepare() {
        pos = new Reference();
        pos.setNameDe("pos");
        pos.setNameEn("pos");
        pos.setRefType(RefType.USER_POSITION);
        pos = referenceRepo.save(pos);

        area = new Reference();
        area.setNameDe("area");
        area.setNameEn("area");
        area.setRefType(RefType.USER_BUSINESS_AREA);
        area = referenceRepo.save(area);

        createVdmaApp();
        registration = registrationService.create(createRegistrationData());
    }

    @Test
    public void testConfirm_AutoConfirmedMember() {
        RegistrationData confirmed = registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_MEMBER, V1_1,
                1L, true);
        Assert.assertEquals(AUTO_CONFIRMED_MEMBER, confirmed.getStatus());
        Assert.assertEquals(V1_1, confirmed.getRegistrationVariant());
        Assert.assertNotNull(confirmed.getUser());
        Assert.assertEquals(1L, confirmed.getUser().getSambaId().longValue());
        Assert.assertTrue(confirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode()) && ua.getRoles().stream()
                        .anyMatch(r -> appConfigProps.getVdma().getRoleMember().equals(r.getName()))));
        Assert.assertTrue(confirmed.getTags().contains(V1_1.getTag()));
        Assert.assertTrue(confirmed.getUser().getTags().contains(V1_1.getTag()));
        Assert.assertNotNull(confirmed.getUser().getCreatePasswordToken());
    }

    @Test
    public void testConfirm_AutoConfirmedNonMember() {
        RegistrationData confirmed = registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_NON_MEMBER,
                V1_2, 1L, true);
        Assert.assertEquals(AUTO_CONFIRMED_NON_MEMBER, confirmed.getStatus());
        Assert.assertEquals(V1_2, confirmed.getRegistrationVariant());
        Assert.assertNotNull(confirmed.getUser());
        Assert.assertEquals(1L, confirmed.getUser().getSambaId().longValue());
        Assert.assertTrue(confirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode())));
        Assert.assertFalse(confirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode()) && ua.getRoles().stream()
                        .anyMatch(r -> appConfigProps.getVdma().getRoleMember().equals(r.getName()))));
        Assert.assertTrue(confirmed.getTags().contains(V1_2.getTag()));
        Assert.assertTrue(confirmed.getUser().getTags().contains(V1_2.getTag()));
        Assert.assertNotNull(confirmed.getUser().getCreatePasswordToken());
    }

    @Test
    public void testConfirm_Manually() {
        RegistrationData confirmed = registrationService.confirm(registration.getToken(), MANUALLY_CONFIRMED, null, 1L,
                true);
        Assert.assertEquals(MANUALLY_CONFIRMED, confirmed.getStatus());
        Assert.assertNotNull(confirmed.getUser());
        Assert.assertEquals(1L, confirmed.getUser().getSambaId().longValue());
        Assert.assertTrue(confirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode()) && ua.getRoles().stream()
                        .anyMatch(r -> appConfigProps.getVdma().getRoleMember().equals(r.getName()))));
        Assert.assertTrue(confirmed.getTags().isEmpty());
        Assert.assertTrue(confirmed.getUser().getTags().isEmpty());
        Assert.assertNotNull(confirmed.getUser().getCreatePasswordToken());
    }

    @Test
    public void testSendToManualReview_ThenConfirmManually() {
        RegistrationData manualReview = registrationService.sendToManualReview(registration.getToken(), V2);

        Assert.assertEquals(MANUAL_REVIEW_PENDING, manualReview.getStatus());
        Assert.assertNull(manualReview.getUser());

        RegistrationData manuallyConfirmed =
                registrationService.confirm(manualReview.getToken(), MANUALLY_CONFIRMED, V3, 1L, false);

        Assert.assertEquals(MANUALLY_CONFIRMED, manuallyConfirmed.getStatus());
        Assert.assertEquals(V2, manuallyConfirmed.getRegistrationVariant());
        Assert.assertNotNull(manuallyConfirmed.getUser());
        Assert.assertEquals(1L, manuallyConfirmed.getUser().getSambaId().longValue());
        Assert.assertTrue(manuallyConfirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode())));
        Assert.assertFalse(manuallyConfirmed.getUser().getApps().stream()
                .anyMatch(ua -> appConfigProps.getVdma().getCode().equals(ua.getAppCode()) && ua.getRoles().stream()
                        .anyMatch(r -> appConfigProps.getVdma().getRoleMember().equals(r.getName()))));
        Assert.assertTrue(manuallyConfirmed.getTags().contains(V2.getTag()));
        Assert.assertTrue(manuallyConfirmed.getUser().getTags().contains(V2.getTag()));
        Assert.assertNotNull(manuallyConfirmed.getUser().getCreatePasswordToken());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfirm_SambaIdIsNull() {
        registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_MEMBER, null, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfirm_SambaIdAlreadyExists() {
        createUser("mail@mail.com", 1L);
        registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_MEMBER, V1_2, 1L, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfirm_UserWithEmailAlreadyExists() {
        createUser(registration.getEmail(), 200L);
        registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_MEMBER, V1_2, 1L, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfirm_WrongStatus() {
        registrationService.confirm(registration.getToken(), AUTO_REJECTED, null, 1L, false);
    }

    @Test(expected = EntityNotExistsException.class)
    public void testConfirm_AlreadyConfirmed() {
        registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_NON_MEMBER, V1_2, 1L, false);

        registrationService.confirm(registration.getToken(), MANUALLY_CONFIRMED, V1_1, 1L, false);
    }

    @Test(expected = EntityNotExistsException.class)
    public void testConfirm_OnlyManualConfirmationIfReviewPending() {
        registrationService.sendToManualReview(registration.getToken(), V2);

        registrationService.confirm(registration.getToken(), AUTO_CONFIRMED_NON_MEMBER, V1_2, 1L, false);
    }

    @Test
    public void testReject_Auto() {
        RegistrationData rejected = registrationService.reject(registration.getToken(), AUTO_REJECTED);
        Assert.assertEquals(AUTO_REJECTED, rejected.getStatus());
        Assert.assertNull(rejected.getUser());
    }

    @Test
    public void testReject_Manually() {
        RegistrationData rejected = registrationService.reject(registration.getToken(), MANUALLY_REJECTED);
        Assert.assertEquals(MANUALLY_REJECTED, rejected.getStatus());
        Assert.assertNull(rejected.getUser());
    }

    @Test
    public void testSendToManualReview_ThenRejectManually() {
        RegistrationData manualReview = registrationService.sendToManualReview(registration.getToken(), V3);

        Assert.assertEquals(MANUAL_REVIEW_PENDING, manualReview.getStatus());
        Assert.assertNull(manualReview.getUser());

        RegistrationData manuallyRejected = registrationService.reject(manualReview.getToken(), MANUALLY_REJECTED);

        Assert.assertEquals(MANUALLY_REJECTED, manuallyRejected.getStatus());
        Assert.assertEquals(V3, manuallyRejected.getRegistrationVariant());
        Assert.assertTrue(manuallyRejected.getTags().contains(V3.getTag()));
        Assert.assertNull(manuallyRejected.getUser());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReject_WrongStatus() {
        registrationService.reject(registration.getToken(), AUTO_CONFIRMED_MEMBER);
    }

    @Test(expected = EntityNotExistsException.class)
    public void testReject_AlreadyRejected() {
        registrationService.reject(registration.getToken(), AUTO_REJECTED);

        registrationService.reject(registration.getToken(), MANUALLY_REJECTED);
    }

    @Test(expected = EntityNotExistsException.class)
    public void testReject_OnlyManualRejectIfReviewPending() {
        registrationService.sendToManualReview(registration.getToken(), V6_2);

        registrationService.reject(registration.getToken(), AUTO_REJECTED);
    }

    private UserData createUser(String email, Long sambaId) {
        UserData user = new UserData();
        user.setPosition(new ReferenceData(pos.getId()));
        user.setBusinessArea(new ReferenceData(area.getId()));
        user.setDsVersion("2.1");
        user.setEmail(email);
        user.setFirstName("1");
        user.setLastName("2");
        user.setFunction("f");
        user.setLanguage(Language.DE);
        user.setSambaId(sambaId);
        user.setGender(Gender.MALE);
        user.setStatus(UserStatus.ACTIVE);
        return userService.create(user);
    }

    private RegistrationData createRegistrationData() {
        ReferenceData position = new ReferenceData();
        position.setId(pos.getId());

        ReferenceData businessArea = new ReferenceData();
        businessArea.setId(area.getId());

        RegistrationData reg = new RegistrationData();
        reg.setPosition(position);
        reg.setBusinessArea(businessArea);
        reg.setDsVersion("1.0.0");
        reg.setEmail("email@email.com");
        reg.setFirstName("first");
        reg.setLastName("last");
        reg.setFunction("func");
        reg.setGender(Gender.MALE);
        reg.setPhone("111");
        reg.setMobile("222");
        reg.setTitle("title");
        reg.setLanguage(Language.DE);

        BaseCompanyInfo company = new BaseCompanyInfo();
        company.setCity("Berlin");
        company.setCountry("Germany");
        company.setStreet("Berger strasse");
        company.setHouseNumber("1A");
        company.setZip("111");
        company.setName("VDMA");

        reg.setCompany(company);
        return reg;
    }

    private void createVdmaApp() {
        CreateAppParam createVdmaApp = new CreateAppParam();
        createVdmaApp.setName(appConfigProps.getVdma().getCode());
        createVdmaApp.setCode(appConfigProps.getVdma().getCode());
        createVdmaApp.setPassword(appConfigProps.getVdma().getCode());
        createVdmaApp.setRoles(
                new HashSet<>(Collections.singletonList(new AppRoleData(appConfigProps.getVdma().getRoleMember()))));
        applicationService.create(createVdmaApp);
    }

    @TestConfiguration
    public static class Cfg {

        @Bean
        public IEventLogService eventLogService(EventLogRepository eventLogRepo, EventLogMapper eventLogMapper,
                AppMessageMapper messageMapper, IUserService userService,
                IApplicationService appService) {
            return new EventLogServiceImpl(eventLogRepo, eventLogMapper, messageMapper, userService, appService);
        }

        @Bean
        public EventLogMapper eventLogMapper() {
            return new EventLogMapperImpl();
        }

        @Bean
        public AppMessageMapper appMessageMapper() {
            return new AppMessageMapperImpl();
        }

        @Bean
        public ApplicationMapper applicationMapper() {
            return new ApplicationMapperImpl();
        }

        @Bean
        public AppRoleMapper appRoleMapper() {
            return new AppRoleMapperImpl();
        }

        @Bean
        public UserMapper userMapper() {
            return new UserMapperImpl();
        }

        @Bean
        public ReferenceMapper referenceMapper() {
            return new ReferenceMapperImpl();
        }

        @Bean
        public RegistrationMapper registrationMapper() {
            return new RegistrationMapperImpl();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(11);
        }

        @Bean
        public BeanPatcher beanPatcher() {
            return new BeanPatcher();
        }

        @Bean
        public IReferenceService referenceService(ReferenceRepository referenceRepository,
                ReferenceMapper referenceMapper) {
            return new ReferenceServiceImpl(referenceRepository, referenceMapper);
        }

        @Bean
        public IApplicationService applicationService(ApplicationRepository appRepo, ApplicationMapper appMapper,
                AppRoleMapper appRoleMapper, PasswordEncoder passwordEncoder) {
            return new ApplicationServiceImpl(appRepo, appMapper, appRoleMapper, passwordEncoder);
        }

        @Bean
        public IUserService userService(UserMapper userMapper, UserRepository userRepo, AppRoleRepository appRoleRepo,
                IApplicationService appService, IReferenceService referenceService,
                BeanPatcher beanPatcher, ApplicationEventPublisher eventPublisher,
                CreateUserPwdTokenRepository createUserPwdTokenRepo,
                RegPwdChangeRepository regPwdChangeRepo) {
            return new UserServiceImpl(userMapper, userRepo, appRoleRepo, appService, referenceService, beanPatcher,
                    eventPublisher, createUserPwdTokenRepo, regPwdChangeRepo);
        }

        @Bean
        public IRegistrationService registrationService(RegistrationRepository registrationRepository,
                ReferenceRepository referenceRepo, RegistrationMapper registrationMapper,
                IUserService userService, IApplicationService applicationService, AppConfigProps appConfigProps,
                CreateUserPwdTokenRepository createUserPwdTokenRepo, ApplicationEventPublisher eventPublisher) {
            return new RegistrationServiceImpl(registrationRepository, referenceRepo, registrationMapper, userService,
                    applicationService, appConfigProps, createUserPwdTokenRepo, eventPublisher);
        }

    }
}
