package org.vdma.loprodb.messaging.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.User;
import static org.vdma.loprodb.event.user.UpdateSource.LOPRODB;
import org.vdma.loprodb.repository.AppRoleRepository;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegPwdChangeRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.impl.ApplicationServiceImpl;
import org.vdma.loprodb.service.impl.ReferenceServiceImpl;
import org.vdma.loprodb.service.impl.UserServiceImpl;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.AppRoleMapperImpl;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapperImpl;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.mapper.ReferenceMapperImpl;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.mapper.UserMapperImpl;
import org.vdma.loprodb.service.patch.BeanPatcher;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserUpdatedMessageSenderTest.Cfg.class)
public class UserUpdatedMessageSenderTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private IReferenceService referenceService;

    @MockBean
    private JmsTemplate jmsTemplate;

    @Autowired
    private IUserService userService;

    @Autowired
    private UserUpdatedMessageSender sender;

    private User user;

    @Before
    public void prepare() {
        user = new User();
        user.setId(1L);
        user.setSambaId(2L);
        user.setEmail("email@user.com");
        user.setExternalId("externalId");
        user.setVersion(1);
        user.setSambaSyncVersion(1L);

        Mockito.when(referenceService.findReference(Mockito.anyLong(), Mockito.any())).thenReturn(new Reference());
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);

        ReflectionTestUtils.setField(sender, "userUpdatedLiferayQueue", "liferay");
        ReflectionTestUtils.setField(sender, "userUpdatedSambaQueue", "samba");
    }

    @Test
    public void testUserCreated_MessageSent() {
        userService.create(createUserData());

        ArgumentCaptor<String> queueCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<UserUpdatedMessage> msgCaptor = ArgumentCaptor.forClass(UserUpdatedMessage.class);

        Mockito.verify(jmsTemplate, times(2)).convertAndSend(queueCaptor.capture(), msgCaptor.capture());

        List<UserUpdatedMessage> messages = msgCaptor.getAllValues();
        List<String> queues = queueCaptor.getAllValues();

        Assert.assertTrue(messages.stream().allMatch(m -> user.getEmail().equals(m.getUser().getEmail())));
        Assert.assertTrue(messages.stream().allMatch(m -> "CREATED".equals(m.getActionType().name())));
        Assert.assertTrue(queues.contains("liferay"));
        Assert.assertTrue(queues.contains("samba"));

        user.setSambaId(null);
        Mockito.clearInvocations(jmsTemplate);

        userService.create(createUserData());
        queueCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(jmsTemplate).convertAndSend(queueCaptor.capture(), msgCaptor.capture());
        Assert.assertFalse(queueCaptor.getAllValues().contains("samba"));
    }

    @Test
    public void testUserUpdated_MessageSent() {
        Mockito.when(userRepository.findByExternalId(Mockito.anyString())).thenReturn(user);

        UserData userData = createUserData();
        userData.setSambaId(user.getSambaId());
        userData.setSambaSyncVersion(user.getSambaSyncVersion());
        userData.setEmail(user.getEmail());
        userService.update("extId", userData, LOPRODB);

        ArgumentCaptor<String> queueCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<UserUpdatedMessage> msgCaptor = ArgumentCaptor.forClass(UserUpdatedMessage.class);

        Mockito.verify(jmsTemplate, times(2)).convertAndSend(queueCaptor.capture(), msgCaptor.capture());

        List<UserUpdatedMessage> messages = msgCaptor.getAllValues();
        List<String> queues = queueCaptor.getAllValues();

        Assert.assertTrue(messages.stream().allMatch(m -> user.getEmail().equals(m.getUser().getEmail())));
        Assert.assertTrue(messages.stream().allMatch(m -> "UPDATED".equals(m.getActionType().name())));
        Assert.assertTrue(queues.contains("liferay"));
        Assert.assertTrue(queues.contains("samba"));

        user.setSambaId(null);
        Mockito.clearInvocations(jmsTemplate);

        userService.update("extId", createUserData(), LOPRODB);
        queueCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(jmsTemplate).convertAndSend(queueCaptor.capture(), msgCaptor.capture());
        Assert.assertFalse(queueCaptor.getAllValues().contains("samba"));
    }

    @Test
    public void testUserPatched_MessageSent() {
        Mockito.when(userRepository.findByEmailIgnoreCase(Mockito.anyString())).thenReturn(user);

        Map<String, Object> data = new HashMap<>();
        data.put("firstName", "Bastian");

        userService.update(UserIdType.EMAIL, "email@vdma.org", data);

        ArgumentCaptor<String> queueCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<UserUpdatedMessage> msgCaptor = ArgumentCaptor.forClass(UserUpdatedMessage.class);

        Mockito.verify(jmsTemplate, times(2)).convertAndSend(queueCaptor.capture(), msgCaptor.capture());

        List<UserUpdatedMessage> messages = msgCaptor.getAllValues();
        List<String> queues = queueCaptor.getAllValues();

        Assert.assertTrue(messages.stream().allMatch(m -> user.getEmail().equals(m.getUser().getEmail())));
        Assert.assertTrue(messages.stream().allMatch(m -> "UPDATED".equals(m.getActionType().name())));
        Assert.assertTrue(queues.contains("liferay"));
        Assert.assertTrue(queues.contains("samba"));

        user.setSambaId(null);
        Mockito.clearInvocations(jmsTemplate);

        userService.update(UserIdType.EMAIL, "email@vdma.org", data);
        queueCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(jmsTemplate).convertAndSend(queueCaptor.capture(), msgCaptor.capture());
        Assert.assertFalse(queueCaptor.getAllValues().contains("samba"));
    }

    @Test
    public void testUserDeleted_MessageSent() {
        Mockito.when(userRepository.findByExternalId(Mockito.anyString())).thenReturn(user);

        userService.delete("extId");

        ArgumentCaptor<String> queueCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<UserUpdatedMessage> msgCaptor = ArgumentCaptor.forClass(UserUpdatedMessage.class);

        Mockito.verify(jmsTemplate, times(2)).convertAndSend(queueCaptor.capture(), msgCaptor.capture());

        List<UserUpdatedMessage> messages = msgCaptor.getAllValues();
        List<String> queues = queueCaptor.getAllValues();

        Assert.assertTrue(messages.stream().allMatch(m -> user.getEmail().equals(m.getUser().getEmail())));
        Assert.assertTrue(messages.stream().allMatch(m -> "DELETED".equals(m.getActionType().name())));
        Assert.assertTrue(queues.contains("liferay"));
        Assert.assertTrue(queues.contains("samba"));

        user.setSambaId(null);
        Mockito.clearInvocations(jmsTemplate);

        userService.delete("extId");
        queueCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(jmsTemplate).convertAndSend(queueCaptor.capture(), msgCaptor.capture());
        Assert.assertFalse(queueCaptor.getAllValues().contains("samba"));
    }

    private UserData createUserData() {
        UserData userData = new UserData();
        userData.setPosition(new ReferenceData(1L));
        userData.setBusinessArea(new ReferenceData(2L));
        return userData;
    }

    public static class Cfg {

        @MockBean
        private ApplicationRepository applicationRepository;

        @MockBean
        protected AppRoleRepository appRoleRepository;

        @MockBean
        private CreateUserPwdTokenRepository createUserPwdTokenRepo;

        @MockBean
        private RegPwdChangeRepository regPwdChangeRepo;

        @Bean
        public UserUpdatedMessageSender userUpdatedMessageSender(JmsTemplate jmsTemplate) {
            return new UserUpdatedMessageSender(jmsTemplate);
        }

        @Bean
        public ApplicationMapper applicationMapper() {
            return new ApplicationMapperImpl();
        }

        @Bean
        public AppRoleMapper appRoleMapper() {
            return new AppRoleMapperImpl();
        }

        @Bean
        public UserMapper userMapper() {
            return new UserMapperImpl();
        }

        @Bean
        public ReferenceMapper referenceMapper() {
            return new ReferenceMapperImpl();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(11);
        }

        @Bean
        public BeanPatcher beanPatcher() {
            return new BeanPatcher();
        }

        @Bean
        public IReferenceService referenceService(ReferenceRepository referenceRepository,
                ReferenceMapper referenceMapper) {
            return new ReferenceServiceImpl(referenceRepository, referenceMapper);
        }

        @Bean
        public IApplicationService applicationService(ApplicationRepository appRepo, ApplicationMapper appMapper,
                AppRoleMapper appRoleMapper, PasswordEncoder passwordEncoder) {
            return new ApplicationServiceImpl(appRepo, appMapper, appRoleMapper, passwordEncoder);
        }

        @Bean
        public IUserService userService(UserMapper userMapper, UserRepository userRepo, AppRoleRepository appRoleRepo,
                IApplicationService appService, IReferenceService referenceService,
                BeanPatcher beanPatcher, ApplicationEventPublisher eventPublisher,
                CreateUserPwdTokenRepository createUserPwdTokenRepo,
                RegPwdChangeRepository regPwdChangeRepo) {
            return new UserServiceImpl(userMapper, userRepo, appRoleRepo, appService, referenceService, beanPatcher,
                    eventPublisher, createUserPwdTokenRepo, regPwdChangeRepo);
        }

    }
}
