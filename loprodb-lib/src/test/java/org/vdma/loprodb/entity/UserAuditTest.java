package org.vdma.loprodb.entity;

import java.util.Collections;
import org.hibernate.envers.RevisionType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.service.AuditService;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.AppRoleMapperImpl;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapperImpl;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.mapper.ReferenceMapperImpl;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.mapper.UserMapperImpl;
import org.vdma.loprodb.service.param.ObjectListParam;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaAuditing
public class UserAuditTest {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private ReferenceRepository referenceRepo;

    @Autowired
    private ApplicationRepository applicationRepo;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuditService auditService;

    @Autowired
    private UserMapper userMapper;

    private Reference pos;
    private Reference area;

    @Before
    public void prepare() {
        pos = referenceRepo.save(new Reference("p1", "p1", RefType.USER_POSITION));
        area = referenceRepo.save(new Reference("a1", "a1", RefType.USER_BUSINESS_AREA));
    }

    @Test
    public void testAudit() {
        User user = new User();
        user.setEmail("email@mail.de");
        user.setStatus(UserStatus.BLOCKED);
        user.setGender(Gender.MALE);
        user.setFirstName("Rob");
        user.setLastName("Stark");
        user.setPhone("911");
        user.setFunction("f1");
        user.setPosition(pos);
        user.setBusinessArea(area);
        user.setLanguage(Language.DE);
        user.setSambaId(1L);
        user = userRepo.saveAndFlush(user);

        TestTransaction.flagForCommit();
        TestTransaction.end();
        TestTransaction.start();

        user.setFirstName("Aria");
        user = userRepo.saveAndFlush(user);

        TestTransaction.flagForCommit();
        TestTransaction.end();
        TestTransaction.start();

        Application a = applicationRepo.saveAndFlush(new Application("a", "a", "a"));
        user.setApps(Collections.singleton(a));
        user = userRepo.saveAndFlush(user);

        TestTransaction.flagForCommit();
        TestTransaction.end();
        TestTransaction.start();

        ObjectListParam objectListParam = new ObjectListParam();
        objectListParam.setPage(0);
        objectListParam.setSortOrder(Sort.Direction.DESC);
        objectListParam.setPageSize(1);

        Page<AuditData<UserData>> auditedEntries = auditService.filter(entityManager.getEntityManager(), User.class,
                user.getId(), userMapper::map, objectListParam);
        Assert.assertEquals(3, auditedEntries.getTotalElements());
        Assert.assertEquals(1, auditedEntries.getSize());
        Assert.assertEquals(1, auditedEntries.getContent().get(0).getEntityData().getApps().size());
        Assert.assertEquals(a.getId(),
                auditedEntries.getContent().get(0).getEntityData().getApps().iterator().next().getAppId());
        Assert.assertEquals(RevisionType.MOD, auditedEntries.getContent().get(0).getRevisionType());

        objectListParam = new ObjectListParam();
        objectListParam.setSortOrder(Sort.Direction.DESC);
        objectListParam.setPage(1);
        objectListParam.setPageSize(1);

        auditedEntries = auditService.filter(entityManager.getEntityManager(), User.class, user.getId(),
                userMapper::map, objectListParam);
        Assert.assertEquals(3, auditedEntries.getTotalElements());
        Assert.assertEquals(1, auditedEntries.getSize());
        Assert.assertEquals("Aria", auditedEntries.getContent().get(0).getEntityData().getFirstName());
        Assert.assertEquals(0, auditedEntries.getContent().get(0).getEntityData().getApps().size());
        Assert.assertEquals(RevisionType.MOD, auditedEntries.getContent().get(0).getRevisionType());

        objectListParam = new ObjectListParam();
        objectListParam.setSortOrder(Sort.Direction.DESC);
        objectListParam.setPage(2);
        objectListParam.setPageSize(1);

        auditedEntries = auditService.filter(entityManager.getEntityManager(), User.class, user.getId(),
                userMapper::map, objectListParam);
        Assert.assertEquals(3, auditedEntries.getTotalElements());
        Assert.assertEquals(1, auditedEntries.getSize());
        Assert.assertEquals("Rob", auditedEntries.getContent().get(0).getEntityData().getFirstName());
        Assert.assertEquals(RevisionType.ADD, auditedEntries.getContent().get(0).getRevisionType());
    }

    @TestConfiguration
    public static class Cfg {
        @Bean
        public UserMapper userMapper() {
            return new UserMapperImpl();
        }

        @Bean
        public ApplicationMapper applicationMapper() {
            return new ApplicationMapperImpl();
        }

        @Bean
        public AppRoleMapper appRoleMapper() {
            return new AppRoleMapperImpl();
        }

        @Bean
        public ReferenceMapper referenceMapper() {
            return new ReferenceMapperImpl();
        }

        @Bean
        public AuditService userAuditService() {
            return new AuditService();
        }
    }
}
