package org.vdma.loprodb.exception;

public class DuplicateKeyException extends RuntimeException {

    public DuplicateKeyException(String message) {
        super(message);
    }

    public static void assertTrue(String message, boolean condition) {
        if (!condition) {
            throw new DuplicateKeyException(message);
        }
    }
}
