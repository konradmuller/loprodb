package org.vdma.loprodb.exception;

public class EntityNotExistsException extends RuntimeException {

    public EntityNotExistsException(String message) {
        super(message);
    }

    public static void assertNotNull(String message, Object object) {
        if (object == null) {
            throw new EntityNotExistsException(message);
        }
    }
}
