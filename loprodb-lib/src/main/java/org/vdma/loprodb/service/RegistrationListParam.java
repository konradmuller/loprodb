package org.vdma.loprodb.service;

import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.service.param.ObjectListParam;

@Getter
@Setter
public class RegistrationListParam extends ObjectListParam {
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private String companyName;
    private String companyCity;
    private Set<RegistrationStatus> statuses = new HashSet<>();
}
