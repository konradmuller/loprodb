package org.vdma.loprodb.service.impl;

import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.entity.AppRole;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.ApplicationRepository;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.mapper.AppRoleMapper;
import org.vdma.loprodb.service.mapper.ApplicationMapper;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.service.param.application.UpdateAppPasswordParam;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements IApplicationService {

    private final ApplicationRepository appRepo;

    private final ApplicationMapper appMapper;
    private final AppRoleMapper appRoleMapper;

    private final PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public ApplicationData create(CreateAppParam data) {
        Application app = new Application();
        app.setPassword(passwordEncoder.encode(data.getPassword()));
        return save(data, app);
    }

    @Transactional
    @Override
    public ApplicationData update(Long id, ApplicationData data) {
        Application app = findApplication(id);
        return save(data, app);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Application app = findApplication(id);
        appRepo.delete(app);
    }

    @Transactional(readOnly = true)
    @Override
    public ApplicationData getApplication(Long id) {
        Application app = findApplication(id);
        return appMapper.map(app);
    }

    @Transactional(readOnly = true)
    @Override
    public ApplicationData getApplication(String code) {
        Application app = findApplication(code);
        return appMapper.map(app);
    }

    @Override
    public Application findApplication(Long id) {
        return appRepo.findById(id)
                .orElseThrow(() -> new EntityNotExistsException(String.format("Application not found by id '%d'", id)));
    }

    @Override
    public Application findApplication(String code) {
        Application app = appRepo.findByCode(code);
        EntityNotExistsException.assertNotNull(String.format("Application not found by code '%s'", code), app);
        return app;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ApplicationData> filter(ObjectListParam listParam) {
        return appRepo
                .findAll(listParam.buildPageRequest())
                .map(appMapper::map);
    }

    @Transactional
    @Override
    public ApplicationData updatePassword(Long id, UpdateAppPasswordParam param) {
        Application app = findApplication(id);
        app.setPassword(passwordEncoder.encode(param.getPassword()));
        return appMapper.map(appRepo.save(app));
    }

    @Override
    public boolean isNameAvailable(String name, Long exceptId) {
        return appRepo.findFirstByNameIgnoreCaseAndIdNot(name, exceptId != null ? exceptId : 0L) == null;
    }

    @Override
    public boolean isCodeAvailable(String code, Long exceptId) {
        return appRepo.findFirstByCodeIgnoreCaseAndIdNot(code, exceptId != null ? exceptId : 0L) == null;
    }

    private ApplicationData save(ApplicationData data, Application app) {
        Application updated = appMapper.updateApplication(data, app);
        if (app.getId() == null) {
            updated.setRoles(data.getRoles().stream().map(roleData -> {
                AppRole newRole = new AppRole();
                newRole.setApplication(updated);
                return appRoleMapper.updateRole(roleData, newRole);
            }).collect(Collectors.toSet()));
        } else {
            data.getRoles().forEach(roleData -> {
                if (roleData.getId() == null) {
                    updated.getRoles().add(new AppRole(roleData.getName(), app));
                } else {
                    for (AppRole role : updated.getRoles()) {
                        if (Objects.equals(role.getId(), roleData.getId())) {
                            appRoleMapper.updateRole(roleData, role);
                            break;
                        }
                    }
                }
            });
            updated.getRoles().removeIf(
                    role -> data.getRoles().stream().noneMatch(r -> Objects.equals(role.getId(), r.getId())));

        }
        return appMapper.map(appRepo.save(updated));
    }
}
