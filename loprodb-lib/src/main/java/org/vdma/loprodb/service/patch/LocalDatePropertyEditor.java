package org.vdma.loprodb.service.patch;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringUtils;

public class LocalDatePropertyEditor extends PropertyEditorSupport {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public String getAsText() {
        LocalDate date = (LocalDate) getValue();
        return date == null ? "" : FORMATTER.format(date);
    }

    @Override
    public void setAsText(String text) {
        if (StringUtils.isBlank(text)) {
            super.setValue(null);
        } else {
            super.setValue(LocalDate.parse(text, FORMATTER));
        }
    }
}
