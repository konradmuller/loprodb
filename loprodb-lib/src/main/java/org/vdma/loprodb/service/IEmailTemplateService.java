package org.vdma.loprodb.service;

import java.util.List;
import org.vdma.loprodb.dto.EmailTemplateData;
import org.vdma.loprodb.entity.EmailTemplate;
import org.vdma.loprodb.entity.EmailTemplateType;

public interface IEmailTemplateService {
    EmailTemplateData update(Long id, EmailTemplateData data);

    EmailTemplateData getEmailTemplate(Long id);

    EmailTemplateData getEmailTemplate(EmailTemplateType type);

    EmailTemplate findEmailTemplate(Long id);

    List<EmailTemplateData> getAll();
}
