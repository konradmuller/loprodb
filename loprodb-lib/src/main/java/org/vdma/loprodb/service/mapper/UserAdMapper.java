package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.vdma.loprodb.dto.UserAdData;
import org.vdma.loprodb.entity.UserAd;

@Mapper(uses = { ReferenceMapper.class }, componentModel = "spring")
public interface UserAdMapper {
    UserAdData map(UserAd userad);
}
