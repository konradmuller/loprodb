package org.vdma.loprodb.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.AppMessageData;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.entity.EventLog;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.repository.EventLogRepository;
import org.vdma.loprodb.repository.specification.EventLogSpecifications;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IEventLogService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.mapper.AppMessageMapper;
import org.vdma.loprodb.service.mapper.EventLogMapper;
import org.vdma.loprodb.service.param.ObjectListParam;

@Component
@RequiredArgsConstructor
public class EventLogServiceImpl implements IEventLogService {

    private final EventLogRepository eventLogRepo;

    private final EventLogMapper eventLogMapper;

    private final AppMessageMapper messageMapper;

    private final IUserService userService;

    private final IApplicationService appService;

    @Transactional(readOnly = true)
    @Override
    public Page<EventLogData> getForUser(Long userId, ObjectListParam listParam) {
        Specification<EventLog> specification = EventLogSpecifications.user(userId);
        if (StringUtils.isNotBlank(listParam.getSearchTerm())) {
            specification = specification.and(EventLogSpecifications.message(listParam.getSearchTerm().trim()));
        }

        return eventLogRepo
                .findAll(specification, listParam.buildPageRequest())
                .map(eventLogMapper::map);
    }

    @Transactional
    @Override
    public EventLogData createLogForUser(Long userId, String appCode, String appMessage) {
        User user = userService.findUser(userId);
        Application app = appService.findApplication(appCode);
        return eventLogMapper.map(eventLogRepo.save(new EventLog(app, user, appMessage)));
    }

    @Transactional(readOnly = true)
    @Override
    public List<AppMessageData> getLastAppMessagesForUser(Long userId) {
        return eventLogRepo.getLastAppMessagesForUser(userId)
                .stream().map(messageMapper::map).collect(Collectors.toList());
    }
}
