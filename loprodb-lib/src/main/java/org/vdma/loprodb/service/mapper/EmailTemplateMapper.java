package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.vdma.loprodb.dto.EmailTemplateData;
import org.vdma.loprodb.entity.EmailTemplate;

@Mapper
public interface EmailTemplateMapper {

    EmailTemplateData map(EmailTemplate template);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "templateType", ignore = true)
    EmailTemplate updateTemplate(EmailTemplateData data, @MappingTarget EmailTemplate template);
}
