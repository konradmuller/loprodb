package org.vdma.loprodb.service.patch;

import java.util.Map;

public interface PatchDelegate {
    void patch(Map<String, Object> data);
}
