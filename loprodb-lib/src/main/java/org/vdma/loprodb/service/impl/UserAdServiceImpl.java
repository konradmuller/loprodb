package org.vdma.loprodb.service.impl;

import com.google.common.collect.ImmutableList;
import java.util.List;
import static java.util.stream.Collectors.toList;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.UserAdData;
import org.vdma.loprodb.entity.RegistrationStatus;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_CONFIRMED;
import org.vdma.loprodb.entity.UserAd;
import org.vdma.loprodb.repository.UserAdRepository;
import org.vdma.loprodb.service.IUserAdService;
import org.vdma.loprodb.service.mapper.UserAdMapper;

@Service
@RequiredArgsConstructor
public class UserAdServiceImpl implements IUserAdService {

    private final UserAdRepository userAdRepository;
    private final UserAdMapper userAdMapper;

    private static final int MAX_USERS_WITH_UNSENT_EMAIL_TO_FETCH = 20;
    private static final List<RegistrationStatus> CONFIRMED_REGISTRATION_STATUSES = ImmutableList.of(
            AUTO_CONFIRMED_MEMBER, AUTO_CONFIRMED_NON_MEMBER, MANUALLY_CONFIRMED);

    @Override
    public List<UserAdData> findRecentUsersWithUnsentLoginEmails() {
        PageRequest pageRequest = 
                PageRequest.of(0, MAX_USERS_WITH_UNSENT_EMAIL_TO_FETCH);
        return userAdRepository.findAdUsersWithUnsentEmails(CONFIRMED_REGISTRATION_STATUSES, pageRequest)
                .stream()
                .map(userAdMapper::map)
                .collect(toList());
    }

    @Override
    @Transactional
    public void setLoginEmailSent(UserAdData userAdData, Boolean loginEmailSent) {
        UserAd userAd =
                userAdRepository.findByUserIdAndCreatedDate(userAdData.getUserId(), userAdData.getCreatedDate());
        userAd.setLoginEmailSent(loginEmailSent);
        userAdRepository.save(userAd);
    }
}
