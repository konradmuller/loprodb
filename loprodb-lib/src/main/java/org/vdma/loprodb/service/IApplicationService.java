package org.vdma.loprodb.service;

import org.springframework.data.domain.Page;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.service.param.ObjectListParam;
import org.vdma.loprodb.service.param.application.CreateAppParam;
import org.vdma.loprodb.service.param.application.UpdateAppPasswordParam;

public interface IApplicationService {
    ApplicationData create(CreateAppParam data);

    ApplicationData update(Long id, ApplicationData data);

    void delete(Long id);

    ApplicationData getApplication(Long id);

    ApplicationData getApplication(String code);

    Application findApplication(Long id);

    Application findApplication(String code);

    Page<ApplicationData> filter(ObjectListParam listParam);

    ApplicationData updatePassword(Long id, UpdateAppPasswordParam param);

    boolean isNameAvailable(String name, Long exceptId);

    boolean isCodeAvailable(String code, Long exceptId);
}
