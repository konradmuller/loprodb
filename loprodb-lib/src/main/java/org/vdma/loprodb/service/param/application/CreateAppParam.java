package org.vdma.loprodb.service.param.application;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.dto.ApplicationData;

@Getter
@Setter
public class CreateAppParam extends ApplicationData {
    @NotNull
    private String password;
}
