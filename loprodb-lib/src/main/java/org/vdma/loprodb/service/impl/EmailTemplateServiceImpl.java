package org.vdma.loprodb.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.EmailTemplateData;
import org.vdma.loprodb.entity.EmailTemplate;
import org.vdma.loprodb.entity.EmailTemplateType;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.EmailTemplateRepository;
import org.vdma.loprodb.service.IEmailTemplateService;
import org.vdma.loprodb.service.mapper.EmailTemplateMapper;

@Service
@RequiredArgsConstructor
public class EmailTemplateServiceImpl implements IEmailTemplateService {

    private final EmailTemplateMapper mapper;
    private final EmailTemplateRepository repo;

    @Transactional
    @Override
    public EmailTemplateData update(Long id, EmailTemplateData data) {
        EmailTemplate template = findEmailTemplate(id);
        template = repo.save(mapper.updateTemplate(data, template));
        return mapper.map(template);
    }

    @Transactional(readOnly = true)
    @Override
    public EmailTemplateData getEmailTemplate(Long id) {
        return mapper.map(findEmailTemplate(id));
    }

    @Transactional(readOnly = true)
    @Override
    public EmailTemplateData getEmailTemplate(EmailTemplateType type) {
        EmailTemplate template = repo.findByTemplateType(type);
        EntityNotExistsException.assertNotNull("Template not found by type " + type.name(), template);
        return mapper.map(template);
    }

    @Override
    public EmailTemplate findEmailTemplate(Long id) {
        return repo.findById(id)
                .orElseThrow(() -> new EntityNotExistsException(String.format("Template not found by id '%d'", id)));
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmailTemplateData> getAll() {
        return repo.findAll().stream().map(mapper::map).collect(Collectors.toList());
    }
}
