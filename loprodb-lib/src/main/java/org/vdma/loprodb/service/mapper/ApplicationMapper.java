package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.entity.Application;

@Mapper(uses = AppRoleMapper.class)
public interface ApplicationMapper {

    ApplicationData map(Application application);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "roles", ignore = true)
    Application updateApplication(ApplicationData data, @MappingTarget Application application);
}
