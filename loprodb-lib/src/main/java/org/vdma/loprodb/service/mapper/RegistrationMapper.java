package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.service.IReferenceService;

@Mapper(uses = { ReferenceMapper.class, UserMapper.class }, componentModel = "spring")
public abstract class RegistrationMapper {

    @Autowired
    private IReferenceService referenceService;

    public abstract RegistrationData map(Registration registration);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "token", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "editor", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "position", qualifiedByName = "findPosition")
    @Mapping(target = "businessArea", qualifiedByName = "findArea")
    public abstract Registration update(RegistrationData registrationData, @MappingTarget Registration registration);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "publicMobile", source = "mobile")
    public abstract UserData mapToUser(Registration registration);

    @Named("findPosition")
    Reference findPosition(ReferenceData position) {
        return findReferenceByType(position, RefType.USER_POSITION);
    }

    @Named("findArea")
    Reference findArea(ReferenceData businessArea) {
        return findReferenceByType(businessArea, RefType.USER_BUSINESS_AREA);
    }

    private Reference findReferenceByType(ReferenceData referenceData, RefType refType) {
        if (referenceData.getId() != null) {
            return referenceService.findReference(referenceData.getId(), refType);
        } else if (referenceData.getSambaId() != null) {
            return referenceService.findReferenceBySambaIdAndType(referenceData.getSambaId(), refType)
                    .orElseThrow(() -> new EntityNotExistsException(
                            String.format("%s not found by samba id %d", refType.name(), referenceData.getSambaId())));
        } else {
            throw new IllegalArgumentException("ID or Samba ID must be specified");
        }
    }
}
