package org.vdma.loprodb.service.patch;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.service.IReferenceService;

@RequiredArgsConstructor
public class UserReferencePatchDelegate implements PatchDelegate {

    private static final String POSITION_KEY = "position";
    private static final String BUSINESS_AREA_KEY = "businessArea";
    private static final String TOPICS_KEY = "topics";
    private static final String ID_KEY = "id";
    private static final String SAMBA_ID_KEY = "sambaId";

    private final User user;
    private final IReferenceService referenceService;

    @Override
    public void patch(Map<String, Object> data) {
        if (data.containsKey(POSITION_KEY)) {
            setPosition((Map<String, Object>) data.get(POSITION_KEY));
        }
        if (data.containsKey(BUSINESS_AREA_KEY)) {
            setBusinessArea((Map<String, Object>) data.get(BUSINESS_AREA_KEY));
        }
        if (data.containsKey(TOPICS_KEY)) {
            setTopics(new HashSet<>((List<Number>) data.get(TOPICS_KEY)));
        }
    }

    private void setPosition(Map<String, Object> position) {
        Assert.notNull(position, "Position cannot be null");
        user.setPosition(findReferenceByType(position, RefType.USER_POSITION));
    }

    private void setBusinessArea(Map<String, Object> area) {
        Assert.notNull(area, "Business Area cannot be null");
        user.setBusinessArea(findReferenceByType(area, RefType.USER_BUSINESS_AREA));
    }

    private Reference findReferenceByType(Map<String, Object> reference, RefType refType) {
        if (reference.containsKey(ID_KEY)) {
            long id = ((Number) reference.get(ID_KEY)).longValue();
            return referenceService.findReference(id, refType);
        } else if (reference.containsKey(SAMBA_ID_KEY)) {
            long sambaId = ((Number) reference.get(SAMBA_ID_KEY)).longValue();
            return referenceService.findReferenceBySambaIdAndType(sambaId, refType)
                    .orElseThrow(() -> new EntityNotExistsException(
                            String.format("%s not found by samba id %d", refType.name(), sambaId)));
        } else {
            throw new IllegalArgumentException("id or sambaId must be specified");
        }
    }

    private <T extends Number> void setTopics(Set<T> topics) {
        if (topics == null || topics.isEmpty()) {
            user.setTopics(Collections.emptySet());
        } else {
            user.setTopics(topics.stream()
                    .map(topicId -> referenceService.findReference(topicId.longValue(), RefType.USER_TOPIC))
                    .collect(Collectors.toSet()));
        }
    }
}
