package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.Reference;

@Mapper
public interface ReferenceMapper {
    ReferenceData map(Reference reference);

    @Mapping(target = "id", ignore = true)
    Reference update(ReferenceData referenceData, @MappingTarget Reference reference);
}
