package org.vdma.loprodb.service;

import java.util.List;
import org.vdma.loprodb.dto.UserAdData;

public interface IUserAdService {
    
    List<UserAdData> findRecentUsersWithUnsentLoginEmails();
    
    void setLoginEmailSent(UserAdData userAdData, Boolean loginEmailSent);
}
