package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.vdma.loprodb.dto.AppMessageData;
import org.vdma.loprodb.entity.IAppMessage;

@Mapper
public interface AppMessageMapper {
    AppMessageData map(IAppMessage message);
}
