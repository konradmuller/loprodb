package org.vdma.loprodb.service.param.application;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateAppPasswordParam {
    @NotNull
    private String password;
}
