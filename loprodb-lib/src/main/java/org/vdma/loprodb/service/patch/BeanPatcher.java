package org.vdma.loprodb.service.patch;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.lang.annotation.Annotation;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BeanPatcher {

    private static ConcurrentMap<Class, Set<String>> editableCache = new ConcurrentHashMap<>();

    public void copyProperties(Object target, Map<String, Object> source, @NotNull Class<?> editable) {
        Set<String> forUpdate = new HashSet<>(source.keySet());
        if (editable != null) {
            Set<String> available = editableCache.computeIfAbsent(editable, key -> Stream.of(BeanUtils
                    .getPropertyDescriptors(key))
                    .map(PropertyDescriptor::getName).collect(Collectors.toSet()));
            forUpdate.retainAll(available);
        }
        BeanWrapperImpl beanWrapper = new CustomBeanWrapper(target);
        forUpdate.forEach(prop -> {
            try {
                beanWrapper.setPropertyValue(prop, source.get(prop));
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        });

    }

    private static class CustomBeanWrapper extends BeanWrapperImpl {

        CustomBeanWrapper(Object target) {
            super(target);
            registerCustomEditor(Date.class, new CustomDateEditor(new StdDateFormat(), true));
            registerCustomEditor(LocalDate.class, new LocalDatePropertyEditor());
        }

        @Override
        public PropertyEditor findCustomEditor(Class<?> requiredType, String propertyPath) {
            if (Arrays.stream(requiredType.getAnnotations()).map(Annotation::annotationType)
                    .anyMatch(a -> a.equals(Embeddable.class))) {
                Object val = getPropertyValue(propertyPath);
                return new EmbeddableEditor(ObjectUtils.defaultIfNull(val, BeanUtils.instantiateClass(requiredType)));
            }
            return super.findCustomEditor(requiredType, propertyPath);
        }
    }

    private static class EmbeddableEditor extends PropertyEditorSupport {

        private final Object embeddable;

        EmbeddableEditor(Object embeddable) {
            this.embeddable = embeddable;
        }

        @Override
        public void setValue(Object value) {
            Map val = (Map) value;
            copyProperties(embeddable, val);
            super.setValue(embeddable);
        }

        private void copyProperties(Object target, Map<String, Object> source) {
            BeanWrapperImpl beanWrapper = new CustomBeanWrapper(target);
            source.keySet().forEach(prop -> {
                try {
                    beanWrapper.setPropertyValue(prop, source.get(prop));
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            });
        }
    }
}
