package org.vdma.loprodb.service.impl;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.specification.ReferenceSpecifications;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.mapper.ReferenceMapper;
import org.vdma.loprodb.service.param.ReferenceListParam;

@Service
@RequiredArgsConstructor
public class ReferenceServiceImpl implements IReferenceService {

    private final ReferenceRepository referenceRepo;
    private final ReferenceMapper referenceMapper;

    @Transactional(readOnly = true)
    @Override
    public Page<ReferenceData> filter(ReferenceListParam listParam) {
        Specification<Reference> spec = Specification.where(ReferenceSpecifications.refType(listParam.getRefType()));
        if (StringUtils.isNotBlank(listParam.getSearchTerm())) {
            spec = spec.and(ReferenceSpecifications.search(listParam.getSearchTerm().trim()));
        }
        return referenceRepo.findAll(spec, listParam.buildPageRequest()).map(referenceMapper::map);
    }

    @Transactional
    @Override
    public ReferenceData create(ReferenceData data) {
        Reference reference = save(data, new Reference());
        return referenceMapper.map(reference);
    }

    @Transactional
    @Override
    public ReferenceData update(Long id, ReferenceData data) {
        Reference reference = save(data, findReference(id));
        return referenceMapper.map(reference);
    }

    @Override
    public Reference findReference(Long id) {
        return referenceRepo.findById(id).orElseThrow(() -> new EntityNotExistsException(String.format(
                "Reference with id '%d' not found ", id)));
    }

    @Override
    public Reference findReference(Long id, RefType refType) {
        Reference r = referenceRepo.findByIdAndRefType(id, refType);
        EntityNotExistsException.assertNotNull(
                String.format("Reference with id '%d' and type '%s' not found ", id, refType), r);
        return r;
    }

    @Override
    public Optional<Reference> findReferenceBySambaId(Long sambaId) {
        return referenceRepo.findBySambaId(sambaId);
    }

    @Override
    public Optional<Reference> findReferenceBySambaIdAndType(Long sambaId, RefType refType) {
        return referenceRepo.findBySambaIdAndRefType(sambaId, refType);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Reference reference = findReference(id);
        referenceRepo.delete(reference);
    }

    @Transactional(readOnly = true)
    @Override
    public ReferenceData getByAnyValueAndType(String value, RefType refType) {
        return referenceRepo.findByAnyNameAndRefType(value, refType)
                .map(referenceMapper::map).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<ReferenceData> getBySambaId(Long sambaId) {
        return findReferenceBySambaId(sambaId).map(referenceMapper::map);
    }

    private Reference save(ReferenceData referenceData, Reference reference) {
        Reference updated = referenceMapper.update(referenceData, reference);
        return referenceRepo.save(updated);
    }
}
