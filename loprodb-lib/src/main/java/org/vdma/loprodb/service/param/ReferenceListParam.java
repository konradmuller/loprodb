package org.vdma.loprodb.service.param;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.vdma.loprodb.entity.RefType;

@Getter
@Setter
@RequiredArgsConstructor
public class ReferenceListParam extends ObjectListParam {
    private final RefType refType;
}
