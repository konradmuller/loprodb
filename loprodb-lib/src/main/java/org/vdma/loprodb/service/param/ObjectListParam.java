package org.vdma.loprodb.service.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import java.util.Collections;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Setter
@Getter
@Builder
public class ObjectListParam {

    @ApiParam(value = "${common.page}", defaultValue = "0")
    @ApiModelProperty(value = "${common.page}", position = 100)
    protected int page = 0;

    @ApiParam(value = "${common.page-size}", defaultValue = "50")
    @ApiModelProperty(value = "${common.page-size}", position = 101)
    protected int pageSize = 50;

    @ApiParam(value = "${common.filter.sort-order}", defaultValue = "ASC", allowableValues = "ASC, DESC")
    @ApiModelProperty(value = "${common.filter.sort-order}", position = 102)
    protected Sort.Direction sortOrder = Sort.Direction.ASC;

    @ApiParam(value = "${common.sort-param.sort-by}")
    @ApiModelProperty(value = "${common.sort-param.sort-by}", position = 103)
    protected String sortBy = "id";

    protected String searchTerm;

    @Tolerate
    public ObjectListParam() {
    }

    private List<Sort.Order> getOrders(boolean ignoreCase) {
        if (StringUtils.isBlank(getSortBy())) {
            return Collections.emptyList();
        }
        Sort.Order order = new Sort.Order(getSortOrder(), getSortBy());
        return Collections.singletonList(ignoreCase ? order.ignoreCase() : order);
    }

    public Pageable buildPageRequest() {
        List<Sort.Order> orders = getOrders(true);
        if (orders.isEmpty()) {
            return PageRequest.of(getPage(), getPageSize());
        }
        return PageRequest.of(getPage(), getPageSize(), Sort.by(orders));
    }

    /**
     * If page value is less then 0 return 0.
     * To avoid exception if it somewhere set negative value.
     *
     * @return page number
     */
    public int getPage() {
        if (getPageSize() < 0) {
            return 0;
        }
        return page;
    }

    /**
     * If page size less then 0 return max result.
     *
     * @return page size
     */
    public int getPageSize() {
        return pageSize < 0 ? Integer.MAX_VALUE : pageSize;
    }

    public ObjectListParam initFromPageable(Pageable pageable) {
        if (pageable != null) {
            setPage(pageable.getPageNumber());
            setPageSize(pageable.getPageSize());
            pageable.getSort().stream().findFirst().ifPresent(order -> {
                setSortOrder(order.getDirection());
                setSortBy(order.getProperty());
            });
        }
        return this;
    }

    public int getOffset() {
        return getPage() * getPageSize();
    }

    public int getLimit() {
        return getPageSize();
    }

    public static ObjectListParam fromPageable(Pageable pageable) {
        return ObjectListParam.builder().build().initFromPageable(pageable);
    }
}
