package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.entity.AppRole;

@Mapper
public interface AppRoleMapper {

    AppRoleData map(AppRole role);

    @Mapping(target = "id", ignore = true)
    AppRole updateRole(AppRoleData data, @MappingTarget AppRole role);
}
