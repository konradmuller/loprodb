package org.vdma.loprodb.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.vdma.loprodb.dto.AppMessageData;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.service.param.ObjectListParam;

public interface IEventLogService {
    Page<EventLogData> getForUser(Long userId, ObjectListParam listParam);

    EventLogData createLogForUser(Long userId, String appCode, String appMessage);

    List<AppMessageData> getLastAppMessagesForUser(Long userId);
}
