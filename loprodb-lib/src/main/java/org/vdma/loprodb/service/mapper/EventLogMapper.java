package org.vdma.loprodb.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.vdma.loprodb.dto.EventLogData;
import org.vdma.loprodb.entity.EventLog;

@Mapper
public interface EventLogMapper {

    @Mapping(target = "appId", source = "application.id")
    @Mapping(target = "appName", source = "application.name")
    @Mapping(target = "userId", source = "user.id")
    EventLogData map(EventLog eventLog);
}
