package org.vdma.loprodb.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.entity.RegistrationVariant;

public interface IRegistrationService {
    RegistrationData create(RegistrationData registrationData);

    RegistrationData confirm(String token, RegistrationStatus status, RegistrationVariant variant, Long sambaId,
            boolean isSambaContactVdmaMember);

    RegistrationData reject(String token, RegistrationStatus status);

    RegistrationData sendToManualReview(String token, RegistrationVariant confirmVariant);

    RegistrationData resetToManualReview(String token);

    Registration findRegistration(Long id);

    Registration findRegistration(String token);
    
    RegistrationData getRegistration(String token);

    RegistrationData getRegistration(String token, RegistrationStatus status);

    Optional<RegistrationData> getConfirmedRegistrationByUser(Long userId);

    Optional<RegistrationData> getRegistrationByUser(String externalId);

    void delete(String token);

    boolean isBlackListEmail(String email);

    Page<RegistrationData> filter(RegistrationListParam listParam);

    int deleteOldRegistrations();
    
    int deleteUnconfirmedRegistrations();

    void updateEditor(String token, String externalId);

    void updateMemo(String token, String memo);

    Optional<RegistrationData> findByEmail(String email);
}
