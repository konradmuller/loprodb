package org.vdma.loprodb.service.mapper;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.vdma.loprodb.dto.UserAppData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.User;

@Mapper(uses = { ReferenceMapper.class }, componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    private AppRoleMapper roleMapper;

    @Mapping(target = "apps", ignore = true)
    public abstract UserData map(User user);
    
    @AfterMapping
    void afterMapping(@MappingTarget UserData data, User user) {
        Set<UserAppData> apps = new HashSet<>();

        user.getApps().forEach(app -> {
            UserAppData appData = new UserAppData();
            appData.setAppId(app.getId());
            appData.setAppName(app.getName());
            appData.setAppCode(app.getCode());
            appData.setRoles(user.getAppRoles().stream()
                    .filter(role -> Objects.equals(role.getApplication().getId(), app.getId()))
                    .map(roleMapper::map)
                    .collect(Collectors.toSet()));
            apps.add(appData);
        });
        data.setApps(apps);
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "externalId", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "photoCheckSum", ignore = true)
    @Mapping(target = "photoChangedOn", ignore = true)
    @Mapping(target = "lastSeenOn", ignore = true)
    @Mapping(target = "position", ignore = true)
    @Mapping(target = "businessArea", ignore = true)
    @Mapping(target = "apps", ignore = true)
    @Mapping(target = "topics", ignore = true)
    public abstract User updateUser(UserData data, @MappingTarget User user);
}
