package org.vdma.loprodb.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.vdma.loprodb.config.AppConfigProps;
import org.vdma.loprodb.dto.AppRoleData;
import org.vdma.loprodb.dto.ApplicationData;
import org.vdma.loprodb.dto.RegistrationData;
import org.vdma.loprodb.dto.UserAppData;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.CreateUserPwdToken;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.RegistrationStatus;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_CONFIRMED_NON_MEMBER;
import static org.vdma.loprodb.entity.RegistrationStatus.AUTO_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_CONFIRMED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUALLY_REJECTED;
import static org.vdma.loprodb.entity.RegistrationStatus.MANUAL_REVIEW_PENDING;
import static org.vdma.loprodb.entity.RegistrationStatus.OPT_IN_PENDING;
import org.vdma.loprodb.entity.RegistrationVariant;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.event.registration.RegistrationCreatedEvent;
import org.vdma.loprodb.event.registration.RegistrationManualCheckEvent;
import org.vdma.loprodb.event.registration.RegistrationRejectedEvent;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.ReferenceRepository;
import org.vdma.loprodb.repository.RegistrationRepository;
import org.vdma.loprodb.repository.specification.ReferenceSpecifications;
import org.vdma.loprodb.repository.specification.RegistrationSpecifications;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IRegistrationService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.RegistrationListParam;
import org.vdma.loprodb.service.mapper.RegistrationMapper;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegistrationServiceImpl implements IRegistrationService {

    private static final String REGISTRATION_NOT_FOUND = "Registration not found";

    private final RegistrationRepository registrationRepo;
    private final ReferenceRepository referenceRepo;
    private final RegistrationMapper registrationMapper;
    private final IUserService userService;
    private final IApplicationService applicationService;
    private final AppConfigProps appConfigProps;
    private final CreateUserPwdTokenRepository createUserPwdTokenRepo;
    private final ApplicationEventPublisher eventPublisher;

    @Transactional
    @Override
    public RegistrationData create(RegistrationData registrationData) {
        Registration registration = new Registration();

        Registration updated = registrationMapper.update(registrationData, registration);
        updated.setStatus(OPT_IN_PENDING);

        updated = registrationRepo.save(updated);

        RegistrationData result = registrationMapper.map(updated);
        eventPublisher.publishEvent(new RegistrationCreatedEvent(result));
        return result;
    }

    @Transactional
    @Override
    public RegistrationData confirm(String token, RegistrationStatus status, RegistrationVariant variant,
            Long sambaId, boolean isSambaContactVdmaMember) {
        Set<RegistrationStatus> findByStatuses = new HashSet<>();
        findByStatuses.add(OPT_IN_PENDING);
        if (status == MANUALLY_CONFIRMED) {
            findByStatuses.add(MANUAL_REVIEW_PENDING);
        }

        Registration registration = registrationRepo.findByTokenAndStatusIn(token, findByStatuses);
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);
        Assert.isTrue(!userService.emailExists(registration.getEmail(), null),
                "User with such email already exists");

        Assert.notNull(sambaId, "Samba contact ID must not be null!");
        Assert.isTrue(!userService.sambaIdExists(sambaId), "User with such samba ID already exists");
        Set<RegistrationStatus> confirmedStatuses =
                EnumSet.of(AUTO_CONFIRMED_MEMBER, AUTO_CONFIRMED_NON_MEMBER, MANUALLY_CONFIRMED);
        Assert.isTrue(confirmedStatuses.contains(status), "Status must be one of \"confirmed\"!");
        if (status != MANUALLY_CONFIRMED && variant == null) {
            throw new IllegalArgumentException("Confirmation variant is required");
        }

        registration.setStatus(status);
        // we should NOT overwrite registration variant if it was set before (when sending to manual review)
        if (registration.getRegistrationVariant() == null && variant != null) {
            registration.setRegistrationVariant(variant);
            registration.setTags(new HashSet<>(Collections.singletonList(variant.getTag())));
        }

        UserData userData = registrationMapper.mapToUser(registration);
        userData.setStatus(UserStatus.ACTIVE);
        userData.setSambaId(sambaId);

        ApplicationData vdmaApp = applicationService.getApplication(appConfigProps.getVdma().getCode());
        UserAppData userAppData = new UserAppData();
        userAppData.setAppId(vdmaApp.getId());

        if (status == AUTO_CONFIRMED_MEMBER || (status == MANUALLY_CONFIRMED && isSambaContactVdmaMember)) {
            AppRoleData vdmaMemberRole = vdmaApp.findRole(appConfigProps.getVdma().getRoleMember());
            EntityNotExistsException.assertNotNull("Role " + appConfigProps.getVdma().getRoleMember() +
                    " not found", vdmaMemberRole);

            userAppData.getRoles().add(vdmaMemberRole);
        }
        userData.setApps(Collections.singleton((userAppData)));

        userData = userService.createFromRegistration(userData, registration);

        User user = userService.findUser(userData.getId());

        CreateUserPwdToken pwdToken = createUserPwdTokenRepo.save(new CreateUserPwdToken(user));

        registration.setUser(user);
        registration = registrationRepo.save(registration);

        RegistrationData registrationData = registrationMapper.map(registration);
        registrationData.getUser().setCreatePasswordToken(pwdToken.getToken());

        return registrationData;
    }

    @Transactional
    @Override
    public RegistrationData reject(String token, RegistrationStatus status) {
        Set<RegistrationStatus> rejectStatuses = EnumSet.of(AUTO_REJECTED, MANUALLY_REJECTED);

        Assert.isTrue(rejectStatuses.contains(status), "Status must be one of \"rejected\"!");

        Set<RegistrationStatus> findByStatuses = new HashSet<>();
        findByStatuses.add(OPT_IN_PENDING);
        if (status == MANUALLY_REJECTED) {
            findByStatuses.add(MANUAL_REVIEW_PENDING);
        }

        Registration registration = registrationRepo.findByTokenAndStatusIn(token, findByStatuses);
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);

        registration.setStatus(status);

        registration = registrationRepo.save(registration);

        RegistrationData data = registrationMapper.map(registration);

        eventPublisher.publishEvent(new RegistrationRejectedEvent(data));
        return data;
    }

    @Transactional
    @Override
    public RegistrationData sendToManualReview(String token, RegistrationVariant registrationVariant) {
        Registration registration = registrationRepo.findByTokenAndStatusIn(token, EnumSet.of(OPT_IN_PENDING));
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);

        registration.setStatus(MANUAL_REVIEW_PENDING);
        registration.setRegistrationVariant(registrationVariant);
        registration.setTags(new HashSet<>(Collections.singletonList(registrationVariant.getTag())));

        registration = registrationRepo.save(registration);
        RegistrationData data = registrationMapper.map(registration);
        
        eventPublisher.publishEvent(new RegistrationManualCheckEvent(data));
        
        return data;
    }
    
    @Transactional
    @Override
    public RegistrationData resetToManualReview(String token) {
        Registration registration = registrationRepo.findByTokenAndStatusIn(
                token, EnumSet.of(MANUALLY_REJECTED, AUTO_REJECTED));
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);

        registration.setStatus(MANUAL_REVIEW_PENDING);

        registration = registrationRepo.save(registration);
        RegistrationData data = registrationMapper.map(registration);
        
        return data;
    }

    @Override
    public Registration findRegistration(Long id) {
        return registrationRepo.findById(id).orElseThrow(() -> new EntityNotExistsException(REGISTRATION_NOT_FOUND));
    }

    @Override
    public Registration findRegistration(String token) {
        Registration registration = registrationRepo.findByToken(token);
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);
        return registration;
    }

    @Transactional(readOnly = true)
    @Override
    public RegistrationData getRegistration(String token) {
        Registration registration = findRegistration(token);
        return registrationMapper.map(registration);
    }

    @Transactional(readOnly = true)
    @Override
    public RegistrationData getRegistration(String token, RegistrationStatus status) {
        Registration registration = registrationRepo.findByTokenAndStatusIn(token, EnumSet.of(status));
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);
        return registrationMapper.map(registration);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RegistrationData> getConfirmedRegistrationByUser(Long userId) {
        Collection<RegistrationStatus> confirmedStatuses =
                EnumSet.of(AUTO_CONFIRMED_MEMBER, AUTO_CONFIRMED_NON_MEMBER, MANUALLY_CONFIRMED);
        return registrationRepo.findByUser_IdAndStatusIn(userId, confirmedStatuses)
                .map(registrationMapper::map);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RegistrationData> getRegistrationByUser(String externalId) {
        return registrationRepo.findByUser_ExternalId(externalId).map(registrationMapper::map);
    }

    @Transactional
    @Override
    public void delete(String token) {
        Registration registration = findRegistration(token);
        registrationRepo.delete(registration);
    }

    @Override
    public boolean isBlackListEmail(String emailParam) {
        if (StringUtils.isBlank(emailParam)) {
            return false;
        }
        String email = emailParam.toLowerCase();
        List<Reference> blackList = referenceRepo.findAll(ReferenceSpecifications.refType(RefType.EMAIL_BLACK_LIST));
        for (Reference black : blackList) {
            String blackVal = black.getNameDe().toLowerCase();
            if (email.startsWith(blackVal) || email.endsWith(blackVal) || email.equals(blackVal)) {
                return true;
            }
        }
        return false;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<RegistrationData> filter(RegistrationListParam listParam) {
        List<Specification<Registration>> specifications = new ArrayList<>();
        if (StringUtils.isNotBlank(listParam.getEmail())) {
            specifications.add(RegistrationSpecifications.email(listParam.getEmail()));
        }
        if (StringUtils.isNotBlank(listParam.getPhone())) {
            specifications.add(RegistrationSpecifications.phone(listParam.getPhone()));
        }
        if (StringUtils.isNotBlank(listParam.getFirstName())) {
            specifications.add(RegistrationSpecifications.firstName(listParam.getFirstName()));
        }
        if (StringUtils.isNotBlank(listParam.getLastName())) {
            specifications.add(RegistrationSpecifications.lastName(listParam.getLastName()));
        }
        if (StringUtils.isNotBlank(listParam.getCompanyName())) {
            specifications.add(RegistrationSpecifications.companyName(listParam.getCompanyName()));
        }
        if (StringUtils.isNotBlank(listParam.getCompanyCity())) {
            specifications.add(RegistrationSpecifications.companyCity(listParam.getCompanyCity()));
        }
        if (listParam.getStatuses() != null && !listParam.getStatuses().isEmpty()) {
            List<RegistrationStatus> statuses = new ArrayList<>(listParam.getStatuses());
            Specification<Registration> statusSpec = RegistrationSpecifications.status(statuses.get(0));
            for (int i = 1; i < statuses.size(); i++) {
                statusSpec = statusSpec.or(RegistrationSpecifications.status(statuses.get(i)));
            }
            specifications.add(statusSpec);
        }
        Specification<Registration> specification = null;
        if (!specifications.isEmpty()) {
            specification = specifications.get(0);
            for (int i = 1; i < specifications.size(); i++) {
                specification = specification.and(specifications.get(i));
            }
        }
        return registrationRepo
                .findAll(specification, listParam.buildPageRequest())
                .map(registrationMapper::map);
    }

    @Transactional
    @Override
    public int deleteOldRegistrations() {
        LocalDateTime oldRegistrationsThreshold = LocalDateTime.now().minusMonths(6);
        List<Registration> old = registrationRepo.findAllByCreatedDateLessThan(oldRegistrationsThreshold);
        registrationRepo.deleteAll(old);
        return old.size();
    }
    
    @Transactional
    @Override
    public int deleteUnconfirmedRegistrations() {
        LocalDateTime treshold = LocalDateTime.now().minusDays(2);
        List<Registration> old = registrationRepo.findAllByCreatedDateLessThanAndStatus(treshold, OPT_IN_PENDING);
        registrationRepo.deleteAll(old);
        return old.size();
    }

    @Override
    public void updateEditor(String token, String externalId) {
        Registration registration = registrationRepo.findByToken(token);
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);
        try {
            User editor = userService.findUser(externalId);
            registration.setEditor(editor);
            registrationRepo.save(registration);
        } catch (EntityNotExistsException ignored) {
            // Do not update. A loophole for admin user which doesn't have actual user account
        }
    }
    
    @Override
    public void updateMemo(String token, String memo) {
        Registration registration = registrationRepo.findByToken(token);
        EntityNotExistsException.assertNotNull(REGISTRATION_NOT_FOUND, registration);
        registration.setMemo(memo);
        registrationRepo.save(registration);
    }
    
    @Override
    public Optional<RegistrationData> findByEmail(String email) {
        return registrationRepo.findByEmail(email).map(registrationMapper::map);
    }
}
