package org.vdma.loprodb.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.vdma.loprodb.dto.ReferenceData;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;
import org.vdma.loprodb.service.param.ReferenceListParam;

public interface IReferenceService {
    ReferenceData create(ReferenceData data);

    ReferenceData update(Long id, ReferenceData data);

    Reference findReference(Long id);

    Reference findReference(Long id, RefType refType);

    Optional<Reference> findReferenceBySambaId(Long sambaId);

    Optional<Reference> findReferenceBySambaIdAndType(Long sambaId, RefType refType);

    void delete(Long id);

    ReferenceData getByAnyValueAndType(String value, RefType refType);

    Optional<ReferenceData> getBySambaId(Long sambaId);

    Page<ReferenceData> filter(ReferenceListParam listParam);
}
