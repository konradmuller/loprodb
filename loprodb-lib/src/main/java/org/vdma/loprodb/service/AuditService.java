package org.vdma.loprodb.service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.MatchMode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.AuditData;
import org.vdma.loprodb.dto.RevinfoData;
import org.vdma.loprodb.entity.Revinfo;
import org.vdma.loprodb.service.param.ObjectListParam;

@Component
@RequiredArgsConstructor
public class AuditService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(readOnly = true)
    public <C, D> Page<AuditData<D>> filter(Class<C> entityClass, Long entityId, Function<C, D> mapper,
            ObjectListParam listParam) {
        return filter(entityManager, entityClass, entityId, mapper, listParam);
    }

    @Transactional(readOnly = true)
    public <C, D> Page<AuditData<D>> filter(EntityManager em, Class<C> entityClass, Long entityId,
            Function<C, D> mapper, ObjectListParam listParam) {
        AuditReader reader = AuditReaderFactory.get(em);

        AuditQuery query = reader.createQuery().forRevisionsOfEntity(entityClass, false, false);
        query.add(AuditEntity.id().eq(entityId));
        addFilter(listParam, query);
        if (listParam.getSortOrder() == Sort.Direction.ASC) {
            query.addOrder(AuditEntity.revisionNumber().asc());
        } else {
            query.addOrder(AuditEntity.revisionNumber().desc());
        }
        query.addOrder(AuditEntity.revisionNumber().desc());
        query.setMaxResults(listParam.getLimit());
        query.setFirstResult(listParam.getOffset());

        List<AuditData<D>> audList = ((List<Object[]>) query.getResultList()).stream().map(row -> {
            C u = (C) row[0];
            Revinfo ri = (Revinfo) row[1];
            RevisionType type = (RevisionType) row[2];
            return new AuditData<>(mapper.apply(u), new RevinfoData(ri.getUsername(), ri.getRevisionDate()), type);
        }).collect(Collectors.toList());

        AuditQuery countQuery = reader.createQuery().forRevisionsOfEntity(entityClass, false);
        countQuery.add(AuditEntity.id().eq(entityId));
        countQuery.addProjection(AuditEntity.id().count());
        addFilter(listParam, countQuery);
        Number size = (Number) countQuery.getSingleResult();

        return new PageImpl<>(audList, listParam.buildPageRequest(), size.longValue());
    }

    private void addFilter(ObjectListParam listParam, AuditQuery query) {
        if (StringUtils.isNotBlank(listParam.getSearchTerm())) {
            query.add(AuditEntity.revisionProperty("username")
                    .like(listParam.getSearchTerm().trim(), MatchMode.ANYWHERE));
        }
    }

}
