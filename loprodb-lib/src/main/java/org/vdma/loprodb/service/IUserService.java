package org.vdma.loprodb.service;

import java.util.Map;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.event.user.UpdateSource;
import org.vdma.loprodb.service.param.UserListParam;

public interface IUserService {
    UserData create(UserData data);

    UserData createFromRegistration(UserData data, Registration registration);

    UserData update(String externalId, UserData data, UpdateSource updateSource);

    UserData update(UserIdType idType, String id, Map<String, Object> data);

    void delete(String externalId);

    UserData getUser(String externalId);

    UserData getUserByIdType(UserIdType idType, String id);

    byte[] getUserPhoto(String externalId);

    void setUserPhoto(UserIdType idType, String id, byte[] photo);

    User findUser(String externalId);

    User findUser(Long id);

    Page<UserData> filter(UserListParam listParam);

    boolean emailExists(String email, String exceptId);

    boolean sambaIdExists(Long sambaId);

    /**
     * similar email - email that equal to the specified except top level domain,
     * e.g. email@vdma.ORG similar to email@vdma.COM
     *
     * @param email
     * @return list of similar emails including source email if exists
     */
    Set<String> getSimilarEmails(String email);

    void changeRegPassword(String externalId, String newPassword);

    void createRegPassword(String externalId, String createPasswordToken, String newPassword);

    void updateLastSeenDate(Long id);
}
