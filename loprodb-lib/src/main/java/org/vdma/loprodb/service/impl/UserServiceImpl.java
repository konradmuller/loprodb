package org.vdma.loprodb.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.dto.UserIdType;
import org.vdma.loprodb.entity.AppRole;
import org.vdma.loprodb.entity.Application;
import org.vdma.loprodb.entity.CreateUserPwdToken;
import org.vdma.loprodb.entity.PatchableUser;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.RegPwdChange;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;
import org.vdma.loprodb.event.user.UpdateSource;
import static org.vdma.loprodb.event.user.UpdateSource.VDMA_ORG;
import org.vdma.loprodb.event.user.UserCreatedEvent;
import org.vdma.loprodb.event.user.UserDeletedEvent;
import org.vdma.loprodb.event.user.UserUpdatedEvent;
import org.vdma.loprodb.exception.EntityNotExistsException;
import org.vdma.loprodb.repository.AppRoleRepository;
import org.vdma.loprodb.repository.CreateUserPwdTokenRepository;
import org.vdma.loprodb.repository.RegPwdChangeRepository;
import org.vdma.loprodb.repository.UserRepository;
import org.vdma.loprodb.repository.specification.UserSpecifications;
import org.vdma.loprodb.service.IApplicationService;
import org.vdma.loprodb.service.IReferenceService;
import org.vdma.loprodb.service.IUserService;
import org.vdma.loprodb.service.mapper.UserMapper;
import org.vdma.loprodb.service.param.UserListParam;
import org.vdma.loprodb.service.patch.BeanPatcher;
import org.vdma.loprodb.service.patch.UserReferencePatchDelegate;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService {

    public static final String USER_NOT_FOUND = "User not found";

    private final UserMapper userMapper;
    private final UserRepository userRepo;
    private final AppRoleRepository appRoleRepo;
    private final IApplicationService appService;
    private final IReferenceService referenceService;
    private final BeanPatcher beanPatcher;
    private final ApplicationEventPublisher eventPublisher;
    private final CreateUserPwdTokenRepository createUserPwdTokenRepo;
    private final RegPwdChangeRepository regPwdChangeRepo;

    @Transactional
    @Override
    public UserData create(UserData data) {
        User user = save(data, new User());
        eventPublisher.publishEvent(new UserCreatedEvent(user));
        return userMapper.map(user);
    }

    @Transactional
    @Override
    public UserData createFromRegistration(UserData data, Registration registration) {
        User user = save(data, new User());
        eventPublisher.publishEvent(new UserCreatedEvent(user, registration));
        return userMapper.map(user);
    }

    @Transactional
    @Override
    public UserData update(String externalId, UserData data, UpdateSource updateSource) {
        log.info("Updating user #{} by DTO...", externalId);
        User user = save(data, findUser(externalId));
        eventPublisher.publishEvent(new UserUpdatedEvent(user, updateSource));
        return userMapper.map(user);
    }

    @Transactional
    @Override
    public UserData update(UserIdType idType, String id, Map<String, Object> data) {
        log.info("Updating user #{} by fields map...", id);
        User user = findUserByIdType(idType, id);
        beanPatcher.copyProperties(user, data, PatchableUser.class);
        new UserReferencePatchDelegate(user, referenceService).patch(data);
        user = userRepo.save(user);
        eventPublisher.publishEvent(new UserUpdatedEvent(user, VDMA_ORG));
        return userMapper.map(user);
    }

    @Transactional
    @Override
    public void delete(String externalId) {
        User user = findUser(externalId);
        eventPublisher.publishEvent(new UserDeletedEvent(user));
        userRepo.delete(user);
    }

    @Transactional(readOnly = true)
    @Override
    public UserData getUser(String externalId) {
        User user = findUser(externalId);
        return userMapper.map(user);
    }

    @Transactional(readOnly = true)
    @Override
    public UserData getUserByIdType(UserIdType idType, String id) {
        User user = findUserByIdType(idType, id);
        return userMapper.map(user);
    }

    @Transactional(readOnly = true)
    @Override
    public byte[] getUserPhoto(String externalId) {
        return findUser(externalId).getPhoto();
    }

    @Transactional
    @Override
    public void setUserPhoto(UserIdType idType, String id, byte[] photo) {
        User user = findUserByIdType(idType, id);
        user.setPhoto(photo);
        user = userRepo.save(user);
        eventPublisher.publishEvent(new UserUpdatedEvent(user, VDMA_ORG));
    }

    @Override
    public User findUser(String externalId) {
        User user = userRepo.findByExternalId(externalId);
        EntityNotExistsException.assertNotNull(USER_NOT_FOUND, user);
        return user;
    }

    @Override
    public User findUser(Long id) {
        return userRepo.findById(id).orElseThrow(() -> new EntityNotExistsException(USER_NOT_FOUND));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<UserData> filter(UserListParam listParam) {
        List<Specification<User>> specifications = new ArrayList<>();
        if (StringUtils.isNotBlank(listParam.getEmail())) {
            specifications.add(UserSpecifications.email(listParam.getEmail()));
        }
        if (StringUtils.isNotBlank(listParam.getFirstName())) {
            specifications.add(UserSpecifications.firstName(listParam.getFirstName()));
        }
        if (StringUtils.isNotBlank(listParam.getLastName())) {
            specifications.add(UserSpecifications.lastName(listParam.getLastName()));
        }
        if (StringUtils.isNotBlank(listParam.getCompanyName())) {
            specifications.add(UserSpecifications.companyName(listParam.getCompanyName()));
        }
        if (StringUtils.isNotBlank(listParam.getCompanyCity())) {
            specifications.add(UserSpecifications.companyCity(listParam.getCompanyCity()));
        }
        if (listParam.getSambaId() != null) {
            specifications.add(UserSpecifications.sambaId(listParam.getSambaId()));
        }
        if (listParam.getStatuses() != null && !listParam.getStatuses().isEmpty()) {
            List<UserStatus> statuses = new ArrayList<>(listParam.getStatuses());
            Specification<User> statusSpec = UserSpecifications.status(statuses.get(0));
            for (int i = 1; i < statuses.size(); i++) {
                statusSpec = statusSpec.or(UserSpecifications.status(statuses.get(i)));
            }
            specifications.add(statusSpec);
        }
        Specification<User> specification = null;
        if (!specifications.isEmpty()) {
            specification = specifications.get(0);
            for (int i = 1; i < specifications.size(); i++) {
                specification = specification.and(specifications.get(i));
            }
        }
        return userRepo
                .findAll(specification, listParam.buildPageRequest())
                .map(userMapper::map);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean emailExists(String email, String exceptId) {
        return userRepo.findByEmailIgnoreCaseAndExternalIdNot(email,
                StringUtils.defaultIfBlank(exceptId, " ")) != null;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean sambaIdExists(Long sambaId) {
        return userRepo.findBySambaId(sambaId) != null;
    }

    @Override
    public Set<String> getSimilarEmails(String email) {
        if (StringUtils.isBlank(email) || !email.contains(".")) {
            return Collections.emptySet();
        }

        String lowercased = email.toLowerCase();

        String emailWithoutTopLevelDomain = lowercased.substring(0, lowercased.lastIndexOf('.') + 1);

        List<User> users = userRepo.findByEmailStartsWithIgnoreCase(emailWithoutTopLevelDomain);
        if (users.isEmpty()) {
            return Collections.emptySet();
        }

        Pattern pattern = Pattern.compile(Pattern.quote(emailWithoutTopLevelDomain) + "[a-z]{2,}",
                Pattern.CASE_INSENSITIVE);

        return users.stream().filter(u -> pattern.matcher(u.getEmail()).matches())
                .map(User::getEmail)
                .collect(Collectors.toSet());
    }

    @Transactional
    @Override
    public void changeRegPassword(String externalId, String newPassword) {
        User user = findUser(externalId);
        user.setRegPwdChangedDate(LocalDateTime.now());
        user = userRepo.save(user);
        eventPublisher.publishEvent(new UserUpdatedEvent(user, VDMA_ORG));

        RegPwdChange change = regPwdChangeRepo.findByUser(user);
        if (change == null) {
            change = new RegPwdChange(user);
            user.setRegPwdChange(change);
        }
        change.setPassword(newPassword);
        regPwdChangeRepo.save(change);
    }

    @Transactional
    @Override
    public void createRegPassword(String externalId, String createPasswordToken, String newPassword) {
        User user = findUser(externalId);
        CreateUserPwdToken token =
                createUserPwdTokenRepo.findByTokenAndUserAndPwdCreatedDateIsNull(createPasswordToken, user);
        EntityNotExistsException.assertNotNull("Token not found", token);

        RegPwdChange change = new RegPwdChange(user);
        change.setPassword(newPassword);
        regPwdChangeRepo.save(change);

        user.setRegPwdChangedDate(LocalDateTime.now());
        user.setRegPwdChange(change);
        user = userRepo.save(user);
        eventPublisher.publishEvent(new UserUpdatedEvent(user, VDMA_ORG));

        token.setPwdCreatedDate(LocalDateTime.now());
        createUserPwdTokenRepo.save(token);
    }

    @Transactional
    @Override
    public void updateLastSeenDate(Long id) {
        log.info("Updating lastSeenDate for user #{}...", id);
        User user = findUser(id);
        user.setLastSeenOn(LocalDateTime.now());
        user = userRepo.save(user);
        eventPublisher.publishEvent(new UserUpdatedEvent(user, VDMA_ORG, true));
    }

    private User findUserByIdType(UserIdType idType, String id) {
        User user;
        switch (idType) {
        case EMAIL:
            user = userRepo.findByEmailIgnoreCase(id);
            break;
        case SAMBA_ID:
            user = userRepo.findBySambaId(Long.valueOf(id));
            break;
        case EXTERNAL_ID:
            user = userRepo.findByExternalId(id);
            break;
        default:
            throw new IllegalArgumentException("Unknown user id type");
        }
        EntityNotExistsException.assertNotNull(USER_NOT_FOUND, user);
        return user;
    }

    private User save(UserData data, User user) {
        User updated = userMapper.updateUser(data, user);
        updated.setPosition(referenceService.findReference(data.getPosition().getId(), RefType.USER_POSITION));
        updated.setBusinessArea(referenceService.findReference(data.getBusinessArea().getId(),
                RefType.USER_BUSINESS_AREA));

        Set<AppRole> roles = new HashSet<>();
        Set<Application> apps = new HashSet<>();

        if (data.getApps() != null) {
            data.getApps().forEach(userAppData -> {
                Application app = appService.findApplication(userAppData.getAppId());
                apps.add(app);

                if (userAppData.getRoles() != null) {
                    roles.addAll(userAppData.getRoles().stream()
                            .map(roleData -> appRoleRepo.findByIdAndApplication(roleData.getId(), app))
                            .filter(Objects::nonNull)
                            .collect(Collectors.toSet()));
                }
            });
        }

        updated.setAppRoles(roles);
        updated.setApps(apps);
        return userRepo.save(user);
    }
}
