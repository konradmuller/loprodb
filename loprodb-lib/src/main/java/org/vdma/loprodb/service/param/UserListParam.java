package org.vdma.loprodb.service.param;

import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.UserStatus;

@Getter
@Setter
public class UserListParam extends ObjectListParam {
    private String email;
    private String firstName;
    private String lastName;
    private String companyName;
    private String companyCity;
    private Set<UserStatus> statuses = new HashSet<>();
    private Long sambaId;
}
