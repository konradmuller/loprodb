package org.vdma.loprodb.messaging.user;

import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.listener.user.UserSmbSyncContextHolder;
import static org.vdma.loprodb.event.user.UpdateSource.LOPRODB;
import org.vdma.loprodb.event.user.UserCreatedEvent;
import org.vdma.loprodb.event.user.UserDeletedEvent;
import org.vdma.loprodb.event.user.UserUpdatedEvent;
import static org.vdma.loprodb.messaging.ActionType.CREATED;
import static org.vdma.loprodb.messaging.ActionType.DELETED;
import static org.vdma.loprodb.messaging.ActionType.UPDATED;

@Component
@RequiredArgsConstructor
@Slf4j
public class UserUpdatedMessageSender {

    private final JmsTemplate jmsTemplate;

    @Value("${jms.queue.user-updated-liferay:loprodb-user-updated-liferay}")
    private String userUpdatedLiferayQueue;

    @Value("${jms.queue.user-updated-samba:loprodb-user-updated-samba}")
    private String userUpdatedSambaQueue;

    @Order(1000)
    @TransactionalEventListener
    public void onUserCreated(UserCreatedEvent event) {
        UserUpdatedMessage message = new UserUpdatedMessage(event.getUser(), CREATED, LOPRODB);
        log.info("User {} created. Sending message to JMS.", event.getUser().getId());
        try {
            jmsTemplate.convertAndSend(userUpdatedLiferayQueue, message);
            if (event.getFromRegistration() != null) {
                log.info("User created after registration confirmation, Samba ID = {}", event.getUser().getSambaId());
            }
            sendToSambaIfNeeded(event.getUser(), message);
        } catch (Exception e) {
            log.error("Failed to send \"CREATED\" message to JMS for user " + event.getUser().getId(), e);
        }
    }

    @TransactionalEventListener
    public void onUserUpdated(UserUpdatedEvent event) {
        UserUpdatedMessage message = new UserUpdatedMessage(event.getUser(), UPDATED, event.getUpdateSource());
        log.info("User {} updated. Sending message to JMS.", event.getUser().getId());
        try {
            if (!event.isLastSeenOnUpdated()) {
                log.info("Sending message to Liferay");
                jmsTemplate.convertAndSend(userUpdatedLiferayQueue, message);
            }
            sendToSambaIfNeeded(event.getUser(), message);
        } catch (Exception e) {
            log.error("Failed to send \"UPDATED\" message to JMS for user " + event.getUser().getId(), e);
        }
    }

    @TransactionalEventListener
    public void onUserDeleted(UserDeletedEvent event) {
        UserUpdatedMessage message = new UserUpdatedMessage(event.getUser(), DELETED, LOPRODB);
        log.info("User {} deleted. Sending message to JMS.", event.getUser().getId());
        try {
            jmsTemplate.convertAndSend(userUpdatedLiferayQueue, message);
            sendToSambaIfNeeded(event.getUser(), message);
        } catch (Exception e) {
            log.error("Failed to send \"DELETED\" message to JMS for user " + event.getUser().getId(), e);
        }
    }

    private void sendToSambaIfNeeded(User u, UserUpdatedMessage message) {
        if (u.getSambaId() == null) {
            return;
        }
        log.info("For user {} samba sync version = {}, user version in samba: {}",
                u.getId(), u.getSambaSyncVersion(), UserSmbSyncContextHolder.getContext().getUserVersionInSamba());
        if (!Objects.equals(u.getSambaSyncVersion(), UserSmbSyncContextHolder.getContext().getUserVersionInSamba())) {
            log.info("Sending {} message to samba, user = {}...", message.getActionType().name(), u.getExternalId());
            jmsTemplate.convertAndSend(userUpdatedSambaQueue, message);
        }
    }
}
