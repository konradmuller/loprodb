package org.vdma.loprodb.messaging;

public enum ActionType {
    CREATED,
    UPDATED,
    DELETED
}
