package org.vdma.loprodb.messaging.user;

import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.event.user.UpdateSource;
import org.vdma.loprodb.messaging.ActionType;

@Getter
@Setter
public class UserUpdatedMessage {

    private UserUpdated user;
    private ActionType actionType;
    private UpdateSource updateSource;

    UserUpdatedMessage(User user, ActionType actionType, UpdateSource updateSource) {
        this.user = new UserUpdated(user);
        this.actionType = actionType;
        this.updateSource = updateSource;
    }

    @Getter
    @Setter
    public static class UserUpdated {

        private String email;
        private Long sambaId;
        private String profdbId;

        UserUpdated(User user) {
            this.email = user.getEmail();
            this.profdbId = user.getExternalId();
            this.sambaId = user.getSambaId();
        }
    }

}
