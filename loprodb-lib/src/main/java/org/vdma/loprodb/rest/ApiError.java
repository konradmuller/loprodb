package org.vdma.loprodb.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
public class ApiError {
    private int status;

    private String error;

    private String message;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String trace;

    public ApiError(HttpStatus status, String message, String trace) {
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = message;
        this.trace = trace;
    }

    public ApiError(HttpStatus status, String message) {
        this(status, message, null);
    }
}
