package org.vdma.loprodb.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.vdma.loprodb.dto.UserData;
import org.vdma.loprodb.entity.RegistrationStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmRegistrationResult {
    private RegistrationStatus status;
    private UserData user;
    private boolean alreadyConfirmed;
}
