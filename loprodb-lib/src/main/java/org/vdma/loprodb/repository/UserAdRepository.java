package org.vdma.loprodb.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.entity.UserAd;

@Repository
public interface UserAdRepository extends JpaRepository<UserAd, Long>, JpaSpecificationExecutor<UserAd> {

    UserAd findByUserIdAndCreatedDate(long userId, LocalDateTime createdDate);
    
    @Query(value = 
            "SELECT u FROM UserAd u JOIN Registration r " + 
                    "ON u.userId = r.user.id " + 
                    "AND r.status in (:confirmedStatuses) " +
                    "and u.loginEmailSent = false " +
                    "and u.adUserPrincipalName is not null " +
                    "and u.adUserId is not null " +
                    "and u.creationType is not null")
    List<UserAd> findAdUsersWithUnsentEmails(
            @Param("confirmedStatuses") List<RegistrationStatus> confirmedStatuses, Pageable pageable);
    
}
