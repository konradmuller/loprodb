package org.vdma.loprodb.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.RegistrationStatus;

public final class RegistrationSpecifications {
    private RegistrationSpecifications() {
    }

    public static Specification<Registration> email(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("email")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> phone(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("phone")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> lastName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("lastName")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> firstName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("firstName")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> companyCity(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("city")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> companyName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("name")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Registration> status(RegistrationStatus value) {
        return (root, query, cb) -> cb.equal(root.get("status"), value);
    }
}
