package org.vdma.loprodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long>, JpaSpecificationExecutor<Application> {
    Application findFirstByNameIgnoreCaseAndIdNot(String name, Long id);

    Application findFirstByCodeIgnoreCaseAndIdNot(String code, Long id);

    Application findByCode(String code);
}
