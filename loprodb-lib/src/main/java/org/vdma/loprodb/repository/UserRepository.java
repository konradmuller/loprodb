package org.vdma.loprodb.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    User findByExternalId(String externalId);

    User findByEmailIgnoreCase(String email);

    User findBySambaId(Long sambaId);

    User findByEmailIgnoreCaseAndExternalIdNot(String email, String externalId);

    List<User> findByEmailStartsWithIgnoreCase(String prefix);
}
