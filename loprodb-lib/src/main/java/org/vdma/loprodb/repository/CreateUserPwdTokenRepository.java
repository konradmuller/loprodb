package org.vdma.loprodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.CreateUserPwdToken;
import org.vdma.loprodb.entity.User;

@Repository
public interface CreateUserPwdTokenRepository extends JpaRepository<CreateUserPwdToken, Long> {

    CreateUserPwdToken findByTokenAndUserAndPwdCreatedDateIsNull(String token, User user);

}
