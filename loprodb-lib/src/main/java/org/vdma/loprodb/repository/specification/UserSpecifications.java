package org.vdma.loprodb.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.vdma.loprodb.entity.User;
import org.vdma.loprodb.entity.UserStatus;

public final class UserSpecifications {
    private UserSpecifications() {
    }

    public static Specification<User> email(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("email")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<User> lastName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("lastName")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<User> firstName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("firstName")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<User> companyCity(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("city")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<User> companyName(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("name")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<User> status(UserStatus value) {
        return (root, query, cb) -> cb.equal(root.get("status"), value);
    }

    public static Specification<User> sambaId(Long value) {
        return (root, query, cb) -> cb.equal(root.get("sambaId"), value);
    }
}
