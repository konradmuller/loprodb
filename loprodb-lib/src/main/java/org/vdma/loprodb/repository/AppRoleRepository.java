package org.vdma.loprodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.AppRole;
import org.vdma.loprodb.entity.Application;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole, Long>, JpaSpecificationExecutor<AppRole> {
    AppRole findByIdAndApplication(Long id, Application application);
}
