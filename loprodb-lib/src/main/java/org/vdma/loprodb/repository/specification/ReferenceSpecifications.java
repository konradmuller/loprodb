package org.vdma.loprodb.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;

public final class ReferenceSpecifications {
    private ReferenceSpecifications() {
    }

    public static Specification<Reference> refType(RefType value) {
        return (root, query, cb) -> cb.equal(root.get("refType"), value);
    }

    public static Specification<Reference> nameDe(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("nameDe")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Reference> nameEn(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("nameEn")), "%" + value.toLowerCase() + "%");
    }

    public static Specification<Reference> search(String value) {
        return nameDe(value).or(nameEn(value));
    }

}
