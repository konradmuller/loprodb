package org.vdma.loprodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.RegPwdChange;
import org.vdma.loprodb.entity.User;

@Repository
public interface RegPwdChangeRepository extends JpaRepository<RegPwdChange, Long> {

    RegPwdChange findByUser(User user);

}
