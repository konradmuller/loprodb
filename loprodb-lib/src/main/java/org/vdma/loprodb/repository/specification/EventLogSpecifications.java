package org.vdma.loprodb.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.vdma.loprodb.entity.EventLog;

public final class EventLogSpecifications {
    private EventLogSpecifications() {
    }

    public static Specification<EventLog> user(Long value) {
        return (root, query, cb) -> cb.equal(root.get("user").get("id"), value);
    }

    public static Specification<EventLog> message(String value) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("message")), "%" + value.toLowerCase() + "%");
    }
}
