package org.vdma.loprodb.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.vdma.loprodb.entity.RefType;
import org.vdma.loprodb.entity.Reference;

public interface ReferenceRepository extends JpaRepository<Reference, Long>, JpaSpecificationExecutor<Reference> {
    Reference findByIdAndRefType(Long id, RefType refType);

    @Query("select ref from Reference ref where (ref.nameDe = :name or ref.nameEn = :name) and ref.refType = :refType")
    Optional<Reference> findByAnyNameAndRefType(@Param("name") String name, @Param("refType") RefType refType);

    Optional<Reference> findBySambaId(Long sambaId);

    Optional<Reference> findBySambaIdAndRefType(Long sambaId, RefType refType);
}
