package org.vdma.loprodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.EmailTemplate;
import org.vdma.loprodb.entity.EmailTemplateType;

@Repository
public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long>, JpaSpecificationExecutor<
        EmailTemplate> {

    EmailTemplate findByTemplateType(EmailTemplateType templateType);

}
