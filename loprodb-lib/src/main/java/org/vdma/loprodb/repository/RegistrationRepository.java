package org.vdma.loprodb.repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.RegistrationStatus;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long>, JpaSpecificationExecutor<
        Registration> {

    Registration findByToken(String token);

    Registration findByTokenAndStatusIn(String token, Collection<RegistrationStatus> statuses);

    Optional<Registration> findByUser_ExternalId(String externalId);

    List<Registration> findAllByCreatedDateLessThan(LocalDateTime date);
    
    List<Registration> findAllByCreatedDateLessThanAndStatus(LocalDateTime date, RegistrationStatus status);
    
    Optional<Registration> findByUser_IdAndStatusIn(Long id, Collection<RegistrationStatus> status);

    Optional<Registration> findByEmail(String email);
}
