package org.vdma.loprodb.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.vdma.loprodb.entity.EventLog;
import org.vdma.loprodb.entity.IAppMessage;

@Repository
public interface EventLogRepository extends JpaRepository<EventLog, Long>, JpaSpecificationExecutor<EventLog> {

    @Query(value = "select el.app_id as appId, el.message as message, el.event_date as messageDate " +
            "from (select app_id, max(event_date) as lastDate from event_log " +
            "where user_id = :user_id group by app_id) l inner join event_log el on el.event_date = l.lastDate " +
            "and el.app_id = l.app_id where el.user_id = :user_id", nativeQuery = true)
    List<IAppMessage> getLastAppMessagesForUser(@Param("user_id") Long user);
}
