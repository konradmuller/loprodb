package org.vdma.loprodb.config;

import java.util.Optional;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "org.vdma.loprodb" })
@EntityScan({ "org.vdma.loprodb" })
@EnableJpaAuditing
public class LibConfig {

    @Bean
    public PasswordEncoder mainEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public AuditorAware<String> auditorAware() {
        return () -> {

            if (SecurityContextHolder.getContext() == null) {
                return Optional.empty();
            }

            if (SecurityContextHolder.getContext().getAuthentication() instanceof AbstractAuthenticationToken) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                AbstractAuthenticationToken token = (AbstractAuthenticationToken) authentication;
                return Optional.of(token.getName());
            }

            return Optional.empty();
        };
    }

}
