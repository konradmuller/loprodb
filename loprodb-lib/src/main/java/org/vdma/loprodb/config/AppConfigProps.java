package org.vdma.loprodb.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "application")
public class AppConfigProps {

    private Vdma vdma;

    private App loprodb;

    private App samba;

    private Internal internal;

    @Getter
    @Setter
    public static class Internal extends App {
        private String token;
    }

    @Getter
    @Setter
    public static class Vdma extends App {
        private String roleMember;
        private String roleEmployee;
    }

    @Getter
    @Setter
    public static class App {
        private String code;
    }
}
