package org.vdma.loprodb.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.jms.ConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.messaginghub.pooled.jms.JmsPoolConnectionFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.converter.SimpleMessageConverter;

@Configuration
@EnableJms
public class JmsConfig {

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerContainerFactory(ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        factory.setMessageConverter(new SimpleMessageConverter());
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    @Bean
    public MessageConverter jacksonJmsMessageConverter(ObjectMapper objectMapper) {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        converter.setObjectMapper(objectMapper);
        return converter;
    }

    @Bean
    @ConditionalOnClass(JmsPoolConnectionFactory.class)
    public InitializingBean connectionFactory(JmsPoolConnectionFactory connectionFactory) {
        return () -> {
            ActiveMQConnectionFactory activeMQConnectionFactory =
                    (ActiveMQConnectionFactory) connectionFactory.getConnectionFactory();
            RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
            redeliveryPolicy.setRedeliveryDelay(5000);
            redeliveryPolicy.setMaximumRedeliveries(10);
            redeliveryPolicy.setUseExponentialBackOff(true);
            redeliveryPolicy.setBackOffMultiplier(3);
            redeliveryPolicy.setInitialRedeliveryDelay(5000);
            // maximum delay = 1 hour
            redeliveryPolicy.setMaximumRedeliveryDelay(1000 * 60 * 60);
            activeMQConnectionFactory.setRedeliveryPolicy(redeliveryPolicy);
        };
    }

}
