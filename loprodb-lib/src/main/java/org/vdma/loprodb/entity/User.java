package org.vdma.loprodb.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.DigestUtils;
import org.vdma.loprodb.entity.listener.user.UserSmbSyncListener;

@Entity
@Getter
@Setter
@Table(name = "user_")
@EntityListeners({ AuditingEntityListener.class, UserSmbSyncListener.class })
@Audited
@EqualsAndHashCode(of = { "id", "email" })
public class User implements PatchableUser {

    @Id
    @SequenceGenerator(name = "user_id_seq_gen", sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "user_id_seq_gen")
    private Long id;

    @Version
    private int version;

    @NotBlank(message = "External id shouldn't be blank.")
    @Column(unique = true, nullable = false, updatable = false, name = "external_id")
    private String externalId;

    @NotNull(message = "Status shouldn't be null.")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private UserStatus status;

    @Column(name = "aim_id")
    private Long aimId;

    @Column(name = "samba_id", unique = true)
    private Long sambaId;

    @Column(name = "auto_deactivation")
    private boolean autoDeactivation;

    @Column(name = "auto_deactivation_date")
    private LocalDate autoDeactivationDate;

    @NotNull(message = "Gender shouldn't be null.")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String title;

    @NotBlank(message = "First name shouldn't be blank.")
    @Column(nullable = false, name = "first_name")
    private String firstName;

    @NotBlank(message = "Last name shouldn't be blank.")
    @Column(nullable = false, name = "last_name")
    private String lastName;

    @Email
    @NotBlank(message = "Email shouldn't be blank.")
    @Column(nullable = false, unique = true)
    private String email;

    private String phone;

    @Column(name = "function_")
    private String function;

    @NotNull(message = "Position shouldn't be null.")
    @ManyToOne(optional = false)
    @JoinColumn(name = "position_id", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Reference position;

    @NotNull(message = "Business Area shouldn't be null.")
    @ManyToOne(optional = false)
    @JoinColumn(name = "business_area_id", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Reference businessArea;

    @NotNull(message = "Language shouldn't be null.")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language = Language.DE;

    @Size(max = 32)
    @Column(name = "ds_version", length = 32)
    private String dsVersion;

    @NotNull
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @NotNull
    @Column(name = "last_modified_date", nullable = false)
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "photo")
    @NotAudited
    private byte[] photo;

    // md5 hash
    @Column(name = "photo_check_sum")
    private String photoCheckSum;

    @Column(name = "photo_changed_on")
    private LocalDateTime photoChangedOn;

    @Column(name = "department")
    private String department;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "public_phone")
    private String publicPhone;

    @Email
    @Column(name = "public_email")
    private String publicEmail;

    @Column(name = "public_mobile")
    private String publicMobile;

    @Embedded
    private ExtendedCompanyInfo company;

    @Column(name = "last_seen_on")
    private LocalDateTime lastSeenOn;

    @OneToMany(mappedBy = "user", cascade = { CascadeType.REMOVE })
    @NotAudited
    private List<EventLog> eventLogs;

    @ManyToMany
    @JoinTable(
            name = "user_app",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "app_id", referencedColumnName = "id"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Set<Application> apps = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "user_app_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Set<AppRole> appRoles = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "user_topic",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "topic_id", referencedColumnName = "id"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Set<Reference> topics = new HashSet<>();

    /**
     * incremented every time when user changes and synchronization with samba is NOT in progress
     */
    @Column(name = "samba_sync_version")
    private Long sambaSyncVersion;

    /**
     * last samba contact sync version known to loprodb
     */
    @Column(name = "samba_contact_version")
    private Long sambaContactVersion;

    @Column(name = "vdma_roles_synchronized")
    private boolean vdmaRolesSynchronized;

    @OneToOne(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Registration registration;

    @OneToOne(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private RegPwdChange regPwdChange;

    @OneToOne(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private CreateUserPwdToken createPwdToken;

    @Column(name = "reg_pwd_changed_date")
    private LocalDateTime regPwdChangedDate;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "USER_TAG", joinColumns = @JoinColumn(name = "USER_ID"))
    @Column(name = "TAG")
    private Set<String> tags = new HashSet<>();

    @NotAudited
    @OneToMany(mappedBy = "user")
    private List<UserAd> userAdEntries;

    public void setPhoto(byte[] photo) {
        this.photo = photo;
        String checksum = photo != null ? DigestUtils.md5DigestAsHex(photo).toUpperCase() : null;
        if (!StringUtils.equals(checksum, getPhotoCheckSum())) {
            setPhotoChangedOn(LocalDateTime.now());
        }
        setPhotoCheckSum(checksum);
    }

    @PrePersist
    protected void prePersist() {
        if (StringUtils.isBlank(getExternalId())) {
            setExternalId(UUID.randomUUID().toString());
        }
    }
}
