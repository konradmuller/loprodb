package org.vdma.loprodb.entity;

import org.apache.commons.lang3.StringUtils;

public enum UserStatus {
    ACTIVE,
    INACTIVE,
    BLOCKED;

    @Override
    public String toString() {
        return StringUtils.capitalize(name().replace("_", " ").toLowerCase());
    }
}
