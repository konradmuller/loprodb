package org.vdma.loprodb.entity;

public enum EmailTemplateType {
    OPT_IN,
    REGISTRATION_CONFIRMED,
    REGISTRATION_REJECTED,
    PASSWORD_RESET,
    REGISTRATION_MANUAL_CHECK
}
