package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "reg_pwd_change")
@NoArgsConstructor
public class RegPwdChange {

    @Id
    @Column(unique = true)
    private Long id;

    @OneToOne(optional = false)
    @NotNull(message = "User shouldn't be null.")
    @JoinColumn(name = "user_id", updatable = false, nullable = false, unique = true)
    @MapsId
    private User user;

    @NotBlank(message = "Password shouldn't be blank.")
    @Column(name = "pwd")
    private String password;

    public RegPwdChange(@NotNull(message = "User shouldn't be null.") User user) {
        this.user = user;
    }
}
