package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Reference {
    @Id
    @SequenceGenerator(name = "ref_id_seq_gen", sequenceName = "ref_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "ref_id_seq_gen")
    private Long id;

    @NotBlank(message = "German name shouldn't be blank.")
    @Column(nullable = false, name = "name_de")
    private String nameDe;

    @Column(name = "name_en")
    private String nameEn;

    @NotNull(message = "Reference type shouldn't be null.")
    @Column(name = "ref_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private RefType refType;

    @Column(name = "samba_id", unique = true)
    private Long sambaId;

    public Reference(@NotBlank(message = "German name shouldn't be blank.") String nameDe, String nameEn,
            @NotNull(message = "Reference type shouldn't be null.") RefType refType) {
        this.nameDe = nameDe;
        this.nameEn = nameEn;
        this.refType = refType;
    }
}
