package org.vdma.loprodb.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Table(name = "EVENT_LOG")
@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class EventLog {
    @Id
    @SequenceGenerator(name = "event_log_id_seq_gen", sequenceName = "event_log_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "event_log_id_seq_gen")
    private Long id;

    @ManyToOne(optional = false)
    @NotNull(message = "Application shouldn't be null.")
    @JoinColumn(name = "app_id", updatable = false, nullable = false)
    private Application application;

    @ManyToOne(optional = false)
    @NotNull(message = "User shouldn't be null.")
    @JoinColumn(name = "user_id", updatable = false, nullable = false)
    private User user;

    @NotNull(message = "Event date shouldn't be null.")
    @CreatedDate
    @Column(name = "event_date", nullable = false, updatable = false)
    private LocalDateTime eventDate;

    private String message;

    public EventLog(@NotNull(message = "Application shouldn't be null.") Application application,
            @NotNull(message = "User shouldn't be null.") User user, String message) {
        this.application = application;
        this.user = user;
        this.message = message;
    }
}
