package org.vdma.loprodb.entity;

public enum Gender {
    MALE,
    FEMALE
}
