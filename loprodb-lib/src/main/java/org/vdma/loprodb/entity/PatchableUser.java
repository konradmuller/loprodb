package org.vdma.loprodb.entity;

import java.time.LocalDate;

public interface PatchableUser {
    void setStatus(UserStatus status);

    void setAimId(Long aimId);

    void setSambaId(Long sambaId);

    void setAutoDeactivation(boolean autoDeactivation);

    void setAutoDeactivationDate(LocalDate autoDeactivationDate);

    void setGender(Gender gender);

    void setTitle(String title);

    void setFirstName(String firstName);

    void setPhone(String phone);

    void setLanguage(Language language);

    void setDsVersion(String dsVersion);

    void setDepartment(String department);

    void setBirthday(LocalDate birthday);

    void setPublicPhone(String publicPhone);

    void setPublicEmail(String publicEmail);

    void setPublicMobile(String publicMobile);

    void setFunction(String function);

    void setPhoto(byte[] photo);
}
