package org.vdma.loprodb.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class UserAdKey implements Serializable {
    private long userId;
    private LocalDateTime createdDate;
}
