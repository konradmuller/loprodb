package org.vdma.loprodb.entity;

import java.util.Collection;
import java.util.HashSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Audited
@EqualsAndHashCode(of = { "id", "code" })
public class Application {

    @Id
    @SequenceGenerator(name = "app_id_seq_gen", sequenceName = "app_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "app_id_seq_gen")
    private Long id;

    @Version
    private int version;

    @NotBlank(message = "Name shouldn't be blank.")
    @Column(unique = true, nullable = false)
    private String name;

    @NotBlank(message = "Code shouldn't be blank.")
    @Column(unique = true, nullable = false)
    private String code;

    @NotBlank(message = "Password shouldn't be blank.")
    @Column(nullable = false, name = "password_")
    private String password;

    @OneToMany(mappedBy = "application", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<AppRole> roles = new HashSet<>();

    public Application(@NotBlank(message = "Name shouldn't be blank.") String name,
            @NotBlank(message = "Code shouldn't be blank.") String code,
            @NotBlank(message = "Password shouldn't be blank.") String password) {
        this.name = name;
        this.code = code;
        this.password = password;
    }
}
