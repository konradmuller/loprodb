package org.vdma.loprodb.entity.listener.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSmbSyncContext {
    private Long userVersionInSamba;
}
