package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class ExtendedCompanyInfo extends BaseCompanyInfo {

    @Column(name = "company_name1")
    private String name1;

    @Column(name = "company_name2")
    private String name2;

    @Column(name = "company_name3")
    private String name3;

    @Column(name = "company_location")
    private String location;

    @Column(name = "company_district")
    private String district;
}
