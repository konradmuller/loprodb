package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.Audited;

@Table(name = "APP_ROLE")
@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Audited
@EqualsAndHashCode
public class AppRole {

    @Id
    @SequenceGenerator(name = "app_role_id_seq_gen", sequenceName = "app_role_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "app_role_id_seq_gen")
    private Long id;

    @NotBlank(message = "Name shouldn't be blank.")
    @Column(nullable = false)
    private String name;

    @ManyToOne(optional = false)
    @NotNull(message = "Application shouldn't be null.")
    @JoinColumn(name = "app_id", updatable = false, nullable = false)
    private Application application;

    public AppRole(String name, Application application) {
        this.name = name;
        this.application = application;
    }
}
