package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "EMAIL_TEMPLATE")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class EmailTemplate {

    @Id
    @SequenceGenerator(name = "email_template_id_seq_gen", sequenceName = "email_template_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "email_template_id_seq_gen")
    private Long id;

    @NotNull(message = "Type shouldn't be null.")
    @Column(nullable = false, name = "template_type", unique = true)
    @Enumerated(EnumType.STRING)
    private EmailTemplateType templateType;

    @NotBlank(message = "German subject shouldn't be blank.")
    @Column(name = "subject_de")
    private String subjectDe;

    @Lob
    @NotBlank(message = "German body shouldn't be blank.")
    @Column(name = "body_de")
    private String bodyDe;

    @NotBlank(message = "English subject shouldn't be blank.")
    @Column(name = "subject_en")
    private String subjectEn;

    @Lob
    @NotBlank(message = "English body shouldn't be blank.")
    @Column(name = "body_en")
    private String bodyEn;

}
