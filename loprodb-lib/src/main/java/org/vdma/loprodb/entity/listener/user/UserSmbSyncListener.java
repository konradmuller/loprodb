package org.vdma.loprodb.entity.listener.user;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.vdma.loprodb.entity.User;

public class UserSmbSyncListener {

    @PrePersist
    public void prePersist(User u) {
        updateSambaSyncVersion(u);
    }

    @PreUpdate
    protected void preUpdate(User u) {
        updateSambaSyncVersion(u);
    }

    private void updateSambaSyncVersion(User u) {
        if (u.getSambaId() != null && UserSmbSyncContextHolder.getContext().getUserVersionInSamba() == null) {
            u.setSambaSyncVersion(System.currentTimeMillis());
        }
    }
}
