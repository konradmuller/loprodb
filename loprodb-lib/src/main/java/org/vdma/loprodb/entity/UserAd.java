package org.vdma.loprodb.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Getter
@Setter
@IdClass(UserAdKey.class)
@Table(name = "user_ad")
public class UserAd {

    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "ad_userid", length = 512)
    private String adUserId;

    @Column(name = "ad_userprincipalname", length = 512)
    private String adUserPrincipalName;

    @Id
    @Column(name = "erstelldatum")
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "email", length = 512)
    private String email;

    @Convert(converter = CreationType.CreationTypeConverter.class)
    @Column(name = "creationtype", length = 512)
    private CreationType creationType;

    @Column(name = "password", length = 50)
    private String password;

    @Column(name = "login_email_sent")
    private boolean loginEmailSent;
    
    @ManyToOne
    @JoinColumn(name="user_id", insertable = false, updatable = false)
    private User user;
}
