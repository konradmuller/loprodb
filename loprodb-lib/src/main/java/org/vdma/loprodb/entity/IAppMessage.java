package org.vdma.loprodb.entity;

import java.time.LocalDateTime;

public interface IAppMessage {
    Long getAppId();

    String getMessage();

    LocalDateTime getMessageDate();
}
