package org.vdma.loprodb.entity;

public enum RefType {
    USER_POSITION,
    USER_BUSINESS_AREA,
    USER_TOPIC,
    EMAIL_BLACK_LIST
}
