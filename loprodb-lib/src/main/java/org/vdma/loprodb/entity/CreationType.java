package org.vdma.loprodb.entity;

import java.util.Arrays;
import java.util.Objects;
import static java.util.Optional.ofNullable;
import javax.persistence.AttributeConverter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CreationType {

    INVITATION("Invitation"),
    LOCAL_ACCOUNT("LocalAccount"),
    EXISTING_ACCOUNT("ExistingAccount"),
    RESTORED_ACCOUNT("RestoredAccount");

    private final String value;

    public static class CreationTypeConverter implements AttributeConverter<CreationType, String> {
        @Override
        public String convertToDatabaseColumn(CreationType creationType) {
            return ofNullable(creationType)
                    .map(CreationType::getValue)
                    .orElse(null);
        }

        @Override
        public CreationType convertToEntityAttribute(String s) {
            return Arrays.stream(CreationType.values())
                    .filter(v -> Objects.equals(s, v.getValue()))
                    .findFirst()
                    .orElse(null);
        }
    }
}