package org.vdma.loprodb.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@MappedSuperclass
public class BaseCompanyInfo {

    @NotBlank(message = "Company name shouldn't be blank.")
    @Column(name = "company_name")
    protected String name;

    @NotBlank(message = "Company zip shouldn't be blank.")
    @Column(name = "company_zip")
    protected String zip;

    @NotBlank(message = "Company city shouldn't be blank.")
    @Column(name = "company_city")
    protected String city;

    @NotBlank(message = "Company street shouldn't be blank.")
    @Column(name = "company_street")
    protected String street;

    @NotBlank(message = "Company house number shouldn't be blank.")
    @Column(name = "company_house_number")
    protected String houseNumber;

    @NotBlank(message = "Company country shouldn't be blank.")
    @Column(name = "company_country")
    protected String country;
}
