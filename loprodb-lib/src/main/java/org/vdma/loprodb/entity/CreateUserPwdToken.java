package org.vdma.loprodb.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Setter
@Table(name = "CREATE_USER_PWD_TOKEN")
@EntityListeners({ AuditingEntityListener.class })
@NoArgsConstructor
public class CreateUserPwdToken {

    @Id
    private Long id;

    @OneToOne(optional = false)
    @NotNull(message = "User shouldn't be null.")
    @JoinColumn(name = "id")
    @MapsId
    private User user;

    @NotBlank(message = "Token shouldn't be blank.")
    @Column(unique = true, nullable = false, updatable = false, name = "token")
    private String token;

    @NotNull
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "pwd_created_date")
    private LocalDateTime pwdCreatedDate;

    public CreateUserPwdToken(@NotNull(message = "User shouldn't be null.") User user) {
        this.user = user;
    }

    @PrePersist
    protected void prePersist() {
        if (StringUtils.isBlank(getToken())) {
            setToken(UUID.randomUUID().toString());
        }
    }

}
