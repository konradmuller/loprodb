package org.vdma.loprodb.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RegistrationVariant {
    V1_1("v1.1"),
    V1_2("v1.2"),
    V2("v2"),
    V3("v3"),
    V6_1("v6.1"),
    V6_2("v6.2");

    @Getter
    private final String tag;
}
