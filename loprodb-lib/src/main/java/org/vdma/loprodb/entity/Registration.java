package org.vdma.loprodb.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Setter
@Table(name = "registration")
@EntityListeners({ AuditingEntityListener.class })
@Audited
public class Registration {

    @Id
    @SequenceGenerator(name = "reg_id_seq_gen", sequenceName = "reg_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "reg_id_seq_gen")
    private Long id;

    @NotBlank(message = "Token shouldn't be blank.")
    @Column(unique = true, nullable = false, updatable = false, name = "token")
    private String token;

    @Version
    private int version;

    @NotNull(message = "Gender shouldn't be null.")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String title;

    @NotBlank(message = "First name shouldn't be blank.")
    @Column(nullable = false, name = "first_name")
    private String firstName;

    @NotBlank(message = "Last name shouldn't be blank.")
    @Column(nullable = false, name = "last_name")
    private String lastName;

    @Email
    @NotBlank(message = "Email shouldn't be blank.")
    @Column(nullable = false, unique = true)
    private String email;

    @NotBlank(message = "Phone shouldn't be blank.")
    @Column(nullable = false)
    private String phone;

    private String mobile;
        
    @NotBlank(message = "Function shouldn't be blank.")
    @Column(name = "function_", nullable = false)
    private String function;

    @NotNull(message = "Position shouldn't be null.")
    @ManyToOne(optional = false)
    @JoinColumn(name = "position_id", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Reference position;

    @NotNull(message = "Business Area shouldn't be null.")
    @ManyToOne(optional = false)
    @JoinColumn(name = "business_area_id", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Reference businessArea;

    @Embedded
    private BaseCompanyInfo company;

    @NotNull
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @NotBlank(message = "DS version shouldn't be blank.")
    @Size(max = 32)
    @Column(name = "ds_version", length = 32)
    private String dsVersion;

    @NotNull
    @Column(name = "last_modified_date", nullable = false)
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @NotNull(message = "Language shouldn't be null.")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language = Language.DE;

    @NotNull(message = "Status shouldn't be null.")
    @Enumerated(EnumType.STRING)
    private RegistrationStatus status = RegistrationStatus.OPT_IN_PENDING;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "registration_variant")
    private RegistrationVariant registrationVariant;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "REGISTRATION_TAG", joinColumns = @JoinColumn(name = "REGISTRATION_ID"))
    @Column(name = "TAG")
    private Set<String> tags = new HashSet<>();
    
    @OneToOne
    @JoinColumn(name = "editor_id")
    private User editor;
    
    @Size(max = 2000)
    @Column(name = "memo", length = 2000)
    private String memo;

    @PrePersist
    protected void prePersist() {
        if (StringUtils.isBlank(getToken())) {
            setToken(UUID.randomUUID().toString());
        }
    }

}
