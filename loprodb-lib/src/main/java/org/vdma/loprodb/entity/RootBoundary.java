package org.vdma.loprodb.entity;

public interface RootBoundary<T> {
    T getBoundary();
}
