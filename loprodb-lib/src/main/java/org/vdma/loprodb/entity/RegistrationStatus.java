package org.vdma.loprodb.entity;

public enum RegistrationStatus {
    OPT_IN_PENDING,
    MANUAL_REVIEW_PENDING,
    AUTO_REJECTED,
    MANUALLY_REJECTED,
    AUTO_CONFIRMED_MEMBER,
    AUTO_CONFIRMED_NON_MEMBER,
    MANUALLY_CONFIRMED
}
