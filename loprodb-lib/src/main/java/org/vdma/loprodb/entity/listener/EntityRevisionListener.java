package org.vdma.loprodb.entity.listener;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.RevisionListener;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vdma.loprodb.entity.Revinfo;

@Slf4j
public class EntityRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        Revinfo revinfo = (Revinfo) revisionEntity;
        if (SecurityContextHolder.getContext() == null) {
            log.warn("Can't get security context when start new revision");
            return;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            log.warn("Can't get authentication from security context when start new revision");
            return;
        }
        if (SecurityContextHolder.getContext().getAuthentication() instanceof AbstractAuthenticationToken) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AbstractAuthenticationToken token = (AbstractAuthenticationToken) authentication;
            revinfo.setUsername(token.getName());
        } else {
            log.warn("Invalid authentication when start new revision");
        }

    }

}
