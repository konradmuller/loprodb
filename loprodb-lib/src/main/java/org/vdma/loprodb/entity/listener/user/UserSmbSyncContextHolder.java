package org.vdma.loprodb.entity.listener.user;

import org.springframework.util.Assert;

public class UserSmbSyncContextHolder {

    private static final ThreadLocal<UserSmbSyncContext> contextHolder = new ThreadLocal<>();

    public static void clearContext() {
        contextHolder.remove();
    }

    public static UserSmbSyncContext getContext() {
        UserSmbSyncContext ctx = contextHolder.get();

        if (ctx == null) {
            ctx = createEmptyContext();
            contextHolder.set(ctx);
        }

        return ctx;
    }

    public static void setContext(UserSmbSyncContext context) {
        Assert.notNull(context, "Only non-null context instances are permitted");
        contextHolder.set(context);
    }

    public static UserSmbSyncContext createEmptyContext() {
        return new UserSmbSyncContext();
    }
}
