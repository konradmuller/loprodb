package org.vdma.loprodb.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserIdType {
    EMAIL("email"),
    SAMBA_ID("sambaId"),
    EXTERNAL_ID("profdbId");

    private String externalName;

    public static UserIdType createFromExternalName(String name) {
        for (UserIdType type : values()) {
            if (type.getExternalName().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format("Cannot detect user id type for '%s'", name));
    }
}
