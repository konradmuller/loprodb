package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ApplicationData {
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String code;

    private Set<AppRoleData> roles = new HashSet<>();

    @JsonIgnore
    public AppRoleData findRole(String name) {
        return roles.stream().filter(r -> r.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}
