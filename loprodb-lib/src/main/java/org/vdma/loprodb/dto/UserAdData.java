package org.vdma.loprodb.dto;

import java.time.LocalDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.CreationType;

@Getter
@Setter
@EqualsAndHashCode(of = {"userId", "createdDate"})
public class UserAdData {

    private Long userId;

    private String adUserId;

    private String adUserPrincipalName;

    private LocalDateTime createdDate;

    private String email;

    private CreationType creationType;

    private String password;

    private boolean loginEmailSent;
}
