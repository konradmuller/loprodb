package org.vdma.loprodb.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.RevisionType;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuditData<T> {
    private T entityData;
    private RevinfoData revInfo;
    private RevisionType revisionType;
}
