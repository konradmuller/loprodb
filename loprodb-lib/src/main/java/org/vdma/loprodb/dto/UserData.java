package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.ExtendedCompanyInfo;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.UserStatus;

@Getter
@Setter
@EqualsAndHashCode(of = "externalId")
public class UserData {

    @JsonIgnore
    private Long id;

    private Long sambaSyncVersion;

    private Long sambaContactVersion;

    private String externalId;

    @NotNull
    private UserStatus status;

    private Long aimId;

    private Long sambaId;

    private boolean autoDeactivation;

    private LocalDate autoDeactivationDate;

    @NotNull
    private Gender gender;

    private String title;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    private String department;

    private LocalDate birthday;

    private String publicPhone;

    @Email
    private String publicEmail;

    private String publicMobile;

    @Email
    @NotBlank
    private String email;

    private String phone;

    @NotNull
    private Language language;

    private String dsVersion;

    private LocalDateTime createdDate;

    private String createdBy;

    private LocalDateTime lastModifiedDate;

    private String lastModifiedBy;

    @JsonIgnore
    private byte[] photo;

    private String photoCheckSum;

    private LocalDateTime photoChangedOn;

    private ExtendedCompanyInfo company;

    @NotBlank
    private String function;

    @NotNull
    private ReferenceData position;

    private Set<ReferenceData> topics;

    @NotNull
    private ReferenceData businessArea;

    private LocalDateTime lastSeenOn;

    private Set<UserAppData> apps;

    private boolean vdmaRolesSynchronized;

    @JsonIgnore
    private LocalDateTime regPwdChangedDate;

    private String createPasswordToken;

    private Set<String> tags = new HashSet<>();

    private List<UserAdData> userAdEntries = new ArrayList<>();

    @JsonIgnore
    public String getInitials() {
        return (firstName.substring(0, 1) + lastName.substring(0, 1)).toUpperCase();
    }

    @JsonIgnore
    public String getFullName() {
        return String.format("%s, %s", lastName, firstName);
    }

    @JsonIgnore
    public boolean hasAccessToApp(String appCode) {
        return getApps().stream().anyMatch(app -> app.getAppCode().equalsIgnoreCase(appCode));
    }

    public ExtendedCompanyInfo getCompany() {
        if (company == null) {
            company = new ExtendedCompanyInfo();
        }
        return company;
    }

    public Optional<UserAdData> getRecentUserAd() {
        return userAdEntries.stream().max(comparing(UserAdData::getCreatedDate));
    }
}
