package org.vdma.loprodb.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RevinfoData {
    private String username;
    private Date timestamp;
}
