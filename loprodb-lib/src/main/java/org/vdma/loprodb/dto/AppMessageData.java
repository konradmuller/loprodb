package org.vdma.loprodb.dto;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppMessageData {
    private Long appId;
    private String message;
    private LocalDateTime messageDate;
}
