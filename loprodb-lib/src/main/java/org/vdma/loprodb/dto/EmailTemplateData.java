package org.vdma.loprodb.dto;

import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.EmailTemplateType;
import org.vdma.loprodb.entity.Language;

@Getter
@Setter
public class EmailTemplateData {
    private Long id;

    private EmailTemplateType templateType;

    @NotBlank
    private String subjectDe;

    @NotBlank
    private String bodyDe;

    @NotBlank
    private String subjectEn;

    @NotBlank
    private String bodyEn;

    public String getSubjectByLang(Language language) {
        return Language.DE == language ? getSubjectDe() : getSubjectEn();
    }

    public String getBodyByLang(Language language) {
        return Language.DE == language ? getBodyDe() : getBodyEn();
    }

}
