package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class AppRoleData {
    @JsonIgnore
    private Long id;

    @NotBlank
    private String name;

    public AppRoleData(@NotBlank String name) {
        this.name = name;
    }
}
