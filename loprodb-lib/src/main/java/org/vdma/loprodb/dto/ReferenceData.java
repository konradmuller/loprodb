package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonView;
import java.util.Locale;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.vdma.loprodb.entity.RefType;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ReferenceData {

    @JsonView({ Views.Read.class, Views.Modify.class })
    private Long id;

    @JsonView({ Views.Read.class })
    private String nameDe;

    @JsonView({ Views.Read.class })
    private String nameEn;

    @JsonView({ Views.Read.class })
    private RefType refType;

    @JsonView({ Views.Read.class, Views.Modify.class })
    private Long sambaId;

    public ReferenceData(Long id) {
        this.id = id;
    }

    public String getNameByLocale(Locale locale) {
        if (locale.getLanguage().equalsIgnoreCase(Locale.GERMAN.getLanguage())) {
            return getNameDe();
        } else {
            return getNameEn();
        }
    }
}
