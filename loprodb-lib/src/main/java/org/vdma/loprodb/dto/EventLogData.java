package org.vdma.loprodb.dto;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventLogData {
    private Long appId;
    private String appName;
    private Long userId;
    private LocalDateTime eventDate;
    private String message;
}
