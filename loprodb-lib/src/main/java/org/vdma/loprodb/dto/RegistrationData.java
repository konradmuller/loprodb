package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiModel;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.vdma.loprodb.entity.BaseCompanyInfo;
import org.vdma.loprodb.entity.Gender;
import org.vdma.loprodb.entity.Language;
import org.vdma.loprodb.entity.RegistrationStatus;
import org.vdma.loprodb.entity.RegistrationVariant;

@ApiModel
@Getter
@Setter
@EqualsAndHashCode(of = "token")
@JsonView({ Views.Modify.class, Views.Read.class })
public class RegistrationData {

    @JsonIgnore
    private Long id;

    @JsonView({ Views.Read.class })
    private String token;

    @NotNull(message = "Gender shouldn't be null.")
    private Gender gender;

    private String title;

    @NotBlank(message = "First name shouldn't be blank.")
    private String firstName;

    @NotBlank(message = "Last name shouldn't be blank.")
    private String lastName;

    @Email
    @NotBlank(message = "Email shouldn't be blank.")
    private String email;

    @NotBlank(message = "Phone shouldn't be blank.")
    private String phone;
    
    private String mobile;

    @NotBlank(message = "Function shouldn't be blank.")
    private String function;

    @NotNull(message = "Position shouldn't be null.")
    private ReferenceData position;

    @NotNull(message = "Business Area shouldn't be null.")
    private ReferenceData businessArea;

    @Valid
    @NotNull(message = "Company info shouldn't be null.")
    private BaseCompanyInfo company;

    @JsonView({ Views.Read.class })
    private LocalDateTime createdDate;

    @JsonView({ Views.Read.class })
    private String createdBy;

    @NotBlank(message = "DS version shouldn't be blank.")
    @Size(max = 32)
    private String dsVersion;

    @JsonView({ Views.Read.class })
    private LocalDateTime lastModifiedDate;

    @JsonView({ Views.Read.class })
    private String lastModifiedBy;

    @NotNull(message = "Language shouldn't be null.")
    private Language language = Language.DE;

    @JsonView({ Views.Read.class })
    private RegistrationStatus status;

    @JsonView({ Views.Read.class })
    private UserData user;

    @JsonView({ Views.Read.class })
    private RegistrationVariant registrationVariant;

    @JsonView({ Views.Read.class })
    private Set<String> tags = new HashSet<>();
    
    @JsonView({ Views.Read.class })
    private UserData editor;

    @JsonView({ Views.Read.class })
    private String memo;

    @JsonIgnore
    public String getFullName() {
        return String.format("%s, %s", lastName, firstName);
    }

    public BaseCompanyInfo getCompany() {
        if (company == null) {
            company = new BaseCompanyInfo();
        }
        return company;
    }
}
