package org.vdma.loprodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "appId" })
@NoArgsConstructor
@AllArgsConstructor
public class UserAppData {
    @JsonIgnore
    private Long appId;
    private String appName;
    private String appCode;
    private Set<AppRoleData> roles = new HashSet<>();

    public UserAppData(Long appId) {
        this.appId = appId;
    }
}
