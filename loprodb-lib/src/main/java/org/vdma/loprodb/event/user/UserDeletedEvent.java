package org.vdma.loprodb.event.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.entity.User;

@Getter
@RequiredArgsConstructor
public class UserDeletedEvent {
    private final User user;
}
