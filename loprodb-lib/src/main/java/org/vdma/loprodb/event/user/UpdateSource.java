package org.vdma.loprodb.event.user;

public enum UpdateSource {
    VDMA_ORG,
    LOPRODB,
    SAMBA
}
