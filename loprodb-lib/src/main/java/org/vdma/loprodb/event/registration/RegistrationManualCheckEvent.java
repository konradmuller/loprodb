package org.vdma.loprodb.event.registration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.dto.RegistrationData;

@RequiredArgsConstructor
@Getter
public class RegistrationManualCheckEvent {
    private final RegistrationData registration;
}
