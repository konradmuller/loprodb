package org.vdma.loprodb.event.registration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.dto.RegistrationData;

@Getter
@RequiredArgsConstructor
public class RegistrationCreatedEvent {
    private final RegistrationData registration;
}
