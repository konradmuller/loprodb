package org.vdma.loprodb.event.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.entity.Registration;
import org.vdma.loprodb.entity.User;

@Getter
@RequiredArgsConstructor
public class UserCreatedEvent {
    private final User user;

    private Registration fromRegistration;

    public UserCreatedEvent(User user, Registration fromRegistration) {
        this(user);
        this.fromRegistration = fromRegistration;
    }
}
