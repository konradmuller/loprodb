package org.vdma.loprodb.event.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.vdma.loprodb.entity.User;

@Getter
@RequiredArgsConstructor
public class UserUpdatedEvent {
    private final User user;
    private final UpdateSource updateSource;

    /**
     * true, if <b>ONLY</b> lastSeenOn property was updated, false otherwise
     */
    public boolean lastSeenOnUpdated;

    public UserUpdatedEvent(final User user, final UpdateSource updateSource, final boolean lastSeenOnUpdated) {
        this(user, updateSource);
        this.lastSeenOnUpdated = lastSeenOnUpdated;
    }
}
